#include <gtest/gtest.h>
#include "../src/Filter.h"

namespace {

  class t_Filter : public ::testing::Test {

  protected:

    t_Filter() {
    }

    virtual ~t_Filter() {
    }

    virtual void SetUp() {
    }

    virtual void TearDown() {

    }
  };

  /* ======================================= */
  /* ======================================= */
  TEST_F(t_Filter, hasRobotMoved_not_set)
  {
    PositionFilter filter;

    ASSERT_EQ(false, filter.hasRobotMoved());
  }

  TEST_F(t_Filter, hasRobotMoved_set_once)
  {
    PositionFilter filter;
    geometry_msgs::Twist position;
    position.linear.x  = 0.0;
    position.linear.y  = 0.0;
    position.angular.z = 0.0;

    filter.setPosition(position);

    ASSERT_EQ(true, filter.hasRobotMoved());
  }

  TEST_F(t_Filter, hasRobotMoved_x_good)
  {
    PositionFilter filter;
    geometry_msgs::Twist position;
    filter.setPosition(position);
    position.linear.x  = 1e9;
    position.linear.y  = 0.0;
    position.angular.z = 0.0;

    filter.setPosition(position);

    ASSERT_EQ(true, filter.hasRobotMoved());
  }

  TEST_F(t_Filter, hasRobotMoved_y_good)
  {
    PositionFilter filter;
    geometry_msgs::Twist position;
    filter.setPosition(position);
    position.linear.x  = 0.0;
    position.linear.y  = 1e9;
    position.angular.z = 0.0;

    filter.setPosition(position);

    ASSERT_EQ(true, filter.hasRobotMoved());
  }

  TEST_F(t_Filter, hasRobotMoved_z_good)
  {
    PositionFilter filter;
    geometry_msgs::Twist position;
    filter.setPosition(position);
    position.linear.x  = 0.0;
    position.linear.y  = 0.0;
    position.angular.z = 1e9;

    filter.setPosition(position);

    ASSERT_EQ(true, filter.hasRobotMoved());
  }

  TEST_F(t_Filter, hasRobotMoved_x_bad)
  {
    PositionFilter filter;
    geometry_msgs::Twist position;
    filter.setPosition(position);
    position.linear.x  = 0.01;
    position.linear.y  = 0.0;
    position.angular.z = 0.0;

    filter.setPosition(position);

    ASSERT_EQ(false, filter.hasRobotMoved());
  }

  TEST_F(t_Filter, hasRobotMoved_y_bad)
  {
    PositionFilter filter;

    geometry_msgs::Twist position;
    filter.setPosition(position);

    position.linear.x  = 0.0;
    position.linear.y  = 0.01;
    position.angular.z = 0.0;

    filter.setPosition(position);

    ASSERT_EQ(false, filter.hasRobotMoved());
  }

  TEST_F(t_Filter, hasRobotMoved_z_bad)
  {
    PositionFilter filter;

    geometry_msgs::Twist position;
    filter.setPosition(position);

    position.linear.x  = 0.0;
    position.linear.y  = 0.0;
    position.angular.z = 0.01;

    filter.setPosition(position);

    ASSERT_EQ(false, filter.hasRobotMoved());
  }

  TEST_F(t_Filter, hasRobotMoved_double_move)
  {
    PositionFilter filter;
    geometry_msgs::Twist position;
    position.linear.x  = 10.0;
    position.linear.y  = 10.0;
    position.angular.z = 0.0;

    filter.setPosition(position);
    filter.setPosition(position);

    ASSERT_EQ(true, filter.hasRobotMoved());
  }

  TEST_F(t_Filter, hasRobotMoved_double_move_reset)
  {
    PositionFilter filter;
    geometry_msgs::Twist position;
    position.linear.x  = 10.0;
    position.linear.y  = 10.0;
    position.angular.z = 0.0;

    filter.setPosition(position);
    filter.setPosition(position);

    ASSERT_EQ(false, filter.hasRobotMoved());
  }

  /* ======================================= */
  TEST_F(t_Filter, isRobotStable_not_set)
  {
    VelocityFilter filter;

    ASSERT_EQ(false, filter.isRobotStable());
  }

  TEST_F(t_Filter, isRobotStable_set_once_0)
  {
    VelocityFilter filter;

    geometry_msgs::Twist velocity;

    filter.setVelocity(velocity);

    ASSERT_EQ(true, filter.isRobotStable());
  }

  TEST_F(t_Filter, isRobotStable_set_once_non_0)
  {
    VelocityFilter filter;

    geometry_msgs::Twist velocity;
    velocity.linear.x = 1e9;

    filter.setVelocity(velocity);

    ASSERT_EQ(false, filter.isRobotStable());
  }

  TEST_F(t_Filter, isRobotStable_set_non_zero_set_zero_once)
  {
    VelocityFilter filter;

    geometry_msgs::Twist velocity;
    velocity.linear.x = 1e9;
    filter.setVelocity(velocity);
    velocity.linear.x = 0.0;
    filter.setVelocity(velocity);

    ASSERT_EQ(false, filter.isRobotStable());
  }

  TEST_F(t_Filter, isRobotStable_set_non_zero_set_zero_11_times)
  {
    VelocityFilter filter;

    geometry_msgs::Twist velocity;
    velocity.linear.x = 1e9;
    filter.setVelocity(velocity);
    velocity.linear.x = 0.0;
    filter.setVelocity(velocity);
    filter.setVelocity(velocity);
    filter.setVelocity(velocity);
    filter.setVelocity(velocity);
    filter.setVelocity(velocity);
    filter.setVelocity(velocity);
    filter.setVelocity(velocity);
    filter.setVelocity(velocity);
    filter.setVelocity(velocity);
    filter.setVelocity(velocity);
    filter.setVelocity(velocity);

    ASSERT_EQ(true, filter.isRobotStable());
  }

  /* ======================================= */
  TEST_F(t_Filter, isRobotStable_set_once_non_0_angular)
  {
    VelocityFilter filter;

    geometry_msgs::Twist velocity;
    velocity.angular.z = 1e9;

    filter.setVelocity(velocity);

    ASSERT_EQ(false, filter.isRobotStable());
  }

  TEST_F(t_Filter, isRobotStable_set_non_zero_set_zero_once_angular)
  {
    VelocityFilter filter;

    geometry_msgs::Twist velocity;
    velocity.angular.z = 1e9;
    filter.setVelocity(velocity);
    velocity.angular.z = 0.0;
    filter.setVelocity(velocity);

    ASSERT_EQ(false, filter.isRobotStable());
  }

  TEST_F(t_Filter, isRobotStable_set_non_zero_set_zero_11_times_angular)
  {
    VelocityFilter filter;

    geometry_msgs::Twist velocity;
    velocity.angular.z = 1e9;
    filter.setVelocity(velocity);
    velocity.angular.z = 0.0;
    filter.setVelocity(velocity);
    filter.setVelocity(velocity);
    filter.setVelocity(velocity);
    filter.setVelocity(velocity);
    filter.setVelocity(velocity);
    filter.setVelocity(velocity);
    filter.setVelocity(velocity);
    filter.setVelocity(velocity);
    filter.setVelocity(velocity);
    filter.setVelocity(velocity);
    filter.setVelocity(velocity);

    ASSERT_EQ(true, filter.isRobotStable());
  }

  /* ======================================= */
} // Namespace
