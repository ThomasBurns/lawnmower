#ifndef FILTER_H
#define FILTER_H

#include <geometry_msgs/Twist.h>

class VelocityFilter
{
public:
    /* The sensors are not ideal, and take time to take a reading.
    * If we are moving this will introduce an error into the value.
    * We need to ensure that we are travelling less than a specific
    * speed to eliminate this error.
    */
  VelocityFilter();

  bool isRobotStable() const;
  void setVelocity(const geometry_msgs::Twist & velocity);
  void setLinearSpeedLimit(double speed);
  void setAngularSpeedLimit(double speed);

private:
  const unsigned int COOL_DOWN_VALUE;
  unsigned int VelocityCooldDown_;
  bool VelocityKnown_;
  double LinearSpeed_;  // m/s
  double AngularSpeed_; // rad/s

  geometry_msgs::Twist Velocity_;
};


/* Scan Filter
 *
 * The purpose of this class is decide whether a new scan from a sensor is valid
 * to be used for mapping. This is achieved by monitoring speed and position, and
 * using these to determine acceptability or not. This class is designed to be
 * sensor agnostic.
 */


class PositionFilter
{
public:
    /* To avoid overloading an occpancy grid we should move/turn
    * a minimum distance before using the next reading.
    */

  PositionFilter();

  bool hasRobotMoved() const;

  void setPosition(const geometry_msgs::Twist & position);
  void setLinearDistance(double distance);
  void setAngularDistance(double distance);

private:
  bool PositionMoved_;
  bool PositionKnown_;

  double ObservationDistance_; // in meters
  double ObservationAngle_; // in Radians

  geometry_msgs::Twist SysPos_;
};

#endif
