
#include "Filter.h"

/* ======================================================= */
/* Constructor Functions */
/* ======================================================= */
VelocityFilter::VelocityFilter():
  COOL_DOWN_VALUE(10),
  VelocityCooldDown_(0),
  VelocityKnown_(false),
  LinearSpeed_(0.1),
  AngularSpeed_(0.1)
{
}

/* ======================================================= */
/* Public Functions */
/* ======================================================= */
bool VelocityFilter::isRobotStable() const
{
  if ( VelocityKnown_ == false ) {
    return false;
  }
  return VelocityCooldDown_== 0;
}

void VelocityFilter::setVelocity(const geometry_msgs::Twist & velocity)
{
  VelocityKnown_ = true;
  Velocity_ = velocity;

  if (( fabs(Velocity_.linear.x)  > LinearSpeed_ ) ||
      ( fabs(Velocity_.angular.z) > AngularSpeed_)) {

    VelocityCooldDown_ = COOL_DOWN_VALUE;
  } else if ( VelocityCooldDown_ > 0 ) {
    VelocityCooldDown_--;
  }
}

void VelocityFilter::setLinearSpeedLimit(double speed)
{
  LinearSpeed_ = speed;
}

void VelocityFilter::setAngularSpeedLimit(double speed)
{
  AngularSpeed_ = speed;
}

/* ======================================================= */
/* Constructor Functions */
/* ======================================================= */
PositionFilter::PositionFilter():
  PositionMoved_(false),
  PositionKnown_(false),
  ObservationDistance_(0.02),
  ObservationAngle_(0.1)
{
  SysPos_.linear.x = 1e9;
}

/* ======================================================= */
/* Public Functions */
/* ======================================================= */
void PositionFilter::setPosition(const geometry_msgs::Twist & position)
{
  if ( PositionKnown_ == false ) {
    SysPos_ = position;
    PositionMoved_ = true;
    PositionKnown_ = true;
    return;
  }

  double dx = SysPos_.linear.x - position.linear.x;
  double dy = SysPos_.linear.y - position.linear.y;
  double dz = SysPos_.linear.z - position.linear.z;

  double linear_distance = sqrt((dx*dx)+(dy*dy)+(dz*dz));
  double angular_distance = fabs(SysPos_.angular.z - position.angular.z );

  if (( linear_distance  >= ObservationDistance_ ) ||
      ( angular_distance >= ObservationAngle_    )) {

    SysPos_ = position;
    PositionMoved_ = true;
  } else {
    PositionMoved_ = false;
  }
}

bool PositionFilter::hasRobotMoved() const
{
  return PositionMoved_;
}

void PositionFilter::setLinearDistance(double distance)
{
  ObservationDistance_ = distance;
}

void PositionFilter::setAngularDistance(double distance)
{
  ObservationAngle_ = distance;
}
