#include <tf/transform_listener.h>

#include <object_filtering/ObjectScan.h>

#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <diagnostic_msgs/KeyValue.h>


#include "Filter.h"

/* ======================================================= */
static ros::Subscriber RangeSub_;
static ros::Subscriber OdomSub_;

static tf::TransformListener *TransformListener_;
static VelocityFilter VelFilter_;
static ros::Publisher FilteredPub_;
static ros::Publisher KeyValuePub_;
static std::map<std::string, PositionFilter *> FilterMap_;

static double SensorMaxRangeLimit_;
static double ObservationDistance_;
static double ObservationAngle_;
static bool ObjectDetected_;

/* ======================================================= */
static void publishObjectFound(std::string frame_id, bool found)
{
  diagnostic_msgs::KeyValue msg;
  msg.key = frame_id;

  if (( ObjectDetected_ == true ) && ( found == false )) {
    msg.value = "false";
    KeyValuePub_.publish(msg);
  }
  else if (( ObjectDetected_ == false ) && ( found == true )) {
    msg.value = "true";
    KeyValuePub_.publish(msg);
  }
  ObjectDetected_ = found;
}

static void newRange(const sensor_msgs::Range & msg)
{
  if ( msg.radiation_type != msg.ULTRASOUND ) {
    return;
  }

  try {
    tf::StampedTransform location;
    TransformListener_->lookupTransform("/world", msg.header.frame_id, ros::Time(0), location);

    geometry_msgs::Twist t;

    t.linear.x = location.getOrigin().getX();
    t.linear.y = location.getOrigin().getY();
    t.linear.z = location.getOrigin().getZ();

    t.angular.x = 0.0;
    t.angular.y = 0.0;
    t.angular.z = tf::getYaw(location.getRotation());

    PositionFilter *ptr;

    if (FilterMap_.find(msg.header.frame_id) == FilterMap_.end()) {
      ptr = new PositionFilter();
      ptr->setLinearDistance(ObservationDistance_);
      ptr->setAngularDistance(ObservationAngle_);
      FilterMap_[msg.header.frame_id] = ptr;
    }

    ptr = FilterMap_[msg.header.frame_id];
    ptr->setPosition(t);

    if ( ptr->hasRobotMoved() == true ) {

      bool objectFound = true;
      sensor_msgs::Range adjustedRange;
      adjustedRange = msg;
      adjustedRange.max_range = SensorMaxRangeLimit_;

      if ( adjustedRange.range >= SensorMaxRangeLimit_ ) {
        adjustedRange.range = SensorMaxRangeLimit_;
        publishObjectFound(msg.header.frame_id, false);
        objectFound = false;

      }

      publishObjectFound(msg.header.frame_id, objectFound);
      if (( objectFound == false ) || ( VelFilter_.isRobotStable() == true )) {
        object_filtering::ObjectScan scan;
        scan.position = t;
        scan.scan = adjustedRange;
        FilteredPub_.publish(scan);
      }
    }
  }
  catch (tf::ConnectivityException ex) {
    ROS_ERROR("USF,tf::ConnectivityException => %s",ex.what());
  }
  catch (tf::ExtrapolationException ex) {
//    ROS_ERROR("tf::ExtrapolationException => %s",ex.what());
  }
  catch (tf::InvalidArgument ex) {
    ROS_ERROR("USF,tf::InvalidArgument => %s",ex.what());
  }
  catch (tf::LookupException ex) {
    ROS_ERROR("USF,tf::LookupException => %s",ex.what());
  }
}

void odomCallback(const nav_msgs::Odometry & odom)
{
  VelFilter_.setVelocity(odom.twist.twist);
}

/* ======================================================= */
static void loadSettings(void)
{
  TransformListener_ = new tf::TransformListener();

  double linSpeed, angSpeed;
  ros::param::param<double>("/us_filter/max",       SensorMaxRangeLimit_, 2.0);
  ros::param::param<double>("/us_filter/lin_speed", linSpeed,             0.1);
  ros::param::param<double>("/us_filter/ang_speed", angSpeed,             0.1);
  ros::param::param<double>("/us_filter/obs_dist",  ObservationDistance_, 0.02);
  ros::param::param<double>("/us_filter/obs_ang",   ObservationAngle_,    0.1);
  VelFilter_.setLinearSpeedLimit(linSpeed);
  VelFilter_.setAngularSpeedLimit(angSpeed);

}

/* ======================================================= */
int main (int argc, char **argv)
{
  ros::init(argc, argv, "ultrasonic_range_filter", ros::init_options::AnonymousName);
  loadSettings();

  ros::NodeHandle node;
  RangeSub_ = node.subscribe("/RangeSensors", 50, &newRange);
  OdomSub_  = node.subscribe("/odom",         20, &odomCallback);
  FilteredPub_ = node.advertise<object_filtering::ObjectScan>("/filtered_scans", 5);
  KeyValuePub_ = node.advertise<diagnostic_msgs::KeyValue>("/object_detected", 1);


  ros::spin();

  delete TransformListener_;

  return 0;
}

