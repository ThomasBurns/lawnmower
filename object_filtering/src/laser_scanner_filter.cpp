
#include "Filter.h"

#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <nav_msgs/Odometry.h>
#include <diagnostic_msgs/KeyValue.h>
#include <sensor_msgs/LaserScan.h>


/* ======================================================= */
static ros::Subscriber LaserSub_;
static ros::Subscriber OdomSub_;

static tf::TransformListener *TransformListener_;
static ros::Publisher FilteredPub_;
static ros::Publisher KeyValuePub_;
static PositionFilter PositionFilter_;

static float SensorMaxRangeLimit_;
static bool ObjectDetected_;
static bool PositionKnown_;

/* ======================================================= */
static void publishObjectFound(std::string frame_id, bool found)
{
  diagnostic_msgs::KeyValue msg;
  msg.key = frame_id;

  if (( ObjectDetected_ == true ) && ( found == false )) {
    msg.value = "false";
    KeyValuePub_.publish(msg);
  }
  else if (( ObjectDetected_ == false ) && ( found == true )) {
    msg.value = "true";
    KeyValuePub_.publish(msg);
  }
  ObjectDetected_ = found;
}

static void newRange(const sensor_msgs::LaserScan & msg)
{
  if (( PositionKnown_ == false ) ||
      ( PositionFilter_.hasRobotMoved() == false )) {
    return;
  }

  bool objectFound = true;
  sensor_msgs::LaserScan adjustedRange = msg;

  for ( size_t i = 0; i < adjustedRange.ranges.size(); i ++ ) {
    if ( adjustedRange.ranges[i] >= SensorMaxRangeLimit_ ) {
      adjustedRange.ranges[i] = SensorMaxRangeLimit_;
      publishObjectFound(msg.header.frame_id, false);
      objectFound = false;
    }
  }
  publishObjectFound(msg.header.frame_id, objectFound);
  FilteredPub_.publish(adjustedRange);
}

void odomCallback(const nav_msgs::Odometry & odom)
{
  geometry_msgs::Twist t;

  t.linear.x = odom.pose.pose.position.x;
  t.linear.y = odom.pose.pose.position.y;
  t.linear.z = odom.pose.pose.position.z;

  t.angular.x = 0.0;
  t.angular.y = 0.0;
  t.angular.z = 0.0;

  PositionFilter_.setPosition(t);
  PositionKnown_ = true;
}

/* ======================================================= */
static void loadSettings(void)
{
  TransformListener_ = new tf::TransformListener();
  ros::param::param<float>("/laser_filter/max",       SensorMaxRangeLimit_, 4.0);
  PositionKnown_ = false;
  PositionFilter_.setLinearDistance(0.1);
}

/* ======================================================= */
int main (int argc, char **argv)
{
  ros::init(argc, argv, "laser_sensor_filter", ros::init_options::AnonymousName);
  ROS_INFO("laser_sensor_filter online");
  loadSettings();

  ros::NodeHandle node;
  LaserSub_ = node.subscribe("/LaserScanner",  5, &newRange);
  OdomSub_  = node.subscribe("/odom",         20, &odomCallback);
  FilteredPub_ = node.advertise<sensor_msgs::LaserScan>("/filtered_laser", 5);
  KeyValuePub_ = node.advertise<diagnostic_msgs::KeyValue>("/laser_object_detected", 1);

  ros::spin();

  delete TransformListener_;

  return 0;
}

