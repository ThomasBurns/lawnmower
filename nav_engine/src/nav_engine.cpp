
#include <ros/ros.h>
#include <tf/transform_broadcaster.h>

#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Twist.h>
#include <mowerlib/INS_Data.h>
#include <mowerlib/Trigonometry.h>

#include <cmath>
#include <stdint.h>

static ros::Subscriber INS_Sub_;
static ros::Publisher  OdomPub_;

static geometry_msgs::Twist Position_;
static geometry_msgs::Twist Velocity_;

static mowerlib::INS_Data InsPacket;
static ros::Time InsPacketTime;

static float AbsOfsset[3];
static float LastAccel[3];

#define AVG_FILTER_SIZE  1024
#define ACCEL_AXIS_X  0
#define ACCEL_AXIS_Y  1
#define ACCEL_AXIS_Z  2

bool FilterFull_;

struct AccelFilter {
  float data[AVG_FILTER_SIZE];
  float average;
  unsigned int filterPos;
  bool dataChanged;

  float maxValue;
  float minValue;
};

struct AccelFilter AxisFilters[3];

static void broadcastPositionUsingTF(void);


static void buildFilter(struct AccelFilter * ptr, float value)
{
  ptr->data[ptr->filterPos] = value;
  if ( ++ptr->filterPos >= AVG_FILTER_SIZE ) {
    ptr->filterPos = 0;
    FilterFull_ = true;
  }
  ptr->dataChanged = true;
}

static float applyFilter(struct AccelFilter * ptr, float value)
{
  if ( ptr->dataChanged == true ) {
    ptr->dataChanged = false;

    ptr->average  = 0.0;

    // Now we build the average.
    for ( int i = 0; i < AVG_FILTER_SIZE; i ++ ) {
      ptr->average += ptr->data[i];
    }
    ptr->average /= (float) AVG_FILTER_SIZE;

    // Now we build the window comparator.
    ptr->maxValue = 0.0;
    ptr->minValue = 0.0;
    for ( int i = 0; i < AVG_FILTER_SIZE; i ++ ) {
      float v = ptr->data[i] - ptr->average;
      if ( v >= 0.0 ) {
        if ( v > ptr->maxValue ) {
          ptr->maxValue = v;
        }
      } else {
        if ( v < ptr->minValue ) {
          ptr->minValue = v;
        }
      }
    }
  }

  float newAccel = value - ptr->average;
  if (( newAccel > ptr->maxValue ) || ( newAccel < ptr->minValue )) {
    return newAccel;
  }
  return 0.0;
}


void insCallback(const mowerlib::INS_Data & msg)
{
  static unsigned int NullCount;

  if ( InsPacketTime.toSec() == 0 ) {
    // first packet;
    ROS_INFO("First INS packet received");
    InsPacketTime = msg.Stamp;
    InsPacket = msg;
    AbsOfsset[0] = msg.AbsOrientation[0];
    AbsOfsset[1] = msg.AbsOrientation[1];
    AbsOfsset[2] = msg.AbsOrientation[2];
    Position_.linear.x = 0.0;
    Position_.linear.y = 0.0;
    Position_.linear.z = 0.0;
    return;
  }

  // How long since the last packet...
  float timeDiff = (msg.Stamp - InsPacketTime).toSec();
  InsPacketTime = msg.Stamp;

  /* ========================================================================= */
  // Calculate angular position and velocity
  Position_.angular.z = -Trig_DegreesToRadians(msg.AbsOrientation[0] - AbsOfsset[0]);
  Position_.angular.y = Trig_DegreesToRadians(msg.AbsOrientation[1] - AbsOfsset[1]);
  Position_.angular.x = Trig_DegreesToRadians(msg.AbsOrientation[2] - AbsOfsset[2]);

  Velocity_.angular.x = Trig_DegreesToRadians((msg.AbsOrientation[0] - InsPacket.AbsOrientation[0]) / timeDiff);
  Velocity_.angular.y = Trig_DegreesToRadians((msg.AbsOrientation[1] - InsPacket.AbsOrientation[1]) / timeDiff);
  Velocity_.angular.z = Trig_DegreesToRadians((msg.AbsOrientation[2] - InsPacket.AbsOrientation[2]) / timeDiff);

  /* ========================================================================= */
  // Calculate relative position & velocity
  if (( Velocity_.angular.x == 0.0 ) &&
      ( Velocity_.angular.y == 0.0 ) &&
      ( Velocity_.angular.z == 0.0 ))
  {
    NullCount++;
  } else {
    NullCount = 0;
  }

  if ( NullCount >= 6 ) {
    Velocity_.linear.x = 0.0;
    Velocity_.linear.y = 0.0;
    Velocity_.linear.z = 0.0;

    buildFilter(&AxisFilters[ACCEL_AXIS_X], msg.LinearAcceleration[ACCEL_AXIS_X]);
    buildFilter(&AxisFilters[ACCEL_AXIS_Y], msg.LinearAcceleration[ACCEL_AXIS_Y]);
    buildFilter(&AxisFilters[ACCEL_AXIS_Z], msg.LinearAcceleration[ACCEL_AXIS_Z]);

  } else {
    float accel;

    accel = applyFilter(&AxisFilters[ACCEL_AXIS_X], msg.LinearAcceleration[ACCEL_AXIS_X]);
    Velocity_.linear.x += (((LastAccel[ACCEL_AXIS_X]+accel)/2.0) * timeDiff)  / 2.0;
    LastAccel[ACCEL_AXIS_X] = accel;

    accel = applyFilter(&AxisFilters[ACCEL_AXIS_Y], msg.LinearAcceleration[ACCEL_AXIS_Y]);
    Velocity_.linear.y += (((LastAccel[ACCEL_AXIS_Y]+accel)/2.0) * timeDiff)  / 2.0;
    LastAccel[ACCEL_AXIS_Y] = accel;

    accel = applyFilter(&AxisFilters[ACCEL_AXIS_Z], msg.LinearAcceleration[ACCEL_AXIS_Z]);
    Velocity_.linear.z += (((LastAccel[ACCEL_AXIS_Z]+accel)/2.0) * timeDiff)  / 2.0;
    LastAccel[ACCEL_AXIS_Z] = accel;
  }

  /* ========================================================================= */
  Position_.linear.x += Velocity_.linear.x * std::cos(Position_.angular.z) * timeDiff;
  Position_.linear.y += Velocity_.linear.x * std::sin(Position_.angular.z) * timeDiff;
  Position_.linear.z = 0.0;

  /* ========================================================================= */
  if ( FilterFull_ == true ) {
    broadcastPositionUsingTF();
  }
  InsPacket = msg;
}

static void broadcastPositionUsingTF(void)
{
  ros::Time current_time = ros::Time::now();
  static tf::TransformBroadcaster Broadcaster_;

  tf::Transform transform;
  tf::Quaternion q;

  transform.setOrigin( tf::Vector3(Position_.linear.x, Position_.linear.y, Position_.linear.z) );
  q.setRPY(Position_.angular.x, Position_.angular.y, Position_.angular.z);
  transform.setRotation(q);
  Broadcaster_.sendTransform(tf::StampedTransform(transform, current_time, "world", "base_link"));


  // Publish the odometry message over ROS
  nav_msgs::Odometry odom;
  odom.header.stamp    = current_time;
  odom.header.frame_id = "odom";

  // Set the position
  odom.pose.pose.position.x = Position_.linear.x;
  odom.pose.pose.position.y = Position_.linear.y;
  odom.pose.pose.position.z = Position_.linear.z;
  tf::quaternionTFToMsg(q, odom.pose.pose.orientation);

  // Set the velocity
  odom.child_frame_id = "base_link";
  odom.twist.twist = Velocity_;

  OdomPub_.publish(odom);
}

int main (int argc, char **argv)
{
  ros::init(argc, argv, "nav_engine");
  ros::NodeHandle node;

  ROS_INFO("Mower Navigation Online");

  ROS_INFO("Initalising comms channels");
  INS_Sub_ = node.subscribe("/robomower/nav/INS", 300, &insCallback);

  OdomPub_ = node.advertise<nav_msgs::Odometry>("odom", 10);

  ROS_INFO("Entering main event loop");
  ros::spin();
  return 0;
}

