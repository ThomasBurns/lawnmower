
#include "RangeSensor.h"

static std::vector<RangeSensor*> Sensors_;
static ros::Publisher Publisher_;
static tf::TransformBroadcaster *Broadcaster_;
static tf::TransformListener *Listener_;

static void CreateUltrasonicSensorList(void)
{
  ros::NodeHandle node;

  RangeSensor::config_t config;
  config.type = 0;

  config.obstaclesOnBorders = true;
  bool infiniteMap, obstaclesOnBorders;
  node.param<bool>("/robomower/map/infinite",  infiniteMap,  false);
  node.param<bool>("/robomower/map/obstaclesOnBorders",  obstaclesOnBorders,  true);

  if (( infiniteMap == true ) || ( obstaclesOnBorders == false )) {
    config.obstaclesOnBorders = false;
  }

  node.param<double>("/robomower/map/MaxWidth",  config.maxWidth,  6.0);
  node.param<double>("/robomower/map/MinWidth",  config.minWidth, -6.0);
  node.param<double>("/robomower/map/MaxHeight", config.maxHeight,  6.0);
  node.param<double>("/robomower/map/MinHeight", config.minHeight, -6.0);

  std::vector<std::string> sensorNames;
  node.getParam("/UltrasonicSensors", sensorNames);
  for ( unsigned int i = 0; i < sensorNames.size(); i ++ ) {

    ros::NodeHandle sensorHandle(sensorNames[i]);
    sensorHandle.param<std::string>("name", config.name, "test");
    sensorHandle.param<double>("range/min", config.minRange, 0.2);
    sensorHandle.param<double>("range/max", config.maxRange, 4.0);
    sensorHandle.param<double>("range/fov", config.fov, 15.0);

    sensorHandle.param<double>("position/x", config.x, 0.0);
    sensorHandle.param<double>("position/y", config.y, 0.0);
    sensorHandle.param<double>("position/z", config.z, 0.0);

    sensorHandle.param<double>("angle/r", config.roll, 0.0);
    sensorHandle.param<double>("angle/p", config.pitch, 0.0);
    sensorHandle.param<double>("angle/y", config.yaw, 0.0);

    RangeSensor *ptr = new RangeSensor(sensorNames[i], config, Broadcaster_, Listener_);
    Sensors_.push_back(ptr);
  }
}

static void CreateInfraredSensorList(void)
{
  ros::NodeHandle node;
  RangeSensor::config_t config;
  config.type = 1;
  config.fov = 0;

  config.obstaclesOnBorders = true;
  bool infiniteMap, obstaclesOnBorders;
  node.param<bool>("/robomower/map/infinite",  infiniteMap,  false);
  node.param<bool>("/robomower/map/obstaclesOnBorders",  obstaclesOnBorders,  true);

  if (( infiniteMap == true ) || ( obstaclesOnBorders == false )) {
    config.obstaclesOnBorders = false;
  }

  node.param<double>("/robomower/map/MaxWidth",  config.maxWidth,  6.0);
  node.param<double>("/robomower/map/MinWidth",  config.minWidth, -6.0);
  node.param<double>("/robomower/map/MaxHeight", config.maxHeight,  6.0);
  node.param<double>("/robomower/map/MinHeight", config.minHeight, -6.0);

  std::vector<std::string> sensorNames;
  node.getParam("/InfraredSensors", sensorNames);
  for ( unsigned int i = 0; i < sensorNames.size(); i ++ ) {

    ros::NodeHandle sensorHandle(sensorNames[i]);
    sensorHandle.param<std::string>("name", config.name, "test");
    sensorHandle.param<double>("range/min", config.minRange, 0.3);
    sensorHandle.param<double>("range/max", config.maxRange, 1.5);

    sensorHandle.param<double>("position/x", config.x, 0.0);
    sensorHandle.param<double>("position/y", config.y, 0.0);
    sensorHandle.param<double>("position/z", config.z, 0.0);

    sensorHandle.param<double>("angle/r", config.roll, 0.0);
    sensorHandle.param<double>("angle/p", config.pitch, 0.0);
    sensorHandle.param<double>("angle/y", config.yaw, 0.0);

    RangeSensor *ptr = new RangeSensor(sensorNames[i], config, Broadcaster_, Listener_);
    Sensors_.push_back(ptr);
  }
}

/* ================================================================= */
int main(int argc, char** argv)
{
  int startupDelay = 0;
  ros::init(argc, argv, "ultrasonic_sim", ros::init_options::AnonymousName);

  ros::NodeHandle node;
  Publisher_ = node.advertise<sensor_msgs::Range>("/RangeSensors", 50);

  ros::Duration delay(0.1);

  Broadcaster_ = new tf::TransformBroadcaster();
  Listener_    = new tf::TransformListener();
  CreateUltrasonicSensorList();
  CreateInfraredSensorList();

  ROS_INFO("Range Sim: live and running");
  while ( ros::ok() ) {
    delay.sleep();
    ros::spinOnce();

    for ( unsigned int i = 0; i < Sensors_.size(); i ++ ) {

      RangeSensor *ptr = Sensors_[i];
      ptr->broadcastPosition();

      if ( startupDelay >= 10 ) {
        ptr->update();
        Publisher_.publish(ptr->getRangeMsg());
      }
    }
    startupDelay++;
  }
  ROS_INFO("Range Sim: shutting down");


  for ( unsigned int i = 0; i < Sensors_.size(); i ++ ) {
    delete Sensors_[i];
  }
  Sensors_.clear();

  delete Broadcaster_;
  delete Listener_;

  return 0;
}

