
#include "LaserScanner.h"
#include <cmath>
#include <angles/angles.h>

#define NUM_SCANS 200

LaserScanner::LaserScanner(const config_t & config, tf::TransformBroadcaster *broadcaster, tf::TransformListener *listener):
  Name_(config.name),
  MinWidth_(config.minWidth),
  MaxWidth_(config.maxWidth),
  MinHeight_(config.minHeight),
  MaxHeight_(config.maxHeight),
  ObstaclesOnBorders_(config.obstaclesOnBorders),
  Broadcaster_(broadcaster),
  Listener_(listener)
{
  LaserMsg_.header.frame_id = Name_;
  LaserMsg_.range_min = 0.2;
  LaserMsg_.range_max = 4.0;

  LaserMsg_.angle_min = 0.0;
  LaserMsg_.angle_max = angles::from_degrees(360.0);
  LaserMsg_.angle_increment = 360.0 / NUM_SCANS;
  LaserMsg_.scan_time = 0.1;
  LaserMsg_.time_increment = 0.0;

  for ( int i = 0; i < NUM_SCANS; i ++ ) {
    LaserMsg_.ranges.push_back(LaserMsg_.range_min);
  }

  SystemTransform.setOrigin( tf::Vector3( config.x, config.y, config.z) );
  tf::Quaternion q;
  q.setRPY(angles::from_degrees(config.roll),
           angles::from_degrees(config.pitch),
           angles::from_degrees(config.yaw));
  SystemTransform.setRotation(q);
}

LaserScanner::~LaserScanner()
{
  ROS_ERROR("Goodbye - %s", Name_.c_str());
}

void LaserScanner::broadcastPosition(void)
{
  ROS_ASSERT(Broadcaster_ != NULL);
  Broadcaster_->sendTransform(tf::StampedTransform(SystemTransform, ros::Time::now(),"base_link", Name_));
}

void LaserScanner::update()
{
  ROS_ASSERT(Listener_ != NULL);
  try {
    tf::StampedTransform location;

    ros::Time now = ros::Time::now();
    ros::Time past = now - ros::Duration(0.2);

    Listener_->waitForTransform("/world", ros::Time(0),
                                Name_, past,
                                "/world", ros::Duration(3.0));

    Listener_->lookupTransform("/world", ros::Time(0),
                               Name_, past,
                               "/world", location);

    tf::Vector3 vec = location.getOrigin();
    double z = tf::getYaw(location.getRotation());

    LaserMsg_.header.seq++;
    LaserMsg_.header.stamp = location.stamp_;

    z += LaserMsg_.angle_min;
    for ( size_t i = 0; i < NUM_SCANS; i ++ ) {

      LaserMsg_.ranges[i] = run(vec.getX(), vec.getY(), angles::normalize_angle(z));
      z += angles::from_degrees(LaserMsg_.angle_increment);
    }
  }
  catch (tf::TransformException ex) {
    ROS_ERROR_THROTTLE(10, "Laser Sim (%s): %s", Name_.c_str(),ex.what());
    return;
  }
}

const sensor_msgs::LaserScan & LaserScanner::getLaserMsg() const
{
  return LaserMsg_;
}

double fRand(double fMin, double fMax)
{
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

/*********************************************************************
 * The algorithm is tuned for 2cm resolution.
 */
double LaserScanner::run(double x, double y, double theta)
{
  if ( ObstaclesOnBorders_ == false ) {
    return LaserMsg_.range_max;
  }

  const double inc = 1.0 / 100.0;
  double xInc = inc * std::cos(theta);
  double yInc = inc * std::sin(theta);

  for ( double len = 0.0; len < LaserMsg_.range_max; len += inc ) {

    x += xInc;
    y += yInc;

    if (( x >= MaxWidth_ ) || ( y <= MinWidth_ ) ||
        ( y >= MaxHeight_) || ( x <= MinHeight_))
    {
      double value = ( LaserMsg_.range_min > (len - inc))? LaserMsg_.range_min: (len - inc);
      return value + fRand(-0.02, 0.02);
    }
  }
  return LaserMsg_.range_max;
}
