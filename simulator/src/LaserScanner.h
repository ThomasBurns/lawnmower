#ifndef RangeSensor_H
#define RangeSensor_H

#include <sensor_msgs/LaserScan.h>

#include <string>

#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>

class LaserScanner
{
public:
  typedef struct {

    std::string name;

    double minWidth;
    double maxWidth;
    double minHeight;
    double maxHeight;

    double x,y,z;
    double roll, pitch, yaw;
    bool obstaclesOnBorders; // true means the system will act as if the map is surrounded by a wall.

  } config_t;

  LaserScanner(const config_t & config, tf::TransformBroadcaster *broadcaster, tf::TransformListener *listener);
  ~LaserScanner();
  void broadcastPosition(void);
  void update();

  const sensor_msgs::LaserScan & getLaserMsg() const;

private:
  double run(double x, double y, double theta);

  const std::string Name_;
  const double MinWidth_;
  const double MaxWidth_;
  const double MinHeight_;
  const double MaxHeight_;
  const bool ObstaclesOnBorders_;

  tf::TransformBroadcaster *Broadcaster_;
  tf::TransformListener    *Listener_;

  tf::Transform SystemTransform;
  sensor_msgs::LaserScan LaserMsg_;
};




#endif
