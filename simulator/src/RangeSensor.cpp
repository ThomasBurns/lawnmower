
#include "RangeSensor.h"
#include <cmath>
#include <angles/angles.h>

RangeSensor::RangeSensor(std::string topicName, const config_t & config, tf::TransformBroadcaster *broadcaster, tf::TransformListener *listener):
  Name_(config.name),
  MinWidth_(config.minWidth),
  MaxWidth_(config.maxWidth),
  MinHeight_(config.minHeight),
  MaxHeight_(config.maxHeight),
  Min_(config.minRange),
  Max_(config.maxRange),
  Width_(config.fov),
  ObstaclesOnBorders_(config.obstaclesOnBorders),
  Broadcaster_(broadcaster),
  Listener_(listener)
{
  RangeMsg_.header.frame_id = Name_;
  RangeMsg_.min_range = Min_;
  RangeMsg_.max_range = Max_;
  if ( config.type == 0 ) {
    RangeMsg_.field_of_view = Width_;
    RangeMsg_.radiation_type = RangeMsg_.ULTRASOUND;
  } else {
    RangeMsg_.field_of_view = 0;
    RangeMsg_.radiation_type = RangeMsg_.INFRARED;
  }

  SystemTransform.setOrigin( tf::Vector3( config.x, config.y, config.z) );
  tf::Quaternion q;
  q.setRPY(angles::from_degrees(config.roll),
           angles::from_degrees(config.pitch),
           angles::from_degrees(config.yaw));
  SystemTransform.setRotation(q);
}

RangeSensor::~RangeSensor()
{
  ROS_ERROR("Goodbye - %s", Name_.c_str());
}

void RangeSensor::broadcastPosition(void)
{
  ROS_ASSERT(Broadcaster_ != NULL);
  Broadcaster_->sendTransform(tf::StampedTransform(SystemTransform, ros::Time::now(),"base_link", Name_));
}

void RangeSensor::update()
{
  ROS_ASSERT(Listener_ != NULL);
  try {
    tf::StampedTransform location;

    ros::Time now = ros::Time::now();
    ros::Time past = now - ros::Duration(0.2);

    Listener_->waitForTransform("/world", ros::Time(0),
                                Name_, past,
                                "/world", ros::Duration(3.0));

    Listener_->lookupTransform("/world", ros::Time(0),
                               Name_, past,
                               "/world", location);

    tf::Vector3 vec = location.getOrigin();

    double x = vec.getX();
    double y = vec.getY();
    double z = tf::getYaw(location.getRotation());

    RangeMsg_.header.seq++;
    RangeMsg_.header.stamp = location.stamp_;

    if ( RangeMsg_.radiation_type == RangeMsg_.INFRARED ) {
      RangeMsg_.range = run_ir(x, y, z);
    } else {
      RangeMsg_.range = run_us(x, y, z);
    }
  }
  catch (tf::TransformException ex) {
    ROS_ERROR_THROTTLE(10, "US Sim (%s): %s", Name_.c_str(),ex.what());
    return;
  }
}

const sensor_msgs::Range & RangeSensor::getRangeMsg() const
{
  return RangeMsg_;
}

/*
 * This is the fasted algorithm I have come up with todate.
 * It is not the most effecient, but it works and works quickly.
 *
 * The algorithm is tuned for 2cm resolution.
 */
double RangeSensor::run_us(double x, double y, double z)
{
  if ( ObstaclesOnBorders_ == false ) {
    return Max_;
  }

  double r, range = Max_;

  for ( double angle = 0.0; angle <= (Width_/2.0); angle += 1.0 ) {
    double positive_angle = angles::normalize_angle(z + angles::from_degrees(angle));
    double negative_angle = angles::normalize_angle(z - angles::from_degrees(angle));
    r = run_ir(x, y, positive_angle);
    if ( r < range ) {
      range = r;
    }
    r = run_ir(x, y, negative_angle);
    if ( r < range ) {
      range = r;
    }
  }
  return range;
}

double RangeSensor::run_ir(double x, double y, double z)
{
  if ( ObstaclesOnBorders_ == false ) {
    return Max_;
  }

  const double inc = 1.0 / 100.0;
  double xInc = inc * std::cos(z);
  double yInc = inc * std::sin(z);

  for ( double len = Min_; len < Max_; len += inc ) {

    x += xInc;
    y += yInc;

    if (( x >= MaxWidth_ ) || ( x <= MinWidth_ ) ||
        ( y >= MaxHeight_) || ( y <= MinHeight_))
    {
      return ( Min_ > (len - inc))? Min_: (len - inc);
    }
  }
  return Max_;
}
