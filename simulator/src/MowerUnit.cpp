
# include "ros/ros.h"

#include "MowerUnit.h"

#define PI 3.14159265

MowerUnit::MowerUnit():
  MinWidth_(0.0),  MaxWidth_(0.0),
  MinHeight_(0.0), MaxHeight_(0.0),
  Radius_(0.0),
  CmdTime_(0.0),   InfiniteMap_(false)
{
  Pos_.linear.x = 0.0;
  Pos_.linear.y = 0.0;
  Pos_.linear.z = 0.0;
  Pos_.angular.x = 0.0;
  Pos_.angular.y = 0.0;
  Pos_.angular.z = 0.0;

  Vel_.linear.x = 0.0;
  Vel_.angular.z = 0.0;
}

void MowerUnit::setMapInfinite(bool infininte)
{
  InfiniteMap_ = infininte;
}


void MowerUnit::setPosition(float pX, float pY, float orient)
{
  Pos_.linear.x = pX;
  Pos_.linear.y = pY;
  Pos_.angular.z = orient;
}

void MowerUnit::setUnitRadius(float radius)
{
  Radius_ = radius;
}


void MowerUnit::setLimits(float minWidth, float maxWidth, float minHeight, float maxHeight)
{
  MinWidth_ = minWidth;
  MaxWidth_ = maxWidth;
  MinHeight_ = minHeight;
  MaxHeight_ = maxHeight;
}

void MowerUnit::velocityCallback(const geometry_msgs::Twist & vel)
{
  CmdTime_ = ros::WallTime::now().toSec();
  Vel_ = vel;
}

float fRand(float value, float range)
{
    float fMin = fabs(value) * -range;
    float fMax = fabs(value) *  range;
    float f = (float)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}


void MowerUnit::update(double dt)
{
  float now = ros::WallTime::now().toSec();
  if ((now - CmdTime_) > 1.0 ) {
    return;
  }

  float v_z = Vel_.angular.z;
  if ( v_z != 0.0 ) {
    v_z += fRand(v_z, 0.1);
  }
  float v_x = Vel_.linear.x;
  if ( v_x != 0.0 ) {
    v_x += fRand(v_x, 0.05);
  }

  Pos_.angular.z = std::fmod(Pos_.angular.z - v_z * dt, 2*PI);
  Pos_.linear.x += std::cos(Pos_.angular.z) * v_x * dt;
  Pos_.linear.y += std::sin(Pos_.angular.z) * v_x * dt;

  if ( InfiniteMap_ == false ) {
    if ( Pos_.linear.x < (MinWidth_+Radius_) ) {
      ROS_ERROR_THROTTLE(10, "Pos_X: Clamp negative");
      Pos_.linear.x = MinWidth_ + Radius_ + 0.001;
    }
    if ( Pos_.linear.y  < (MinHeight_+Radius_) ) {
      ROS_ERROR_THROTTLE(10, "Pos_Y: Clamp negative");
      Pos_.linear.y  = MinHeight_ + Radius_ + 0.001;
    }
     if ( Pos_.linear.x >= (MaxWidth_-Radius_) ) {
      ROS_ERROR_THROTTLE(10, "Pos_X: Clamp positive");
      Pos_.linear.x = MaxWidth_ - Radius_ - 0.001;
    }
     if ( Pos_.linear.y  >= (MaxHeight_-Radius_) ) {
      ROS_ERROR_THROTTLE(10, "Pos_Y: Clamp positive");
      Pos_.linear.y  = MaxHeight_ - Radius_ - 0.001;
    }
  }

}

const geometry_msgs::Twist & MowerUnit::getPos() const
{
  return Pos_;
}

const geometry_msgs::Twist & MowerUnit::getRealVelocity() const
{
  return Vel_;
}
