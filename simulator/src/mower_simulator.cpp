
#include "MowerUnit.h"

#include <ros/ros.h>
#include <tf/transform_broadcaster.h>

#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Range.h>

static ros::Subscriber VelSub_;
static ros::Publisher  OdomPub_;
static ros::Publisher  HardwarePub_;

static MowerUnit MUnit_;
static tf::TransformBroadcaster *Broadcaster_;

/* ================================================================= */
static void updatePosition()
{
  ros::Time current_time = ros::Time::now();
  ROS_ASSERT(Broadcaster_ != NULL);
  tf::Transform transform;
  tf::Quaternion q;

  MUnit_.update(0.02);
  geometry_msgs::Twist p = MUnit_.getPos();

  transform.setOrigin( tf::Vector3(p.linear.x, p.linear.y, p.linear.z) );
  q.setRPY(p.angular.x, p.angular.y, p.angular.z);
  transform.setRotation(q);
  Broadcaster_->sendTransform(tf::StampedTransform(transform, current_time, "world", "base_link"));

  // Publish the odometry message over ROS
  nav_msgs::Odometry odom;
  odom.header.stamp    = current_time;
  odom.header.frame_id = "odom";

  // Set the position
  odom.pose.pose.position.x = p.linear.x;
  odom.pose.pose.position.y = p.linear.y;
  odom.pose.pose.position.z = p.linear.z;
  tf::quaternionTFToMsg(q, odom.pose.pose.orientation);

  // Set the velocity
  odom.child_frame_id = "base_link";
  odom.twist.twist = MUnit_.getRealVelocity();

  OdomPub_.publish(odom);
}

static void loadSettings(void)
{
  ros::NodeHandle node;

  bool infiniteMap;
  ros::param::param<bool>("/robomower/map/infinite",  infiniteMap,  false);
  MUnit_.setMapInfinite(infiniteMap);

  float minWidth, maxWidth, minHeight, maxHeight;
  ros::param::param<float>("/robomower/map/MaxWidth",  maxWidth,  6.0);
  ros::param::param<float>("/robomower/map/MinWidth",  minWidth, -6.0);
  ros::param::param<float>("/robomower/map/MaxHeight", maxHeight,  6.0);
  ros::param::param<float>("/robomower/map/MinHeight", minHeight, -6.0);

  MUnit_.setLimits(minWidth,
                   maxWidth,
                   minHeight,
                   maxHeight);

  float radius;
  ros::param::param<float>("/robomower/system/BladeRadius", radius, 0.0);
  MUnit_.setUnitRadius(radius);


  float startX, startY;
  ros::param::param<float>("/robomower/map/StartWidth", startX, 0.0);
  ros::param::param<float>("/robomower/map/StartHeight", startY, 0.0);

  MUnit_.setPosition(startX,
                     startY,
                     0.0);

  OdomPub_ = node.advertise<nav_msgs::Odometry>("odom", 10);
  VelSub_   = node.subscribe("/cmd_vel", 5, &MowerUnit::velocityCallback, &MUnit_);

  Broadcaster_ = new tf::TransformBroadcaster();
}

/* ================================================================= */
int main(int argc, char** argv)
{
  ros::init(argc, argv, "mower_sim");

  loadSettings();

  ROS_INFO("Simulator live and running");
  ros::Duration delay(0.02);
  while ( ros::ok() ) {
    delay.sleep();
    ros::spinOnce();

    updatePosition();
  }

  return 0;
}

