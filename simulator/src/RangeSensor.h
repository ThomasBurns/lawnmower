#ifndef RangeSensor_H
#define RangeSensor_H

#include <sensor_msgs/Range.h>

#include <string>

#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>


class RangeSensor
{
public:
  typedef struct {
    std::string name;
    int type;
    double minWidth;
    double maxWidth;
    double minHeight;
    double maxHeight;
    double minRange;
    double maxRange;
    double fov;

    double x,y,z;
    double roll, pitch, yaw;
    bool obstaclesOnBorders; // true means the system will act as if the map is surrounded by a wall.

  } config_t;

  RangeSensor(std::string topicName, const config_t & config, tf::TransformBroadcaster *broadcaster, tf::TransformListener *listener);
  ~RangeSensor();
  void broadcastPosition(void );
  void updateSonicRange();

  void update();

  const sensor_msgs::Range & getRangeMsg() const;

private:
  double run_ir(double x, double y, double z);
  double run_us(double x, double y, double z);

  const std::string Name_;
  const double MinWidth_;
  const double MaxWidth_;
  const double MinHeight_;
  const double MaxHeight_;
  const double Min_, Max_, Width_;
  const bool ObstaclesOnBorders_;

  tf::TransformBroadcaster *Broadcaster_;
  tf::TransformListener    *Listener_;

  tf::Transform SystemTransform;
  sensor_msgs::Range RangeMsg_;
};




#endif
