#ifndef MOWER_UNIT_H
#define MOWER_UNIT_H

#include <geometry_msgs/Twist.h>

class MowerUnit
{
public:
  MowerUnit();

  void setMapInfinite(bool infininte);

  void setPosition(float pX, float pY, float orient);
  void setUnitRadius(float radius);

  void setLimits(float minWidth, float maxWidth, float minHeight, float maxHeight);
  void update(double dt);

  const geometry_msgs::Twist & getPos() const;

  void velocityCallback(const geometry_msgs::Twist & vel);

  const geometry_msgs::Twist & getRealVelocity() const;

private:
  float MinWidth_;
  float MaxWidth_;
  float MinHeight_;
  float MaxHeight_;
  float Radius_;

  float CmdTime_;
  bool InfiniteMap_;

  geometry_msgs::Twist Pos_;
  geometry_msgs::Twist Vel_;
};

#endif
