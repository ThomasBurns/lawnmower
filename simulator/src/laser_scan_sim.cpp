
#include "LaserScanner.h"

static LaserScanner *Sensor_;
static ros::Publisher Publisher_;
static tf::TransformBroadcaster *Broadcaster_;
static tf::TransformListener *Listener_;

static void CreateSensor(void)
{
  ros::NodeHandle sensorHandle;

  LaserScanner::config_t config;

  sensorHandle.param<double>("/robomower/map/MaxWidth",  config.maxWidth,  6.0);
  sensorHandle.param<double>("/robomower/map/MinWidth",  config.minWidth, -6.0);
  sensorHandle.param<double>("/robomower/map/MaxHeight", config.maxHeight,  6.0);
  sensorHandle.param<double>("/robomower/map/MinHeight", config.minHeight, -6.0);

  config.obstaclesOnBorders = true;
  bool infiniteMap, obstaclesOnBorders;
  sensorHandle.param<bool>("/robomower/map/infinite",  infiniteMap,  false);
  sensorHandle.param<bool>("/robomower/map/obstaclesOnBorders",  obstaclesOnBorders,  true);

  if (( infiniteMap == true ) || ( obstaclesOnBorders == false )) {
    config.obstaclesOnBorders = false;
  }

  sensorHandle.param<std::string>("name", config.name, "laser");

  sensorHandle.param<double>("position/x", config.x, 0.0);
  sensorHandle.param<double>("position/y", config.y, 0.0);
  sensorHandle.param<double>("position/z", config.z, 0.0);

  sensorHandle.param<double>("angle/r", config.roll, 0.0);
  sensorHandle.param<double>("angle/p", config.pitch, 0.0);
  sensorHandle.param<double>("angle/y", config.yaw, 0.0);

  Sensor_ = new LaserScanner(config, Broadcaster_, Listener_);
  ROS_ASSERT(Sensor_ != NULL);
}

/* ================================================================= */
int main(int argc, char** argv)
{
  int startupDelay = 0;
  ros::init(argc, argv, "laser_scanner", ros::init_options::AnonymousName);

  ros::NodeHandle node;
  Publisher_ = node.advertise<sensor_msgs::LaserScan>("/LaserScanner", 5);

  ros::Duration delay(0.1);

  Broadcaster_ = new tf::TransformBroadcaster();
  Listener_    = new tf::TransformListener();
  CreateSensor();

  ROS_INFO("Laser Scanner: live and running");
  while ( ros::ok() ) {
    delay.sleep();
    ros::spinOnce();

    Sensor_->broadcastPosition();
    if ( startupDelay >= 10 ) {
      Sensor_->update();
      Publisher_.publish(Sensor_->getLaserMsg());
    }
    startupDelay++;

  }
  ROS_INFO("Laser Scanner: shutting down");

  delete Broadcaster_;
  delete Listener_;

  return 0;
}

