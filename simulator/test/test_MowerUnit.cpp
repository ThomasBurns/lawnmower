#include <gtest/gtest.h>
#include "../src/MowerUnit.h"
#include <cmath>


static double Trig_DegreesToRadians(double degrees)
{
  return ( degrees * M_PI ) / 180.0;
}


namespace {

  class t_MowerUnit : public ::testing::Test {

  public:
    MowerUnit *mower;
    geometry_msgs::Twist Vel_;

  protected:

    t_MowerUnit() {
    }

    virtual ~t_MowerUnit() {
    }

    virtual void SetUp() {
      mower = new MowerUnit();
      mower->setPosition(0.0, 0.0, 0.0);
      mower->setLimits(-5.0, 5.0, -5.0, 5.0);
      Vel_.linear.x = 0.0;
      Vel_.angular.z = 0.0;
    }

    virtual void TearDown() {
      delete mower;

    }
  };

  /* ======================================= */
  TEST_F(t_MowerUnit, orientation_pos)
  {
    Vel_.linear.x = 0.0;
    Vel_.angular.z = 1.0;
    mower->velocityCallback(Vel_);
    mower->update(1.0);
    const geometry_msgs::Twist & pos = mower->getPos();

    ASSERT_FLOAT_EQ(-1.0, pos.angular.z);
  }

  TEST_F(t_MowerUnit, orientation_neg)
  {
    Vel_.linear.x = 0.0;
    Vel_.angular.z = -1.0;
    mower->velocityCallback(Vel_);
    mower->update(1.0);
    const geometry_msgs::Twist & pos = mower->getPos();

    ASSERT_FLOAT_EQ(1.0, pos.angular.z);
  }

  /* ======================================= */
  TEST_F(t_MowerUnit, pos_x_O0_X1)
  {
    Vel_.linear.x = 1.0;
    mower->velocityCallback(Vel_);
    mower->update(1.0);
    const geometry_msgs::Twist & pos = mower->getPos();

    GTEST_ASSERT_LT(1.0-0.01, pos.linear.x);
    GTEST_ASSERT_GT(1.0+0.01, pos.linear.x);

    GTEST_ASSERT_LT(0.0-0.01, pos.linear.y);
    GTEST_ASSERT_GT(0.0+0.01, pos.linear.y);
  }

  TEST_F(t_MowerUnit, pos_x_O0_Xm1)
  {
    Vel_.linear.x = -1.0;
    mower->velocityCallback(Vel_);
    mower->update(1.0);
    const geometry_msgs::Twist & pos = mower->getPos();

    GTEST_ASSERT_LT(-1.0-0.01, pos.linear.x);
    GTEST_ASSERT_GT(-1.0+0.01, pos.linear.x);

    GTEST_ASSERT_LT(0.0-0.01, pos.linear.y);
    GTEST_ASSERT_GT(0.0+0.01, pos.linear.y);
  }

  /* ======================================= */
  TEST_F(t_MowerUnit, pos_y_O90_Y1)
  {
    Vel_.linear.x = 1.0;
    Vel_.angular.z = Trig_DegreesToRadians(90);
    mower->velocityCallback(Vel_);
    mower->update(1.0);
    const geometry_msgs::Twist & pos = mower->getPos();

    GTEST_ASSERT_LT(0.0-0.01, pos.linear.x);
    GTEST_ASSERT_GT(0.0+0.01, pos.linear.x);

    GTEST_ASSERT_LT(-1.0-0.01, pos.linear.y);
    GTEST_ASSERT_GT(-1.0+0.01, pos.linear.y);
  }

  /* ======================================= */
  TEST_F(t_MowerUnit, pos_y_O90_Ym1)
  {
    Vel_.linear.x = -1.0;
    Vel_.angular.z = Trig_DegreesToRadians(90);
    mower->velocityCallback(Vel_);
    mower->update(1.0);
    const geometry_msgs::Twist & pos = mower->getPos();

    GTEST_ASSERT_LT(0.0-0.01, pos.linear.x);
    GTEST_ASSERT_GT(0.0+0.01, pos.linear.x);

    GTEST_ASSERT_LT(1.0-0.01, pos.linear.y);
    GTEST_ASSERT_GT(1.0+0.01, pos.linear.y);
  }

  TEST_F(t_MowerUnit, pos_y_Om90_Y1)
  {
    Vel_.linear.x = 1.0;
    Vel_.angular.z = Trig_DegreesToRadians(-90);
    mower->velocityCallback(Vel_);
    mower->update(1.0);
    const geometry_msgs::Twist & pos = mower->getPos();

    GTEST_ASSERT_LT(0.0-0.01, pos.linear.x);
    GTEST_ASSERT_GT(0.0+0.01, pos.linear.x);

    GTEST_ASSERT_LT(1.0-0.01, pos.linear.y);
    GTEST_ASSERT_GT(1.0+0.01, pos.linear.y);
  }

  TEST_F(t_MowerUnit, pos_y_Om90_Ym1)
  {
    Vel_.linear.x = -1.0;
    Vel_.angular.z = Trig_DegreesToRadians(-90);
    mower->velocityCallback(Vel_);
    mower->update(1.0);
    const geometry_msgs::Twist & pos = mower->getPos();

    GTEST_ASSERT_LT(0.0-0.01, pos.linear.x);
    GTEST_ASSERT_GT(0.0+0.01, pos.linear.x);

    GTEST_ASSERT_LT(-1.0-0.01, pos.linear.y);
    GTEST_ASSERT_GT(-1.0+0.01, pos.linear.y);
  }

  /* ======================================= */
  TEST_F(t_MowerUnit, infinite_map)
  {
    mower->setMapInfinite(true);
    Vel_.linear.x = 101.0;
    Vel_.angular.z = 0.0;
    mower->velocityCallback(Vel_);
    mower->update(1.0);
    const geometry_msgs::Twist & pos = mower->getPos();

    GTEST_ASSERT_GT(pos.linear.x, 100.0);
    GTEST_ASSERT_LT(pos.linear.y,  0.0001);
    GTEST_ASSERT_GT(pos.linear.y, -0.0001);
  }

} // Namespace
