#include <gtest/gtest.h>
#include "../src/IR_Unit.h"
#include <cmath>

static float DegreesToRadians(float degrees)
{
  return ( degrees * M_PI ) / 180.0;
}

namespace {

  class t_IR_Unit : public ::testing::Test {

  protected:
    geometry_msgs::Twist Pos;

    t_IR_Unit() {
    }

    virtual ~t_IR_Unit() {
    }

    virtual void SetUp() {
    }

    virtual void TearDown() {

    }
  };

  /* ======================================= */
  TEST_F(t_IR_Unit, min_range)
  {
    IR_Unit sensor(0.0, 10.0, 0.0, 10.0, 0.2, 4.0);

    geometry_msgs::Twist pos;
    pos.linear.x = 9.9;
    pos.linear.y = 9.9;
    pos.angular.z = DegreesToRadians(90);

    ASSERT_FLOAT_EQ(0.2, sensor.run(pos));
  }

  TEST_F(t_IR_Unit, max_range)
  {
    IR_Unit sensor(0.0, 10.0, 0.0, 10.0, 0.2, 4.0);

    geometry_msgs::Twist pos;
    pos.linear.x = 5.0;
    pos.linear.y = 5.0;
    pos.angular.z = DegreesToRadians(90);

    ASSERT_FLOAT_EQ(4.0, sensor.run(pos));
  }

  /* ======================================= */
  TEST_F(t_IR_Unit, straight_range_Left)
  {
    IR_Unit sensor(0.0, 10.0, 0.0, 10.0, 0.2, 4.0);
    geometry_msgs::Twist pos;
    pos.linear.x = 1.0;
    pos.linear.y = 5.0;
    pos.angular.z = DegreesToRadians(180);

    float r = sensor.run(pos);
    GTEST_ASSERT_LT(1.0-0.03, r);
    GTEST_ASSERT_GT(1.0+0.03, r);
  }

  TEST_F(t_IR_Unit, straight_range_Right)
  {
    IR_Unit sensor(0.0, 10.0, 0.0, 10.0, 0.2, 4.0);

    geometry_msgs::Twist pos;
    pos.linear.x = 9.0;
    pos.linear.y = 5.0;
    pos.angular.z = DegreesToRadians(0);

    float r = sensor.run(pos);
    GTEST_ASSERT_LT(1.0-0.03, r);
    GTEST_ASSERT_GT(1.0+0.03, r);
  }

  TEST_F(t_IR_Unit, straight_range_Up)
  {
    IR_Unit sensor(0.0, 10.0, 0.0, 10.0, 0.2, 4.0);

    geometry_msgs::Twist pos;
    pos.linear.x = 5.0;
    pos.linear.y = 9.0;
    pos.angular.z = DegreesToRadians(90);

    float r = sensor.run(pos);
    GTEST_ASSERT_LT(1.0-0.02, r);
    GTEST_ASSERT_GT(1.0+0.02, r);
  }

  TEST_F(t_IR_Unit, straight_range_Down)
  {
    IR_Unit sensor(0.0, 10.0, 0.0, 10.0, 0.2, 4.0);

    geometry_msgs::Twist pos;
    pos.linear.x = 5.0;
    pos.linear.y = 1.0;
    pos.angular.z = DegreesToRadians(-90);

    float r = sensor.run(pos);
    GTEST_ASSERT_LT(1.0-0.03, r);
    GTEST_ASSERT_GT(1.0+0.03, r);
  }

  /* ======================================= */
  TEST_F(t_IR_Unit, diagonal_top_left)
  {
    IR_Unit sensor(0.0, 10.0, 0.0, 10.0, 0.2, 4.0);

    geometry_msgs::Twist pos;
    pos.linear.x = 1.0;
    pos.linear.y = 1.0;
    pos.angular.z = DegreesToRadians(135);

    float r = sensor.run(pos);
    GTEST_ASSERT_LT(1.4-0.02, r);
    GTEST_ASSERT_GT(1.4+0.02, r);
  }

  TEST_F(t_IR_Unit, diagonal_top_right)
  {
    IR_Unit sensor(0.0, 10.0, 0.0, 10.0, 0.2, 4.0);

    geometry_msgs::Twist pos;
    pos.linear.x = 9.0;
    pos.linear.y = 1.0;
    pos.angular.z = DegreesToRadians(45);

    float r = sensor.run(pos);
    GTEST_ASSERT_LT(1.4-0.03, r);
    GTEST_ASSERT_GT(1.4+0.03, r);
  }

  TEST_F(t_IR_Unit, diagonal_bottom_left)
  {
    IR_Unit sensor(0.0, 10.0, 0.0, 10.0, 0.2, 4.0);

    geometry_msgs::Twist pos;
    pos.linear.x = 1.0;
    pos.linear.y = 9.0;
    pos.angular.z = DegreesToRadians(-135);

    float r = sensor.run(pos);
    GTEST_ASSERT_LT(1.4-0.03, r);
    GTEST_ASSERT_GT(1.4+0.03, r);
  }

  TEST_F(t_IR_Unit, diagonal_bottom_right)
  {
    IR_Unit sensor(0.0, 10.0, 0.0, 10.0, 0.2, 4.0);

    geometry_msgs::Twist pos;
    pos.linear.x = 9.0;
    pos.linear.y = 9.0;
    pos.angular.z = DegreesToRadians(-45);

    float r = sensor.run(pos);
    GTEST_ASSERT_LT(1.4-0.03, r);
    GTEST_ASSERT_GT(1.4+0.03, r);
  }
  
  TEST_F(t_IR_Unit, timing)
  {
    IR_Unit sensor(0.0, 10.0, 0.0, 10.0, 0.2, 4.0);

    geometry_msgs::Twist pos;
    pos.linear.x = 9.0;
    pos.linear.y = 9.0;
    pos.angular.z = DegreesToRadians(-45);
    
    for ( int i = 0; i < 100; i ++ ) {
      sensor.run(pos);
    }
  }
  


} // Namespace