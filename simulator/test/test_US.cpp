#include <gtest/gtest.h>
#include "../src/US_Unit.h"
#include <cmath>

static float DegreesToRadians(float degrees)
{
  return ( degrees * M_PI ) / 180.0;
}

namespace {

  class t_US_Unit : public ::testing::Test {

  protected:
    geometry_msgs::Twist Pos;

    t_US_Unit() {
    }

    virtual ~t_US_Unit() {
    }

    virtual void SetUp() {
    }

    virtual void TearDown() {

    }
  };

  /* ======================================= */
  TEST_F(t_US_Unit, min_range)
  {
    US_Unit us(0.0, 10.0, 0.0, 10.0, 0.2, 4.0, 15.0);

    geometry_msgs::Twist pos;
    pos.linear.x = 9.9;
    pos.linear.y = 9.9;
    pos.angular.z = DegreesToRadians(90);

    ASSERT_FLOAT_EQ(0.2, us.run(pos));
  }

  TEST_F(t_US_Unit, max_range)
  {
    US_Unit us(0.0, 10.0, 0.0, 10.0, 0.2, 4.0, 15.0);

    geometry_msgs::Twist pos;
    pos.linear.x = 5.0;
    pos.linear.y = 5.0;
    pos.angular.z = DegreesToRadians(90);

    ASSERT_FLOAT_EQ(4.0, us.run(pos));
  }

  /* ======================================= */
  TEST_F(t_US_Unit, straight_range_Left)
  {
    US_Unit us(0.0, 10.0, 0.0, 10.0, 0.2, 4.0, 15.0);
    geometry_msgs::Twist pos;
    pos.linear.x = 1.0;
    pos.linear.y = 5.0;
    pos.angular.z = DegreesToRadians(180);

    float r = us.run(pos);
    GTEST_ASSERT_LT(1.0-0.03, r);
    GTEST_ASSERT_GT(1.0+0.03, r);
  }

  TEST_F(t_US_Unit, straight_range_Right)
  {
    US_Unit us(0.0, 10.0, 0.0, 10.0, 0.2, 4.0, 15.0);

    geometry_msgs::Twist pos;
    pos.linear.x = 9.0;
    pos.linear.y = 5.0;
    pos.angular.z = DegreesToRadians(0);

    float r = us.run(pos);
    GTEST_ASSERT_LT(1.0-0.03, r);
    GTEST_ASSERT_GT(1.0+0.03, r);
  }

  TEST_F(t_US_Unit, straight_range_Up)
  {
    US_Unit us(0.0, 10.0, 0.0, 10.0, 0.2, 4.0, 15.0);

    geometry_msgs::Twist pos;
    pos.linear.x = 5.0;
    pos.linear.y = 9.0;
    pos.angular.z = DegreesToRadians(90);

    float r = us.run(pos);
    GTEST_ASSERT_LT(1.0-0.02, r);
    GTEST_ASSERT_GT(1.0+0.02, r);
  }

  TEST_F(t_US_Unit, straight_range_Down)
  {
    US_Unit us(0.0, 10.0, 0.0, 10.0, 0.2, 4.0, 15.0);

    geometry_msgs::Twist pos;
    pos.linear.x = 5.0;
    pos.linear.y = 1.0;
    pos.angular.z = DegreesToRadians(-90);

    float r = us.run(pos);
    GTEST_ASSERT_LT(1.0-0.03, r);
    GTEST_ASSERT_GT(1.0+0.03, r);
  }

  /* ======================================= */
  TEST_F(t_US_Unit, diagonal_top_left)
  {
    US_Unit us(0.0, 10.0, 0.0, 10.0, 0.2, 4.0, 15.0);

    geometry_msgs::Twist pos;
    pos.linear.x = 1.0;
    pos.linear.y = 1.0;
    pos.angular.z = DegreesToRadians(135);

    float r = us.run(pos);
    GTEST_ASSERT_LT(1.27-0.02, r);
    GTEST_ASSERT_GT(1.27+0.02, r);
  }

  TEST_F(t_US_Unit, diagonal_top_right)
  {
    US_Unit us(0.0, 10.0, 0.0, 10.0, 0.2, 4.0, 15.0);

    geometry_msgs::Twist pos;
    pos.linear.x = 9.0;
    pos.linear.y = 1.0;
    pos.angular.z = DegreesToRadians(45);

    float r = us.run(pos);
    GTEST_ASSERT_LT(1.27-0.03, r);
    GTEST_ASSERT_GT(1.27+0.03, r);
  }

  TEST_F(t_US_Unit, diagonal_bottom_left)
  {
    US_Unit us(0.0, 10.0, 0.0, 10.0, 0.2, 4.0, 15.0);

    geometry_msgs::Twist pos;
    pos.linear.x = 1.0;
    pos.linear.y = 9.0;
    pos.angular.z = DegreesToRadians(-135);

    float r = us.run(pos);
    GTEST_ASSERT_LT(1.27-0.03, r);
    GTEST_ASSERT_GT(1.27+0.03, r);
  }

  TEST_F(t_US_Unit, diagonal_bottom_right)
  {
    US_Unit us(0.0, 10.0, 0.0, 10.0, 0.2, 4.0, 15.0);

    geometry_msgs::Twist pos;
    pos.linear.x = 9.0;
    pos.linear.y = 9.0;
    pos.angular.z = DegreesToRadians(-45);

    float r = us.run(pos);
    GTEST_ASSERT_LT(1.27-0.03, r);
    GTEST_ASSERT_GT(1.27+0.03, r);
  }
  
  TEST_F(t_US_Unit, timing)
  {
    US_Unit us(0.0, 10.0, 0.0, 10.0, 0.2, 4.0, 15.0);

    geometry_msgs::Twist pos;
    pos.linear.x = 9.0;
    pos.linear.y = 9.0;
    pos.angular.z = DegreesToRadians(-45);
    
    for ( int i = 0; i < 100; i ++ ) {
      us.run(pos);
    }
  }
  


} // Namespace