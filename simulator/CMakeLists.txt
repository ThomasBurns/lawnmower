cmake_minimum_required(VERSION 2.8.3)
project(simulator)

find_package(catkin REQUIRED COMPONENTS
  geometry_msgs
  message_generation
  rosconsole
  roscpp
  roscpp_serialization
  roslib
  rostime
  std_msgs
  std_srvs
  tf
)

include_directories(
  ${catkin_INCLUDE_DIRS}
  ${mowerlib_INCLUDE_DIRS}
  ${roscpp_INCLUDE_DIRS}
)
set(CMAKE_CXX_FLAGS "-Wall")

link_directories(${catkin_LIBRARY_DIRS})

catkin_package(CATKIN_DEPENDS
  geometry_msgs
  message_runtime
  std_msgs
  std_srvs
)

catkin_add_gtest(${PROJECT_NAME}_test
    src/MowerUnit.cpp
    test/test_MowerUnit.cpp
    test/utest.cpp
)
target_link_libraries(${PROJECT_NAME}_test ${catkin_LIBRARIES})

set(mower_sim_SRCS
  src/mower_simulator.cpp
  src/MowerUnit.cpp
)

add_executable(mower_sim ${mower_sim_SRCS})
target_link_libraries(mower_sim ${catkin_LIBRARIES})

set(range_sim_SRCS
  src/range_sim.cpp
  src/RangeSensor.cpp
)

add_executable(range_sim ${range_sim_SRCS})
target_link_libraries(range_sim ${catkin_LIBRARIES})

set(laser_sim_SRCS
  src/laser_scan_sim.cpp
  src/LaserScanner.cpp
)

add_executable(laser_sim ${laser_sim_SRCS})
target_link_libraries(laser_sim ${catkin_LIBRARIES})


