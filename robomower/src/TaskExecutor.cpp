
#include "TaskExecutor.h"
#include "ControlProxy.h"

#include <ros/ros.h>

const float STATE_DELAY =  0.25;


TaskExecutor::TaskExecutor():
  Active_(false)
{
}

TaskExecutor::~TaskExecutor()
{
  clearAllSteps();
}

/* ================================================== */
/* Public Methods */
void TaskExecutor::start(float linearSpeed, float angularSpeed)
{
  ControlProxy_SetSpeedLimits(linearSpeed, angularSpeed);
  triggerStep();
}

void TaskExecutor::run()
{
  if ( Active_ == true ) {
    if ( ControlProxy_reachedTarget() == false ) {
      return;
    }
    // here when we have reached our target
    Active_ = false;
    Steps_.erase (Steps_.begin());
    if ( Steps_.size() != 0 ) {
      triggerStep();
    }
  }
}

void TaskExecutor::clearAllSteps()
{
  Steps_.clear();
}

bool TaskExecutor::systemRunning() const
{
  return Active_ && (stepsRemaining() != 0);
}

unsigned int TaskExecutor::stepsRemaining() const
{
  return Steps_.size();
}

void TaskExecutor::AddStep_MoveToRelativePosition(float x, float y)
{
  AtomicStep_t step;
  step.abs = false;
  step.pos = true;
  step.x = x;
  step.y = y;
  Steps_.push_back(step);
}

void TaskExecutor::AddStep_MoveToAbsPosition(float x, float y)
{
  AtomicStep_t step;
  step.abs = true;
  step.pos = true;
  step.x = x;
  step.y = y;
  Steps_.push_back(step);
}

void TaskExecutor::AddStep_MoveToAbsAngle(float theta)
{
  AtomicStep_t step;
  step.abs = true;
  step.pos = false;
  step.x = theta;
  step.y = 0.0;
  Steps_.push_back(step);
}

void TaskExecutor::AddStep_MoveToRelativeAngle(float theta)
{
  AtomicStep_t step;
  step.abs = false;
  step.pos = false;
  step.x = theta;
  step.y = 0.0;
  Steps_.push_back(step);
}

/* ================================================== */
/* Private Methods */
void TaskExecutor::triggerStep()
{
  if ( Steps_.size() == 0 ) {
    return;
  }
  AtomicStep_t op = Steps_.front();

  if ( op.pos == true ) {
    ControlProxy_SetTargetPosition(op.x, op.y, STATE_DELAY);
  } else {
    ControlProxy_SetTargetAngle(op.x, STATE_DELAY);
  }
  Active_ = true;
}


/* ================================================== */
/* Static Methods */
