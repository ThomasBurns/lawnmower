#ifndef CM_IDLE_H
#define CM_IDLE_H

#include "ControlMode.h"
#include "ControlProxy.h"

class CM_IdleMode: protected ControlMode
{
public:
  CM_IdleMode(const ControlModeModules_t & modules):
    ControlMode(modules, "Idle")
  {
    ControlProxy_SetSpeedLimits(0.0, 0.0);
    ControlProxy_SetTargetPosition(Position_);
  }

  void run()
  {
  }

private:

};

#endif
