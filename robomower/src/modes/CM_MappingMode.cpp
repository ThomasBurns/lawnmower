
#include "CM_MappingMode.h"
#include "ControlProxy.h"
#include "TaskExecutor.h"

#include <ros/ros.h>
#include <mowerlib/MapUtils.h>
#include <mowerlib/Trigonometry.h>

const static int   NUM_STEPS   = 100;
const float STATE_DELAY =  0.25;

const static int NUM_TURN_POINTS = 12;
const static float TURN_INC = 360.0 / (float) NUM_TURN_POINTS;

const static float NORMAL_LINEAR_SPEED  = 0.5;
const static float MAPPING_LINEAR_SPEED = 0.075;

const static float NORMAL_ANGULAR_SPEED = 10.0;


CM_MappingMode::CM_MappingMode(const ControlModeModules_t & modules):
  ControlMode(modules, "Mapping"),
  LastObstacleState_(false)
{
  Executor_ = new TaskExecutor();
  ControlProxy_SetSpeedLimits(NORMAL_LINEAR_SPEED, NORMAL_ANGULAR_SPEED);
  ControlProxy_SetTargetPosition(Position_, STATE_DELAY);
  GenerateWaypoints();
}

CM_MappingMode::~CM_MappingMode()
{
  delete Executor_;
}

void CM_MappingMode::run()
{
  Executor_->run();
  if ( Executor_->systemRunning() == false ) {
    markModeComplete();
    return;
  }

  bool obstacleState = areThereObstaclesAround();
  if ( LastObstacleState_ != obstacleState ) {
    LastObstacleState_ = obstacleState;
    startMoveOperation();
  }
}

void CM_MappingMode::startMoveOperation()
{
  float linSpeed = (areThereObstaclesAround()==false)? NORMAL_LINEAR_SPEED: MAPPING_LINEAR_SPEED;
  float angSPeed = NORMAL_ANGULAR_SPEED;
  ControlProxy_SetSpeedLimits(linSpeed, angSPeed);
  Executor_->start(linSpeed, angSPeed);
}

void CM_MappingMode::GenerateWaypoints()
{
  Executor_->AddStep_MoveToAbsPosition( 4.0,  0.0);

  Executor_->AddStep_MoveToAbsPosition( 3.0,  3.0);
  Executor_->AddStep_MoveToAbsPosition( 5.0,  5.0);

  Executor_->AddStep_MoveToAbsPosition( 0.0,  3.5);
  Executor_->AddStep_MoveToAbsPosition(-5.0,  5.0);

  Executor_->AddStep_MoveToAbsPosition(-3.5,  0.0);
  Executor_->AddStep_MoveToAbsPosition(-5.0, -5.0);

  Executor_->AddStep_MoveToAbsPosition( 0.0, -3.5);
  Executor_->AddStep_MoveToAbsPosition( 5.0, -5.0);

  Executor_->AddStep_MoveToAbsPosition( 5.0,  0.0);
  Executor_->AddStep_MoveToAbsPosition( 0.0,  0.0);

  startMoveOperation();
}

/* ==================================================================== */
/* ==================================================================== */
