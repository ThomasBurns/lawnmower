#ifndef CM_MAPPING_MODE_H
#define CM_MAPPING_MODE_H

#include "ControlMode.h"
#include <list>

class OccupancyMap;
class TaskExecutor;

class CM_MappingMode: protected ControlMode
{
public:
  CM_MappingMode(const ControlModeModules_t & modules);
  ~CM_MappingMode();

  void run();

private:
  void startMoveOperation();
  void GenerateWaypoints();

  TaskExecutor *Executor_;
  bool LastObstacleState_;
};

#endif
