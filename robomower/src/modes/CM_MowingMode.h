#ifndef CM_MOWING_MODE_H
#define CM_MOWING_MODE_H

#include <mowerlib/WayPoint.h>

#include "ControlMode.h"

class CM_MowingMode: protected ControlMode
{
public:
  CM_MowingMode(const ControlModeModules_t & modules);

  void run();

  static void setBladeRadius(float radius);

private:
  bool Moving_;
  unsigned int SegmentNumber;
  unsigned int StartTime_;

  void GenerateSpiralPath();
  void GenerateLinedPath();
};


#endif
