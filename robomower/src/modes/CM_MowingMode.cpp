
#include "CM_MowingMode.h"
#include "TravelMap.h"
#include "ControlProxy.h"
#include "GridStorage.h"

#include "ros/ros.h"
#include <mowerlib/Trigonometry.h>

static float Radius_;


CM_MowingMode::CM_MowingMode(const ControlModeModules_t & modules):
  ControlMode(modules, "Mowing"),
  Moving_(false),
  SegmentNumber(0)
{
  StartTime_ = ros::Time::now().toSec();
  ControlProxy_SetSpeedLimits(1.0, 10.0);


  GenerateSpiralPath();
//    GenerateLinedPath();

  ControlProxy_SetTargetPosition(Position_);
  WayPoint startPos;
  startPos.X = Position_.linear.x;
  startPos.Y = Position_.linear.y;
  WaypointList.push_back(startPos);
  float distance = WayPoint_Sum(startPos, WaypointList);
  ROS_INFO("Primary Mission: Starting");
  ROS_INFO("Waypoints: %u, Distance: %1.2f", (unsigned int)WaypointList.size(), distance);

}

void CM_MowingMode::setBladeRadius(float radius)
{
  ROS_ASSERT(isnan(radius) == false);
  Radius_ = radius;
}

void CM_MowingMode::run()
{
  if ( Moving_ == false ) {
      if ( WaypointList.size() == 0 ) {
        ROS_INFO("Primary Mission: Completed");
        unsigned int time = ros::Time::now().toSec() - StartTime_;
        ROS_INFO("Time taken %02d:%02d", time / 60, time % 60 );
        markModeComplete();

      } else {
        WayPoint wp = WaypointList.front();
        WaypointList.erase (WaypointList.begin());
        ControlProxy_SetTargetPosition(wp.X, wp.Y);

        SegmentNumber++;
        ROS_INFO("Segment Started, %u, (%2.3f, %2.3f) => (%2.3f, %2.3f)", SegmentNumber, Position_.linear.x, Position_.linear.y, wp.X, wp.Y);
        Moving_ = true;
      }

  } else {
    if ( ControlProxy_reachedTarget() == true ) {
      ROS_INFO("Segment Completed, %u",SegmentNumber);
      ControlProxy_SetTargetPosition(Position_);
      Moving_ = false;
    }
  }
}

void CM_MowingMode::GenerateSpiralPath()
{
  double radius = Radius_*1.45;

  const GridStorage::Dimensions_t & d = Grid_.getDimensions();
  float left   = d.minWidth + 0.5;
  float right  = d.maxWidth - 0.5;
  float top    = d.minHeight + 0.5;
  float bottom = d.maxHeight - 0.5;

  while (( left < right ) &&
         ( top  < bottom )) {

    WaypointList.push_back(WayPoint(left, top));
    WaypointList.push_back(WayPoint(right, top));
    top  += radius;

    WaypointList.push_back(WayPoint(right, bottom));
    right  -= radius;

    WaypointList.push_back(WayPoint(left, bottom));
    WaypointList.push_back(WayPoint(left, top));

    left += radius;
    bottom -= radius;
  }
}

void CM_MowingMode::GenerateLinedPath()
{
  const GridStorage::Dimensions_t & d = Grid_.getDimensions();
  WayPoint pt1, pt2;
  double radius = Radius_*1.45;
  float x_axis = d.minHeight + (Radius_*0.75);
  bool reverse = false;

  while ( x_axis < d.maxWidth ) {

    pt1.X = x_axis;
    pt2.X = x_axis;

    if ( reverse == false ) {
      pt1.Y = d.minHeight + (Radius_*0.75);
      pt2.Y = d.maxHeight - (Radius_*0.75);
    } else {
      pt1.Y = d.maxHeight - (Radius_*0.75);
      pt2.Y = d.minHeight + (Radius_*0.75);
    }
    WaypointList.push_back(pt1);
    WaypointList.push_back(pt2);

    x_axis += radius;
    reverse = (reverse == true)? false: true;
  }
}
