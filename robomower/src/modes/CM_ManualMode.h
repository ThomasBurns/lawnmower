#ifndef CM_MANUAL_CONTROL_H
#define CM_MANUAL_CONTROL_H

#include "ControlMode.h"
#include "sensor_msgs/Joy.h"

namespace ros {
  class Subscriber;
};

class CM_ManualMode: protected ControlMode
{
public:
  CM_ManualMode(const ControlModeModules_t & modules);
  ~CM_ManualMode();

  void run();

private:
  void joystickUpdate(const sensor_msgs::Joy & update);

  float MaxFwd_, MaxTurn_;
  float AnglularRate_;
  unsigned int PacketTime_;
  ros::Subscriber *JoySub_;

};

#endif
