
#include "CM_ManualMode.h"
#include "ControlProxy.h"

#include "ros/ros.h"

CM_ManualMode::CM_ManualMode(const ControlModeModules_t & modules):
  ControlMode(modules, "Manual Mode"), AnglularRate_(0.0), PacketTime_(0)
{
  ros::NodeHandle node;

  JoySub_ = new ros::Subscriber();
  *JoySub_  = node.subscribe("/joy", 10, &CM_ManualMode::joystickUpdate, this);

  ros::param::param<float>("/robosetting/velocity/MaxLinear", MaxFwd_, 1.0);
  float degrees;
  ros::param::param<float>("/robosetting/velocity/MaxAngular", degrees, 45.0);
  MaxTurn_ = ( degrees * 3.14 ) / 180.0;
}

CM_ManualMode::~CM_ManualMode()
{
  JoySub_->shutdown();
  delete JoySub_;
}

void CM_ManualMode::run()
{
  float now = ros::Time::now().sec;
  if (( now - PacketTime_) > 60 ) {
    ControlProxy_SetSpeedLimits(0.0, 0.0);
    ControlProxy_SetTargetPosition(Position_);
    PacketTime_ = now;

  } else {

    float angle = Position_.angular.z + ( AnglularRate_ * MaxTurn_);
    float x = Position_.linear.x + std::cos(angle);
    float y = Position_.linear.y + std::sin(angle);
    ControlProxy_SetTargetPosition(x, y);
  }
}

void CM_ManualMode::joystickUpdate(const sensor_msgs::Joy & update)
{
  ROS_ASSERT(isnan(update.axes[0]) == false);
  ROS_ASSERT(isnan(update.axes[1]) == false);

  PacketTime_ = ros::Time::now().sec;

  AnglularRate_ = update.axes[0];

  float fwd  = MaxFwd_  * update.axes[1];
  float turn = fabs(MaxTurn_ * update.axes[0]);
  ControlProxy_SetSpeedLimits(fwd,  turn);
}

