
#include "ControlMode.h"
#include "ControlProxy.h"
#include "CM_Idle.h"
#include "CM_ManualMode.h"
#include "CM_MowingMode.h"
#include "CM_MappingMode.h"

#include "GridStorage.h"
#include "TravelMap.h"
#include "OccupancyFilter.h"
#include "OccupancyMap.h"

#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <tf/transform_listener.h>
#include <std_srvs/Empty.h>

#include <mowerlib/Map_TravelLayer.h>
#include <mowerlib/Map_MapLayer.h>
#include <mowerlib/MowerState.h>
#include <mowerlib/SetMowerState.h>
#include <mowerlib/WayPointsMsg.h>
#include <mowerlib/MapSize.h>

#include <mowerlib/Conversion.h>

/* ======================================================= */
#define SYSTEM_RESOLUTION   50

/* ======================================================= */
static ros::Subscriber OdomSub_;
static ros::Timer      HeartbeatTimer_;
static ros::Timer      AdvancedProccessTimer_;

static ros::Publisher  MapLayerPub_;
static ros::Publisher  TravelPub_;
static ros::Publisher  SystemStatePub_;
static ros::Publisher  WaypointsPub_;
static ros::Publisher  MapSizePub_;

static ros::ServiceServer GetUnit_Service_;
static ros::ServiceServer SetMState_Service_;


static geometry_msgs::Twist SysPos_; // X, Y, Theta
static geometry_msgs::Twist SysVelocity_; // X, Y, Theta
static ControlMode * Control_;

static GridStorage     MapStorage_(SYSTEM_RESOLUTION);
static TravelMap       TravelMap_(&MapStorage_);
static OccupancyMap    OccupancyMap_(MapStorage_, SysPos_);
static OccupancyFilter OccupancyFilter_(MapStorage_);

static int SystemMode_;
static bool PositionReceived_;

static void CheckMapStatus(void);
static void publishMapTravelGrid(GridRef_t grid);
static void publishMapGrid(GridRef_t grid);
static void PublishMapSize();

/* --------------------------------------------------- */
static void Factory_ControlMode(int mode)
{
  if (( mode == SystemMode_ ) && ( Control_ != NULL )) {
    return;
  }
  SystemMode_ = mode;

  if ( Control_ != NULL ) {
    delete Control_;
    Control_ = NULL;
  }

  ControlMode::ControlModeModules_t mod = {
    .pos = SysPos_,
    .grid = MapStorage_,
    .map = TravelMap_
  };

  switch ( mode ) {

    case mowerlib::MowerState::STATE_IDLE:
      Control_ = (ControlMode *) new CM_IdleMode(mod);
      break;

    case mowerlib::MowerState::STATE_MANUAL:
      Control_ = (ControlMode *) new CM_ManualMode(mod);
      break;

    case mowerlib::MowerState::STATE_MOWING:
      Control_ = (ControlMode *) new CM_MowingMode(mod);
      break;

    case mowerlib::MowerState::STATE_MAPPING:
      Control_ = (ControlMode *) new CM_MappingMode(mod);
      break;

    default:
      ROS_ASSERT("Unknown State");
      break;
  }

  ROS_ASSERT(Control_ != NULL);
  Control_->runMode();
  ROS_INFO("Switched to mode: \"%s\"", Control_->ModeName_.c_str());

  mowerlib::MowerState mState;
  mState.State = mode;
  SystemStatePub_.publish(mState);
}

/* ======================================================= */
/* ======================================================= */
static void odomCallback(const nav_msgs::Odometry & odom)
{
  SysVelocity_ = odom.twist.twist;

  geometry_msgs::Twist pos;
  pos.linear.x = odom.pose.pose.position.x;
  pos.linear.y = odom.pose.pose.position.y;
  pos.linear.z = odom.pose.pose.position.z;

  tf::Quaternion q;
  tf::quaternionMsgToTF(odom.pose.pose.orientation, q);
  tf::Matrix3x3(q).getRPY(pos.angular.x, pos.angular.y, pos.angular.z);

  if ( PositionReceived_ == false ) {
    GridStorage::Dimensions_t d;
    d.maxWidth  = pos.linear.x + 1;
    d.minWidth  = pos.linear.x - 1;
    d.minHeight = pos.linear.y - 1;
    d.maxHeight = pos.linear.y + 1;
    MapStorage_.preallocateMemory(d); // for speed.
    PositionReceived_ = true;
  }
  if ( Equal(SysPos_, pos) == false ) {

    SysPos_ = pos;
    TravelMap_.markTravelled(pos);
    if ( SystemMode_ == mowerlib::MowerState::STATE_MOWING ) {
      TravelMap_.markMown(pos);
    }
  }
}

/******************************************
 * This Timer thread is used for running the core control
 * logic.
 */

static void HeartbeatCallback(const ros::TimerEvent & e)
{
  ROS_ASSERT(Control_ != NULL);

  ControlProxy_Run();
  Control_->runMode();

  if ( Control_->modeComplete() == true ) {
    ROS_INFO("Mode %s has completed its operations.", Control_->ModeName_.c_str());
    Factory_ControlMode(mowerlib::MowerState::STATE_IDLE);
  }
  CheckMapStatus();
}

/******************************************
 * This Timer thread is used for running higher level
 * process that do not need to run as often.
 *
 */

static void AdvancedProcessCallback(const ros::TimerEvent & e)
{
//  OccupancyMap_.runIntrospection();
}

/* ======================================================= */
bool SetMState_(mowerlib::SetMowerState::Request  &req,
                mowerlib::SetMowerState::Response &res )
{
  Factory_ControlMode(req.State);
  res.Success = true;
  return true;
}

/* ======================================================= */
static void publishMapTravelGrid(GridRef_t grid)
{
  ROS_ASSERT(MapStorage_.doesGridExist(grid) == true);
  MapGrid_ptr p = MapStorage_.getGrid(grid);

  mowerlib::Map_TravelLayer m;
  m.Width  = grid.width;
  m.Height = grid.height;
  m.Resolution = MapStorage_.getResolution();

  p->readTravelledData(&m.Data);
  p->clearTravelDirty();
  TravelPub_.publish(m);

  p->recordTravelledData();
}

static void publishMapGrid(GridRef_t grid)
{
  ROS_ASSERT(MapStorage_.doesGridExist(grid) == true);
  MapGrid_ptr p = MapStorage_.getGrid(grid);

  mowerlib::Map_MapLayer m;
  m.Width  = grid.width;
  m.Height = grid.height;
  m.Resolution = MapStorage_.getResolution();

  p->readMapData(&m.Data);
  p->clearMapDirty();
  MapLayerPub_.publish(m);
  p->recordMapData();
}

bool GetUnit_(std_srvs::Empty::Request  &req,
              std_srvs::Empty::Response &res )
{
  MapStorage_.markAllAsDirty();
  return true;
}

static void CheckMapStatus(void)
{
  if ( MapStorage_.haveDimensionsChanged() ) {
    PublishMapSize();
  }

  static unsigned int waypointSize;

  int count = 10;
  while ( count-- != 0 ) {
    const GridRef_List & dirtyList = MapGrid::getTravelDirtyList();
    if ( dirtyList.size() == 0 ) {
      break;
    }
    publishMapTravelGrid(*dirtyList.begin());
  }

  count = 10;
  while ( count-- != 0 ) {
    const GridRef_List & dirtyList = MapGrid::getMapDirtyList();
    if ( dirtyList.size() == 0 ) {
      break;
    }
    publishMapGrid(*dirtyList.begin());
    OccupancyMap_.addModifiedGrid(*dirtyList.begin());
  }


  const WayPointList_t & wlist = Control_->getWaypointList();
  if ( waypointSize != wlist.size() ) {
    mowerlib::WayPointsMsg msg;
    mowerlib::WayPointMsg pt;

    pt.X = ControlProxy_getTarget().X;
    pt.Y = ControlProxy_getTarget().Y;
    msg.WayPoints.push_back(pt);
    for (WayPointList_t::const_iterator ci = wlist.begin(); ci != wlist.end(); ++ci) {
      pt.X = ci->X;
      pt.Y = ci->Y;
      msg.WayPoints.push_back(pt);
    }

    WaypointsPub_.publish(msg);
    waypointSize = wlist.size();
  }
}

static void PublishMapSize()
{
  mowerlib::MapSize msg;

  MapStorage_.clearDimensionsChangedFlag();
  GridStorage::Dimensions_t d = MapStorage_.getDimensions();

  msg.MaxHeight = d.maxHeight;
  msg.MinHeight = d.minHeight;
  msg.MaxWidth = d.maxWidth;
  msg.MinWidth = d.minWidth;
  msg.Resolution = MapStorage_.getResolution();
  MapSizePub_.publish(msg);
}

/* ======================================================= */
static void loadConfig()
{
  float radius;
  ros::param::param<float>("/robomower/system/BladeRadius", radius, 0.23);
  TravelMap_.setBladeRadius(fabs(radius), SYSTEM_RESOLUTION);
  CM_MowingMode::setBladeRadius(fabs(radius));
}

/* ======================================================= */
int main (int argc, char **argv)
{
  ros::init(argc, argv, "lawn_mower");
  ros::NodeHandle node;
  SubGrid_t::setResolution(SYSTEM_RESOLUTION);

  ROS_INFO("RoboMower Online");

  ControlProxy_Connect();
  loadConfig();

  HeartbeatTimer_ = node.createTimer(ros::Duration(0.1), &HeartbeatCallback);

  AdvancedProccessTimer_  = node.createTimer(ros::Duration(1.0), &AdvancedProcessCallback);

  OdomSub_ = node.subscribe("odom", 20, &odomCallback);

  MapLayerPub_    = node.advertise<mowerlib::Map_MapLayer>   ("/robomower/map/Map",       10);
  TravelPub_      = node.advertise<mowerlib::Map_TravelLayer>("/robomower/map/Travel",    10);
  SystemStatePub_ = node.advertise<mowerlib::MowerState>     ("/robomower/system/State",  1, true);
  WaypointsPub_   = node.advertise<mowerlib::WayPointsMsg>   ("/robomower/map/Waypoints", 1, true);
  MapSizePub_     = node.advertise<mowerlib::MapSize>        ("/robomower/map/Size",      1, true);
  PublishMapSize();

  GetUnit_Service_ = node.advertiseService("/robomower/srv/GetUnit", GetUnit_);
  SetMState_Service_ = node.advertiseService("/robomower/system/SetState", SetMState_);

  OccupancyFilter_.connect(true);


  Factory_ControlMode(mowerlib::MowerState::STATE_IDLE);

  ros::spin();
  return 0;
}

