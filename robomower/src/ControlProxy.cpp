
#include <ros/ros.h>

#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/terminal_state.h>
#include <position_control/ControlVectorAction.h>

#include "ControlProxy.h"


static ros::Publisher     TargetPositionPub_;
static ros::Time          StartTime_;

static Point2D_t TargetPoint_;
static bool PublishNewTarget_;
static bool TargetLoaded_;

static float LinearVelocity_, AngularVelocity_;
static float MaxAngularVelocity_, MaxLinearVelocity_;

static float TargetAngle_;
static bool MoveToAngle_;
static bool MoveToPosition_;

// create the action client
// true causes the client to spin its own thread
static actionlib::SimpleActionClient<position_control::ControlVectorAction> *ActClient_;

void ControlProxy_Connect()
{
  ros::NodeHandle node;

  ActClient_ = new actionlib::SimpleActionClient<position_control::ControlVectorAction>("/pos_con_act", true);

  TargetPoint_ = Point2D_t(1e-9, 1e-9);
  PublishNewTarget_ = false;
  TargetLoaded_ = false;
  StartTime_ = ros::Time::now();
  
  TargetAngle_ = 0.0;
  MoveToAngle_ = false;
  MoveToPosition_ = false;

  float degrees;
  ros::param::param<float>("/robosetting/velocity/MaxLinear", MaxLinearVelocity_, 1.0);
  ros::param::param<float>("/robosetting/velocity/MaxAngular", degrees, 45.0);
  MaxAngularVelocity_ = ( degrees * M_PI ) / 180.0;
  
  
  TargetPositionPub_ = node.advertise<geometry_msgs::Point>("/robomower/nav/TargetPosition", 5, true);
  ActClient_->waitForServer(); //will wait for infinite time
}

void ControlProxy_SetSpeedLimits(float fwd, float turn)
{
  if ( turn > MaxAngularVelocity_ ) {
    turn = MaxAngularVelocity_;
  }
  if ( fwd > MaxLinearVelocity_ ) {
    fwd = MaxLinearVelocity_;
  }
  
  LinearVelocity_ = fwd;
  AngularVelocity_ = turn;
  PublishNewTarget_ = true;
}

void ControlProxy_Run()
{
  if (( PublishNewTarget_ == true ) &&
      ( ros::Time::now() >= StartTime_ )) {

    position_control::ControlVectorGoal goal;
    goal.MoveToPosition = MoveToPosition_;
    goal.X = TargetPoint_.X;
    goal.Y = TargetPoint_.Y;
  
    goal.MoveToAngle = MoveToAngle_;
    goal.TargetAngle = TargetAngle_;
  
    goal.AngularVelocity = AngularVelocity_;
    goal.LinearVelocity  = LinearVelocity_;
    ActClient_->sendGoal(goal);

    TargetPositionPub_.publish(TargetPoint_.convert());
    PublishNewTarget_ = false;
    TargetLoaded_ = true;
  }
}

/*
 *  Target Position Related Functions
 */
bool ControlProxy_reachedTarget()
{
  if ( PublishNewTarget_ == true ) {
    return false;
  }
  if ( TargetLoaded_ == false ) {
    return true;
  }
  actionlib::SimpleClientGoalState state = ActClient_->getState();

  if (( state == actionlib::SimpleClientGoalState::PENDING ) ||
      ( state == actionlib::SimpleClientGoalState::ACTIVE  )) {

    return false;
  }
  return true;
}

const Point2D_t & ControlProxy_getTarget()
{
  return TargetPoint_;
}

void ControlProxy_SetTargetAngle(float theta, float delay)
{
  if ( TargetAngle_ != theta ) {
    MoveToAngle_    = true;
    MoveToPosition_ = false;
    
    TargetAngle_ = theta;
    StartTime_ = ros::Time::now() + ros::Duration(delay);
    PublishNewTarget_ = true;
  }
}

void ControlProxy_SetTargetPosition(float x, float y, float delay)
{
  ControlProxy_SetTargetPosition (Point2D_t(x,y), delay);
}

void ControlProxy_SetTargetPosition(Point2D_t pt, float delay)
{
  if ( TargetPoint_ != pt ) {
    StartTime_ = ros::Time::now() + ros::Duration(delay);
    PublishNewTarget_ = true;
    TargetPoint_ = pt;
    
    MoveToAngle_    = false;
    MoveToPosition_ = true;    
  }
}

