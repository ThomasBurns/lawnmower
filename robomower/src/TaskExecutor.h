#ifndef TASK_EXECUTOR_H
#define TASK_EXECUTOR_H

#include <list>

/* ================================================== */
/* ================================================== */
class TaskExecutor
{
public:
  TaskExecutor();
  ~TaskExecutor();

  void start(float linearSpeed, float angularSpeed);
  void run();
  void clearAllSteps();
  bool systemRunning() const;
  unsigned int stepsRemaining() const;

  void AddStep_MoveToAbsPosition(float x, float y);
  void AddStep_MoveToAbsAngle(float theta);

private:
  // Not yet implemented
  void AddStep_MoveToRelativePosition(float x, float y);
  void AddStep_MoveToRelativeAngle(float theta);

  void triggerStep();

  typedef struct {
    bool abs;
    bool pos;
    float x, y;
  } AtomicStep_t;

  std::list<AtomicStep_t> Steps_;
  bool Active_;

};

#endif
