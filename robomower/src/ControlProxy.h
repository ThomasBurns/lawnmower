#ifndef CONTROL_PROXY_H
#define CONTROL_PROXY_H

#include <mowerlib/MapUtils.h>

void ControlProxy_Connect();
void ControlProxy_SetSpeedLimits(float fwd, float turn);

void ControlProxy_Run();

bool ControlProxy_reachedTarget();
const Point2D_t & ControlProxy_getTarget();

void ControlProxy_SetTargetAngle(float theta, float delay = 0.0);

void ControlProxy_SetTargetPosition(float x, float y, float delay = 0.0);
void ControlProxy_SetTargetPosition(Point2D_t pt, float delay = 0.0);



#endif
