
#ifndef CONTROL_MODE_H
#define CONTROL_MODE_H

#include <ros/ros.h>
#include <string>
#include <map>
#include <mowerlib/WayPoint.h>

#include <diagnostic_msgs/KeyValue.h>
#include "geometry_msgs/Twist.h"
#include "geometry_msgs/Point.h"

// Forward declarations
class TravelMap;
class GridStorage;

class ControlMode
{
public:
  typedef struct {
    const geometry_msgs::Twist & pos;
    const GridStorage & grid;
    const TravelMap & map;

  } ControlModeModules_t;

  ControlMode(const ControlModeModules_t & modules, std::string name);
  virtual ~ControlMode();

  const std::string ModeName_;
  void runMode();

  bool modeComplete() const;

  const WayPointList_t & getWaypointList() const;
  bool areThereObstaclesAround() const;


protected:
  virtual void run() = 0;
  void markModeComplete();

  const GridStorage & Grid_;
  const TravelMap & Map_;
  const geometry_msgs::Twist & Position_;
  WayPointList_t WaypointList;

private:
  void activeObstacleCallback(const diagnostic_msgs::KeyValue & msg);

  bool ModeCompleted;
  std::map<std::string, bool> ActiveObstacles_;
  ros::Subscriber KeyValueSub_;

};

#endif