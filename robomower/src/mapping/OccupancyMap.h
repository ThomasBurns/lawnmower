#ifndef OCCUPANCY_MAP_H
#define OCCUPANCY_MAP_H

#include <stdint.h>
#include <map>
#include <geometry_msgs/Twist.h>
#include <boost/shared_ptr.hpp>

#include "MapGrid.h"

class GridStorage;
class GridRef_t;
class WavefrontGrid;
typedef boost::shared_ptr<WavefrontGrid> WavefrontGrid_ptr;

class OccupancyMap
{
public:
  OccupancyMap(const GridStorage & storage, const geometry_msgs::Twist & position);

  /*******************
   * A Bound map means that is has
   * found all of the boundries
   * that defines its environment.
   * A complete map is bound AND knows
   * the value of all map pixels.
   */
  bool isMapBound() const;
  bool isMapComplete() const;

  /*******************
   * Introspection is a task that can be run less frequently.
   * It searches through the grids looking to find which pixels
   * are not reachable. It also determines whether the map
   * is bound or not.
   */
  void runIntrospection();
  void addModifiedGrid(GridRef_t grid);

private:
  bool checkIfUnknown(const GridPosition_t & gp);
  bool checkIfObstacle(const GridPosition_t & gp);
  bool checkIfClear(const GridPosition_t & gp);

  bool runWavefront();
  bool searchForMapEdge(GridPosition_t  * point);
  bool checkIfPatternMatches(const GridPosition_t * point);

  void resetWavefront();
  void resetWavefrontGrid(WavefrontGrid_ptr ptr);

  const GridStorage & Storage_;
  const geometry_msgs::Twist & Position_;
  bool RunBoundsCheck_;
  GridPosition_t EdgeStartPoint_;

  std::map<GridRef_t, WavefrontGrid_ptr> WavefrontGrids_;
  typedef std::map<GridRef_t, WavefrontGrid_ptr>::iterator WavefrontGrids_iterator;
  std::vector<GridPosition_t> WavefrontEdgeGrids_;

  bool MapEdgesBound_;
  bool MapCompleted_;

};

#endif
