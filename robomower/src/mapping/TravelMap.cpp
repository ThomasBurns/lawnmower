
#include "TravelMap.h"
#include "GridStorage.h"
#include "ros/ros.h"

#include <cmath>
#include <cstdio>

TravelMap::TravelMap(GridStorage * storage):
  Storage_(storage)
{
  ROS_ASSERT( storage != NULL);
}

/* ======================================================================= */
/* ======================================================================= */
void TravelMap::setBladeRadius(float radius, unsigned int resolution)
{
  ROS_ASSERT(radius > 0.0 );
  float pixelSize = 1.0 / (float) resolution;

  BladePoints_.push_back(Point2D_t( 0.0, 0.0));
  for ( float y = pixelSize; y < radius; y += pixelSize ) {
    // expand in the Y direction.
    float maxX = sqrt((radius*radius) - (y*y));

    BladePoints_.push_back(Point2D_t( 0.0,  y));
    BladePoints_.push_back(Point2D_t( 0.0, -y));

    for ( float x = pixelSize; x < maxX; x += pixelSize ) {

      BladePoints_.push_back(Point2D_t( x,  y));
      BladePoints_.push_back(Point2D_t( x, -y));
      BladePoints_.push_back(Point2D_t(-x,  y));
      BladePoints_.push_back(Point2D_t(-x, -y));
    }
  }
}

void TravelMap::markTravelled(geometry_msgs::Twist pt)
{
  MapGrid_ptr p = Storage_->getGrid(GridRef_t( pt ));
  // If the grid has been locked this will return null when the position is
  // out of bounds.
  if ( p != NULL ) {
    SubGrid_t s( pt );

    TravelData_t travel = p->readTravelLayerPixel(s);
    travel.Travelled_ = true;
    p->writeTravelLayerPixel(s, travel);
  }
}

void TravelMap::markMown(geometry_msgs::Twist pt)
{
  markMown(pt.linear.x, pt.linear.y);
}

void TravelMap::markMown(float x, float y)
{
  const GridStorage::Dimensions_t & d = Storage_->getDimensions();
  ROS_ASSERT( y <  d.maxHeight);
  ROS_ASSERT( y >= d.minHeight);
  ROS_ASSERT( x <  d.maxWidth);
  ROS_ASSERT( x >= d.minWidth);


  markGridAsMown(x, y);
  for ( unsigned int i = 0; i < BladePoints_.size(); i ++ ) {
    markGridAsMown(x + BladePoints_[i].X, y + BladePoints_[i].Y);
  }
}

/*****************************************************************************
 * Private Functions
 */
void TravelMap::markGridAsMown(float x, float y)
{
  MapGrid_ptr p = Storage_->getGrid(GridRef_t( x, y));
  // If the grid has been locked this will return null when the position is
  // out of bounds.
  if ( p != NULL ) {
    SubGrid_t s( x, y);

    TravelData_t travel = p->readTravelLayerPixel(s);
    travel.Mown_ = true;
    p->writeTravelLayerPixel(s, travel);
  }
}

/* ======================================================================= */
/* ======================================================================= */
