
#include "OccupancyFilter.h"

#include "GridStorage.h"

#include <vector>

#include <ros/ros.h>
#include <mowerlib/Conversion.h>
#include <mowerlib/Trigonometry.h>
#include <angles/angles.h>

static const float SQRT_2 = sqrt(2.0);

/* ======================================================== */
/* ======================================================== */
// Constructors & Operators
OccupancyFilter::OccupancyFilter(GridStorage  & storage):
  Storage_(storage), Listener_(NULL)
{
  ObstacleList_ = new Point2D_Vector();
  ClearList_    = new Point2D_Vector();
}

OccupancyFilter::~OccupancyFilter()
{
  delete ObstacleList_;
  delete ClearList_;
  if ( Listener_ != NULL) {
    delete Listener_;
  }
}

void OccupancyFilter::connect(bool use_laser)
{
  ros::NodeHandle node;
  if ( use_laser == true ) {
    FilteredSub_ = node.subscribe("/LaserScanner",    5, &OccupancyFilter::newLaserScan, this);
  } else {
    FilteredSub_ = node.subscribe("/filtered_scans", 50, &OccupancyFilter::newScan, this);
  }
}

/* ======================================================== */
// Public Functions.
void OccupancyFilter::getObstaclePoints(Point2D_Vector * list, const sensor_msgs::Range & scan, const geometry_msgs::Twist & position)
{
  ROS_ASSERT(list != NULL);
  Point2D_t pt;
  list->clear();

  float circumference = scan.range * 2.0 * M_PI;
  float arc = circumference * (scan.field_of_view / 360.0);

  float grid_size = 1.0 / (float)Storage_.getResolution();
  float number_of_points = (arc / (grid_size * SQRT_2)) * 2.0;
  float angle_increment = Trig_DegreesToRadians(scan.field_of_view / number_of_points);

  int count = floor(number_of_points / 2.0);

  pt.X = position.linear.x + (scan.range * cos(position.angular.z));
  pt.Y = position.linear.y + (scan.range * sin(position.angular.z));
  list->push_back(pt);

  float neg_angle = position.angular.z - angle_increment;
  float pos_angle = position.angular.z + angle_increment;

  for ( int i = 0; i < count; i ++ ) {

    pt.X = position.linear.x + (scan.range * cos(neg_angle));
    pt.Y = position.linear.y + (scan.range * sin(neg_angle));
    list->push_back(pt);

    pt.X = position.linear.x + (scan.range * cos(pos_angle));
    pt.Y = position.linear.y + (scan.range * sin(pos_angle));
    list->push_back(pt);

    neg_angle -= angle_increment;
    pos_angle += angle_increment;
  }
}

void OccupancyFilter::getClearPoints(Point2D_Vector * list, const sensor_msgs::Range & scan, const geometry_msgs::Twist & position)
{
  ROS_ASSERT(list != NULL);
  Point2D_t pt;

  if (( scan.range < scan.min_range ) ||
      ( scan.range > scan.max_range )) {
    // The range is outside of aceptable bounds.
    return;
  }
  list->clear();

  float grid_size = (1.0 / (float)Storage_.getResolution());
  float range    = scan.min_range;

  while ( range < scan.range ) {

    float circumference = range * 2.0 * M_PI;
    float arc = circumference * (scan.field_of_view / 360.0);

    float number_of_points = (arc / (grid_size * 2.0)) * 2.0;
    float angle_increment = Trig_DegreesToRadians(scan.field_of_view / number_of_points);

    int count = floor(number_of_points / 2.0);

    pt.X = position.linear.x + (range * cos(position.angular.z));
    pt.Y = position.linear.y + (range * sin(position.angular.z));
    list->push_back(pt);

    float neg_angle = position.angular.z - angle_increment;
    float pos_angle = position.angular.z + angle_increment;

    for ( int i = 0; i < count; i ++ ) {

      pt.X = position.linear.x + (range * cos(neg_angle));
      pt.Y = position.linear.y + (range * sin(neg_angle));
      list->push_back(pt);

      pt.X = position.linear.x + (range * cos(pos_angle));
      pt.Y = position.linear.y + (range * sin(pos_angle));
      list->push_back(pt);

      neg_angle -= angle_increment;
      pos_angle += angle_increment;
    }
    range += grid_size * 0.75;
  }
}

/* ======================================================== */
/* ======================================================== */
// Private Functions.
void OccupancyFilter::newScan(const object_filtering::ObjectScan & range)
{
  sensor_msgs::Range scan = range.scan;

  if ( scan.range < scan.max_range ) {

    ObstacleList_->clear();
    getObstaclePoints(ObstacleList_, scan, range.position);

    for ( size_t i = 0; i < ObstacleList_->size(); i ++ ) {
      markOccupied(ObstacleList_->at(i));
    }

  } else {
    scan.range = scan.max_range;
  }

  ClearList_->clear();
  getClearPoints(ClearList_, scan, range.position);

  for ( size_t i = 0; i < ClearList_->size(); i ++ ) {
    markClear(ClearList_->at(i));
  }
}

void OccupancyFilter::newLaserScan(const sensor_msgs::LaserScan & scan)
{
  if ( Listener_ == NULL ) {
    Listener_ = new  tf::TransformListener();
  }
  try {
    tf::StampedTransform location;
    Listener_->lookupTransform("/world", scan.header.frame_id, ros::Time(0), location);

    geometry_msgs::Twist t;

    t.linear.x = location.getOrigin().getX();
    t.linear.y = location.getOrigin().getY();
    t.linear.z = location.getOrigin().getZ();

    t.angular.x = 0.0;
    t.angular.y = 0.0;
    t.angular.z = tf::getYaw(location.getRotation());

    float increment =   1.0 / (float) Storage_.getResolution();
    ROS_ASSERT(increment > 0.0);
    ROS_ASSERT(increment < 1.0);
    float min_range = scan.range_min;
    float max_range = scan.range_max;
    Point2D_t pt;

    float angle = t.angular.z;

    for ( size_t i = 0; i < scan.ranges.size(); i ++ ) {
      float range = scan.ranges[i];
      float cos_x = std::cos(angle);
      float sin_y = std::sin(angle);

      if ( range < max_range ) {
        pt.X = (cos_x * range) + t.linear.x;
        pt.Y = (sin_y * range) + t.linear.y;
        markOccupied(pt);
      }
      for ( float r = min_range; r < range; r += increment ) {
        pt.X = (cos_x * r) + t.linear.x;
        pt.Y = (sin_y * r) + t.linear.y;
        markClear(pt);
      }
      angle += angles::from_degrees(scan.angle_increment);
    }
  }
  catch (tf::ConnectivityException ex) {
    ROS_ERROR("OFP,tf::ConnectivityException => %s",ex.what());
    return;
  }
  catch (tf::ExtrapolationException ex) {
//    ROS_ERROR("OFP, tf::ExtrapolationException => %s",ex.what());
    return;
  }
  catch (tf::InvalidArgument ex) {
    ROS_ERROR("OFP,tf::InvalidArgument => %s",ex.what());
    return;
  }
  catch (tf::LookupException ex) {
    ROS_ERROR("OFP,tf::LookupException => %s",ex.what());
    return;
  }
}

void OccupancyFilter::markOccupied(Point2D_t pt)
{
  MapGrid_ptr p = Storage_.getGrid(GridRef_t( pt.X, pt.Y ));
  if ( p != NULL ) {
    SubGrid_t s( pt.X, pt.Y );

    MapData_t pixel = p->readMapLayerPixel(s) ;
    pixel.Occupied_ = (pixel.Occupied_ << 1 ) | 1;
    pixel.Seen_     = (pixel.Seen_     << 1 ) | 1;
    p->writeMapLayerPixel(s, pixel);
  }
}

void OccupancyFilter::markClear(Point2D_t pt)
{
  MapGrid_ptr p = Storage_.getGrid(GridRef_t( pt.X, pt.Y ));
  if ( p != NULL ) {
    SubGrid_t s( pt.X, pt.Y );

    MapData_t pixel = p->readMapLayerPixel(s) ;
    pixel.Occupied_ = (pixel.Occupied_ << 1 ) | 0;
    pixel.Seen_     = (pixel.Seen_     << 1 ) | 1;
    p->writeMapLayerPixel(s, pixel);
  }
}

/* ======================================================== */
/* ======================================================== */
// Static Functions.
