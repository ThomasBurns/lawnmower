#ifndef GRID_STORAGE_H
#define GRID_STORAGE_H

#include <stdint.h>
#include <map>
#include <vector>
#include <boost/shared_ptr.hpp>
#include "MapGrid.h"

typedef boost::shared_ptr<const MapGrid> MapGrid_const;
typedef boost::shared_ptr<MapGrid> MapGrid_ptr;

//  This module is used to hold all the Map Grids used by the robot.
//  This module holds them, returns a shared pointer to a grid when requested.
//  Internally it uses a std::map to hold the data.
//

class GridStorage
{
public:
  GridStorage(int resolution); // resolution, i.e. how many subdivisiond per square meter.
  virtual ~GridStorage();

  typedef struct {
    // all dimensions are in meters.
    int16_t minHeight;
    int16_t maxHeight;
    int16_t minWidth;
    int16_t maxWidth;

  } Dimensions_t;

  int getResolution() const;
  float getPixelWidth() const; // in meters

  // Locked means the module will not create new map grids
  // when presented with coordinates that are not currently in the database.
  void lockGrid(bool en);
  bool isLocked() const;

  // Pre-assign the grids bound in the square as defined by Dimensions_t d
  void preallocateMemory(Dimensions_t d);
  Dimensions_t getDimensions() const;

  // Returns true when the dimensions have changed.
  bool haveDimensionsChanged();
  void clearDimensionsChangedFlag();

  // If not locked will create a new Map Grid if point does not exist.
  // Returns a NULL pointer if locked.
  MapGrid_ptr getGrid(GridRef_t ref);
  MapGrid_const getGrid(GridRef_t ref) const;
  bool doesGridExist(GridRef_t ref) const;

  int getNumberOfGrids() const;
  void markAllAsDirty();

  std::vector<GridRef_t> getAllGridRefs() const;
  std::vector<GridRef_t> getEdgeGridRef() const;
  int getNumGrids() const;


private:
  MapGrid_ptr create(GridRef_t ref);

  typedef std::map<GridRef_t, MapGrid_ptr> StorageMap;
  StorageMap GridMap_;
  const uint8_t Resolution_;
  Dimensions_t Size_;
  bool Locked_;
  bool DimensionsChanged_;

};

#endif
