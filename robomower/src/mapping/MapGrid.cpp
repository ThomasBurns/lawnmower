
#include "MapGrid.h"
#include <ros/ros.h>


static GridRef_List TravelDirtyList_;
static GridRef_List MapDirtyList_;

/* ======================================================== */
/* ======================================================== */
// Constructors & Operators

MapGrid::MapGrid(GridRef_t grid, int resolution):
  Grid_(grid), Resolution_(resolution),
  TravelDirty_(false), MapDirty_(false)
{
  ROS_ASSERT(resolution >= MIN_RESOLUTION && resolution <= MAX_RESOLUTION );

  ROS_DEBUG("Grid Created: %d %d", this->Grid_.width, this->Grid_.height);

  int num_grids = resolution*resolution;

  TravelledData_.reserve(num_grids+1);
  MapData_.reserve(num_grids+1);

  for ( int i = 0; i < num_grids; i ++ ) {

    TravelData_t t;
    TravelledData_.push_back(t.compress());

    MapData_t m;
    MapData_.push_back(m.compress());
  }
}

MapGrid::~MapGrid()
{
  ROS_DEBUG("Grid Destroyed: %d %d", this->Grid_.width, this->Grid_.height);
  this->clearMapDirty(); // just in case it's stored in the list
  this->clearTravelDirty(); // just in case it's stored in the list
}

const int MapGrid::MAX_RESOLUTION = 100;
const int MapGrid::MIN_RESOLUTION = 1;

/* ======================================================== */
// Public Functions.
GridRef_t MapGrid::getGridLocation() const
{
  return Grid_;
}
/*
MapGrid::SubGrid_t MapGrid::getSubGrid(double x, double y) const
{
  ROS_ASSERT(isnan(x) == false);
  ROS_ASSERT(isnan(y) == false);

  MapGrid::SubGrid_t s;
  GridRef_t g(x,y);

  double fractionX = (x * Resolution_) - (g.width * Resolution_);
  s.width = fractionX;

  double fractionY = (y * Resolution_) - (g.height * Resolution_);
  s.height = fractionY;

  ROS_ASSERT(s.width < Resolution_ );
  ROS_ASSERT(s.height < Resolution_ );
  return s;
}

MapGrid::SubGrid_t MapGrid::getSubGrid(const geometry_msgs::Point & pt) const
{
  return getSubGrid(pt.x, pt.y);
}

MapGrid::SubGrid_t MapGrid::getSubGrid(const geometry_msgs::Twist & pt) const
{
  return getSubGrid(pt.linear.x, pt.linear.y);
}
*/
/* ======================================================== */
TravelData_t MapGrid::readTravelLayerPixel(SubGrid_t pt) const
{
  return TravelData_t(TravelledData_[pt.calcOffset()]);
}

void MapGrid::writeTravelLayerPixel(SubGrid_t pt, TravelData_t data)
{
  int offset = pt.calcOffset();
  TravelDataSize_t b = data.compress();

  if ( b != TravelledData_[offset] ) {
    setTravelDirty();
    TravelledData_[offset] = b;
  }
}


bool MapGrid::isTravelDirty() const
{
  return TravelDirty_;
}

void MapGrid::clearTravelDirty()
{
  if ( TravelDirty_ != false ) {
    TravelDirty_ = false;
    TravelDirtyList_.remove(this->Grid_);
  }
}

void MapGrid::setTravelDirty()
{
  if ( TravelDirty_ != true ) {
    TravelDirty_ = true;
    TravelDirtyList_.push_front(this->Grid_);
  }
}

void MapGrid::readTravelledData(TravelledData_Vector * vec) const
{
  ROS_ASSERT(vec != NULL );
  *vec = TravelledData_;
}

void MapGrid::recordTravelledData() const
{
  char filename[1024];

  snprintf(filename, sizeof(filename), "/opt/mower/maps/Travel_%d_%d", Grid_.width, Grid_.height);

  if ( FILE *file = fopen(filename, "w") ) {
    fprintf(file, "Travel Grid: %d:%d\n", Grid_.width, Grid_.height);

    SubGrid_t pt;
    for ( pt.height = Resolution_-1; pt.height < Resolution_; pt.height-- ) {

      fprintf(file, "Height: %2d, ", pt.height);

      for ( pt.width = 0; pt.width < Resolution_; pt.width ++ ) {
        fprintf(file, "%2X, ", TravelledData_[pt.calcOffset()]);
      }
      fprintf(file, "\n");
    }

    fclose(file);
  } else {
    ROS_ERROR("Failed to open file: %s", filename);
  }

}

/* ======================================================== */
MapData_t MapGrid::readMapLayerPixel(SubGrid_t pt) const
{
  int offset = pt.calcOffset();
  return MapData_t(MapData_[offset]);
}

void MapGrid::writeMapLayerPixel(SubGrid_t pt, MapData_t data)
{
  int offset = pt.calcOffset();
  MapDataSize_t b = data.compress();

  if ( b != MapData_[offset] ) {
    setMapDirty();
    MapData_[offset] = b;
  }
}

MapDataStats_t MapGrid::getMapStats(void) const
{
  MapDataStats_t stats = { 0 };

  for ( unsigned int i = 0; i < MapData_.size(); i ++ ) {
    MapData_t m(MapData_[i]);

    if ( m.Unreachable_ ) {
      stats.Unreachable ++ ;
    } else if ( m.Seen_ == 0 ) {
      stats.Unknown ++;
    } else if ( m.Occupied_ != 0 ) {
      stats.Obstacles ++;
    }
  }
  return stats;
}

// Dirty means the map has been updated
bool MapGrid::isMapDirty() const
{
  return MapDirty_;
}

void MapGrid::clearMapDirty()
{
  if ( MapDirty_ != false ) {
    MapDirty_ = false;
    MapDirtyList_.remove(this->Grid_);
  }
}

void MapGrid::setMapDirty()
{
  if ( MapDirty_ != true ) {
    MapDirty_ = true;
    MapDirtyList_.push_back(this->Grid_);
  }
}

bool MapGrid::isUnknown(SubGrid_t pt) const
{
  MapData_t pixel = readMapLayerPixel(pt);
  return pixel.Seen_ == 0;
}

bool MapGrid::isObstacle(SubGrid_t pt) const
{
  MapData_t pixel = readMapLayerPixel(pt);

  if (( pixel.Occupied_ != 0 ) && ( pixel.Seen_ != 0 )) {
    return true;
  }
  return false;
}

bool MapGrid::isClear(SubGrid_t pt) const
{
  MapData_t pixel = readMapLayerPixel(pt);
  if (( pixel.Occupied_ == 0 ) && ( pixel.Seen_ != 0 )) {
    return true;
  }
  return false;
}

void MapGrid::readMapData(MapData_Vector * vec) const
{
  ROS_ASSERT(vec != NULL );
  *vec = MapData_;
}

void MapGrid::recordMapData() const
{
  char filename[1024];

  snprintf(filename, sizeof(filename), "/opt/mower/maps/Map_%d_%d", Grid_.width, Grid_.height);

  if ( FILE *file = fopen(filename, "w") ) {
    fprintf(file, "Occupancy Grid: %d:%d\n", Grid_.width, Grid_.height);

    SubGrid_t pt;
    fprintf(file, "Width :     ");
    for ( pt.width = 0; pt.width < Resolution_; pt.width ++ ) {

      fprintf(file, "%4d, ", pt.width);
    }
    fprintf(file, "\n");
    for ( pt.height = Resolution_-1; pt.height < Resolution_; pt.height-- ) {

      fprintf(file, "Height: %2d, ", pt.height);

      for ( pt.width = 0; pt.width < Resolution_; pt.width ++ ) {

        fprintf(file, "%4X, ", MapData_[pt.calcOffset()]);
      }
      fprintf(file, "\n");
    }

    fclose(file);
  } else {
    ROS_ERROR("Failed to open file: %s", filename);
  }
}

/* ======================================================== */
/* ======================================================== */
// Private Functions.
/* ======================================================== */
/* ======================================================== */
// Static Functions.
const GridRef_List & MapGrid::getTravelDirtyList()
{
  return TravelDirtyList_;
}

const GridRef_List & MapGrid::getMapDirtyList()
{
  return MapDirtyList_;
}

void MapGrid::clearDirtyList()
{
  MapDirtyList_.clear();
  TravelDirtyList_.clear();
}

