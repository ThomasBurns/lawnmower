#ifndef WAVEFRONTGRID_H
#define WAVEFRONTGRID_H

#include <stdint.h>
#include <ros/ros.h>
#include "MapGrid.h"

class WavefrontGrid
{
public:
  WavefrontGrid(uint8_t resolution);

  static const int32_t GRID_INVALID = -1;
  static const int32_t GRID_UNKNOWN = -2;

  typedef boost::shared_ptr<std::vector<int32_t> > WavefrontVector_ptr;

  int32_t getValue(SubGrid_t pt) const;
  void    setValue(SubGrid_t pt, int32_t value);
  void reset();

  bool isComplete() const;
  void setComplete(bool complete);

private:
  const uint8_t Resolution_;
  std::vector<int32_t> Vector_;
  bool Complete_;
};

#endif // WAVEFRONTGRID_H
