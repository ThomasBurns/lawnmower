#include "WaveFrontGrid.h"

WavefrontGrid::WavefrontGrid(uint8_t resolution):
  Resolution_(resolution), Complete_(false)
{
  for ( int i = 0; i < (Resolution_*Resolution_); i ++) {
    Vector_.push_back(0);
  }
}

int32_t WavefrontGrid::getValue(SubGrid_t pt) const
{
  return Vector_[pt.calcOffset()];
}

void WavefrontGrid::setValue(SubGrid_t pt, int32_t value)
{
  Vector_[pt.calcOffset()] = value;
}

void WavefrontGrid::reset()
{
  for ( int i = 0; i < (Resolution_*Resolution_); i ++) {
    Vector_[i] = 0;
  }
}

bool WavefrontGrid::isComplete() const
{
  return Complete_;
}

void WavefrontGrid::setComplete(bool complete)
{
  Complete_ = complete;
}
