
#include "OccupancyMap.h"
#include "GridStorage.h"
#include "MapGrid.h"
#include "WaveFrontGrid.h"

#include <ros/ros.h>
#include <mowerlib/Trigonometry.h>

#include <cstdio>
#include <cstring>
#include <sys/stat.h>
#include <fcntl.h>

/*
 *
 * is Map Bound Logic?
 * we need a starting point.
 * We take our position and start moving outwards till we start finding obstacles.
 * Once we do we begin the process of finding a matching pattern. Once we do that is out starting point.
 *  ** Patern Matching **
 * For best results we need three consecutive obstacles, in a row, not diagonal.
 * Then one side of this line should be unknown and the other half should be known and obstacle free.
 *
 *
 * The next step is to implement a wavefront algorithm. We creep along all obstacles adding a count field.
 * We reach the end when we run out of empty grids. Ideally there shouldn't be more than one grid with the
 * highest number, maybe two grids with this number. Any more and I think we might have a problem. Or at
 * least my assumption begin to fall apart.
 *
 * We then check these final pixels to ensure that they meet the inital pattern matching criteria.
 * If so we are done and the map is bound.
 *
 * Assumptions.
 * We are measuring obstacles and walls. This means that the actual edge of the map should be thin. Most
 * of it should only be a single grid thick. Since we are using ultrasonic sensors this might not work quite
 * so well and we get weird shapes sometimes. Regardless the long lines should be only a grid thick. One one
 * side it is known and on the other unknown.
 *
 * Long term there may be some value in adding a post processing stage which filters out the weird shapes.
 * Some sort of averaging, baysian algorithm that begins to determine the 'real' outline.
 * Either that or use a laser scanner/lidar for mapping and ultrasonic for large scale collision avoidance.
 *
 */

OccupancyMap::OccupancyMap(const GridStorage & storage, const geometry_msgs::Twist & position):
  Storage_(storage), Position_(position), RunBoundsCheck_(false),
  MapEdgesBound_(false), MapCompleted_(false)
{
}

bool OccupancyMap::isMapBound() const
{
  return MapEdgesBound_;
}

bool OccupancyMap::isMapComplete() const
{
  return MapCompleted_;
}

void OccupancyMap::runIntrospection()
{
  if ( isMapBound() == false ) {
    if ( RunBoundsCheck_ == true ) {

      if ( runWavefront() ) {
        MapEdgesBound_ = true;
      }

      RunBoundsCheck_ = false;
    }
    return;
  }
}


void OccupancyMap::addModifiedGrid(GridRef_t grid)
{
  int16_t resolution = Storage_.getResolution();
  if (WavefrontGrids_.find(grid) == WavefrontGrids_.end()) {
    WavefrontGrid_ptr new_grid( new WavefrontGrid(resolution));
    WavefrontGrids_[grid] = new_grid;
  }

  WavefrontGrid_ptr wavePtr = WavefrontGrids_[grid];
  MapGrid_const mapPtr = Storage_.getGrid(grid);
  ROS_ASSERT(wavePtr != NULL);
  ROS_ASSERT(mapPtr  != NULL);

  bool complete = true;
  SubGrid_t pt;
  for ( pt.width = 0; pt.width < resolution; pt.width++ ) {
    for ( pt.height = 0; pt.height < resolution; pt.height++ ) {

      int32_t value;

      if ( mapPtr->isUnknown(pt) == true ) {
        value = WavefrontGrid::GRID_UNKNOWN;
        complete = false;
      } else if ( mapPtr->isObstacle(pt) == false ) {
        value = WavefrontGrid::GRID_INVALID;
      } else {
        value = 0;
      }
      wavePtr->setValue(pt, value);
    }
  }
  wavePtr->setComplete(complete);

  // if the grid is one of the edge grids,
  // then flag it so we can run the is map bound algorithm!
  std::vector<GridRef_t> vec = Storage_.getEdgeGridRef();
  for ( size_t i = 0; i < vec.size(); i ++ ) {
    if ( vec[i] == grid ) {
      RunBoundsCheck_ = true;
      break;
    }
  }
}

bool OccupancyMap::checkIfUnknown(const GridPosition_t & gp)
{
  MapGrid_const mapPtr = Storage_.getGrid(gp.getGrid());
  if ( mapPtr == NULL ) {
    return false;
  }
  SubGrid_t s = gp.getSub();
  return mapPtr->isUnknown(s);
}

bool OccupancyMap::checkIfObstacle(const GridPosition_t & gp)
{
  MapGrid_const mapPtr = Storage_.getGrid(gp.getGrid());
  if ( mapPtr == NULL ) {
    return false;
  }
  SubGrid_t s = gp.getSub();
  return mapPtr->isObstacle(s);
}

bool OccupancyMap::checkIfClear(const GridPosition_t & gp)
{
  MapGrid_const mapPtr = Storage_.getGrid(gp.getGrid());
  if ( mapPtr == NULL ) {
    return false;
  }
  SubGrid_t s = gp.getSub();
  return mapPtr->isClear(s);
}


#define NUM_BORDER_GRIDS  8
const int8_t BorderGridsW_[NUM_BORDER_GRIDS] = {
   0, 0, 1, 1, 1,-1,-1,-1
};
const int8_t BorderGridsH_[NUM_BORDER_GRIDS] = {
   1,-1, 1, 0,-1, 1, 0,-1
};

bool OccupancyMap::runWavefront()
{
//  ROS_ERROR("runWavefont()");

  if ( checkIfPatternMatches(&EdgeStartPoint_) == false ) {
//      ROS_ERROR("Map Edge - searching");
    if ( searchForMapEdge(&EdgeStartPoint_) == false ) {
//      ROS_ERROR("runWavefont() - no start point");
      return false;
    }
  }
//  ROS_ERROR("Map Edge: %d %d: %d %d",
//            EdgeStartPoint_.getGrid().width, EdgeStartPoint_.getGrid().height,
//            EdgeStartPoint_.getSub().width,  EdgeStartPoint_.getSub().height);

  resetWavefront();

  int32_t waveNumber = 0, numGrids = 0;
  std::vector<GridPosition_t> edgeGrids, nextEdges, unknownPoints;
  edgeGrids.push_back(EdgeStartPoint_);

  WavefrontGrid_ptr wavePtr = WavefrontGrids_[EdgeStartPoint_.getGrid()];
  ROS_ASSERT(wavePtr != NULL);
  wavePtr->setValue(EdgeStartPoint_.getSub(), 1);


  while ( edgeGrids.size() != 0 ) {
    waveNumber++;
    nextEdges.clear();
    for ( size_t edge = 0; edge < edgeGrids.size(); edge ++ ) {

      bool nonobstacleGrid = false;
      bool endGrid = true;
      for ( int i = 0; i < NUM_BORDER_GRIDS; i ++ ) {

        GridPosition_t gp = edgeGrids[edge];
        gp.adjustSub(BorderGridsW_[i], BorderGridsH_[i]);

        // this makes sure we don't accidently create a new grid by mistake.
        if (WavefrontGrids_.find(gp.getGrid()) == WavefrontGrids_.end()) {
          continue;
        }

        wavePtr = WavefrontGrids_[gp.getGrid()];
        ROS_ASSERT(wavePtr != NULL);

        MapGrid_const mapPtr = Storage_.getGrid(gp.getGrid());
        ROS_ASSERT(mapPtr != NULL);

        SubGrid_t s = gp.getSub();
        if (( mapPtr->isObstacle(s)) &&
            ( wavePtr->getValue(s) == 0 )) {

          wavePtr->setValue(s, waveNumber);
          nextEdges.push_back(gp);
          numGrids++;
          endGrid = false;
        }
        if ( mapPtr->isClear(s)) {
          nonobstacleGrid = true;
        }
      }
      if (( endGrid == true ) && (nonobstacleGrid == true)) {
        unknownPoints.push_back(edgeGrids[edge]);
      }
    }
    edgeGrids = nextEdges;
  }

  WavefrontEdgeGrids_ = unknownPoints;
//  ROS_ERROR("unknownPoints.size() %02u", (uint32_t)WavefrontEdgeGrids_.size());
//  ROS_ERROR("runWavefont() - %d (%d)", waveNumber, numGrids);
//  ROS_ERROR(" ");
  return false;
}

/* This algorithm extraporlates the robots position until is finds an
 * acceptable starting point.
 */
bool OccupancyMap::searchForMapEdge(GridPosition_t * point)
{
  ROS_ASSERT(point != NULL);

  float res = 1.0 / SubGrid_t::getResolution();

  float angles[4];
  angles[0] = Position_.angular.z;
  angles[1] = Trig_LimitRadians(Position_.angular.z + Trig_DegreesToRadians(90));
  angles[2] = Trig_LimitRadians(Position_.angular.z - Trig_DegreesToRadians(90));
  angles[3] = -Position_.angular.z;

  for ( int i = 0; i < 4; i ++ ) {
    geometry_msgs::Twist pos = Position_;
    *point = GridPosition_t(pos);
    while ( Storage_.doesGridExist(point->getGrid()) ) {
      MapGrid_const mapPtr = Storage_.getGrid(point->getGrid());
      ROS_ASSERT(mapPtr != NULL);

      pos.linear.x += res * std::cos(angles[i]);
      pos.linear.y += res * std::sin(angles[i]);

      if ( checkIfPatternMatches(point) == true ) {
        return true;
      }
      *point = GridPosition_t(pos);
    }
  }
  return false;
}

/*  checkIfPatternMatches()
 *  We are looking for a series of three consecutive obstacle grids with
 *  one side being clear.
 *  These girds have to be in a straight, non diagonal line.
 *  Diagonal lines allow the edge of a ultrasonic sensor to be used which
 *  causes problems at times.
 */
bool OccupancyMap::checkIfPatternMatches(const GridPosition_t * point)
{
  ROS_ASSERT(point != NULL);

  if ( checkIfObstacle(*point) == false) {
    return false;
  }

  GridPosition_t top    = *point;
  top.adjustSub( 0,  1);
  GridPosition_t bottom = *point;
  bottom.adjustSub( 0, -1);
  GridPosition_t left   = *point;
  left.adjustSub(-1,  0);
  GridPosition_t right  = *point;
  right.adjustSub( 1,  0);

  GridPosition_t top_left    = *point;
  GridPosition_t bottom_left = *point;
  GridPosition_t top_right   = *point;
  GridPosition_t bottom_right  = *point;

  if (( checkIfObstacle(top)    == true ) &&
      ( checkIfObstacle(bottom) == true ) &&
      ( checkIfObstacle(left)   == false ) && \
      ( checkIfObstacle(right)  == false )) {

    top_left.adjustSub( -1,  1);
    bottom_left.adjustSub( -1, -1);
    top_right.adjustSub(1,  1);
    bottom_right.adjustSub( 1,  -1);

    // we are looking at a vertical line.
    if (( checkIfClear(top_right)      == true ) &&
        ( checkIfClear(right)          == true ) &&
        ( checkIfClear(bottom_right)   == true )) {
      return true;
    }

    if (( checkIfClear(top_left)        == true ) &&
        ( checkIfClear(left)            == true ) &&
        ( checkIfClear(bottom_left)     == true )) {
      return true;
    }
  } else
  if (( checkIfObstacle(top)    == false ) &&
      ( checkIfObstacle(bottom) == false ) &&
      ( checkIfObstacle(left)   == true  ) &&
      ( checkIfObstacle(right)  == true  )) {

    top_left.adjustSub( -1,  1);
    bottom_left.adjustSub( -1, -1);
    top_right.adjustSub(1,  1);
    bottom_right.adjustSub( 1,  -1);

    // we are looking at a horizontal line.

    if (( checkIfClear(bottom_left)  == true ) &&
        ( checkIfClear(bottom)       == true ) &&
        ( checkIfClear(bottom_right) == true )) {
      return true;
    }
    if (( checkIfClear(top_left)  == true ) &&
        ( checkIfClear(top)       == true ) &&
        ( checkIfClear(top_right) == true )) {
      return true;
    }
  }
  return false;
}

void OccupancyMap::resetWavefront()
{
  for( WavefrontGrids_iterator iterator = WavefrontGrids_.begin();
       iterator != WavefrontGrids_.end();
       iterator++) {

    ROS_ASSERT(iterator->second != NULL);
    resetWavefrontGrid(iterator->second);
  }
}

void OccupancyMap::resetWavefrontGrid(WavefrontGrid_ptr ptr)
{
  SubGrid_t pt;

  int16_t res = Storage_.getResolution();
  for ( pt.width = 0; pt.width < res; pt.width ++ ) {
    for ( pt.height = 0; pt.height < res; pt.height ++ ) {
      if ( ptr->getValue(pt) > 0 ) {
        ptr->setValue( pt, 0);
      }
    }
  }
}
