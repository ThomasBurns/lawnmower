#ifndef MAP_GRID_H
#define MAP_GRID_H

#include <stdint.h>
#include <list>
#include <vector>

#include <mowerlib/MapUtils.h>
#include <mowerlib/MapPixel.h>

/* Map Grid.
 * This represent 1 square meter of space.
 * It is divided up into resolution by resolution sub grid.
 * There are many layers to the map (where have we been, obstacle grids, terrain types, etc).
 * This module is designed to be expanded and new layers added as required.
 *
 * To save space these layers are compressed in memory when they are not needed. This is because
 * a map may cover tens or even of hundreds of square meters.
 *
 * Each grid is referenced by its relative location (GridRef_t).
 * When a grid is updated the dirty flag is set. This must be manually cleared.
 *
 */

typedef std::vector<TravelDataSize_t> TravelledData_Vector;
typedef std::vector<MapDataSize_t>    MapData_Vector;

class MapGrid
{
public:
  MapGrid(GridRef_t grid, int resolution);
  virtual ~MapGrid();

  static const int MAX_RESOLUTION;
  static const int MIN_RESOLUTION;

  GridRef_t getGridLocation() const;
/*
  // This is used to determine which sub grid we want to access.
  typedef struct  {
    uint8_t width;
    uint8_t height;

  } SubGrid_t;

  SubGrid_t getSubGrid(double x, double y) const ;
  SubGrid_t getSubGrid(const geometry_msgs::Point & pt) const;
  SubGrid_t getSubGrid(const geometry_msgs::Twist & pt) const;
*/
  /* ======================================================== */
  // Travelled Functions.
  // I.e. Have we been here before on the map.
  // Future idea: a boolean function to return true when the whole grid has been visited.

  TravelData_t readTravelLayerPixel(SubGrid_t pt) const;
  void writeTravelLayerPixel(SubGrid_t pt, TravelData_t data);

  // Dirty means the map has been updated
  bool isTravelDirty() const;
  void clearTravelDirty();
  void setTravelDirty();

  void readTravelledData(TravelledData_Vector * vec) const;
  void recordTravelledData() const;


  /* ======================================================== */
  // Map Functions.
  // I.e. A description of the world around us.
  // Future idea: a boolean function to return true when the whole grid has been processed.

  MapData_t readMapLayerPixel(SubGrid_t pt) const;
  void writeMapLayerPixel(SubGrid_t pt, MapData_t data);

  MapDataStats_t getMapStats(void) const;

  // Dirty means the map has been updated
  bool isMapDirty() const;
  void clearMapDirty();
  void setMapDirty();

  bool isUnknown(SubGrid_t pt) const;
  bool isObstacle(SubGrid_t pt) const;
  bool isClear(SubGrid_t pt) const;


  void readMapData      (MapData_Vector * vec) const;
  void recordMapData() const;

  /* ======================================================== */
  // Static Functions.
  static const GridRef_List & getTravelDirtyList();
  static const GridRef_List & getMapDirtyList();
  static void  clearDirtyList();

private:

  // --------------------------
  // Private methods.
  unsigned int calcOffset(SubGrid_t pt) const;

  // --------------------------
  // Private Variables.
  GridRef_t Grid_;
  uint8_t Resolution_;
  bool TravelDirty_;
  bool MapDirty_;

  std::vector<TravelDataSize_t> TravelledData_;
  std::vector<MapDataSize_t>    MapData_;
};

#endif
