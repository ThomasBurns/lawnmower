#ifndef OCCUPANCY_FILTER_H
#define OCCUPANCY_FILTER_H

#include <string>

#include <ros/ros.h>
#include <object_filtering/ObjectScan.h>
#include <mowerlib/MapUtils.h>
#include <sensor_msgs/LaserScan.h>
#include <tf/transform_listener.h>

class GridStorage;

class OccupancyFilter
{
public:
  OccupancyFilter(GridStorage  & storage);
  virtual ~OccupancyFilter();

  void connect(bool use_laser);

  void getObstaclePoints(Point2D_Vector * list, const sensor_msgs::Range & scan, const geometry_msgs::Twist & position);
  void getClearPoints(Point2D_Vector * list, const sensor_msgs::Range & scan, const geometry_msgs::Twist & position);

private:
  void newScan(const object_filtering::ObjectScan & range);
  void newLaserScan(const sensor_msgs::LaserScan & scan);

  void markOccupied(Point2D_t pt);
  void markClear(Point2D_t pt);

  ros::Subscriber FilteredSub_;

  /******************************
   * The Vectors are dynamic for effeciency reasons.
   * By preallocating them I can reuse them.
   * Sine the newScan function runs often this will about
   * countless calls to new/delete. The memory footprint
   * will grow until it is large enough, then it will stabilise.
   */
  Point2D_Vector * ObstacleList_;
  Point2D_Vector * ClearList_;
  GridStorage  & Storage_;
  tf::TransformListener * Listener_;
};

#endif
