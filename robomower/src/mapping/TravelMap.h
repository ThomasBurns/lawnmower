#ifndef SYSTEM_MAP_H
#define SYSTEM_MAP_H

#include <stdint.h>
#include <vector>
#include <geometry_msgs/Twist.h>

#include "MapGrid.h"

class GridStorage;

class TravelMap
{
public:
  TravelMap(GridStorage * storage);

  void setBladeRadius(float radius, unsigned int resolution);
  
  void markTravelled(geometry_msgs::Twist pt);

  void markMown(geometry_msgs::Twist pt);
  void markMown(float x, float y);
  
private:
  void markGridAsMown(float x, float y);
  
  GridStorage * Storage_;
  std::vector<Point2D_t> BladePoints_;
};

#endif
