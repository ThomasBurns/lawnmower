
#include <ros/ros.h>

#include "GridStorage.h"

/* ======================================================== */
/* ======================================================== */
// Constructors & Operators
GridStorage::GridStorage(int resolution):
  Resolution_(resolution), Locked_(false),
  DimensionsChanged_(false)
{
  Size_.minHeight = 0;
  Size_.maxHeight = 0;
  Size_.maxWidth = 0;
  Size_.minWidth = 0;
}

GridStorage::~GridStorage()
{
  GridMap_.clear();
}

/* ======================================================== */
// Public Functions.
int GridStorage::getResolution() const
{
  return Resolution_;
}

int GridStorage::getNumberOfGrids() const
{
  return GridMap_.size();
}

void GridStorage::lockGrid(bool en)
{
  Locked_ = en;
}

bool GridStorage::isLocked() const
{
  return Locked_;
}

MapGrid_ptr GridStorage::getGrid(GridRef_t ref)
{
  if (GridMap_.find(ref) == GridMap_.end()) {
    return  create(ref);
  }
  return GridMap_.at(ref);
}

MapGrid_const GridStorage::getGrid(GridRef_t ref) const
{
  if (GridMap_.find(ref) == GridMap_.end()) {
    MapGrid_const p;
    return p;
  }
  return GridMap_.at(ref);
}

bool GridStorage::doesGridExist(GridRef_t ref) const
{
  return (GridMap_.find(ref) != GridMap_.end());
}

void GridStorage::preallocateMemory(Dimensions_t d)
{
  for ( int x = d.minWidth; x < d.maxWidth; x ++ ) {
    for ( int y = d.minHeight; y < d.maxHeight; y ++ ) {
      getGrid(GridRef_t(x,y));
    }
  }
  markAllAsDirty();
}

GridStorage::Dimensions_t GridStorage::getDimensions() const
{
  return Size_;
}

float GridStorage::getPixelWidth() const
{
  return 1.0 / (float)Resolution_;
}

bool GridStorage::haveDimensionsChanged()
{
  return DimensionsChanged_;
}

void GridStorage::clearDimensionsChangedFlag()
{
  DimensionsChanged_ = false;
}

void GridStorage::markAllAsDirty()
{
  for( StorageMap::iterator iterator = GridMap_.begin(); iterator != GridMap_.end(); iterator++) {
    iterator->second->setMapDirty();
    iterator->second->setTravelDirty();
  }
}

std::vector<GridRef_t> GridStorage::getAllGridRefs() const
{
  std::vector<GridRef_t> vec;
  for( StorageMap::const_iterator iterator = GridMap_.begin(); iterator != GridMap_.end(); iterator++) {
    vec.push_back(iterator->first);
  }
  return vec;
}

static const GridRef_t CheckIfEdge[8] = {
  GridRef_t(  1, -1),
  GridRef_t(  1,  0),
  GridRef_t(  1,  1),
  GridRef_t(  0,  1),
  GridRef_t(  0, -1),
  GridRef_t( -1, -1),
  GridRef_t( -1,  0),
  GridRef_t( -1,  1)
};

std::vector<GridRef_t> GridStorage::getEdgeGridRef() const
{
  std::vector<GridRef_t> vec;
  for( StorageMap::const_iterator iterator = GridMap_.begin(); iterator != GridMap_.end(); iterator++) {

    for ( size_t i = 0; i < 8; i ++ ) {
      GridRef_t g = iterator->first + CheckIfEdge[i];
      if ( doesGridExist(g) == false ) {
        vec.push_back(iterator->first);
        break;
      }
    }
  }
  return vec;
}

int GridStorage::getNumGrids() const
{
  return GridMap_.size();
}


/* ======================================================== */
/* ======================================================== */
// Private Functions.
MapGrid_ptr GridStorage::create(GridRef_t ref)
{
  if ( Locked_ == true ) {
    MapGrid_ptr empty_ptr;
    return empty_ptr;
  }
  if ( GridMap_.size() == 0 ) {
    Size_.minWidth  = ref.width;
    Size_.maxWidth  = ref.width;
    Size_.minHeight = ref.height;
    Size_.maxHeight = ref.height;
    DimensionsChanged_ = true;

  } else {
    if ( ref.height < Size_.minHeight ) {
      Size_.minHeight = ref.height;
      DimensionsChanged_ = true;
    }
    if ( ref.height >= Size_.maxHeight ) {
      Size_.maxHeight = ref.height+1;
      DimensionsChanged_ = true;
    }
    if ( ref.width < Size_.minWidth ) {
      Size_.minWidth = ref.width;
      DimensionsChanged_ = true;
    }
    if ( ref.width >= Size_.maxWidth ) {
      Size_.maxWidth = ref.width+1;
      DimensionsChanged_ = true;
    }
  }
  MapGrid_ptr p( new MapGrid(ref, Resolution_));
  GridMap_[ref] = p;
  return p;
}

/* ======================================================== */
/* ======================================================== */
// Static Functions.
