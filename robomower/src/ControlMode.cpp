
#include <ros/ros.h>

#include "ControlMode.h"
#include "TravelMap.h"

ControlMode::ControlMode(const ControlMode::ControlModeModules_t & modules, std::string name):
  ModeName_(name), Grid_(modules.grid), Map_(modules.map), Position_(modules.pos),
  ModeCompleted(false)
{
  ros::NodeHandle node;
  KeyValueSub_ = node.subscribe("/object_detected", 10, &ControlMode::activeObstacleCallback, this);
}

ControlMode::~ControlMode()
{
  WaypointList.clear();
}

void ControlMode::runMode()
{
  if ( ModeCompleted == false ) {
    run();
  }
}

const WayPointList_t & ControlMode::getWaypointList() const
{
  return WaypointList;
}

bool ControlMode::modeComplete() const
{
  return ModeCompleted;
}

void ControlMode::markModeComplete()
{
  ModeCompleted = true;
}

bool ControlMode::areThereObstaclesAround() const
{
  for( std::map<std::string, bool>::const_iterator iterator = ActiveObstacles_.begin();
       iterator != ActiveObstacles_.end();
       iterator++) {

    if ( iterator->second ==  true ) {
      return true;
    }
  }
  return false;
}

void ControlMode::activeObstacleCallback(const diagnostic_msgs::KeyValue & msg)
{
  ActiveObstacles_[msg.key] = msg.value.compare("true") == 0;
}