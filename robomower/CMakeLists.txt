# %Tag(FULLTEXT)%
cmake_minimum_required(VERSION 2.8.3)
project(robomower)

find_package(catkin REQUIRED COMPONENTS
  roscpp
  rospy
  tf
  std_msgs
  genmsg
  mowerlib
)

catkin_package(
  CATKIN_DEPENDS message_runtime
)

include_directories(
  src/
  src/mapping
  src/modes
  ${catkin_INCLUDE_DIRS}
  ${mowerlib_INCLUDE_DIRS}
  ${roscpp_INCLUDE_DIRS}
)
set(CMAKE_CXX_FLAGS "-Wall")

catkin_add_gtest(${PROJECT_NAME}_test
    test/utest.cpp
    src/mapping/GridStorage.cpp
    src/mapping/MapGrid.cpp
    src/mapping/OccupancyMap.cpp
    src/mapping/TravelMap.cpp
    src/mapping/WaveFrontGrid.cpp
    test/test_GridStorage.cpp
    test/test_MapGrid.cpp
    test/test_SystemMap.cpp
)
target_link_libraries(${PROJECT_NAME}_test ${catkin_LIBRARIES} ${mowerlib_LIBRARIES})

add_executable(robomower
    src/modes/CM_ManualMode.cpp
    src/modes/CM_MowingMode.cpp
    src/modes/CM_MappingMode.cpp
    src/mapping/GridStorage.cpp
    src/mapping/MapGrid.cpp
    src/mapping/OccupancyFilter.cpp
    src/mapping/OccupancyMap.cpp
    src/mapping/TravelMap.cpp
    src/mapping/WaveFrontGrid.cpp
    src/ControlMode.cpp
    src/ControlProxy.cpp
    src/TaskExecutor.cpp
    src/lawn_mower.cpp
)
target_link_libraries(robomower ${catkin_LIBRARIES} ${mowerlib_LIBRARIES})
add_dependencies(robomower
  mowerlib_generate_messages
  position_control_generate_messages
  ${position_control_EXPORTED_TARGETS}
)


# %EndTag(FULLTEXT)%
