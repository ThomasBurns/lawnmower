#include <gtest/gtest.h>
#include "../src/mapping/OccupancyFilter.h"
#include "../src/mapping/GridStorage.h"
#include "../src/mapping/MapGrid.h"

#include <mowerlib/Trigonometry.h>
#include <vector>

namespace {
  class test_OccupancyFilter: public ::testing::Test {

  protected:
    GridStorage *Storage_;
    OccupancyFilter *Filter_;

    test_OccupancyFilter() {
    }

    virtual ~test_OccupancyFilter() {
    }

    virtual void SetUp() {
      Storage_ = new GridStorage(100);

      OccupancySettings_t settings;
      settings.storage = Storage_;
      settings.occMap  = NULL;
      settings.maxRangeLimit = 1.0;
      settings.maxLinearSpeed = 1.0;
      settings.maxAngularSpeed = 1.0;
      settings.observationDistance = 1.0;
      settings.observationAngle = Trig_DegreesToRadians(45);
      settings.maximumSampleTime = 0.1;
      settings.resolution = 100;

      Filter_ = new OccupancyFilter(settings);
      Filter_->setVelocity(0.0, 0.0);
    }

    virtual void TearDown() {
      MapGrid::clearDirtyList();
      delete Filter_;
      delete Storage_;
    }
  };

  
  
  int countDuplicates(Point2D_Vector * list)
  {
    ROS_ASSERT(list != NULL);
    int count = 0;
    
    for ( unsigned int i = 0; i < list->size(); i ++ ) {
      
      for ( unsigned int j = (i+1); j < list->size(); j ++ ) {
        if ( list->at(i) == list->at(j) ) {
          count++;
        } 
      }
    }   
    return count;
  }

  int countDuplicatesInClearPoints(Point2D_Vector * clearList, const Point2D_Vector & obstacleList )
  {
    ROS_ASSERT(clearList != NULL);
    int count = 0;  
    
    for ( unsigned int i = 0; i < clearList->size(); i ++ ) {
      
      for ( unsigned int j = 0; j < obstacleList.size(); j ++ ) {
        if ( clearList->at(i) == obstacleList.at(j) ) {
          count++;
        } 
      }
    }   
    return count;
  }    
  
  /* =================================================== */
  TEST_F(test_OccupancyFilter, isRobotStable_true)
  {
    Filter_->setVelocity(0.0, 0.0);
    ASSERT_EQ(true, Filter_->isRobotStable() );
  }

  TEST_F(test_OccupancyFilter, isRobotStable_fast_linear)
  {
    Filter_->setVelocity(1.1, 0.0);
    ASSERT_EQ(false, Filter_->isRobotStable() );
  }

  TEST_F(test_OccupancyFilter, isRobotStable_fast_angle)
  {
    Filter_->setVelocity(0.0, 1.1);
    ASSERT_EQ(false, Filter_->isRobotStable() );
  }

  TEST_F(test_OccupancyFilter, isRobotStable_neg_angle)
  {
    Filter_->setVelocity(0.0, -1.1);
    ASSERT_EQ(false, Filter_->isRobotStable());
  }

  TEST_F(test_OccupancyFilter, isRobotStable_neg_lin)
  {
    Filter_->setVelocity(-1.1, 0.0);
    ASSERT_EQ(false, Filter_->isRobotStable() );
  }

  /* =================================================== */
  TEST_F(test_OccupancyFilter, hasRobotMoved__same_position)
  {
    sensor_msgs::Range scan;
    scan.radiation_type = scan.ULTRASOUND;
    scan.field_of_view = 90;
    scan.min_range = 0.2;
    scan.max_range = 1.0;
    scan.range = 1.0;

    geometry_msgs::Twist position;
    position.linear.x = 0.0;
    position.linear.y = 0.0;
    position.linear.z = 0.0;
    position.angular.x = 0.0;
    position.angular.y = 0.0;
    position.angular.z = 0.0;

    Filter_->newScan("N/A", scan, position);
    Filter_->newScan("N/A", scan, position);
    ASSERT_EQ(false, Filter_->hasRobotMoved(position) );

  }

  TEST_F(test_OccupancyFilter, hasRobotMoved_short_distance)
  {
    sensor_msgs::Range scan;
    scan.radiation_type = scan.ULTRASOUND;
    scan.field_of_view = 90;
    scan.min_range = 0.2;
    scan.max_range = 1.0;
    scan.range = 1.0;

    geometry_msgs::Twist position;
    position.linear.x = 0.0;
    position.linear.y = 0.0;
    position.linear.z = 0.0;
    position.angular.x = 0.0;
    position.angular.y = 0.0;
    position.angular.z = 0.0;

    Filter_->newScan("N/A", scan, position);
    position.linear.y = 0.5;
    Filter_->newScan("N/A", scan, position);
    ASSERT_EQ(false, Filter_->hasRobotMoved(position) );
  }

  TEST_F(test_OccupancyFilter, hasRobotMoved_just_moved)
  {
    sensor_msgs::Range scan;
    scan.radiation_type = scan.ULTRASOUND;
    scan.field_of_view = 90;
    scan.min_range = 0.2;
    scan.max_range = 1.0;
    scan.range = 1.0;

    geometry_msgs::Twist position;
    position.linear.x = 0.0;
    position.linear.y = 0.0;
    position.linear.z = 0.0;
    position.angular.x = 0.0;
    position.angular.y = 0.0;
    position.angular.z = 0.0;

    ASSERT_EQ(true, Filter_->isRobotStable() );
    Filter_->newScan("N/A", scan, position);
    position.linear.y = 1.5;
    Filter_->newScan("N/A", scan, position);
    position.linear.y = 1.0;
    ASSERT_EQ(false, Filter_->hasRobotMoved(position) );
  }

  TEST_F(test_OccupancyFilter, hasRobotMoved_small_angle)
  {
    sensor_msgs::Range scan;
    scan.radiation_type = scan.ULTRASOUND;
    scan.field_of_view = 90;
    scan.min_range = 0.2;
    scan.max_range = 1.0;
    scan.range = 1.0;

    geometry_msgs::Twist position;
    position.linear.x = 0.0;
    position.linear.y = 0.0;
    position.linear.z = 0.0;
    position.angular.x = 0.0;
    position.angular.y = 0.0;
    position.angular.z = 0.0;

    ASSERT_EQ(true, Filter_->isRobotStable() );
    
    Filter_->newScan("N/A", scan, position);
    position.angular.z = 0.5;
    Filter_->newScan("N/A", scan, position);
    ASSERT_EQ(false, Filter_->hasRobotMoved(position) );
  }

  TEST_F(test_OccupancyFilter, hasRobotMoved_large_angle)
  {
    sensor_msgs::Range scan;
    scan.radiation_type = scan.ULTRASOUND;
    scan.field_of_view = 90;
    scan.min_range = 0.2;
    scan.max_range = 1.0;
    scan.range = 1.0;

    geometry_msgs::Twist position;
    position.linear.x = 0.0;
    position.linear.y = 0.0;
    position.linear.z = 0.0;
    position.angular.x = 0.0;
    position.angular.y = 0.0;
    position.angular.z = 0.0;

    Filter_->newScan("N/A", scan, position);
    position.angular.z = -1.5;
    ASSERT_EQ(true, Filter_->hasRobotMoved(position) );
  }

  /* =================================================== */
  /* =================================================== */
  TEST_F(test_OccupancyFilter, getObstaclePoints_listSize)
  {
    sensor_msgs::Range scan;
    scan.radiation_type = scan.ULTRASOUND;
    scan.field_of_view = 45;
    scan.min_range = 0.2;
    scan.max_range = 1.0;
    scan.range = 1.0;

    geometry_msgs::Twist position;
    position.linear.x = 0.0;
    position.linear.y = 0.0;
    position.linear.z = 0.0;
    position.angular.x = 0.0;
    position.angular.y = 0.0;
    position.angular.z = Trig_DegreesToRadians(45);

    Point2D_Vector list;
    Filter_->getObstaclePoints(&list, scan, position);
    ASSERT_EQ(111, list.size() );
  }
  
  TEST_F(test_OccupancyFilter, getObstaclePoints_num_duplicates)
  {
    sensor_msgs::Range scan;
    scan.radiation_type = scan.ULTRASOUND;
    scan.field_of_view = 45;
    scan.min_range = 0.2;
    scan.max_range = 1.0;
    scan.range = 1.0;

    geometry_msgs::Twist position;
    position.linear.x = 0.0;
    position.linear.y = 0.0;
    position.linear.z = 0.0;
    position.angular.x = 0.0;
    position.angular.y = 0.0;
    position.angular.z = Trig_DegreesToRadians(45);

    Point2D_Vector list;
    Filter_->getObstaclePoints(&list, scan, position);
    ASSERT_EQ(0, countDuplicates(&list) );
  }
  
  /* =================================================== */
  TEST_F(test_OccupancyFilter, newScan_time)
  {
    sensor_msgs::Range scan;
    scan.radiation_type = scan.ULTRASOUND;
    scan.field_of_view = 15;
    scan.min_range = 0.2;
    scan.max_range = 2.0;
    scan.range = 1.0;

    geometry_msgs::Twist position;
    position.linear.x = 1.0;
    position.linear.y = 0.0;
    position.linear.z = 0.0;
    position.angular.x = 0.0;
    position.angular.y = 0.0;
    position.angular.z = Trig_DegreesToRadians(45);

    Filter_->setVelocity(0.0, 0.0);
    ASSERT_EQ(true, Filter_->isRobotStable() );   
    Filter_->newScan("N/A", scan, position);
    ASSERT_EQ(false, Filter_->hasRobotMoved(position) );
  }

  /* =================================================== */
  TEST_F(test_OccupancyFilter, getClearPoints_num_duplicates)
  {
    sensor_msgs::Range scan;
    scan.radiation_type = scan.ULTRASOUND;
    scan.field_of_view = 45;
    scan.min_range = 0.2;
    scan.max_range = 2.0;
    scan.range = 1.0;

    geometry_msgs::Twist position;
    position.linear.x = 0.0;
    position.linear.y = 0.0;
    position.linear.z = 0.0;
    position.angular.x = 0.0;
    position.angular.y = 0.0;
    position.angular.z = Trig_DegreesToRadians(45);

    Point2D_Vector list;
    Filter_->getClearPoints(&list, scan, position);
    ASSERT_EQ(0, countDuplicates(&list));
  }
  
  TEST_F(test_OccupancyFilter, duplicates_between_clear_and_obstacle)
  {
    sensor_msgs::Range scan;
    scan.radiation_type = scan.ULTRASOUND;
    scan.field_of_view = 45;
    scan.min_range = 0.2;
    scan.max_range = 2.0;
    scan.range = 1.0;

    geometry_msgs::Twist position;
    position.linear.x = 0.0;
    position.linear.y = 0.0;
    position.linear.z = 0.0;
    position.angular.x = 0.0;
    position.angular.y = 0.0;
    position.angular.z = Trig_DegreesToRadians(45);

    Point2D_Vector clearList, obstacleList;
    Filter_->getObstaclePoints(&obstacleList, scan, position);
    Filter_->getClearPoints(&clearList, scan, position);
   
    ASSERT_EQ(0, countDuplicatesInClearPoints(&clearList, obstacleList ));
  }  

}