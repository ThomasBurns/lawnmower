#include <gtest/gtest.h>
#include "../src/mapping/TravelMap.h"
#include "../src/mapping/GridStorage.h"
#include "../src/mapping/MapGrid.h"

static int calcOffset(int x, int y, int res);

namespace {
  class t_SystemMap : public ::testing::Test {

  protected:
    GridStorage::Dimensions_t Dimensions_;
    GridStorage *Grid_;
    TravelMap *Map_;

    t_SystemMap() {
    }

    virtual ~t_SystemMap() {
    }

    virtual void SetUp() {
      Dimensions_.maxHeight =  10;
      Dimensions_.minHeight = -2;
      Dimensions_.maxWidth  =  10;
      Dimensions_.minWidth  = -2;

      Grid_ = new GridStorage(50);
      Grid_->preallocateMemory(Dimensions_);

      Map_ = new TravelMap(Grid_);
    }

    virtual void TearDown() {
    }
  };



  TEST_F(t_SystemMap, checkRadius)
  {
    Map_->setBladeRadius(0.02*3.0, 100);
    Map_->markMown(0.5, 0.5);

    MapGrid_ptr p = Grid_->getGrid(GridRef_t(0.5,0.5));
    TravelledData_Vector vec;

    p->readTravelledData(&vec) ;

    ASSERT_NE(0, vec[calcOffset(25,25, 50)] );

    ASSERT_NE(0, vec[calcOffset(25,24, 50)] );
    ASSERT_NE(0, vec[calcOffset(25,26, 50)] );
    ASSERT_NE(0, vec[calcOffset(24,25, 50)] );
    ASSERT_NE(0, vec[calcOffset(26,25, 50)] );

    ASSERT_NE(0, vec[calcOffset(25,23, 50)] );
    ASSERT_NE(0, vec[calcOffset(25,27, 50)] );
    ASSERT_NE(0, vec[calcOffset(23,25, 50)] );
    ASSERT_NE(0, vec[calcOffset(27,25, 50)] );

    ASSERT_NE(0, vec[calcOffset(24,26, 50)] );
    ASSERT_NE(0, vec[calcOffset(24,24, 50)] );
    ASSERT_NE(0, vec[calcOffset(26,26, 50)] );
    ASSERT_NE(0, vec[calcOffset(26,24, 50)] );
  }

  TEST_F(t_SystemMap, negative_position)
  {
    Map_->markMown(-0.5, -0.5);

    MapGrid_ptr p = Grid_->getGrid(GridRef_t(-0.5,-0.5));
    TravelledData_Vector vec;

    p->readTravelledData(&vec) ;


    ASSERT_NE(0, vec[calcOffset(25,25, 50)] );
  }

  TEST_F(t_SystemMap, null_position)
  {
    Map_->markMown(0.0, -0.0);

    MapGrid_ptr p = Grid_->getGrid(GridRef_t(0.0,-0.0));
    TravelledData_Vector vec;

    p->readTravelledData(&vec) ;


    ASSERT_NE(0, vec[calcOffset(0,0, 50)] );
  }

  /* ------------------------------------------------- */
}

static int calcOffset(int x, int y, int res)
{
  return (x*res)+y;
}
