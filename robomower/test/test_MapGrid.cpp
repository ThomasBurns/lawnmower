
#include <gtest/gtest.h>
#include "../src/mapping/MapGrid.h"

namespace {
  class t_MapGrid : public ::testing::Test {

  protected:

    t_MapGrid() {
    }

    virtual ~t_MapGrid() {
    }

    virtual void SetUp() {
    }

    virtual void TearDown() {
      MapGrid::clearDirtyList();
    }
  };

  /* =================================================== */
  /* =================================================== */

  TEST_F(t_MapGrid, MapGrid_constructor_grid)
  {
    MapGrid map(GridRef_t(-1,5), 50);

    GridRef_t g = map.getGridLocation();

    ASSERT_EQ(-1, g.width );
    ASSERT_EQ( 5, g.height );
  }

  /* =================================================== */
  /* =================================================== */
  TEST_F(t_MapGrid, MapGrid_settravelled)
  {
    MapGrid map(GridRef_t(0,0), 50);

    SubGrid_t s = SubGrid_t(0.1,0.2);

    map.writeTravelLayerPixel(s, TravelData_t(0xFF));

    ASSERT_EQ( true, map.readTravelLayerPixel(s).Travelled_ );
  }

  TEST_F(t_MapGrid, MapGrid_settravelled_negative)
  {
    MapGrid map(GridRef_t(-1.5,-1.5), 50);

    SubGrid_t s = SubGrid_t(-1.5,-1.5);

    map.writeTravelLayerPixel(s, TravelData_t(0xFF));

    ASSERT_EQ( true, map.readTravelLayerPixel(s).Travelled_ );
  }

  TEST_F(t_MapGrid, MapGrid_settravelled_dirty_set)
  {
    MapGrid map(GridRef_t(0,0), 50);

    SubGrid_t s = SubGrid_t(0.1,0.2);

    map.writeTravelLayerPixel(s, TravelData_t(~0));

    ASSERT_EQ( true, map.isTravelDirty() );
  }

  TEST_F(t_MapGrid, MapGrid_settravelled_dirty_set_then_cleared)
  {
    MapGrid map(GridRef_t(0,0), 50);

    SubGrid_t s = SubGrid_t(0.1,0.2);

    map.writeTravelLayerPixel(s, TravelData_t(~0));
    map.clearTravelDirty();

    ASSERT_EQ( false, map.isTravelDirty() );
  }

  /* =================================================== */
  /* =================================================== */
  TEST_F(t_MapGrid, MapGrid_dirty_add_to_list)
  {
    MapGrid map(GridRef_t(1,2), 50);
    map.writeTravelLayerPixel(SubGrid_t(1.1,2.2), TravelData_t(0xFF));

    const GridRef_List & list_ = MapGrid::getTravelDirtyList();

    bool found = false;
    for (GridRef_List::const_iterator it=list_.begin(); it != list_.end(); ++it) {

      GridRef_t g(1,2);
      if ( g == *it ) {
        found = true;
        break;
      }
    }
    ASSERT_EQ( true, found );
  }

  TEST_F(t_MapGrid, MapGrid_dirty_remove_from_list)
  {
    MapGrid map(GridRef_t(0,0), 50);
    map.writeTravelLayerPixel(SubGrid_t(0.1,0.2), TravelData_t(0xFF));
    map.clearTravelDirty();

    const GridRef_List & list_ = MapGrid::getTravelDirtyList();

    bool found = false;
    for (GridRef_List::const_iterator it=list_.begin(); it != list_.end(); ++it) {

      GridRef_t g(1,2);
      if ( g == *it ) {
        found = true;
        break;
      }
    }
    ASSERT_EQ( false, found );
  }

  /* =================================================== */
  /* =================================================== */

}
