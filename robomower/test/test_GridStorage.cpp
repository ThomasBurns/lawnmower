
#include <gtest/gtest.h>
#include "../src/mapping/GridStorage.h"
#include "../src/mapping/MapGrid.h"

namespace {
  class test_GridStorage : public ::testing::Test {

  protected:

    test_GridStorage() {
    }

    virtual ~test_GridStorage() {
    }

    virtual void SetUp() {
    }

    virtual void TearDown() {
      MapGrid::clearDirtyList();
    }
  };

  /* =================================================== */
  /* =================================================== */
  TEST_F(test_GridStorage, constructor_number_of_grids)
  {
    GridStorage grid(50);

    ASSERT_EQ( 0, grid.getNumberOfGrids());
  }

  TEST_F(test_GridStorage, number_of_grids_one)
  {
    GridStorage grid(50);

    grid.getGrid(GridRef_t(1,2));

    ASSERT_EQ( 1, grid.getNumberOfGrids());
  }

  /* =================================================== */
  /* =================================================== */
  TEST_F(test_GridStorage, test_grid_ref_matches)
  {
    GridStorage grid(50);

    MapGrid_ptr p = grid.getGrid(GridRef_t(1,2));

    ASSERT_EQ( GridRef_t(1,2), p->getGridLocation());
  }

  TEST_F(test_GridStorage, test_grid_ref_matches_multiple_first)
  {
    GridStorage grid(50);

    grid.getGrid(GridRef_t(1,2));
    grid.getGrid(GridRef_t(2,2));
    grid.getGrid(GridRef_t(3,2));
    grid.getGrid(GridRef_t(3,5));
    MapGrid_ptr p = grid.getGrid(GridRef_t(1,2));

    ASSERT_EQ( GridRef_t(1,2), p->getGridLocation());
  }

  TEST_F(test_GridStorage, test_grid_ref_matches_multiple_middle)
  {
    GridStorage grid(50);

    grid.getGrid(GridRef_t(2,2));
    grid.getGrid(GridRef_t(3,2));
    grid.getGrid(GridRef_t(1,2));
    grid.getGrid(GridRef_t(3,5));

    MapGrid_ptr p = grid.getGrid(GridRef_t(1,2));

    ASSERT_EQ( GridRef_t(1,2), p->getGridLocation());
  }

  TEST_F(test_GridStorage, test_grid_size_unique)
  {
    GridStorage grid(50);

    grid.getGrid(GridRef_t(2,2));
    grid.getGrid(GridRef_t(3,2));
    grid.getGrid(GridRef_t(1,2));
    grid.getGrid(GridRef_t(3,5));

    ASSERT_EQ( 4, grid.getNumberOfGrids());
  }

  TEST_F(test_GridStorage, test_grid_size_multiple_duplicates)
  {
    GridStorage grid(50);

    grid.getGrid(GridRef_t(2,2));
    grid.getGrid(GridRef_t(3,2));
    grid.getGrid(GridRef_t(1,2));
    grid.getGrid(GridRef_t(3,2));
    grid.getGrid(GridRef_t(1,2));
    grid.getGrid(GridRef_t(3,5));

    ASSERT_EQ( 4, grid.getNumberOfGrids());
  }

  /* =================================================== */
  /* =================================================== */
  TEST_F(test_GridStorage, test_grid_ref_matches_const)
  {
    GridStorage grid(50);
    const GridStorage * gc = &grid;

    grid.getGrid(GridRef_t(1,2));
    MapGrid_const p = gc->getGrid(GridRef_t(1,2));

    ASSERT_EQ( GridRef_t(1,2), p->getGridLocation());
  }

  TEST_F(test_GridStorage, test_grid_ref_NULL)
  {
    GridStorage grid(50);
    const GridStorage * gc = &grid;

    MapGrid_const p = gc->getGrid(GridRef_t(1,2));

    if ( p ) {
      ASSERT_EQ( 0, 1);
    }
  }

  /* =================================================== */
  /* =================================================== */
  TEST_F(test_GridStorage, locked_enabled)
  {
    GridStorage grid(50);
    grid.lockGrid(true);

    ASSERT_EQ( true, grid.isLocked());

    MapGrid_ptr p = grid.getGrid(GridRef_t(1,2));

    if ( p ) {
      ASSERT_EQ( 0, 1);
    }
  }


}