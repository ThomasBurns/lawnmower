
#include <ros/ros.h>
#include <geometry_msgs/Twist.h>

#include <cstdio>
#include <cstring>
#include <termios.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>

#include "TempModule.h"
#include "MagModule.h"
#include "InsModule.h"
#include "VelocityModule.h"

static TempModule Temp_;
static MagModule  Mag_;
static InsModule  INS_;
static VelocityModule Velocity_;

static const char *PortName_ = "/dev/ttyACM0";

//====================================
int Serial_Openport(const char * port)
{

  int port_id = open(port, O_RDWR | O_NOCTTY);
  if( port_id < 0 ) {
    return -1;
  }
  fcntl(port_id, F_SETFL, 0);

  struct termios options;
  memset(&options, 0, sizeof(options));  /* clear the new struct */
  options.c_cflag = B115200 | CS8 | CLOCAL | CREAD;
  options.c_iflag = IGNPAR;
  options.c_oflag = 0;
  options.c_lflag = 0;
  options.c_cc[VMIN] =1;      // block untill n bytes are received
  options.c_cc[VTIME] =1;     // block untill a timer expires (n * 100 mSec.)

  if( tcsetattr(port_id, TCSANOW, &options) == -1) {
    close(port_id);
    return -1;
  }
  return port_id;
}

//====================================
#define BUFFER_SIZE  256
static char TextBuffer[BUFFER_SIZE];
static unsigned int TextIn, TextOut;
static char TextLine[BUFFER_SIZE];
static unsigned int TextOffset;


static void storeText(const char * line)
{
  while (( *line != 0 )) {
    TextBuffer[TextIn] = *line++;
    TextIn = (TextIn+1) & (BUFFER_SIZE-1);
  }
}

static bool extractLine(void)
{
  while ( TextIn != TextOut ) {
    char c = TextBuffer[TextOut];
    TextOut = (TextOut+1) & (BUFFER_SIZE-1);

    if ( c == '[' ) {
      TextOffset = 0;
    }

    if ( TextOffset < (sizeof(TextLine)-1)) {
      TextLine[TextOffset++] = c;
      TextLine[TextOffset] = 0;
    }
    if ( c == ']' ) {
      return true;
    }
  }
  return false;
}

static void processLine(void)
{
  if ( strncmp(TextLine, "[MAG=", 5) == 0 ) {
    Mag_.processLine(TextLine);

  } else if ( strncmp(TextLine, "[INS=", 5) == 0 ) {
    INS_.processLine(TextLine);

  } else if ( strncmp(TextLine, "[Temp=", 6) == 0 ) {
    Temp_.processLine(TextLine);

  } else if ( strncmp(TextLine, "[Enc", 4) == 0 ) {
      ;

  } else {
    ROS_ERROR("Unknown Line: %s", TextLine);
  }
}

//====================================
void sendVelocity(int fd)
{
  if ( Velocity_.dataReady() == true ) {

    char buf[64];
    Velocity_.clearDataReadyFlag();

    int l = Velocity_.getLeftDutyTime();
    int r = Velocity_.getRightDutyTime();
    snprintf(buf, sizeof(buf), "duty %d %d\r\n", l, r);
    write(fd, buf, strlen(buf));
  }
}

//====================================
int main (int argc, char **argv)
{
  ros::init(argc, argv, "DFRover");
  ros::NodeHandle node;


  ROS_INFO("DFRover Online");

  INS_.connect();
  Mag_.connect();
  Temp_.connect();
  Velocity_.connect();

  int serial_id = -1;

  while ( ros::ok() ) {

    if ( serial_id < 0 ) {
      serial_id = Serial_Openport(PortName_);
      if ( serial_id < 0 ) {
        ROS_ERROR_ONCE("Failed to open port %s, will retry every 3 seconds", PortName_);
        ros::Duration d(3);
        d.sleep();
     } else {
        ROS_INFO_ONCE("Serial port opened.");
     }
    } else {
      // Run serial port
      char buffer[256];
      int i = read(serial_id, buffer, sizeof(buffer));
      if ( i < 0 ) {
        close(serial_id);
        ROS_INFO("Serial port error");
        serial_id = -1;

      } else {
        buffer[i] = 0;
        storeText(buffer);
        if ( extractLine() == true ) {
          processLine();
        }
      }
      sendVelocity(serial_id);
    }
    ros::spinOnce();
  }
  return 0;
}
