#ifndef TEMP_MODULE_H
#define TEMP_MODULE_H

#include <ros/ros.h>

class TempModule
{
public:
  TempModule();

  void connect();
  void processLine(const char *line);

private:
  ros::Publisher TempPub_;
};

#endif
