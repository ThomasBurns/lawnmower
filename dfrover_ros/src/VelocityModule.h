#ifndef VELOCITY_MODULE_H
#define VELOCITY_MODULE_H

#include <ros/ros.h>
#include <geometry_msgs/Twist.h>

class VelocityModule
{
public:
  VelocityModule();

  void connect();

  bool dataReady();
  void clearDataReadyFlag();

  int getLeftDutyTime() const;
  int getRightDutyTime() const;

private:
  void velCallback(const geometry_msgs::Twist & vel);
  float limitVelocity(float velocity, float limit);
  int calcDutyTime(float velocity);

  ros::Subscriber VelocitySub_;
  float LinearSpeed_;
  float AngularSpeed_;

  int LeftDutyTime_, RightDutyTime_;

  bool NewData_;
  unsigned int SecondCounter_;
};


#endif