
#include "VelocityModule.h"
#include <cmath>

/************************************************
 * Robot Dimensions and other parameters
 */
const float MIN_WHEEL_VELOCITY = 0.018;
const float MAX_WHEEL_VELOCITY = 0.080;

const unsigned int MAX_DUTY_TIME = 325; // ms

const float ENCODER_PPR  = 16.0;
const float WHEEL_CIRCUMFERENCE = 0.094; // m

const float ROBOT_WIDTH = 0.09; // m
const float ROBOT_CIRCUMFERENCE = (ROBOT_WIDTH*M_PI); // m

/* *********************************************** */
/* *********************************************** */

VelocityModule::VelocityModule():
  LinearSpeed_(0.0), AngularSpeed_(0.0),
  LeftDutyTime_(0), RightDutyTime_(0),
  NewData_(false), SecondCounter_(1)
{
}

void VelocityModule::connect()
{
  ros::NodeHandle node;

  VelocitySub_ = node.subscribe("/cmd_vel", 10, &VelocityModule::velCallback, this);
  ros::param::param<float>("/robosetting/velocity/MaxLinear", LinearSpeed_, 1.0);
  ros::param::param<float>("/robosetting/velocity/MaxAngular", AngularSpeed_, 45.0);
  AngularSpeed_ = ( AngularSpeed_ * 3.14159 ) / 180.0;
}

float VelocityModule::limitVelocity(float velocity, float limit)
{
  if ( velocity > limit ) {
      return limit;
  }
  if ( velocity < -limit ) {
      return -limit;
  }
  return velocity;
}

int VelocityModule::calcDutyTime(float velocity)
{
  if ( velocity == 0.0 ) {
      return 0;
  }
  float revPerSec = velocity / WHEEL_CIRCUMFERENCE;
  float pulsesPerSec = revPerSec / ENCODER_PPR;
  float dutyTime = 1.0 / pulsesPerSec;

  return (int)dutyTime;
}

void VelocityModule::velCallback(const geometry_msgs::Twist & vel)
{
  ROS_DEBUG("velCallback(%f, %f)", vel.linear.x, vel.angular.z);
  int leftDuty = 0, rightDuty = 0;

  if (( vel.linear.x != 0.0 ) || ( vel.angular.z != 0.0 )) {

    float angularSpeed  = limitVelocity((vel.angular.z / (2*M_PI)) * ROBOT_CIRCUMFERENCE, AngularSpeed_); // converts radians/sec to meters/sec
    float leftVelocity  = limitVelocity(vel.linear.x, LinearSpeed_) + angularSpeed; // in meters/sec
    float rightVelocity = limitVelocity(vel.linear.x, LinearSpeed_) - angularSpeed; // in meters/sec

    leftDuty  = calcDutyTime(leftVelocity);
    rightDuty = calcDutyTime(rightVelocity);

    ROS_DEBUG("duty (%d, %d)", leftDuty, rightDuty);
  }

  if (( LeftDutyTime_ != leftDuty ) || ( RightDutyTime_ != rightDuty )) {

    LeftDutyTime_ = leftDuty;
    RightDutyTime_ = rightDuty;
    NewData_ = true;
  }
}

bool VelocityModule::dataReady()
{
  unsigned int s = ros::Time::now().sec;
  if ( SecondCounter_ != s ) {
    SecondCounter_ = s;
    NewData_ = true;
  }
  return NewData_;
}

void VelocityModule::clearDataReadyFlag()
{
  NewData_ = false;
}

int VelocityModule::getLeftDutyTime() const
{
  return LeftDutyTime_;
}

int VelocityModule::getRightDutyTime() const
{
  return  RightDutyTime_;
}

