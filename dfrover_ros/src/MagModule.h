#ifndef MAG_MODULE_H
#define MAG_MODULE_H

#include <ros/ros.h>

class MagModule
{
public:
  MagModule();

  void connect();
  void processLine(const char *line);

private:
  bool checkValueRange(float v);
  ros::Publisher MagPub_;
};

#endif
