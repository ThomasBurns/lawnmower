#ifndef INS_MODULE_H
#define INS_MODULE_H

#include <ros/ros.h>

class InsModule
{
public:
  InsModule();

  void connect();
  void processLine(const char *line);

private:
  bool checkOrientationValueRange(float v);
  bool checkAccelerationValueRange(float v);
  ros::Publisher InsPub_;
};

#endif
