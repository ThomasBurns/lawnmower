
#include "TempModule.h"

#include <sensor_msgs/Temperature.h>
#include <cstdio>


TempModule::TempModule()
{
}

void TempModule::connect()
{
  ros::NodeHandle node;
  TempPub_ = node.advertise<sensor_msgs::Temperature>("/robomower/nav/Temp", 5);
}

void TempModule::processLine(const char *line)
{
  int t;
  int rv = sscanf(line+6, "%d", &t);

  if ( rv != 1 ) {
    ROS_ERROR("Temp Str: %s", line);
    ROS_ERROR("Temp RV:  %d", rv);

  } else if ( t < -40 || t > 80 ) {
    ROS_ERROR("Temp Out of bounds: %d", t);


  } else {
    sensor_msgs::Temperature temperature;
    temperature.header.stamp = ros::Time::now();
    temperature.temperature = t;
    temperature.variance = -1;
    TempPub_.publish(temperature);
  }

}
