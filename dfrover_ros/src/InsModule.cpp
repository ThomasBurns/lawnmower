

#include "InsModule.h"

#include "mowerlib/INS_Data.h"

InsModule::InsModule()
{
}

void InsModule::connect()
{
  ros::NodeHandle node;
  InsPub_ = node.advertise<mowerlib::INS_Data>("/robomower/nav/INS", 300);
}

bool InsModule::checkOrientationValueRange(float v)
{
  if (( v < -360.0 ) || ( v > 360.0 )) {
    return false;
  }
  return true;
}

bool InsModule::checkAccelerationValueRange(float v)
{
  if (( v < -20.0 ) || ( v > 20.0 )) {
    return false;
  }
  return true;
}

void InsModule::processLine(const char *line)
{
  mowerlib::INS_Data ins;

  int rv = sscanf(line+5, "%f,%f,%f,%f,%f,%f",
                  &ins.LinearAcceleration[0],
                  &ins.LinearAcceleration[1],
                  &ins.LinearAcceleration[2],
                  &ins.AbsOrientation[0],
                  &ins.AbsOrientation[1],
                  &ins.AbsOrientation[2]);

  if ( rv != 6 ) {
    ROS_ERROR("INS Str: %s", line);
    ROS_ERROR("INS RV:  %d", rv);

  } else if (( checkOrientationValueRange(ins.AbsOrientation[0]) == false ) ||
             ( checkOrientationValueRange(ins.AbsOrientation[1]) == false ) ||
             ( checkOrientationValueRange(ins.AbsOrientation[2]) == false ))
  {
    ROS_ERROR("INS Out of bounds, Orr: %f:%f:%f", ins.AbsOrientation[0], ins.AbsOrientation[1], ins.AbsOrientation[2]);

  } else if (( checkAccelerationValueRange(ins.LinearAcceleration[0]) == false ) ||
             ( checkAccelerationValueRange(ins.LinearAcceleration[1]) == false ) ||
             ( checkAccelerationValueRange(ins.LinearAcceleration[2]) == false ))
  {
    ROS_ERROR("INS Out of bounds, Accel: %f:%f:%f", ins.LinearAcceleration[0], ins.LinearAcceleration[1], ins.LinearAcceleration[2]);

  } else if (( isnan(ins.AbsOrientation[0]) == false ) &&
             ( isnan(ins.AbsOrientation[1]) == false ) &&
             ( isnan(ins.AbsOrientation[2]) == false ) &&
             ( isnan(ins.LinearAcceleration[0]) == false ) &&
             ( isnan(ins.LinearAcceleration[1]) == false ) &&
             ( isnan(ins.LinearAcceleration[2]) == false )) {

    ins.Stamp = ros::Time::now();
    InsPub_.publish(ins);
  } else {
    ROS_ERROR("INS Range: %s", line);
  }
}
