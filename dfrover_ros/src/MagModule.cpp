
#include "MagModule.h"

#include "sensor_msgs/MagneticField.h"
#include <cstdio>

MagModule::MagModule()
{
}

void MagModule::connect()
{
  ros::NodeHandle node;
  MagPub_ = node.advertise<sensor_msgs::MagneticField>("/robomower/nav/Mag", 300);
}

bool MagModule::checkValueRange(float v)
{
  if (( v > 250.0 ) || ( v < -250.0 )) {
    return false;
  }
  return true;
}

void MagModule::processLine(const char *line)
{
  sensor_msgs::MagneticField magnetics;

  int rv = sscanf(line+5, "%lf,%lf,%lf",
                  &magnetics.magnetic_field.x,
                  &magnetics.magnetic_field.y,
                  &magnetics.magnetic_field.z);

  if ( rv != 3 ) {
    ROS_ERROR("MAG Str: %s", line);
    ROS_ERROR("MAG RV:  %d", rv);

  } else if (( checkValueRange(magnetics.magnetic_field.x) == false ) ||
             ( checkValueRange(magnetics.magnetic_field.y) == false ) ||
             ( checkValueRange(magnetics.magnetic_field.z) == false ))
  {
    ROS_ERROR("MAG Out of bounds: %f:%f:%f", magnetics.magnetic_field.x, magnetics.magnetic_field.y, magnetics.magnetic_field.z);

  } else if (( isnan(magnetics.magnetic_field.x) == false ) &&
             ( isnan(magnetics.magnetic_field.y) == false ) &&
             ( isnan(magnetics.magnetic_field.z) == false )) {

    magnetics.header.stamp = ros::Time::now();
    MagPub_.publish(magnetics);
  } else {
    ROS_ERROR("MAG Range: %s", line);
  }
}


