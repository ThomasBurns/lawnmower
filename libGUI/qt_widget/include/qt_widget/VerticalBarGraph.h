#ifndef VERTICALBARGRAPH_H
#define VERTICALBARGRAPH_H

#include <qt_widget/AbstractBarGraph.h>

class VerticalBarGraph: public AbstractBarGraph
{
  Q_OBJECT
public:
    VerticalBarGraph(QWidget* parent = 0);

    void paintEvent(QPaintEvent *event);

};

#endif // VERTICALBARGRAPH_H
