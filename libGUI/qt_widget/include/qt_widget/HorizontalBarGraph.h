#ifndef HORIZONTALBARGRAPH_H
#define HORIZONTALBARGRAPH_H

#include <qt_widget/AbstractBarGraph.h>

class HorizontalBarGraph: public AbstractBarGraph
{
  Q_OBJECT
public:
    HorizontalBarGraph(QWidget* parent = 0);

    void paintEvent(QPaintEvent *event);

};

#endif // HORIZONTALBARGRAPH_H
