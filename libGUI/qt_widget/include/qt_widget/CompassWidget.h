#ifndef COMPASSWIDGET_H
#define COMPASSWIDGET_H

#include <QWidget>

class CompassWidget: public QWidget
{
  Q_OBJECT

public:
    CompassWidget(QWidget* parent = 0);
    void  enableTarget(bool en);

public slots:
    void setBearing(int degrees);
    void setTarget(int degrees);

protected:
    QSize minimumSizeHint() const;
    QSize sizeHint() const;
    void paintEvent(QPaintEvent *event);

private:
    void drawSolidProngs(QPainter & p);
    void drawHollowProngs(QPainter & p);
    void drawTarget(QPainter & p);
    void drawArrow(QPainter & p);

    int Degrees_;
    int Target_;
    bool DrawTarget_;

};

#endif // COMPASSWIDGET_H
