#ifndef ABSTRACTBARGRAPH_H
#define ABSTRACTBARGRAPH_H

#include <QWidget>
#include <QColor>

class AbstractBarGraph: public QWidget
{
  Q_OBJECT
public:
    AbstractBarGraph(unsigned int width, unsigned int height, QWidget* parent = 0);

    void setLimits(float min, float max);
    void setValue(float v);
    void setTarget(float t);

protected:
    QSize minimumSizeHint() const;
    QSize sizeHint() const;
    virtual void paintEvent(QPaintEvent *event) = 0;

    static const QColor TargetColor;
    static const QColor ValueColor;
    static const QColor BackgroundPen;
    static const QColor BackgroundBrush;

    unsigned int Width_, Height_;
    float Max_;
    float Min_;
    float Target_;
    float Value_;

};

#endif // ABSTRACTBARGRAPH_H
