#include "qt_widget/VerticalBarGraph.h"

#include <QPainter>

VerticalBarGraph::VerticalBarGraph(QWidget* parent):
  AbstractBarGraph(30, 200, parent)
{
}

void VerticalBarGraph::paintEvent(QPaintEvent *)
{
  QPainter painter(this);
  painter.setRenderHint(QPainter::Antialiasing, true);

  // draw border
  painter.setPen(BackgroundPen);
  painter.setBrush(BackgroundBrush);
  painter.drawRect(0,0,29,199);
  painter.drawRect(0,100,29,1);

  float half = (Max_ - Min_) / 2.0 ;
  float halfway = half + Min_;

  // draw Target bar
  painter.setPen(TargetColor);
  painter.setBrush(TargetColor);

  if ( Target_ >= halfway ) {
    // draw in top box.
    float scale = (Target_ / half) * 100;
    painter.drawRect(1, 99-scale,13, scale);

  } else {
    // draw in bottom box.
    float scale = (Target_ / half) * -100;
    painter.drawRect(1, 102, 13, scale);
  }

  painter.setPen(ValueColor);
  painter.setBrush(ValueColor);
  if ( Value_ >= halfway ) {
    // draw in right hand box.
    float scale = (Value_ / half) * 100;
    painter.drawRect(15, 99-scale, 13, scale);

  } else {
    // draw in left hand box.
    float scale = (Value_ / half) * -100;
    painter.drawRect(15, 102, 13, scale);
  }
}
