#include "qt_widget/AbstractBarGraph.h"

AbstractBarGraph::AbstractBarGraph(unsigned int width, unsigned int height, QWidget* parent):
  QWidget(parent),
  Width_(width), Height_(height),
  Max_(0.0), Min_(0.0),
  Target_(0.0), Value_(0.0)
{

}

void AbstractBarGraph::setLimits(float min, float max)
{  
  Max_ = max;
  Min_ = min;
}

void AbstractBarGraph::setValue(float v)
{
  if ( v > Max_ ) {
    v = Max_;
  }
  if ( v < Min_ ) {
    v = Min_;
  }
  if ( Value_ != v ) {
    Value_ = v;
    this->update();
  }
}

void AbstractBarGraph::setTarget(float t)
{
  if ( t > Max_ ) {
    t = Max_;
  }
  if ( t < Min_ ) {
    t = Min_;
  }
  if ( Target_ != t ) {
    Target_ = t;
    this->update();
  }
}

QSize AbstractBarGraph::minimumSizeHint() const
{
  return QSize(Width_, Height_);
}

QSize AbstractBarGraph::sizeHint() const
{
  return QSize(Width_, Height_);
}

const QColor AbstractBarGraph::TargetColor = QColor(255,32,32);
const QColor AbstractBarGraph::ValueColor = QColor(22,201,36);
const QColor AbstractBarGraph::BackgroundPen = QColor(0,0,0);
const QColor AbstractBarGraph::BackgroundBrush = QColor(250,250,250);
