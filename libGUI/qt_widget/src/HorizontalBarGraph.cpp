#include "qt_widget/HorizontalBarGraph.h"

#include <QPainter>

HorizontalBarGraph::HorizontalBarGraph(QWidget* parent):
  AbstractBarGraph(200, 30, parent)
{

}

void HorizontalBarGraph::paintEvent(QPaintEvent *)
{
  QPainter painter(this);
  painter.setRenderHint(QPainter::Antialiasing, true);

  // draw border
  painter.setPen(BackgroundPen);
  painter.setBrush(BackgroundBrush);
  painter.drawRect(0,0,199,29);
  painter.drawRect(100,0,1,29);

  float half = (Max_ - Min_) / 2.0 ;
  float halfway = half + Min_;

  // draw Target bar
  painter.setPen(TargetColor);
  painter.setBrush(TargetColor);

  if ( Target_ >= halfway ) {
    // draw in right hand box.
    float scale = (Target_ / half) * 100;
    painter.drawRect(102,1,scale,13);

  } else {
    // draw in left hand box.
    float scale = (Target_ / half) * -100;
    painter.drawRect(99-scale,1, scale,13);
  }

  painter.setPen(ValueColor);
  painter.setBrush(ValueColor);
  if ( Value_ >= halfway ) {
    // draw in right hand box.
    float scale = (Value_ / half) * 100;
    painter.drawRect(102,15,scale,13);

  } else {
    // draw in left hand box.
    float scale = (Value_ / half) * -100;
    painter.drawRect(99-scale,15, scale,13);
  }

}
