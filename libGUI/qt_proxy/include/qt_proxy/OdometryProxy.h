#ifndef ODOM_PROXY_H
#define ODOM_PROXY_H

#include <QObject>
#include <ros/ros.h>

#include <odometer/Odometry.h>

class OdometryProxy: public QObject
{
  Q_OBJECT
public:
  OdometryProxy(QObject *parent = 0);
  void connect();
  
signals:
  void newDistance(float total, float session);
  
public slots:
  void resetDistance(void);

private:
  void odomCallback(const odometer::Odometry & odom);
  
  ros::ServiceClient ResetClient_;
  ros::Subscriber OdometrySub_;
};

#endif
