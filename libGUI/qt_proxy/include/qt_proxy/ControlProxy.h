#ifndef CONTROL_PROXY_H
#define CONTROL_PROXY_H

#include <QObject>

#include <ros/ros.h>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/terminal_state.h>
#include <position_control/ControlVectorAction.h>

class QTimer;

class ControlProxy: public QObject
{
  Q_OBJECT
public:
  ControlProxy(QObject *parent = 0);
  void connect();
  
  float getMaxLineraVelocity() const;
  float getMaxAngularVelocity() const;
  
signals:
  void systemReady();

  void feedback(float linErr, float angErr);
  void operationComplete(float linRmsError, float angRmsError);

public slots:
  void newMoveOperation(float x, float y);
  void newTargetAngle(float angle);
  void setSpeedLimits(float fwd, float turn);

private slots:
  void checkServer(void);
  void checkActionServer(void);

private:
  float LinearVelocity_, AngularVelocity_;
  float MaxAngularVelocity_, MaxLinearVelocity_;

  ros::Publisher     TargetPositionPub_;

  // create the action client
  // true causes the client to spin its own thread
  actionlib::SimpleActionClient<position_control::ControlVectorAction> *ActClient_;

  QTimer *Timer_;
};

#endif
