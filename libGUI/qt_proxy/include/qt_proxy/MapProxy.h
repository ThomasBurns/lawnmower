#ifndef MAP_PROXY_H
#define MAP_PROXY_H

#include <QObject>

#include <ros/ros.h>

#include <mowerlib/Map_TravelLayer.h>
#include <mowerlib/Map_MapLayer.h>
#include <mowerlib/SetMowerState.h>
#include <mowerlib/WayPoint.h>
#include <mowerlib/WayPointsMsg.h>
#include <mowerlib/MapSize.h>

class MapProxy: public QObject
{
  Q_OBJECT
public:
  MapProxy(QObject *parent = 0);
  void connect();

  static bool checkServices();
  bool isServiceDataLoaded();
  void loadMap();

signals:
  void resolutionChanged(int resolution);
  void mapDimensionsChanged(float minWidth, float maxWidth, float minHeight, float maxHeight);

  void updateTravelPlot(int16_t width, int16_t height, const std::vector<uint8_t>  & data);
  void updateMapPlot(int16_t width, int16_t height, const std::vector<uint32_t>  & data);
  void newWaypointList(WayPointList_t list);

private:
  void TravelLayerUpdate(const mowerlib::Map_TravelLayer & msg);
  void MapLayerUpdate(const mowerlib::Map_MapLayer & msg);
  void MapSizeUpdate(const mowerlib::MapSize & msg);
  void WaypointCallback(const mowerlib::WayPointsMsg & msg);

  ros::Subscriber  MapLayerSub_;
  ros::Subscriber  TravelSub_;

  ros::Subscriber MapSizeSub_;
  ros::Subscriber WaypointSub_;

  ros::ServiceClient GetUnitClient_;

  bool ServicesLoaded_;
  unsigned int Resolution_;
  int16_t MinWidth_, MaxWidth_;
  int16_t MinHeight_,MaxHeight_;

};

#endif
