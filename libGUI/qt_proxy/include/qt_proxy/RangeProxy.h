#ifndef RANGE_PROXY_H
#define RANGE_PROXY_H

#include <QObject>
#include <string>
#include <vector>

#include <ros/ros.h>

#include <sensor_msgs/Range.h>
#include <geometry_msgs/Twist.h>
#include <tf/transform_listener.h>


class RangeProxy: public QObject
{
  Q_OBJECT
public:
  RangeProxy(QObject *parent = 0);
  ~RangeProxy();
  void connect();

  void newScan(std::string name, const sensor_msgs::Range & msg, const geometry_msgs::Twist & position);

signals:
  void newRange_IR(sensor_msgs::Range range, geometry_msgs::Twist pos);
  void newRange_US(sensor_msgs::Range range, geometry_msgs::Twist pos);

private:
  void newReading(const sensor_msgs::Range & msg);

  ros::Subscriber RangeSub_;
  tf::TransformListener Listener_;

};


#endif
