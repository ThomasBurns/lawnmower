#ifndef IMU_PROXY_H
#define IMU_PROXY_H

#include <QObject>
#include <ros/ros.h>

#include <mowerlib/INS_Data.h>
#include <sensor_msgs/MagneticField.h>

class ImuProxy: public QObject
{
  Q_OBJECT
public:
  ImuProxy(QObject *parent = 0);
  virtual ~ImuProxy();

  void connect();
  
signals:
  void newInsData(float *linAccel, float *absOrr);
  void newMagData(float x, float y, float z);
    
private:
  void magCallback(const sensor_msgs::MagneticField & msg);
  void imuCallback(const mowerlib::INS_Data & msg);
  
  ros::Subscriber MagSub_;
  ros::Subscriber ImuSub_;
};

#endif

