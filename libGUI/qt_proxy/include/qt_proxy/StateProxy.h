#ifndef STATE_PROXY_H
#define STATE_PROXY_H

#include <QObject>

#include <ros/ros.h>

#include <mowerlib/MowerState.h>
#include <mowerlib/SetMowerState.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Point.h>

class StateProxy: public QObject
{
  Q_OBJECT
public:

  typedef enum {
    md_Idle   = mowerlib::MowerState::STATE_IDLE,
    md_Manual = mowerlib::MowerState::STATE_MANUAL,
    md_Mowing = mowerlib::MowerState::STATE_MOWING,
    md_Mapping= mowerlib::MowerState::STATE_MAPPING

  } MowerMode_t;

  StateProxy(QObject *parent = 0);
  virtual ~StateProxy();

  void connect();

signals:
  void newRoboMowerState(int state);
  void newRealVelocity(geometry_msgs::Twist);
  void newTargetVelocity(geometry_msgs::Twist);

  void newRealPosition(geometry_msgs::Twist pos);
  void newTargetPoint(geometry_msgs::Point pos);

public slots:
  void sysStateChanged(int slot);

private slots:

private:
  void odomCallback(const nav_msgs::Odometry & odom);
  void targetVelocityCallback(const geometry_msgs::Twist & vel);
  void targetCallback(const geometry_msgs::Point & msg);
  void sysStateCallback(const mowerlib::MowerState & data);


  ros::Subscriber OdomSub_;
  ros::Subscriber TargetVelSub_;
  ros::Subscriber PointSub_;

  ros::Subscriber SystemStateSub_;
  ros::ServiceClient SetMowerStateClient_;

  geometry_msgs::Twist Position_;
  geometry_msgs::Point TargetPoint_;

  geometry_msgs::Twist RealVelocity_;
  geometry_msgs::Twist TargetVelocity_;

};

#endif
