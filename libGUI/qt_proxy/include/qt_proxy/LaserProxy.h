#ifndef LASER_PROXY_H
#define LASER_PROXY_H

#include <QObject>
#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <sensor_msgs/LaserScan.h>
#include <geometry_msgs/Twist.h>

class LaserProxy: public QObject
{
  Q_OBJECT
public:
  LaserProxy(QObject *parent = 0);
  virtual ~LaserProxy();

  void connect();

signals:
  void newReading(geometry_msgs::Twist pos,  sensor_msgs::LaserScan msg);

private:
  void receiver(const sensor_msgs::LaserScan & msg);

  ros::Subscriber RangeSub_;
  tf::TransformListener Listener_;

};

#endif

