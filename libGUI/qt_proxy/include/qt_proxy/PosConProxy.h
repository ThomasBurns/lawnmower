#ifndef POS_CON_PROXH_H
#define POS_CON_PROXH_H

#include <QObject>
#include <ros/ros.h>

class PosConProxy: public QObject
{
  Q_OBJECT
public:
  PosConProxy(QObject *parent = 0);
  ~PosConProxy();
  void connect();
  
  typedef enum {
    parAngP = 0,
    parAngI = 1,
    parAngD = 2
    
  } Par_t;
  
  static bool checkServices();
  bool isServiceDataLoaded();
  

signals:
  void newAngularPID(float p, float i, float d);
  
public slots:
  void updateParamater(int par, float v);
  void getPIDValues(void);

private:
  
  ros::ServiceClient GetPidClient_;
  ros::ServiceClient SetPidClient_;
  
  bool ServicesLoaded_;
};

#endif
