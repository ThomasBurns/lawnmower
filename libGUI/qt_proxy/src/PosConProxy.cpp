
#include "qt_proxy/PosConProxy.h"

#include <position_control/PID.h>
#include <position_control/setPID.h>


PosConProxy::PosConProxy(QObject *parent):
  QObject(parent),
  ServicesLoaded_(false)
{
}

PosConProxy::~PosConProxy()
{
}

void PosConProxy::connect()
{
  ros::NodeHandle node;
  ROS_DEBUG("PosConProxy::connect()");
  GetPidClient_ = node.serviceClient<position_control::PID>("/robomower/poscon/GetPID");
  SetPidClient_ = node.serviceClient<position_control::setPID>("/robomower/poscon/SetPID");
}

bool PosConProxy::checkServices()
{
  return (( ros::service::waitForService( "/robomower/poscon/GetPID", 1) == true) && 
          ( ros::service::waitForService( "/robomower/poscon/SetPID", 1) == true));
}

bool PosConProxy::isServiceDataLoaded()
{
  return ServicesLoaded_;
}


void PosConProxy::getPIDValues(void) 
{ 
  position_control::PID msg;
  ROS_ASSERT( GetPidClient_.call(msg) == true );
  ServicesLoaded_ = true;
  
  emit newAngularPID(msg.response.Angle_P,
                     msg.response.Angle_I,
                     msg.response.Angle_D);
}


void PosConProxy::updateParamater(int par, float v)
{
  position_control::setPID msg;
  int p;
  if ( (Par_t)par == parAngP ) {
    p = msg.request.PAR_ANGLE_P;
    
  } else if ( (Par_t)par == parAngI ) {
    p = msg.request.PAR_ANGLE_I;
    
  } else if ( (Par_t)par == parAngD ) {
    p = msg.request.PAR_ANGLE_D;
    
  } else {
    return ;
  }
    
  msg.request.Parameter = p;
  msg.request.Value = v;
  ROS_ASSERT( SetPidClient_.call(msg) == true );
  
}