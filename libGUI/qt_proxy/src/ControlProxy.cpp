
#include "qt_proxy/ControlProxy.h"

#include <QTimer>

ControlProxy::ControlProxy(QObject *parent):
  QObject(parent),
  LinearVelocity_(0.0), AngularVelocity_(0.0),
  MaxAngularVelocity_(0.0), MaxLinearVelocity_(0.0)
{
  Timer_ = new QTimer(this);
  Timer_->setInterval(2000);
  QObject::connect(Timer_, SIGNAL(timeout()), this, SLOT(checkServer()));
}

void ControlProxy::connect()
{
  ros::NodeHandle node;

  ActClient_ = new actionlib::SimpleActionClient<position_control::ControlVectorAction>("/pos_con_act", true);

  float degrees;
  ros::param::param<float>("/robosetting/velocity/MaxLinear", MaxLinearVelocity_, 1.0);
  ros::param::param<float>("/robosetting/velocity/MaxAngular", degrees, 45.0);
  MaxAngularVelocity_ = ( degrees * M_PI ) / 180.0;
  Timer_->start();
}

void ControlProxy::newMoveOperation(float x, float y)
{
  position_control::ControlVectorGoal goal;
  goal.MoveToPosition = true;
  goal.X = x;
  goal.Y = y;

  goal.MoveToAngle = false;
  goal.TargetAngle = 0.0;

  goal.AngularVelocity = AngularVelocity_;
  goal.LinearVelocity  = LinearVelocity_;
  ActClient_->sendGoal(goal);
  Timer_->start();
}

void ControlProxy::newTargetAngle(float angle)
{
  position_control::ControlVectorGoal goal;
  goal.MoveToPosition = false;
  goal.X = 0.0;
  goal.Y = 0.0;

  goal.MoveToAngle = true;
  goal.TargetAngle = angle;

  goal.AngularVelocity = AngularVelocity_;
  goal.LinearVelocity  = LinearVelocity_;
  ActClient_->sendGoal(goal);
  Timer_->start();
}

void ControlProxy::setSpeedLimits(float fwd, float turn)
{
  if ( turn > MaxAngularVelocity_ ) {
    turn = MaxAngularVelocity_;
  }
  if ( fwd > MaxLinearVelocity_ ) {
    fwd = MaxLinearVelocity_;
  }
  LinearVelocity_ = fwd;
  AngularVelocity_ = turn;
}

void ControlProxy::checkServer(void)
{
  // check if server is online
  if ( ActClient_->waitForServer(ros::Duration(0.001)) ) {
    emit systemReady();
    Timer_->stop();
    Timer_->setInterval(50);

    disconnect(Timer_, SIGNAL(timeout()), this, SLOT(checkServer()));
    QObject::connect(Timer_, SIGNAL(timeout()), this, SLOT(checkActionServer()));
  }
}

void ControlProxy::checkActionServer(void)
{
  actionlib::SimpleClientGoalState state = ActClient_->getState();

  if ( state == actionlib::SimpleClientGoalState::PENDING ) {
    return;
  }
  if ( state == actionlib::SimpleClientGoalState::ACTIVE ) {
    emit feedback(1e-9, 1e-9);
    return;
  }

  Timer_->stop();
  emit operationComplete(1e-8,1e-8);
}

float ControlProxy::getMaxLineraVelocity() const
{
  return MaxLinearVelocity_;
}

float ControlProxy::getMaxAngularVelocity() const
{
  return MaxAngularVelocity_;
}
