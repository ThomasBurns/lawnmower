
#include "qt_proxy/LaserProxy.h"


LaserProxy::LaserProxy(QObject *parent):
  QObject(parent)
{
}

LaserProxy::~LaserProxy()
{
}

void LaserProxy::connect()
{
  ros::NodeHandle node;
  RangeSub_    = node.subscribe("/LaserScanner", 5, &LaserProxy::receiver, this);
}

void LaserProxy::receiver(const sensor_msgs::LaserScan & msg)
{
  try {
    tf::StampedTransform location;
    Listener_.lookupTransform("/world", msg.header.frame_id, ros::Time(0), location);

    geometry_msgs::Twist t;

    t.linear.x = location.getOrigin().getX();
    t.linear.y = location.getOrigin().getY();
    t.linear.z = location.getOrigin().getZ();

    t.angular.x = 0.0;
    t.angular.y = 0.0;
    t.angular.z = tf::getYaw(location.getRotation());
    emit newReading(t, msg);
  }
  catch (tf::ConnectivityException ex) {
    ROS_ERROR("QLP,tf::ConnectivityException => %s",ex.what());
    return;
  }
  catch (tf::ExtrapolationException ex) {
//    ROS_ERROR("tf::ExtrapolationException => %s",ex.what());
    return;
  }
  catch (tf::InvalidArgument ex) {
    ROS_ERROR("QLP,tf::InvalidArgument => %s",ex.what());
    return;
  }
  catch (tf::LookupException ex) {
    ROS_ERROR("QLP,tf::LookupException => %s",ex.what());
    return;
  }
}

