
#include "qt_proxy/RangeProxy.h"


RangeProxy::RangeProxy(QObject *parent):
  QObject(parent)
{
}

RangeProxy::~RangeProxy()
{
}

void RangeProxy::connect()
{
  ros::NodeHandle node;
  RangeSub_    = node.subscribe("/RangeSensors", 50, &RangeProxy::newReading, this);
}

void RangeProxy::newReading(const sensor_msgs::Range & msg)
{
  try {
    tf::StampedTransform location;
    Listener_.lookupTransform("/world", msg.header.frame_id, ros::Time(0), location);

    geometry_msgs::Twist t;

    t.linear.x = location.getOrigin().getX();
    t.linear.y = location.getOrigin().getY();
    t.linear.z = location.getOrigin().getZ();

    t.angular.x = 0.0;
    t.angular.y = 0.0;
    t.angular.z = tf::getYaw(location.getRotation());

    if ( msg.radiation_type == msg.ULTRASOUND ) {
      emit newRange_US(msg, t);
    } else {
      emit newRange_IR(msg, t);
    }
  }
  catch (tf::ConnectivityException ex) {
    ROS_ERROR("QTP,tf::ConnectivityException => %s",ex.what());
    return;
  }
  catch (tf::ExtrapolationException ex) {
//    ROS_ERROR("tf::ExtrapolationException => %s",ex.what());
    return;
  }
  catch (tf::InvalidArgument ex) {
    ROS_ERROR("QTP,tf::InvalidArgument => %s",ex.what());
    return;
  }
  catch (tf::LookupException ex) {
    ROS_ERROR("QTP,tf::LookupException => %s",ex.what());
    return;
  }
}

