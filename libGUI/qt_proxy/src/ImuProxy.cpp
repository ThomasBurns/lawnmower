
#include "qt_proxy/ImuProxy.h"


ImuProxy::ImuProxy(QObject *parent):
  QObject(parent)
{
}

ImuProxy::~ImuProxy()
{
}

void ImuProxy::connect()
{
  ros::NodeHandle node;
  ROS_DEBUG("ImuProxy::connect()");
  MagSub_ = node.subscribe("/robomower/nav/Mag", 20,  &ImuProxy::magCallback, this);
  ImuSub_ = node.subscribe("/robomower/nav/INS", 100, &ImuProxy::imuCallback, this);
}

void ImuProxy::magCallback(const sensor_msgs::MagneticField & msg)
{
  emit newMagData(msg.magnetic_field.x, msg.magnetic_field.y, msg.magnetic_field.z);
}

void ImuProxy::imuCallback(const mowerlib::INS_Data & msg)
{
  float linAccel[3], absOrr[3];

  absOrr[0] = msg.AbsOrientation[0];
  absOrr[1] = msg.AbsOrientation[1];
  absOrr[2] = msg.AbsOrientation[2];
  
  linAccel[0] = msg.LinearAcceleration[0];
  linAccel[1] = msg.LinearAcceleration[1];
  linAccel[2] = msg.LinearAcceleration[2];
  
  emit newInsData(linAccel, absOrr);
}
