
#include "qt_proxy/MapProxy.h"
#include <std_srvs/Empty.h>

MapProxy::MapProxy(QObject *parent):
  QObject(parent),
  ServicesLoaded_(false),
  Resolution_(0),
  MinWidth_(0),  MaxWidth_(0),
  MinHeight_(0), MaxHeight_(0)
{
}

void MapProxy::connect()
{
  ROS_DEBUG("MapProxy::connect()");
  ros::NodeHandle node;
  TravelSub_    = node.subscribe("/robomower/map/Travel", 100, &MapProxy::TravelLayerUpdate, this);
  MapLayerSub_  = node.subscribe("/robomower/map/Map",    100, &MapProxy::MapLayerUpdate,    this);

  WaypointSub_ = node.subscribe("/robomower/map/Waypoints", 10, &MapProxy::WaypointCallback, this);
  MapSizeSub_  = node.subscribe("/robomower/map/Size", 1, &MapProxy::MapSizeUpdate, this);

  GetUnitClient_ = node.serviceClient<std_srvs::Empty>("/robomower/srv/GetUnit");
//  ROS_ASSERT( ros::service::waitForService( "/robomower/srv/GetUnit", 10000) == true);
}

bool MapProxy::checkServices()
{
  return ros::service::waitForService( "/robomower/srv/GetUnit", 1) == true;
}

bool MapProxy::isServiceDataLoaded()
{
  return ServicesLoaded_;
}

void MapProxy::loadMap()
{

  if ( checkServices() ) {
    std_srvs::Empty msg;
    ROS_ASSERT( GetUnitClient_.call(msg) == true );
    ServicesLoaded_ = true;
  }
}


/* ============================================ */
void MapProxy::TravelLayerUpdate(const mowerlib::Map_TravelLayer & msg)
{
  if ((msg.Width  >= MinWidth_) &&
      (msg.Width  <  MaxWidth_) &&
      (msg.Height >= MinHeight_) &&
      (msg.Height <  MaxHeight_)) {

    ROS_ASSERT(msg.Data.size() ==  (Resolution_*Resolution_));
    emit updateTravelPlot(msg.Width, msg.Height, msg.Data);
  }
}

/* ============================================ */
void MapProxy::MapLayerUpdate(const mowerlib::Map_MapLayer & msg)
{
  if ((msg.Width  >= MinWidth_) &&
      (msg.Width  <  MaxWidth_) &&
      (msg.Height >= MinHeight_) &&
      (msg.Height <  MaxHeight_)) {

    ROS_ASSERT(msg.Data.size() ==  (Resolution_*Resolution_));
    emit updateMapPlot(msg.Width, msg.Height, msg.Data);
  }
}

/* ============================================ */
void MapProxy::MapSizeUpdate(const mowerlib::MapSize & msg)
{
  Resolution_ = msg.Resolution;
  MaxHeight_  = msg.MaxHeight;
  MinHeight_  = msg.MinHeight;
  MaxWidth_   = msg.MaxWidth;
  MinWidth_   = msg.MinWidth;

  emit resolutionChanged(msg.Resolution);
  emit mapDimensionsChanged(msg.MinWidth, msg.MaxWidth, msg.MinHeight, msg.MaxHeight);
}

/* ============================================ */
void MapProxy::WaypointCallback(const mowerlib::WayPointsMsg & msg)
{
  WayPointList_t waypoints;

  for ( unsigned int i = 0; i < msg.WayPoints.size(); i ++ ) {
    WayPoint pt;
    pt.X = msg.WayPoints[i].X;
    pt.Y = msg.WayPoints[i].Y;
    waypoints.push_back(pt);
  }
  emit newWaypointList(waypoints);

}

