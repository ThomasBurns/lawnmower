
#include "qt_proxy/StateProxy.h"

#include <mowerlib/SetMowerState.h>
#include <mowerlib/Conversion.h>

const float FILTER_SAMPLES = 4.0;

StateProxy::StateProxy(QObject *parent):
  QObject(parent)
{
}

StateProxy::~StateProxy()
{
}

void StateProxy::connect()
{
  ROS_DEBUG("StateProxy::connect()");
  ros::NodeHandle node;
  OdomSub_ = node.subscribe("odom", 20, &StateProxy::odomCallback, this);
  TargetVelSub_ = node.subscribe("/cmd_vel", 10, &StateProxy::targetVelocityCallback, this);
  PointSub_ = node.subscribe("/robomower/nav/TargetPosition", 20, &StateProxy::targetCallback, this);

  SystemStateSub_ = node.subscribe("/robomower/system/State", 2, &StateProxy::sysStateCallback, this);
  SetMowerStateClient_ = node.serviceClient<mowerlib::SetMowerState>("/robomower/system/SetState");

  Position_.linear.x = 1e9;
  TargetPoint_.x = 1e9;

  RealVelocity_.linear.y = 1e9;
  TargetVelocity_.linear.y = 1e9;

}

void StateProxy::odomCallback(const nav_msgs::Odometry & odom)
{
  if ( Equal(odom.twist.twist, RealVelocity_ ) == false ) {
    emit newRealVelocity(odom.twist.twist);
    RealVelocity_ = odom.twist.twist;
  }

  geometry_msgs::Twist pos;
  pos.linear.x = odom.pose.pose.position.x;
  pos.linear.y = odom.pose.pose.position.y;
  pos.linear.z = odom.pose.pose.position.z;

  tf::Quaternion q;
  tf::quaternionMsgToTF(odom.pose.pose.orientation, q);
  tf::Matrix3x3(q).getRPY(pos.angular.x, pos.angular.y, pos.angular.z);

  if ( Equal(pos, Position_ ) == false ) {
    Position_ = pos;
    emit newRealPosition(pos);
  }
}

void StateProxy::targetVelocityCallback(const geometry_msgs::Twist & vel)
{
  ROS_ASSERT(isnan(vel.linear.x) == false);
  ROS_ASSERT(isnan(vel.linear.y) == false);
  ROS_ASSERT(isnan(vel.linear.z) == false);
  ROS_ASSERT(isnan(vel.angular.x) == false);
  ROS_ASSERT(isnan(vel.angular.y) == false);
  ROS_ASSERT(isnan(vel.angular.z) == false);

  if ( Equal(vel, TargetVelocity_ ) == false ) {

    TargetVelocity_ = vel;
    emit newTargetVelocity(vel);
  }
}

void StateProxy::targetCallback(const geometry_msgs::Point & msg)
{
  ROS_ASSERT(isnan(msg.x) == false);
  ROS_ASSERT(isnan(msg.y) == false);
  ROS_ASSERT(isnan(msg.z) == false);

  if ( Equal(msg, TargetPoint_ ) == false ) {
    TargetPoint_ = msg;
    emit newTargetPoint(msg);
  }
}

void StateProxy::sysStateCallback(const mowerlib::MowerState & data)
{
  emit newRoboMowerState(data.State);
}

void StateProxy::sysStateChanged(int slot)
{
  mowerlib::SetMowerState req;
  req.request.State = slot;
  SetMowerStateClient_.call(req);
}
