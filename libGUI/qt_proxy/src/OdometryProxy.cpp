
#include "qt_proxy/OdometryProxy.h"
#include <odometer/Reset.h>



OdometryProxy::OdometryProxy(QObject *parent):
  QObject(parent)
{
}

void OdometryProxy::connect()
{
  ros::NodeHandle node;
  
  ROS_DEBUG("OdometryProxy::connect()");
  ResetClient_ = node.serviceClient<odometer::Reset>("/odometer/Reset");
  OdometrySub_ = node.subscribe("/odometer/odometry",2, &OdometryProxy::odomCallback, this);
}

void OdometryProxy::resetDistance(void)
{
  odometer::Reset r;
  ResetClient_.call(r);
}

void OdometryProxy::odomCallback(const odometer::Odometry & odom)
{
  emit newDistance(odom.TotalDistance, odom.SessionDistance);
}

