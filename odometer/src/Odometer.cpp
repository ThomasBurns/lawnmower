
#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Twist.h>
#include <cmath>

#include "odometer/Odometry.h"
#include "odometer/Reset.h"

// ROS Data
static ros::Subscriber OdomSub_;
static ros::Publisher  OdometryPub_;
static ros::ServiceServer ResetService_;

static odometer::Odometry Travelled_;
static bool PositionKnown_;
static geometry_msgs::Point Position_;
static const char * Filename_ = "/opt/mower/Odometry";

/* ======================================================== */
static void odomCallback(const nav_msgs::Odometry & odom)
{
  ROS_ASSERT(isnan(odom.pose.pose.position.x) == false);
  ROS_ASSERT(isnan(odom.pose.pose.position.y) == false);
  ROS_ASSERT(isnan(odom.pose.pose.position.z) == false);

  if ( PositionKnown_ == false ) {
    PositionKnown_ = true;
    Position_ = odom.pose.pose.position;
    return;
  }

  double d_x = odom.pose.pose.position.x - Position_.x;
  double d_y = odom.pose.pose.position.y - Position_.y;
  double d_z = odom.pose.pose.position.z - Position_.z;
  double dist = sqrt((d_x*d_x) + (d_y*d_y) + (d_z*d_z));

  if ( dist != 0.0 ) {

    Travelled_.TotalDistance += dist;
    Travelled_.SessionDistance += dist;
    OdometryPub_.publish(Travelled_);

    if ( FILE *file = fopen(Filename_, "w") ) {
      fprintf(file, "%f", Travelled_.TotalDistance);
      fclose(file);
    }
    Position_ = odom.pose.pose.position;
  }
}

//====================================
static bool resetDistance(odometer::Reset::Request  &req,
                          odometer::Reset::Response &res )
{
  Travelled_.SessionStart = ros::Time::now();
  Travelled_.SessionDistance = 0.0;
  return true;
}

//====================================
static void LoadDistance(void)
{
  Travelled_.TotalDistance = 0.0;
  if (FILE *file = fopen(Filename_, "r")) {

    char data[128];
    int rv = fread(data,1, 128, file);
    if ( rv > 0 ) {

      data[rv] = 0;
      float d = atof(data);

      if (( d < 0.0 ) ||
          ( isnan(d) == true )) {
        ROS_ERROR("File contents invalid %s", Filename_);

      } else {
        ROS_INFO("Total travelled distance loaded: %f", d);
        Travelled_.TotalDistance = d;
      }
    } else {
      ROS_ERROR("Empty file %s", Filename_);
    }
    fclose(file);
  } else {
    ROS_ERROR("Failed to read file %s", Filename_);
  }
}

//====================================
int main (int argc, char **argv)
{
  ros::init(argc, argv, "Odometer");
  ros::NodeHandle node;

  ROS_INFO("Odometer online");
  LoadDistance();
  Travelled_.SessionDistance = 0.0;
  Travelled_.SessionStart = ros::Time::now();

  ResetService_    = node.advertiseService("/odometer/Reset", resetDistance);

  OdomSub_ = node.subscribe("odom", 20, &odomCallback);
  OdometryPub_     = node.advertise<odometer::Odometry>("/odometer/odometry",2, true);
  OdometryPub_.publish(Travelled_);

  ros::spin();
  return 0;
}

