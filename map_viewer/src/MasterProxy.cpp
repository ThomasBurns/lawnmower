
#include "MasterProxy.h"

#include <qt_proxy/StateProxy.h>
#include <qt_proxy/MapProxy.h>
#include <qt_proxy/ImuProxy.h>
#include <qt_proxy/PosConProxy.h>
#include <qt_proxy/OdometryProxy.h>
#include <qt_proxy/RangeProxy.h>
#include <qt_proxy/LaserProxy.h>


#include <QTimer>

#include <ros/ros.h>

MasterProxy::MasterProxy(QObject *parent):
  QObject(parent)
{
  StateProxy_    = new StateProxy(this);
  MapProxy_      = new MapProxy(this);
  ImuProxy_      = new ImuProxy(this);
  PosConProxy_   = new PosConProxy(this);
  OdometryProxy_ = new OdometryProxy(this);
  RangeProxy_    = new RangeProxy(this);
  LaserProxy_    = new LaserProxy(this);

  ServiceTimer_ = new QTimer();
  ServiceTimer_->setInterval(2000);

  QObject::connect(ServiceTimer_, SIGNAL(timeout()), this, SLOT(checkServices()));
}

void MasterProxy::connect()
{
  StateProxy_->connect();
  MapProxy_->connect();
  PosConProxy_->connect();
  ImuProxy_->connect();
  OdometryProxy_->connect();
  RangeProxy_->connect();
  LaserProxy_->connect();

  ServiceTimer_->start();
}

SubModules_t MasterProxy::getSubModules() const
{
  SubModules_t submodules = {
    .imu    = *ImuProxy_,
    .map    = *MapProxy_,
    .odom   = *OdometryProxy_,
    .posCon = *PosConProxy_,
    .state  = *StateProxy_,
    .range  = *RangeProxy_,
    .laser  = *LaserProxy_
  };

  return submodules;
}

void MasterProxy::checkServices()
{
  if (( PosConProxy_->checkServices() == true ) &&
      ( PosConProxy_->isServiceDataLoaded() == false )) {

    PosConProxy_->getPIDValues();
  }

  if (( MapProxy_->checkServices() == true ) &&
      ( MapProxy_->isServiceDataLoaded() == false )) {

    MapProxy_->loadMap();
  }
}


