#ifndef MOTOR_TUNING_H
#define MOTOR_TUNING_H

#include <QObject>

class ControlProxy;
class QTimer;

class MotorTuning: public QObject
{
  Q_OBJECT
public:
  MotorTuning(QObject *parent = 0);

  float getMaxLineraVelocity() const;
  float getMaxAngularVelocity() const;

signals:
  void ActionSystemReady();
  void ActionFeedback(float linErr, float angErr);
  void ActionStep(int remaining, float linRmsError, float angRmsError);
  void ActionComplete();

public slots:
  void startOperation(int cycles, float time, float linVel, float angVel);

private slots:
  void timer(void);
  void systemReady();
  void feedback(float linErr, float angErr);
//  void operationComplete(float linRmsError, float angRmsError);



private:
  ControlProxy *ControlProxy_;
  QTimer *SystemTimer_;

  int CycleCounter_;
  float LinRmsError_;
  float AngRmsError_;
  float RmsCounter_;

};


#endif

