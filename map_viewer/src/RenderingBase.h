#ifndef RENDERING_BASE_H
#define RENDERING_BASE_H

#include <QWidget>

class RenderingBase : public QWidget
{
  Q_OBJECT

public:
  RenderingBase();

  static const int QPAINT_FULL_CIRCLE = 5760;

  int getGridPosition_X(int x) const;
  int getGridPosition_Y(int y) const;

  int convertPosToGrid_X(float x) const;
  int convertPosToGrid_Y(float y) const;

  int convertStartAngle_Degrees(float degrees) const;

  int getResolution() const;

public slots:
  void resolutionChanged(int resolution);
  void mapDimensionsChanged(float minWidth, float maxWidth, float minHeight, float maxHeight);

private:
  const int Resolution_;
  int MinWidth_, MaxWidth_;
  int MinHeight_, MaxHeight_;
};


#endif
