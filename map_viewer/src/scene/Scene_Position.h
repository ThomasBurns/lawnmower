#ifndef SCENE_POSITION_H
#define SCENE_POSITION_H

#include <QGraphicsItem>
#include <QWidget>

#include <geometry_msgs/Twist.h>

class Graphic_Position;
class RenderingBase;

class Scene_Position: public QWidget
{
  Q_OBJECT

public:
  Scene_Position(int z, const RenderingBase & base);
  ~Scene_Position();

  QGraphicsItem * getItem();

private slots:
  void reposition();
  void newRealPosition(geometry_msgs::Twist pos);

private:
  Graphic_Position *Graphic_;
  const RenderingBase & Base_;

  unsigned int Robot_X, Robot_Y;
  bool Redraw_;
};


#endif
