#ifndef SCENE_IR_H
#define SCENE_IR_H

#include <QGraphicsItem>
#include <QWidget>
#include <map>
#include <string>

#include <sensor_msgs/Range.h>
#include <geometry_msgs/Twist.h>


class Graphic_IR;
class RenderingBase;

class Scene_IR: public QWidget
{
Q_OBJECT

public:
  Scene_IR(int z, const RenderingBase & base);
  ~Scene_IR();

signals:
  void newItem(QGraphicsItem* item);

public slots:
  void toggleView();

private slots:
  void newRange_IR(sensor_msgs::Range range, geometry_msgs::Twist pos);

private:
  const int Z_;
  const RenderingBase & Base_;
  bool Visible_;
  std::map<std::string, Graphic_IR*> Sensors_;
};

#endif
