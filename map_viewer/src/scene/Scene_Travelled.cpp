
#include "Scene_Travelled.h"
#include "RenderingBase.h"

#include <QtWidgets>
#include <QDebug>

#include <ros/ros.h>
#include <mowerlib/MapPixel.h>

#include <cmath>

/* ================================================================ */
class Graphic_TravelledGrid: public QGraphicsItem
{
public:
  Graphic_TravelledGrid(int resolution) :
    Resolution_(resolution), AllTravelled_(false)
  {
  }

  QRectF boundingRect() const
  {
      return QRectF(0, 0, Resolution_, Resolution_);
  }

  QPainterPath shape() const
  {
      QPainterPath path;
      path.addRect(0, 0, Resolution_, Resolution_);
      return path;
  }

  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
  {
    Q_UNUSED(widget);
    Q_UNUSED(option);
    unsigned int i;

    painter->setPen(QPen(QColor(25, 141, 120), 0, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));
    if ( AllTravelled_ == true ) {
      painter->fillRect(QRectF(0, 0, Resolution_, Resolution_), QColor(25, 141, 120));
    } else {
      i = 0;
      for ( uint32_t h = 0; h < Resolution_; h ++ ) {
        for ( uint32_t w = 0; w < Resolution_; w ++ ) {

          TravelData_t t(Data_[i++]);
          if ( t.Mown_ ) {
            painter->drawPoint(w, (Resolution_-1) - h);
          }
        }
      }
    }

    painter->setPen(QPen(QColor(255, 0, 128), 0, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));
    i = 0;
    for ( uint32_t h = 0; h < Resolution_; h ++ ) {
      for ( uint32_t w = 0; w < Resolution_; w ++ ) {

        TravelData_t t(Data_[i++]);
        if ( t.Travelled_ ) {
          painter->drawPoint(w, (Resolution_-1) - h);
        }
      }
    }
  }

  void checkData(void)
  {
    for ( unsigned int i = 0; i < Data_.size(); i ++ ) {
      TravelData_t t(Data_[i++]);
      if ( t.Mown_ == false ) {
        AllTravelled_ = false;
        return;
      }
    }
    AllTravelled_ = true;
  }


  std::vector<uint8_t>  Data_;
private:
  const unsigned int Resolution_;
  bool AllTravelled_;
};

/*************************************************************************************************/
Scene_Travelled::Scene_Travelled(int z, const RenderingBase & base):
  Z_(z), Base_(base)
{
}

Scene_Travelled::~Scene_Travelled()
{
}

/* ======================================================================= */
void Scene_Travelled::reposition()
{
  typedef std::map<GridRef_t, Graphic_TravelledGrid*>::iterator gridIterator;
  for ( gridIterator iterator = Grid_.begin(); iterator != Grid_.end(); iterator++ ) {
    positionGrids(iterator->first);
  }
}

void Scene_Travelled::updatePlot(int16_t width, int16_t height, const std::vector<uint8_t>  & data)
{
  GridRef_t g(width, height);

  Graphic_TravelledGrid *item;

  if ( Grid_.find(g) == Grid_.end() ) {
    item = new Graphic_TravelledGrid(Base_.getResolution());
    Grid_[g] = item;
    item->setZValue(Z_);
    item->setCacheMode(QGraphicsItem::ItemCoordinateCache);
    positionGrids(g);

    emit newItem((QGraphicsItem*)item);
  } else {
    item = Grid_[g];
  }
  ROS_ASSERT(item != NULL);

  item->Data_ = data;
  item->checkData();
  item->update();
}

void Scene_Travelled::positionGrids(GridRef_t g)
{
  int x = Base_.getGridPosition_X(g.width);
  int y = Base_.getGridPosition_Y(g.height) - Base_.getResolution();
  Grid_[g]->setPos(x,y);
}
