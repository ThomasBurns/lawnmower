#ifndef SCENE_SURFACE_H
#define SCENE_SURFACE_H

#include <QGraphicsItem>
#include <QWidget>
#include <stdint.h>
#include <mowerlib/MapUtils.h>

class Graphic_SurfaceGrid;
class RenderingBase;

class Scene_Surface: public QWidget
{
Q_OBJECT

public:
  Scene_Surface(int z, const RenderingBase & base);
  ~Scene_Surface();

signals:
  void newItem(QGraphicsItem* item);

public slots:
  void reposition();
  void touch(int16_t width, int16_t height);
  void updateMapPlot(int16_t width, int16_t height, const std::vector<uint32_t>  & data);
  void updatePlot(int16_t width, int16_t height, const std::vector<uint8_t>  & data);

private:
  void positionGrids(GridRef_t g);
  void checkGrid(int16_t width, int16_t height);

  const unsigned int Z_;
  std::map<GridRef_t, Graphic_SurfaceGrid*> Grid_;
  const RenderingBase & Base_;
};
#endif
