#ifndef SCENE_WAYPOINT_H
#define SCENE_WAYPOINT_H


#include <QGraphicsItem>
#include <QWidget>

#include <mowerlib/WayPoint.h>
#include <geometry_msgs/Twist.h>

class Graphic_Waypoint;
class RenderingBase;

class Scene_Waypoint: public QWidget
{
  Q_OBJECT

public:
  Scene_Waypoint(int z, int length, const RenderingBase & base);
  ~Scene_Waypoint();

  QGraphicsItem * getItem();

private slots:
  void newRealPosition(geometry_msgs::Twist pos);
  void newWaypointList(WayPointList_t list);
  void newRoboMowerState(int state);

private:
  Graphic_Waypoint    * Graphic_;
  const RenderingBase & Base_;
};

#endif

