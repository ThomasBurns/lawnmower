
#include "Scene_Waypoint.h"

#include "RenderingBase.h"
#include <qt_proxy/StateProxy.h>

#include <QtWidgets>
#include <QDebug>

class Graphic_Waypoint: public QGraphicsItem
{

public:

  Graphic_Waypoint(int z, int length, const RenderingBase & base):
    Length_(length),
    Base_(base),
    PosX_(0.0), PosY_(0.0),
    MowerState_((int)StateProxy::md_Idle)
  {
    setZValue(z);
    setPos(QPointF(0, 0));
    setCacheMode(QGraphicsItem::ItemCoordinateCache);
  }

  QRectF boundingRect() const
  {
      return QRectF(0, 0, Length_, Length_);
  }

  QPainterPath shape() const
  {
      QPainterPath path;
      path.addRect(0, 0, Length_, Length_);
      return path;
  }

  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
  {
    Q_UNUSED(widget);
    Q_UNUSED(option);


    if (( MowerState_ == (int)StateProxy::md_Idle   ) ||
        ( MowerState_ == (int)StateProxy::md_Manual )) {
      return;
    }
    int lastX = Base_.convertPosToGrid_X(PosX_);
    int lastY = Base_.convertPosToGrid_Y(PosY_);

    painter->setPen(QPen(Qt::black, 1, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));

    for (WayPointList_t::const_iterator ci = List_.begin(); ci != List_.end(); ++ci) {
      WayPoint pt = *ci;

      int x = Base_.convertPosToGrid_X(pt.X);
      int y = Base_.convertPosToGrid_Y(pt.Y);

      painter->drawLine(lastX, lastY, x, y);
      lastX = x;
      lastY = y;
    }
  }

  const unsigned int Length_;
  const RenderingBase & Base_;
  float PosX_, PosY_;
  WayPointList_t List_;
  int MowerState_;

};

/*************************************************************************************************/
Scene_Waypoint::Scene_Waypoint(int z, int length, const RenderingBase & base):
  Base_(base)
{
  Graphic_ = new Graphic_Waypoint(z, length, base);
}

Scene_Waypoint::~Scene_Waypoint()
{
  delete Graphic_;
}

QGraphicsItem * Scene_Waypoint::getItem()
{
  return Graphic_;
}

void Scene_Waypoint::newRealPosition(geometry_msgs::Twist pos)
{
  Graphic_->PosX_ = pos.linear.x;
  Graphic_->PosY_ = pos.linear.y;
  Graphic_->update();
}

void Scene_Waypoint::newWaypointList(WayPointList_t list)
{
  Graphic_->List_ = list;
  Graphic_->update();
}

void Scene_Waypoint::newRoboMowerState(int state)
{
  Graphic_->MowerState_ = state;
  Graphic_->update();
}

