

#include <QtWidgets>
#include <qt_proxy/StateProxy.h>

#include "Scene_Position.h"
#include "RenderingBase.h"

class Graphic_Position: public QGraphicsItem
{

public:

  Graphic_Position()
  {
  }

  QRectF boundingRect() const
  {
      return QRectF(0, 0, 20, 20);
  }

  QPainterPath shape() const
  {
      QPainterPath path;
      path.addRect(0, 0, 20, 20);
      return path;
  }

  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
  {
    Q_UNUSED(widget);
    Q_UNUSED(option);

    QColor color(193, 172, 109);
    painter->setBrush(color);
    painter->setPen(QPen(color, 5, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));
    painter->drawPie(5, 5, 10, 10, 0, RenderingBase::QPAINT_FULL_CIRCLE );
  }

};

/*************************************************************************************************/
Scene_Position::Scene_Position(int z, const RenderingBase & base):
  Base_(base), Robot_X(0), Robot_Y(0), Redraw_(true)
{
  Graphic_ = new Graphic_Position();
  Graphic_->setZValue(z);
  Graphic_->setPos(QPointF(0, 0));
  Graphic_->setCacheMode(QGraphicsItem::ItemCoordinateCache);
}

Scene_Position::~Scene_Position()
{
  delete Graphic_;
}

QGraphicsItem * Scene_Position::getItem()
{
  return Graphic_;
}

void Scene_Position::newRealPosition(geometry_msgs::Twist pos)
{
  unsigned int gX = Base_.convertPosToGrid_X(pos.linear.x);
  unsigned int gY = Base_.convertPosToGrid_Y(pos.linear.y);

  if (( Robot_Y != gY ) ||
      ( Robot_X != gX ) ||
      ( Redraw_ == true )) {

    Robot_X = gX;
    Robot_Y = gY;
    Redraw_ = false;
    Graphic_->setPos(gX-10, gY-10);
  }
}

void Scene_Position::reposition()
{
  Redraw_ = true;
  Graphic_->update();
}

