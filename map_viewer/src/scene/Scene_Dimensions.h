#ifndef SCENE_DIMENSIONS_H
#define SCENE_DIMENSIONS_H

#include <QGraphicsItem>
#include <QWidget>

#include <geometry_msgs/Twist.h>

class Graphic_Dimensions;
class RenderingBase;

class Scene_Dimensions: public QWidget
{
  Q_OBJECT

public:
  Scene_Dimensions(int z, const RenderingBase & base);
  ~Scene_Dimensions();

  QGraphicsItem * getItem();

public slots:
  void mapDimensionsChanged(float minWidth, float maxWidth, float minHeight, float maxHeight);

private:
  Graphic_Dimensions *Graphic_;
  const RenderingBase & Base_;
};


#endif
