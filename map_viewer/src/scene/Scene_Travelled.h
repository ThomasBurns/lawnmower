#ifndef SCENE_TRAVELLED_H
#define SCENE_TRAVELLED_H

#include <QGraphicsItem>
#include <QWidget>
#include <stdint.h>
#include <map>
#include <mowerlib/MapUtils.h>

class Graphic_Travelled;
class Graphic_TravelledGrid;
class RenderingBase;

class Scene_Travelled: public QWidget
{
Q_OBJECT

public:
  Scene_Travelled(int z, const RenderingBase & base);
  ~Scene_Travelled();


signals:
  void newItem(QGraphicsItem* item);

public slots:
  void reposition();
  void updatePlot(int16_t width, int16_t height, const std::vector<uint8_t>  & data);

private slots:

private:
  void positionGrids(GridRef_t g);

  const unsigned int Z_;
  std::map<GridRef_t, Graphic_TravelledGrid*> Grid_;
  const RenderingBase & Base_;
};

#endif
