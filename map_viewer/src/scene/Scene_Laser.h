#ifndef SCENE_LASER_H
#define SCENE_LASER_H

#include <QGraphicsItem>
#include <QWidget>
#include <map>
#include <string>

#include <sensor_msgs/LaserScan.h>
#include <geometry_msgs/Twist.h>


class Graphic_Laser;
class RenderingBase;

class Scene_Laser: public QWidget
{
Q_OBJECT

public:
  Scene_Laser(int z, const RenderingBase & base);
  ~Scene_Laser();

  QGraphicsItem * getItem();

public slots:
  void toggleView();
  void newReading(geometry_msgs::Twist pos, sensor_msgs::LaserScan msg);

private:
  Graphic_Laser    * Graphic_;
  const RenderingBase & Base_;
  bool Visible_;
};

#endif
