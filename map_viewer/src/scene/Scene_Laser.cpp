
#include "Scene_Laser.h"
#include "RenderingBase.h"

#include <qt_proxy/LaserProxy.h>
#include <mowerlib/Trigonometry.h>
#include <mowerlib/Conversion.h>

#include <QtWidgets>
#include <QDebug>
#include <angles/angles.h>

#include <map>

class Graphic_Laser: public QGraphicsItem
{

public:

  Graphic_Laser(unsigned int resolution):
    Resolution_(resolution), Size_(0), Update_(true)
  {
  }

  QRectF boundingRect() const
  {
      return QRectF(0, 0, Size_*2, Size_*2);
  }

  QPainterPath shape() const
  {
      QPainterPath path;
      path.addRect(0, 0, Size_*2, Size_*2);
      return path;
  }

  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
  {
    Q_UNUSED(widget);
    Q_UNUSED(option);

    painter->setBrush(QColor(231,250,255, 100));
    painter->setPen(QPen(QColor(237, 28, 36), 2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

    float angle = Pos_.angular.z;
    for ( size_t i = 0; i < Scan_.ranges.size(); i ++ ) {

      int x = std::cos(angle) * Scan_.ranges[i] * Resolution_;
      int y = std::sin(angle) * Scan_.ranges[i] * Resolution_;
      angle += angles::from_degrees(Scan_.angle_increment);

      painter->drawLine(Size_, Size_, Size_+x, Size_-y);
    }
    Update_ = false;
  }

  void setScan(const sensor_msgs::LaserScan & laser)
  {
    Scan_ = laser;
    Update_ = true;
    float range = 0.0;
    for ( size_t i = 0; i < Scan_.ranges.size(); i ++ ) {
      if ( Scan_.ranges[i] > range)
        range = Scan_.ranges[i];

    }
    Size_ = range * Resolution_ * 2;
  }

  bool updateRequired() const
  {
    return Update_;
  }

  int getSize() const
  {
    return Size_;
  }

  geometry_msgs::Twist  Pos_;

private:
  const unsigned int Resolution_;
  sensor_msgs::LaserScan  Scan_;
  unsigned int Size_;
  bool Update_;
};

/*************************************************************************************************/
Scene_Laser::Scene_Laser(int z, const RenderingBase & base):
  Base_(base), Visible_(false)
{
  Graphic_ = new Graphic_Laser(Base_.getResolution());
  Graphic_->setZValue(z);
  Graphic_->setPos(QPointF(0, 0));
  Graphic_->setCacheMode(QGraphicsItem::ItemCoordinateCache);
}

Scene_Laser::~Scene_Laser()
{
  delete Graphic_;
}

QGraphicsItem * Scene_Laser::getItem()
{
  return Graphic_;
}

void Scene_Laser::newReading(geometry_msgs::Twist pos, sensor_msgs::LaserScan msg)
{
  if ( Visible_ == false ) {
    return;
  }
  ROS_ASSERT(Graphic_ != NULL);
  Graphic_->setScan( msg);

  // This filter stops the system redawring every time the same repeated scan is received.
  if (( Equal(Graphic_->Pos_, pos) == false ) || ( Graphic_->updateRequired() == true )) {

    Graphic_->Pos_ = pos;
    int x = Base_.convertPosToGrid_X(pos.linear.x) - (Graphic_->getSize());
    int y = Base_.convertPosToGrid_Y(pos.linear.y) - (Graphic_->getSize());
    Graphic_->setPos(x,y);
    Graphic_->update();
  }
}

void Scene_Laser::toggleView()
{
  Visible_ = !Visible_;
  Graphic_->setVisible(Visible_);
}
