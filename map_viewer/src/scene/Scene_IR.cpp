
#include "Scene_IR.h"
#include "RenderingBase.h"

#include <qt_proxy/RangeProxy.h>
#include <mowerlib/Trigonometry.h>
#include <mowerlib/Conversion.h>

#include <QtWidgets>
#include <QDebug>

#include <map>

class Graphic_IR: public QGraphicsItem
{

public:

  Graphic_IR(int resolution):
    Resolution_(resolution), Size_(0), Angle_(1e9)
  {
  }

  QRectF boundingRect() const
  {
      return QRectF(0, 0, Size_*2, Size_*2);
  }

  QPainterPath shape() const
  {
      QPainterPath path;
      path.addRect(0, 0, Size_*2, Size_*2);
      return path;
  }

  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
  {
    Q_UNUSED(widget);
    Q_UNUSED(option);

    painter->setPen(QPen(QColor(237, 28, 36), 2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

    int stopX = std::cos(Angle_) * Size_;
    int stopY = std::sin(Angle_) * Size_;

    painter->drawLine(Size_, Size_, Size_+stopX, Size_-stopY);
  }

  void setRange(float range)
  {
    ROS_ASSERT( range >= 0.0);
    unsigned int newSize = range * Resolution_;
    if ( Size_ != newSize ) {
      Size_ = newSize;
      this->update();
    }
  }

  void setAngle(float angle)
  {
    if ( Angle_ != angle ) {
      Angle_ = angle;
      this->update();
    }
  }

  const int Resolution_;
  unsigned int Size_;
  float Angle_;
  geometry_msgs::Twist  Pos_;

};

/*************************************************************************************************/
Scene_IR::Scene_IR(int z, const RenderingBase & base):
  Z_(z), Base_(base), Visible_(false)
{
}

Scene_IR::~Scene_IR()
{
  typedef std::map<std::string, Graphic_IR*>::iterator usIterator;

  for ( usIterator iterator = Sensors_.begin(); iterator != Sensors_.end(); iterator++ ) {
    delete iterator->second;
  }
}

void Scene_IR::newRange_IR(sensor_msgs::Range range, geometry_msgs::Twist pos)
{
  Graphic_IR *item;
  std::string name = range.header.frame_id;

  if ( Sensors_.find(name) == Sensors_.end() ) {
    item = new Graphic_IR(Base_.getResolution());

    item->setZValue(Z_);
    item->setPos(QPointF(0, 0));
    item->setCacheMode(QGraphicsItem::ItemCoordinateCache);
    item->setVisible(Visible_);
    Sensors_[name] = item;
    item->Pos_.linear.x = 1e9;

    emit newItem((QGraphicsItem*)item);
  } else {
    item = Sensors_[name];
  }

  ROS_ASSERT(item != NULL);
  item->setRange(range.range);
  item->setAngle( pos.angular.z );
//  item->setAngle(Trig_RadiansToDegrees( pos.angular.z ));
  // This filter stops the system redawring every time the same repeated scan is received.
  if ( Equal(item->Pos_, pos) == false ) {

    int x = Base_.convertPosToGrid_X(pos.linear.x) - (item->Size_);
    int y = Base_.convertPosToGrid_Y(pos.linear.y) - (item->Size_);
    item->setPos(x,y);

    item->Pos_ = pos;
    item->update();
  }
}

void Scene_IR::toggleView()
{
  Visible_ = !Visible_;
  typedef std::map<std::string, Graphic_IR*>::iterator usIterator;

  for ( usIterator iterator = Sensors_.begin(); iterator != Sensors_.end(); iterator++ ) {
    iterator->second->setVisible(Visible_);
  }
}
