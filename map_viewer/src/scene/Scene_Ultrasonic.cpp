
#include "Scene_Ultrasonic.h"
#include "RenderingBase.h"

#include <qt_proxy/RangeProxy.h>
#include <mowerlib/Trigonometry.h>
#include <mowerlib/Conversion.h>

#include <QtWidgets>
#include <QDebug>

#include <map>

class Graphic_Ultrasonic: public QGraphicsItem
{

public:

  Graphic_Ultrasonic(int resolution, float fov):
    Resolution_(resolution), FOV_(fov), Size_(0), Angle_(1e9), Update_(false)
  {
  }

  QRectF boundingRect() const
  {
      return QRectF(0, 0, Size_*2, Size_*2);
  }

  QPainterPath shape() const
  {
      QPainterPath path;
      path.addRect(0, 0, Size_*2, Size_*2);
      return path;
  }

  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
  {
    Q_UNUSED(widget);
    Q_UNUSED(option);

    painter->setBrush(QColor(231,250,255, 100));
    painter->setPen(QPen(QColor(231,250,255), 2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

    float start = (Angle_ - (FOV_ / 2.0)) * 16;
    painter->drawPie(0, 0, Size_, Size_, start, (FOV_*16) );
    Update_ = false;
  }

  void setRange(float range)
  {
    ROS_ASSERT( range >= 0.0);
    unsigned int newSize = range * Resolution_ * 2;
    if ( Size_ != newSize ) {
      Size_ = newSize;
      Update_ = true;
      this->update();
    }
  }

  void setAngle(float angle)
  {
    if ( Angle_ != angle ) {
      Angle_ = angle;
      Update_ = true;
      this->update();
    }
  }


//private:
  const int Resolution_;
  const float FOV_;
  unsigned int Size_;
  float Angle_;
  geometry_msgs::Twist  Pos_;
  bool Update_;
};

/*************************************************************************************************/
Scene_Ultrasonic::Scene_Ultrasonic(int z, const RenderingBase & base):
  Z_(z), Base_(base), Visible_(false)
{
}

Scene_Ultrasonic::~Scene_Ultrasonic()
{
  typedef std::map<std::string, Graphic_Ultrasonic*>::iterator usIterator;

  for ( usIterator iterator = Sensors_.begin(); iterator != Sensors_.end(); iterator++ ) {
    delete iterator->second;
  }
}

void Scene_Ultrasonic::newRange_US(sensor_msgs::Range range, geometry_msgs::Twist pos)
{
  Graphic_Ultrasonic *item;

  std::string name = range.header.frame_id;
  if ( Sensors_.find(name) == Sensors_.end() ) {
    item = new Graphic_Ultrasonic(Base_.getResolution(), range.field_of_view);

    item->setZValue(Z_);
    item->setPos(QPointF(0, 0));
    item->setCacheMode(QGraphicsItem::ItemCoordinateCache);
    Sensors_[name] = item;
    item->setVisible(Visible_);
    item->Pos_.linear.x = 1e9;

    emit newItem((QGraphicsItem*)item);
  } else {
    item = Sensors_[name];
  }

  ROS_ASSERT(item != NULL);
  item->setRange(range.range);
  item->setAngle(Trig_RadiansToDegrees( pos.angular.z ));
  // This filter stops the system redawring every time the same repeated scan is received.
  if (( Equal(item->Pos_, pos) == false ) || ( item->Update_ == true )) {

    int x = Base_.convertPosToGrid_X(pos.linear.x) - (item->Size_/2);
    int y = Base_.convertPosToGrid_Y(pos.linear.y) - (item->Size_/2);
    item->setPos(x,y);

    item->Pos_ = pos;
    item->update();
  }
}

void Scene_Ultrasonic::toggleView()
{
  Visible_ = !Visible_;
  typedef std::map<std::string, Graphic_Ultrasonic*>::iterator usIterator;

  for ( usIterator iterator = Sensors_.begin(); iterator != Sensors_.end(); iterator++ ) {
    iterator->second->setVisible(Visible_);
  }
}
