

#include <QtWidgets>
#include <qt_proxy/StateProxy.h>
#include <stdint.h>
#include <stdio.h>
#include "Scene_Dimensions.h"
#include "RenderingBase.h"

class Graphic_Dimensions: public QGraphicsItem
{

public:

  Graphic_Dimensions():
    MinWidth_(0), MaxWidth_(0),
    MinHeight_(0), MaxHeight_(0)
  {
  }

  QRectF boundingRect() const
  {
      return QRectF(0, 0, 100, 75);
  }

  QPainterPath shape() const
  {
      QPainterPath path;
      path.addRect(0, 0, 100, 75);
      return path;
  }

  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
  {
    Q_UNUSED(widget);
    Q_UNUSED(option);

    QColor color(0, 0, 0);
    painter->setBrush(color);
    painter->setPen(QPen(color, 5, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));
//    painter->setFont();

    char buffer[256];
    snprintf(buffer, sizeof(buffer), "Width:  %2d:%2d", MinWidth_, MaxWidth_);
    painter->drawText(10, 20, QString(buffer));
    snprintf(buffer, sizeof(buffer), "Height: %2d:%2d", MinHeight_, MaxHeight_);
    painter->drawText(10, 40, QString(buffer));
}

  int16_t MinWidth_, MaxWidth_;
  int16_t MinHeight_, MaxHeight_;

};

/*************************************************************************************************/
Scene_Dimensions::Scene_Dimensions(int z, const RenderingBase & base):
  Base_(base)
{
  Graphic_ = new Graphic_Dimensions();
  Graphic_->setZValue(z);
  Graphic_->setPos(QPointF(0, 0));
  Graphic_->setCacheMode(QGraphicsItem::ItemCoordinateCache);
}

Scene_Dimensions::~Scene_Dimensions()
{
  delete Graphic_;
}

QGraphicsItem * Scene_Dimensions::getItem()
{
  return Graphic_;
}

void Scene_Dimensions::mapDimensionsChanged(float minWidth, float maxWidth, float minHeight, float maxHeight)
{
  Graphic_->MinWidth_  = minWidth;
  Graphic_->MaxWidth_  = maxWidth;
  Graphic_->MinHeight_ = minHeight;
  Graphic_->MaxHeight_ = maxHeight;
  Graphic_->update();
}

