
#include "Scene_Obstacles.h"
#include "RenderingBase.h"

#include <QPainter>
#include <ros/ros.h>
#include <cmath>

#include <mowerlib/MapPixel.h>

#include <QtWidgets>
#include <QDebug>

class Graphic_ObstacleGrid: public QGraphicsItem
{

public:
  Graphic_ObstacleGrid(int resolution) :
    Resolution_(resolution), AllUnknown_(false)
  {
  }

  QRectF boundingRect() const
  {
      return QRectF(0, 0, Resolution_, Resolution_);
  }

  QPainterPath shape() const
  {
      QPainterPath path;
      path.addRect(0, 0, Resolution_, Resolution_);
      return path;
  }

  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
  {
    Q_UNUSED(widget);
    Q_UNUSED(option);
    unsigned int i = 0;

    QPen obstacle(QColor(  0,   0,   0)); //  black
    QPen unknown (QColor(192, 192, 192));

    painter->setPen(unknown);
    if ( AllUnknown_ == true ) {
      painter->fillRect(QRectF(0, 0, Resolution_, Resolution_), QColor(192, 192, 192));
    } else {
      i = 0;
      for ( uint32_t h = 0; h < Resolution_; h ++ ) {
        for ( uint32_t w = 0; w < Resolution_; w ++ ) {

          MapData_t t(Data_[i++]);
          if (( t.Seen_ == 0 ) &&
              ( t.Unreachable_ == false )) {

            painter->drawPoint(w, (Resolution_-1) - h);
          }
        }
      }
    }

    painter->setPen(obstacle);
    i = 0;
    for ( uint32_t h = 0; h < Resolution_; h ++ ) {
      for ( uint32_t w = 0; w < Resolution_; w ++ ) {

        MapData_t t(Data_[i++]);
        if (( t.Seen_ == 0 ) || ( t.Occupied_ == 0 )) {
          continue;

        }
        painter->drawPoint(w, (Resolution_-1) - h);
      }
    }
  }

  void checkData(void)
  {
    for ( unsigned int i = 0; i < Data_.size(); i ++ ) {
      MapData_t t(Data_[i++]);
      if ( t.Seen_ != 0 ) {
        AllUnknown_ = false;
        return;
      }
    }
    AllUnknown_ = true;
  }

  std::vector<uint32_t>  Data_;
private:
  const unsigned int Resolution_;
  bool AllUnknown_;

};

/*************************************************************************************************/
Scene_Obstacles::Scene_Obstacles(int z, const RenderingBase & base):
  Z_(z), Base_(base)
{
}

Scene_Obstacles::~Scene_Obstacles()
{
}

/* ======================================================================= */
void Scene_Obstacles::reposition()
{
  typedef std::map<GridRef_t, Graphic_ObstacleGrid*>::iterator gridIterator;
  for ( gridIterator iterator = Grid_.begin(); iterator != Grid_.end(); iterator++ ) {
    positionGrids(iterator->first);
  }
}

/* ======================================================================= */
void Scene_Obstacles::updateMapPlot(int16_t width, int16_t height, const std::vector<uint32_t>  & data)
{
  GridRef_t g(width, height);

  Graphic_ObstacleGrid *item;

  if ( Grid_.find(g) == Grid_.end() ) {
    item = new Graphic_ObstacleGrid(Base_.getResolution());
    Grid_[g] = item;
    item->setZValue(Z_);
    item->setCacheMode(QGraphicsItem::ItemCoordinateCache);
    positionGrids(g);

    emit newItem((QGraphicsItem*)item);
  } else {
    item = Grid_[g];
  }
  ROS_ASSERT(item != NULL);

  item->Data_ = data;
  item->checkData();
  item->update();
}

void Scene_Obstacles::positionGrids(GridRef_t g)
{
  int x = Base_.getGridPosition_X(g.width);
  int y = Base_.getGridPosition_Y(g.height) - Base_.getResolution();
  Grid_[g]->setPos(x,y);
}

