
#include "Scene_Surface.h"
#include "RenderingBase.h"

#include <QPainter>
#include <ros/ros.h>
#include <QtWidgets>
#include <QDebug>

class Graphic_SurfaceGrid: public QGraphicsItem
{
public:
  Graphic_SurfaceGrid(int resolution):
    Resolution_(resolution)
  {
  }

  QRectF boundingRect() const
  {
      return QRectF(0, 0, Resolution_, Resolution_);
  }

  QPainterPath shape() const
  {
      QPainterPath path;
      path.addRect(0, 0, Resolution_, Resolution_);
      return path;
  }

  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
  {
    Q_UNUSED(widget);
    Q_UNUSED(option);

    QColor fillColor(156, 236, 245);
    painter->fillRect(QRectF(0, 0, Resolution_, Resolution_), fillColor);
  }

  const unsigned int Resolution_;
};

Scene_Surface::Scene_Surface(int z, const RenderingBase & base):
  Z_(z), Base_(base)
{
}

Scene_Surface::~Scene_Surface()
{
}

void Scene_Surface::reposition()
{
  typedef std::map<GridRef_t, Graphic_SurfaceGrid*>::iterator gridIterator;
  for ( gridIterator iterator = Grid_.begin(); iterator != Grid_.end(); iterator++ ) {
    positionGrids(iterator->first);
  }
}

void Scene_Surface::touch(int16_t width, int16_t height)
{
  checkGrid(width, height);
}


void Scene_Surface::updateMapPlot(int16_t width, int16_t height, const std::vector<uint32_t>  & data)
{
  checkGrid(width, height);
}

void Scene_Surface::updatePlot(int16_t width, int16_t height, const std::vector<uint8_t>  & data)
{
  checkGrid(width, height);
}

void Scene_Surface::checkGrid(int16_t width, int16_t height)
{
  GridRef_t g(width, height);

  Graphic_SurfaceGrid *item;

  if ( Grid_.find(g) == Grid_.end() ) {
    item = new Graphic_SurfaceGrid(Base_.getResolution());
    Grid_[g] = item;
    item->setZValue(Z_);
    item->setCacheMode(QGraphicsItem::ItemCoordinateCache);
    positionGrids(g);
    emit newItem((QGraphicsItem*)item);
  }
}

void Scene_Surface::positionGrids(GridRef_t g)
{
  int x = Base_.getGridPosition_X(g.width);
  int y = Base_.getGridPosition_Y(g.height) - Base_.getResolution();
  Grid_[g]->setPos(x,y);
}

