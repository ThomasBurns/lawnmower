#ifndef SCENE_ULTRASONIC_H
#define SCENE_ULTRASONIC_H

#include <QGraphicsItem>
#include <QWidget>
#include <map>
#include <string>

#include <sensor_msgs/Range.h>
#include <geometry_msgs/Twist.h>


class Graphic_Ultrasonic;
class RenderingBase;

class Scene_Ultrasonic: public QWidget
{
Q_OBJECT

public:
  Scene_Ultrasonic(int z, const RenderingBase & base);
  ~Scene_Ultrasonic();

signals:
  void newItem(QGraphicsItem* item);

public slots:
  void toggleView();

private slots:
  void newRange_US(sensor_msgs::Range range, geometry_msgs::Twist pos);

private:
  const int Z_;
  const RenderingBase & Base_;
  bool Visible_;
  std::map<std::string, Graphic_Ultrasonic*> Sensors_;
};

#endif
