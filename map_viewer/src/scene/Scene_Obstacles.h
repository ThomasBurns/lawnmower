#ifndef SCENE_OBSTACLES_H
#define SCENE_OBSTACLES_H

#include <QGraphicsItem>
#include <QWidget>
#include <vector>
#include <stdint.h>
#include <map>
#include <mowerlib/MapUtils.h>

class Graphic_ObstacleGrid;
class RenderingBase;

class Scene_Obstacles: public QWidget
{
Q_OBJECT

public:
  Scene_Obstacles(int z, const RenderingBase & base);
  ~Scene_Obstacles();

signals:
  void newItem(QGraphicsItem* item);

public slots:
  void reposition();
  void updateMapPlot(int16_t width, int16_t height, const std::vector<uint32_t>  & data);

private slots:

private:
  void positionGrids(GridRef_t g);

  const unsigned int Z_;
  std::map<GridRef_t, Graphic_ObstacleGrid*> Grid_;
  const RenderingBase & Base_;
};
#endif
