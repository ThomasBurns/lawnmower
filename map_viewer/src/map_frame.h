
#include <QMainWindow>
#include <QWidget>
#include <QTimer>
#include <QGridLayout>
#include <QTabWidget>

class MasterProxy;
class QAction;
class QMenu;


class MapFrame : public QMainWindow
{
  Q_OBJECT
public:
  MapFrame();
  virtual ~MapFrame();

signals:

private slots:

private:
  void createActions();
  void createMenus();

  QMenu   *FileMenu_;
  QMenu   *ViewMenu_;

  QAction *ExitAct_;
  QAction *ViewIrAct_;
  QAction *ViewUsAct_;
  QAction *ViewLsAct_;

  MasterProxy  *MasterProxy_;

  QGridLayout MainLayout_;
  QTabWidget  TabWidget;
};
