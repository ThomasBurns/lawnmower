
#include "MotorTuning.h"
#include <qt_proxy/ControlProxy.h>

#include <QDebug>
#include <QTimer>

MotorTuning::MotorTuning(QObject *parent):
  QObject(parent),  LinRmsError_(1e-9),
  AngRmsError_(1e-9),RmsCounter_(0.0)
{
  ControlProxy_  = new ControlProxy(this);
  SystemTimer_ = new QTimer(this);
  SystemTimer_->stop();
  connect(SystemTimer_,       SIGNAL(timeout()), this, SLOT(timer()));

  connect(ControlProxy_, SIGNAL(systemReady()), this, SLOT(systemReady()));
  connect(ControlProxy_, SIGNAL(feedback(float,float)), this, SLOT(feedback(float,float)));

  ControlProxy_->connect();
}

void MotorTuning::startOperation(int cycles, float time, float linVel, float angVel)
{
  CycleCounter_ = cycles;
  SystemTimer_->setInterval(time * 1000.0);
  SystemTimer_->start();
  ControlProxy_->setSpeedLimits(linVel, angVel);
  ControlProxy_->newTargetAngle(0.0);
}

void MotorTuning::timer(void)
{
  float linRmsError = sqrt(LinRmsError_/RmsCounter_);
  float angRmsError = sqrt(AngRmsError_/RmsCounter_);

  emit ActionStep(CycleCounter_, linRmsError, angRmsError);
  LinRmsError_ = 1e-9;
  AngRmsError_ = 1e-9;
  RmsCounter_ = 0.0;

  CycleCounter_--;
  if ( CycleCounter_ < 0 ) {
    ControlProxy_->newTargetAngle(0.0);
    SystemTimer_->stop();
    emit ActionComplete();

  } else {
    float angle = (CycleCounter_&1)? 0.78: -0.78;
    ControlProxy_->newTargetAngle(angle);
  }
}

void MotorTuning::systemReady()
{
  emit ActionSystemReady();
}

void MotorTuning::feedback(float linErr, float angErr)
{
  LinRmsError_ += (linErr*linErr);
  AngRmsError_ += (angErr*angErr);
  RmsCounter_ += 1.0;

  emit ActionFeedback(linErr, angErr);
}

float MotorTuning::getMaxLineraVelocity() const
{
  return ControlProxy_->getMaxLineraVelocity();
}

float MotorTuning::getMaxAngularVelocity() const
{
  return ControlProxy_->getMaxAngularVelocity();
}

