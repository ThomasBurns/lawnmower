/*
 * Copyright (c) 2009, Willow Garage, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Willow Garage, Inc. nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <QApplication>

#include <ros/ros.h>
#include "map_frame.h"
#include <stdint.h>
#include <string>
#include <vector>

#include <geometry_msgs/Point.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Range.h>
#include <sensor_msgs/LaserScan.h>
#include <qt_proxy/MapProxy.h>


class MapViewer : public QApplication
{
public:
  ros::NodeHandlePtr nh_;

  MapViewer(int& argc, char** argv)
    : QApplication(argc, argv)
  {
    ros::init(argc, argv, "Map_Viewer", ros::init_options::NoSigintHandler);
    nh_.reset(new ros::NodeHandle);

    qRegisterMetaType<int8_t> ("int8_t");
    qRegisterMetaType<int16_t>("int16_t");
    qRegisterMetaType<int32_t>("int32_t");

    qRegisterMetaType<uint8_t> ("uint8_t");
    qRegisterMetaType<uint16_t>("uint16_t");
    qRegisterMetaType<uint32_t>("uint32_t");

    qRegisterMetaType<std::string>("std::string");
    qRegisterMetaType<std::vector<uint8_t> > ("std::vector<uint8_t>");
    qRegisterMetaType<std::vector<uint32_t> >("std::vector<uint32_t>");

    qRegisterMetaType<WayPointList_t >("WayPointList_t");
    qRegisterMetaType<sensor_msgs::Range>("sensor_msgs::Range");
    qRegisterMetaType<geometry_msgs::Point>("geometry_msgs::Point");
    qRegisterMetaType<geometry_msgs::Twist>("geometry_msgs::Twist");
    qRegisterMetaType<sensor_msgs::LaserScan>("sensor_msgs::LaserScan");
  }

  int exec()
  {
    MapFrame frame;

    ros::AsyncSpinner spinner(0);
    frame.show();
    spinner.start();
    return QApplication::exec();
  }
};

int main(int argc, char** argv)
{
  MapViewer app(argc, argv);
  return app.exec();
}

