
#include "RenderingBase.h"

#include <ros/ros.h>

RenderingBase::RenderingBase():
  Resolution_(50),
  MinWidth_(0),  MaxWidth_(0),
  MinHeight_(0), MaxHeight_(0)
{
  MinWidth_ =  MinHeight_ = 0.0;
  MaxWidth_ = MaxHeight_ = 2.0;
}

int RenderingBase::getGridPosition_X(int x) const
{
  return fabs(x - MinWidth_) * Resolution_;
}

int RenderingBase::getGridPosition_Y(int y) const
{
  int grid = fabs(MaxHeight_ - MinHeight_) * Resolution_;
  return grid - (fabs(y - MinHeight_) * Resolution_);
}

int RenderingBase::convertPosToGrid_X(float x) const
{
  double intPart, fractPart;
  fractPart = modf(x, &intPart);
  return getGridPosition_X(x) + (fractPart * Resolution_);
}

int RenderingBase::convertPosToGrid_Y(float y) const
{
  double intPart, fractPart;

  fractPart = modf(y, &intPart);
  return getGridPosition_Y(intPart) - (fractPart * Resolution_);
}

void RenderingBase::mapDimensionsChanged(float minWidth, float maxWidth, float minHeight, float maxHeight)
{
  MinWidth_ = minWidth;
  MaxWidth_ = maxWidth;
  MinHeight_ = minHeight;
  MaxHeight_ = maxHeight;
}

int RenderingBase::convertStartAngle_Degrees(float degrees) const
{
  return degrees * 16;
}

void RenderingBase::resolutionChanged(int resolution)
{
//  Resolution_ = resolution;
}

int RenderingBase::getResolution() const
{
  return Resolution_;
}
