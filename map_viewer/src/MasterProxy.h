#ifndef MASTERPROXY_H
#define MASTERPROXY_H

#include <QObject>

class QTimer;

class ImuProxy;
class MapProxy;
class OdometryProxy;
class PosConProxy;
class StateProxy;
class RangeProxy;
class LaserProxy;

typedef struct {
  const ImuProxy      & imu;
  const MapProxy      & map;
  const OdometryProxy & odom;
  const PosConProxy   & posCon;
  const StateProxy    & state;
  const RangeProxy    & range;
  const LaserProxy    & laser;

} SubModules_t;


class MasterProxy: QObject
{
  Q_OBJECT

public:
  MasterProxy(QObject *parent = 0);

  void connect();
  SubModules_t getSubModules() const;

signals:

public slots:

private slots:
  void checkServices();

private:
  QTimer *ServiceTimer_;

  StateProxy    * StateProxy_;
  MapProxy      * MapProxy_;
  ImuProxy      * ImuProxy_;
  PosConProxy   * PosConProxy_;
  OdometryProxy * OdometryProxy_;
  RangeProxy    * RangeProxy_;
  LaserProxy    * LaserProxy_;

};


#endif
