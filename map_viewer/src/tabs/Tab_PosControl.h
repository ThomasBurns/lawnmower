#ifndef TAB_POS_CONTROL_H
#define TAB_POS_CONTROL_H

#include <QWidget>

#include "MasterProxy.h"

class QDoubleSpinBox;
class QPushButton;
class QLabel;
class QComboBox;

class MotorTuning;

class Tab_PosControl : public QWidget
{
  Q_OBJECT

public:
  Tab_PosControl(SubModules_t submodules, QWidget* parent = 0);
  virtual ~Tab_PosControl();

signals:
  void updateParamater(int par, float v);

public slots:
  void newAngularPID(float p, float i, float d);

private slots:
  void angP_changed(double v);
  void angI_changed(double v);
  void angD_changed(double v);
  void startClicked(bool clicked);

  void ActionSystemReady();
  void ActionFeedback(float linErr, float angErr);
  void StepComplete(int remaining, float linRmsErr, float angRmsErr);
  void ActionComplete();


private:
  void enableWidgets();
  void disableWidgets();

  QDoubleSpinBox *AngPID[3];
//  QDoubleSpinBox *LinPID[3];

  QComboBox      *TestTypeComboBox_;
  QPushButton    *StartButton_;
  QDoubleSpinBox *CyclesSpinBox_;
  QDoubleSpinBox *CycleTimeSpinBox_;
  QDoubleSpinBox *LinearSpeedSpinBox_;
  QDoubleSpinBox *AngularSpeedSpinBox_;
  QLabel *CycleNumber_;
  QLabel *CycleError_;
  QLabel *CycleRms_;

  MotorTuning *MotorTuning_;
};


#endif
