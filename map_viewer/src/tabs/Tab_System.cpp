#include "Tab_System.h"

#include <qt_proxy/StateProxy.h>
#include <qt_proxy/MapProxy.h>
#include <qt_widget/CompassWidget.h>
#include <qt_widget/HorizontalBarGraph.h>
#include <qt_widget/VerticalBarGraph.h>

#include <mowerlib/Trigonometry.h>

#include <QGridLayout>
#include <QComboBox>
#include <QLabel>

Tab_System::Tab_System(SubModules_t submodules, QWidget* parent):
  QWidget(parent),
  StateProxy_(submodules.state),
  MapProxy_(submodules.map)
{
  QLabel *label;
  // --------------------------------------------------
  // Robot Map Data layout

  lbl_RealPos = new QLabel(this);
  lbl_TargetPos = new QLabel(this);
  lbl_RealTheta = new QLabel(this);
  lbl_TargetTheta = new QLabel(this);
  lbl_Speed = new QLabel(this);
  lbl_WpSize = new QLabel(this);
  lbl_WpDist = new QLabel(this);
  lbl_TotalDist = new QLabel(this);
  lbl_SessionDist = new QLabel(this);


  QGridLayout *mainLayout = new QGridLayout(this);

  Compass_ = new CompassWidget(this);
  HGraph_  = new HorizontalBarGraph(this);
  VGraph_  = new VerticalBarGraph(this);
  HGraph_->setLimits(-1.01, 1.011);
  VGraph_->setLimits(-1.0, 1.0);


  cb_SysState = new QComboBox(this);
  cb_SysState->insertItem(StateProxy::md_Idle,   "Idle");
  cb_SysState->insertItem(StateProxy::md_Manual, "Manual");
  cb_SysState->insertItem(StateProxy::md_Mowing, "Mowing");
  cb_SysState->insertItem(StateProxy::md_Mapping,"Mapping");

  mainLayout->setColumnStretch( 6, 10);
  mainLayout->setRowStretch(9, 10);

  label = new QLabel("Real");
  mainLayout->addWidget(label, 0, 1);
  label = new QLabel("Target");
  mainLayout->addWidget(label, 0, 2);

  label = new QLabel("Position");
  mainLayout->addWidget(label, 1, 0);
  mainLayout->addWidget(lbl_RealPos, 1, 1);
  mainLayout->addWidget(lbl_TargetPos, 1, 2);

  label = new QLabel("Bearing");
  mainLayout->addWidget(label, 2, 0);
  mainLayout->addWidget(lbl_RealTheta, 2, 1);
  mainLayout->addWidget(lbl_TargetTheta, 2, 2);

  label = new QLabel("Speed");
  mainLayout->addWidget(label, 3, 0);
  mainLayout->addWidget(lbl_Speed, 3, 1, 1, 2);

  label = new QLabel("System State");
  mainLayout->addWidget(label, 4, 0);
  mainLayout->addWidget(cb_SysState, 4, 1, 1, 2);

  label = new QLabel("#");
  mainLayout->addWidget(label, 5, 1);
  label = new QLabel("Distance");
  mainLayout->addWidget(label, 5, 2);

  label = new QLabel("Waypoints");
  mainLayout->addWidget(label, 6, 0);
  mainLayout->addWidget(lbl_WpSize, 6, 1);
  mainLayout->addWidget(lbl_WpDist, 6, 2);

  label = new QLabel("Total");
  mainLayout->addWidget(label, 7, 1);
  label = new QLabel("Session");
  mainLayout->addWidget(label, 7, 2);
  label = new QLabel("Odometer");
  mainLayout->addWidget(label, 8, 0);
  mainLayout->addWidget(lbl_TotalDist, 8, 1);
  mainLayout->addWidget(lbl_SessionDist, 8, 2);

  mainLayout->addWidget(Compass_, 0, 6, 6, 1);
  mainLayout->addWidget(HGraph_ , 7, 6, 3, 1);
  mainLayout->addWidget(VGraph_ , 0, 5, 6, 1);

  setLayout(mainLayout);

  connect(cb_SysState, SIGNAL(activated(int )), &StateProxy_, SLOT(sysStateChanged(int)));
  connect(&StateProxy_, SIGNAL(newRoboMowerState(int)), cb_SysState, SLOT(setCurrentIndex(int)));

  connect(&StateProxy_, SIGNAL(newRealVelocity(geometry_msgs::Twist)),     this, SLOT(newRealVelocity(geometry_msgs::Twist)));
  connect(&StateProxy_, SIGNAL(newTargetVelocity(geometry_msgs::Twist)),   this, SLOT(newTargetVelocity(geometry_msgs::Twist)));

  connect(&StateProxy_, SIGNAL(newRealPosition(geometry_msgs::Twist)),     this, SLOT(newRealPosition(geometry_msgs::Twist)));
  connect(&StateProxy_, SIGNAL(newTargetPoint(geometry_msgs::Point)),      this, SLOT(newTargetPoint(geometry_msgs::Point)));

  connect(&MapProxy_, SIGNAL(newWaypointList(WayPointList_t)),             this, SLOT(newWaypointList(WayPointList_t)));
}

Tab_System::~Tab_System()
{
  disconnect(cb_SysState, SIGNAL(activated(int )), &StateProxy_, SLOT(sysStateChanged(int)));
  disconnect(&StateProxy_, SIGNAL(newRoboMowerState(int)), cb_SysState, SLOT(setCurrentIndex(int)));
}

void Tab_System::newDistance(float total, float session)
{
  char buff[128];

  if ( total >= 2000.0 ) {
    snprintf(buff, sizeof(buff), "%1.1f km", total / 1000.0 );
  } else {
    snprintf(buff, sizeof(buff), "%1.1f m", total);
  }
  lbl_TotalDist->setText(buff);

  if ( session >= 2000.0 ) {
    snprintf(buff, sizeof(buff), "%1.1f km", session / 1000.0 );
  } else {
    snprintf(buff, sizeof(buff), "%1.1f m", session);
  }
  lbl_SessionDist->setText(buff);
}

void Tab_System::load()
{

}

void Tab_System::newRealVelocity(geometry_msgs::Twist vel)
{
  char buff[128];
  snprintf(buff, sizeof(buff), "%2.2f:%2.2f", vel.linear.x , vel.angular.z );
  lbl_Speed->setText(buff);
  HGraph_->setValue(vel.angular.z);
  VGraph_->setValue(vel.linear.x);
}

void Tab_System::newTargetVelocity(geometry_msgs::Twist vel)
{
  HGraph_->setTarget(vel.angular.z);
  VGraph_->setTarget(vel.linear.x);
}

void Tab_System::newRealPosition(geometry_msgs::Twist pos)
{
  char buff[128];
  Position_ = pos;
  snprintf(buff, sizeof(buff), "%2.2f:%2.2f", Position_.linear.x ,Position_.linear.y );
  lbl_RealPos->setText(buff);

  float yaw = Trig_LimitRadians(Position_.angular.z);
  yaw = Trig_RadiansToDegrees(yaw);
  Compass_->setBearing(round(yaw));

  snprintf(buff, sizeof(buff), "%2.1f", yaw );
  lbl_RealTheta->setText(buff);
  updateTargetAngle();
}

void Tab_System::newTargetPoint(geometry_msgs::Point pos)
{
  char buff[128];
  Target_ = pos;
  snprintf(buff, sizeof(buff), "%2.2f:%2.2f", Target_.x , Target_.y );
  lbl_TargetPos->setText(buff);
  updateTargetAngle();
}

void Tab_System::updateTargetAngle()
{
  float yaw = Trig_CalcTargetAngle(Position_, Target_);
  yaw = Trig_RadiansToDegrees(yaw);
  Compass_->setTarget(round(yaw));

  char buff[128];
  snprintf(buff, sizeof(buff), "%2.1f", yaw );
  lbl_TargetTheta->setText(buff);
}

void Tab_System::newWaypointList(WayPointList_t list)
{
  char buff[128];
  lbl_WpSize->setText(QString::number(list.size()));

  WayPoint start;
  start.X = Position_.linear.x;
  start.Y = Position_.linear.y;

  snprintf(buff, sizeof(buff), "%1.2f", WayPoint_Sum(start, list) );
  lbl_WpDist->setText(buff);
}

