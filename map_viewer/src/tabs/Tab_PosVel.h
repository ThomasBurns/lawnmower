#ifndef TAB_POS_VEL_H
#define TAB_POS_VEL_H

#include "MasterProxy.h"

#include <geometry_msgs/Twist.h>

#include <QWidget>

class QLabel;

class Tab_PosVel : public QWidget
{
  Q_OBJECT

public:
  Tab_PosVel(SubModules_t submodules, QWidget* parent = 0);
  virtual ~Tab_PosVel();

signals:

public slots:

private slots:
  void newRealVelocity(geometry_msgs::Twist vel);
  void newRealPosition(geometry_msgs::Twist pos);


private:
  QLabel *lbl_Pos[6];
  QLabel *lbl_Vel[6];
};



#endif
