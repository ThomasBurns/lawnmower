#ifndef TAB_SYSTEM_H
#define TAB_SYSTEM_H

#include "MasterProxy.h"
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Point.h>
#include <mowerlib/WayPoint.h>

#include <QWidget>

class CompassWidget;
class HorizontalBarGraph;
class VerticalBarGraph;

class QComboBox;
class QLabel;

class Tab_System : public QWidget
{
  Q_OBJECT

public:
  Tab_System(SubModules_t submodules, QWidget* parent = 0);
  virtual ~Tab_System();

  void load();

signals:

public slots:
  void newDistance(float total, float session);

private slots:
  void newRealVelocity(geometry_msgs::Twist vel);
  void newTargetVelocity(geometry_msgs::Twist vel);

  void newRealPosition(geometry_msgs::Twist pos);
  void newTargetPoint(geometry_msgs::Point pos);

  void newWaypointList(WayPointList_t list);


private:
  void updateTargetAngle();

  const StateProxy & StateProxy_;
  const MapProxy   & MapProxy_;

  CompassWidget *Compass_;
  HorizontalBarGraph *HGraph_;
  VerticalBarGraph   *VGraph_;

  geometry_msgs::Twist Position_;
  geometry_msgs::Point Target_;

  QLabel *lbl_RealPos;
  QLabel *lbl_TargetPos;
  QLabel *lbl_RealTheta;
  QLabel *lbl_TargetTheta;
  QLabel *lbl_Speed;
  QLabel *lbl_WpSize;
  QLabel *lbl_WpDist;
  QLabel *lbl_TotalDist;
  QLabel *lbl_SessionDist;
  QComboBox *cb_SysState;
};

#endif
