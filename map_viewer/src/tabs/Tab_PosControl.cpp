
#include "Tab_PosControl.h"
#include "MotorTuning.h"

#include <QLabel>
#include <QGridLayout>
#include <QDoubleSpinBox>
#include <QPushButton>
#include <QComboBox>

#include <QDebug>

const char *LabelNames[3] = {
  "P", "I", "D"
};

Tab_PosControl::Tab_PosControl(SubModules_t submodules, QWidget* parent):
  QWidget(parent)
{
  (void) submodules;
  QGridLayout *mainLayout = new QGridLayout(this);
  MotorTuning_ = new MotorTuning(this);

  QLabel *label;
  label = new QLabel("Angular PID", this);
  mainLayout->addWidget(label, 0, 1);

  LinearSpeedSpinBox_ = new QDoubleSpinBox(this);
  LinearSpeedSpinBox_->setMaximum(MotorTuning_->getMaxLineraVelocity());
  LinearSpeedSpinBox_->setValue(MotorTuning_->getMaxLineraVelocity());
  LinearSpeedSpinBox_->setMinimum(0.0);
  LinearSpeedSpinBox_->setDecimals(2);
  LinearSpeedSpinBox_->setSingleStep(0.01);
  LinearSpeedSpinBox_->setEnabled(true);
  AngularSpeedSpinBox_ = new QDoubleSpinBox(this);
  AngularSpeedSpinBox_->setMaximum(MotorTuning_->getMaxAngularVelocity());
  AngularSpeedSpinBox_->setValue(MotorTuning_->getMaxAngularVelocity());
  AngularSpeedSpinBox_->setMinimum(0.0);
  AngularSpeedSpinBox_->setDecimals(2);
  AngularSpeedSpinBox_->setSingleStep(0.01);
  AngularSpeedSpinBox_->setEnabled(true);


  for ( int i = 0; i < 3; i ++ ) {
    AngPID[i] = new QDoubleSpinBox(this);
    AngPID[i]->setMaximum(10.0);
    AngPID[i]->setMinimum(0.0);
    AngPID[i]->setDecimals(2);
    AngPID[i]->setSingleStep(0.01);
    AngPID[i]->setEnabled(false);

    label = new QLabel(LabelNames[i], this);
    mainLayout->addWidget(label,     i+1, 0);
    mainLayout->addWidget(AngPID[i], i+1, 1);

  }
  label = new QLabel("Position PID", this);
  mainLayout->addWidget(label, 4, 1);

  CycleTimeSpinBox_ = new QDoubleSpinBox(this);
  CycleTimeSpinBox_->setMaximum(30.0);
  CycleTimeSpinBox_->setMinimum( 5.0);
  CycleTimeSpinBox_->setDecimals(0);
  CycleTimeSpinBox_->setSingleStep(1.0);

  StartButton_ = new QPushButton("Start", this);
  CyclesSpinBox_ = new QDoubleSpinBox(this);
  CyclesSpinBox_->setMaximum(50.0);
  CyclesSpinBox_->setMinimum( 2.0);
  CyclesSpinBox_->setValue(5.0);
  CyclesSpinBox_->setDecimals(0);
  CyclesSpinBox_->setSingleStep(1.0);

  CycleNumber_   = new QLabel("N/A", this);
  CycleError_    = new QLabel("N/A", this);
  CycleRms_      = new QLabel("N/A", this);

  TestTypeComboBox_ = new QComboBox(this);
  TestTypeComboBox_->insertItem(0,   "Anglular");
//  TestTypeComboBox_->insertItem(1,   "Position");

  mainLayout->addWidget(new QLabel("Run Test", this),    0, 2, 1, 2);
  mainLayout->addWidget(new QLabel("Num Cycles", this),  1, 2);
  mainLayout->addWidget(CyclesSpinBox_,                  1, 3);
  mainLayout->addWidget(new QLabel("Cycles Time", this), 2, 2);
  mainLayout->addWidget(CycleTimeSpinBox_  ,             2, 3);

  mainLayout->addWidget(new QLabel("Test Type", this), 3, 2);
  mainLayout->addWidget(TestTypeComboBox_            , 3, 3);

  mainLayout->addWidget(StartButton_,                     4, 2, 1, 2);
  mainLayout->addWidget(new QLabel("# Remaining:", this), 5, 2);
  mainLayout->addWidget(CycleNumber_,                     5, 3);
  mainLayout->addWidget(new QLabel("Cycle Error:", this), 6, 2);
  mainLayout->addWidget(CycleError_,                      6, 3);
  mainLayout->addWidget(new QLabel("RMS Error:", this),   7, 2);
  mainLayout->addWidget(CycleRms_,                        7, 3);

  mainLayout->addWidget(new QLabel("Linear Speed:", this),   1, 4);
  mainLayout->addWidget(LinearSpeedSpinBox_,                 1, 5);
  mainLayout->addWidget(new QLabel("Linear Speed:", this),   2, 4);
  mainLayout->addWidget(AngularSpeedSpinBox_,                2, 5);

  mainLayout->setColumnStretch( 100, 100);
  mainLayout->setRowStretch(100, 100);
  setLayout(mainLayout);

  connect(StartButton_, SIGNAL(clicked(bool)), this, SLOT(startClicked(bool)));
  connect(MotorTuning_, SIGNAL(ActionSystemReady()), this, SLOT(ActionSystemReady()));
  connect(MotorTuning_, SIGNAL(ActionComplete()), this, SLOT(ActionComplete()));
  connect(MotorTuning_, SIGNAL(ActionFeedback(float,float)), this, SLOT(ActionFeedback(float,float)));
  connect(MotorTuning_, SIGNAL(ActionStep(int,float,float)), this, SLOT(StepComplete(int,float,float)));

  connect(AngPID[0], SIGNAL(valueChanged(double)), this, SLOT(angP_changed(double)));
  connect(AngPID[1], SIGNAL(valueChanged(double)), this, SLOT(angI_changed(double)));
  connect(AngPID[2], SIGNAL(valueChanged(double)), this, SLOT(angD_changed(double)));
  disableWidgets();
}

Tab_PosControl::~Tab_PosControl()
{
  disconnect(AngPID[0], SIGNAL(valueChanged(double)), this, SLOT(angP_changed(double)));
  disconnect(AngPID[1], SIGNAL(valueChanged(double)), this, SLOT(angI_changed(double)));
  disconnect(AngPID[2], SIGNAL(valueChanged(double)), this, SLOT(angD_changed(double)));
}

void Tab_PosControl::newAngularPID(float p, float i, float d)
{
  AngPID[0]->setEnabled(true);
  AngPID[1]->setEnabled(true);
  AngPID[2]->setEnabled(true);

  AngPID[0]->setValue(p);
  AngPID[1]->setValue(i);
  AngPID[2]->setValue(d);
}

void Tab_PosControl::angP_changed(double v)
{
  emit updateParamater(0, v);
}

void Tab_PosControl::angI_changed(double v)
{
  emit updateParamater(1, v);
}
void Tab_PosControl::angD_changed(double v)
{
  emit updateParamater(2, v);
}

void Tab_PosControl::ActionSystemReady()
{
  enableWidgets();
}

void Tab_PosControl::startClicked(bool clicked)
{
  (void) clicked;
  int numCycles = CyclesSpinBox_->value();
  float cycleTime = CycleTimeSpinBox_->value();
  float linVel = LinearSpeedSpinBox_->value();
  float angVel = AngularSpeedSpinBox_->value();
  MotorTuning_->startOperation(numCycles, cycleTime, linVel, angVel);

  disableWidgets();
}

void Tab_PosControl::ActionFeedback(float linErr, float angErr)
{
  char buff[128];
  snprintf(buff, sizeof(buff), "%2.2f:%0.3f", linErr, angErr);
  CycleError_->setText(buff);
}

void Tab_PosControl::StepComplete(int remaining, float linRmsErr, float angRmsErr)
{
  CycleNumber_->setText(QString::number(remaining));

  char buff[128];
  snprintf(buff, sizeof(buff), "%2.2f:%0.3f", linRmsErr, angRmsErr);
  CycleError_->setText(buff);
  CycleRms_->setText(buff);
}

void Tab_PosControl::ActionComplete()
{
  enableWidgets();
}

void Tab_PosControl::enableWidgets()
{
  CycleTimeSpinBox_->setEnabled(true);
  CyclesSpinBox_->setEnabled(true);
  StartButton_->setEnabled(true);
}

void Tab_PosControl::disableWidgets()
{
  CycleTimeSpinBox_->setEnabled(false);
  CyclesSpinBox_->setEnabled(false);
  StartButton_->setEnabled(false);
}



