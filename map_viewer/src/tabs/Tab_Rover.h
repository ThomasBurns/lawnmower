#ifndef TAB_ROVER_H
#define TAB_ROVER_H

#include <QWidget>

#include "MasterProxy.h"

class Tab_Rover : public QWidget
{
  Q_OBJECT
  
public:
  Tab_Rover(SubModules_t submodules, QWidget* parent = 0);
  virtual ~Tab_Rover();

  void load();

public slots:  

private:

};

#endif
