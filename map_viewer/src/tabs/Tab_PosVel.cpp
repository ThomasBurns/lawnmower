
#include "Tab_PosVel.h"
#include <qt_proxy/StateProxy.h>

#include <mowerlib/Trigonometry.h>

#include <QFormLayout>
#include <QLabel>

Tab_PosVel::Tab_PosVel(SubModules_t submodules, QWidget* parent):
  QWidget(parent)
{
  QFormLayout *form = new QFormLayout(this);

  for ( int i = 0; i < 6; i ++ ) {
    lbl_Pos[i] = new QLabel(this);
    lbl_Vel[i] = new QLabel(this);
  }
  
  form->insertRow(0, "Position", (QWidget*)NULL);
  form->insertRow(1, "X:",     lbl_Pos[0]);
  form->insertRow(2, "Y:",     lbl_Pos[1]);
  form->insertRow(3, "Z:",     lbl_Pos[2]);
  form->insertRow(4, "Roll:",  lbl_Pos[3]);
  form->insertRow(5, "Pitch:", lbl_Pos[4]);
  form->insertRow(6, "Yaw:",   lbl_Pos[5]);

  form->insertRow(7,  "Velocity", (QWidget*)NULL);
  form->insertRow(8,  "X:",     lbl_Vel[0]);
  form->insertRow(9,  "Y:",     lbl_Vel[1]);
  form->insertRow(10, "Z:",     lbl_Vel[2]);
  form->insertRow(11, "Roll:",  lbl_Vel[3]);
  form->insertRow(12, "Pitch:", lbl_Vel[4]);
  form->insertRow(13, "Yaw:",   lbl_Vel[5]);

  setLayout(form);

  connect(&submodules.state, SIGNAL(newRealVelocity(geometry_msgs::Twist)),     this, SLOT(newRealVelocity(geometry_msgs::Twist)));
  connect(&submodules.state, SIGNAL(newRealPosition(geometry_msgs::Twist)),     this, SLOT(newRealPosition(geometry_msgs::Twist)));


}

Tab_PosVel::~Tab_PosVel()
{
}

void Tab_PosVel::newRealVelocity(geometry_msgs::Twist vel)
{
  char buf[128];
  float angle;

  snprintf(buf, sizeof(buf), "%1.3f", vel.linear.x);
  lbl_Vel[0]->setText(buf);
  snprintf(buf, sizeof(buf), "%1.3f", vel.linear.y);
  lbl_Vel[1]->setText(buf);
  snprintf(buf, sizeof(buf), "%1.3f", vel.linear.z);
  lbl_Vel[2]->setText(buf);

  angle = Trig_RadiansToDegrees(Trig_LimitRadians(vel.angular.x));
  snprintf(buf, sizeof(buf), "%1.3f", angle );
  lbl_Vel[3]->setText(buf);

  angle = Trig_RadiansToDegrees(Trig_LimitRadians(vel.angular.y));
  snprintf(buf, sizeof(buf), "%1.3f", angle );
  lbl_Vel[4]->setText(buf);

  angle = Trig_RadiansToDegrees(Trig_LimitRadians(vel.angular.z));
  snprintf(buf, sizeof(buf), "%1.3f", angle );
  lbl_Vel[5]->setText(buf);
}

void Tab_PosVel::newRealPosition(geometry_msgs::Twist pos)
{
  char buf[128];
  double angle;

  snprintf(buf, sizeof(buf), "%1.2f", pos.linear.x);
  lbl_Pos[0]->setText(buf);
  snprintf(buf, sizeof(buf), "%1.2f", pos.linear.y);
  lbl_Pos[1]->setText(buf);
  snprintf(buf, sizeof(buf), "%1.2f", pos.linear.z);
  lbl_Pos[2]->setText(buf);

  angle = Trig_RadiansToDegrees(Trig_LimitRadians(pos.angular.x));
  snprintf(buf, sizeof(buf), "%3.1f", angle );
  lbl_Pos[3]->setText(buf);

  angle = Trig_RadiansToDegrees(Trig_LimitRadians(pos.angular.y));
  snprintf(buf, sizeof(buf), "%3.1f", angle );
  lbl_Pos[4]->setText(buf);

  angle = Trig_RadiansToDegrees(Trig_LimitRadians(pos.angular.z));
  snprintf(buf, sizeof(buf), "%3.1f", angle );
  lbl_Pos[5]->setText(buf);
}

