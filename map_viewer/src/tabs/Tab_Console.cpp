
#include "Tab_Console.h"

#include "Scene_Dimensions.h"
#include "Scene_IR.h"
#include "Scene_Obstacles.h"
#include "Scene_Surface.h"
#include "Scene_Position.h"
#include "Scene_Travelled.h"
#include "Scene_Ultrasonic.h"
#include "Scene_Waypoint.h"
#include "Scene_Laser.h"

#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QLabel>
#include <QCheckBox>
#include <QGridLayout>
#include <QDebug>

#define RENDER_AREA_SIZE   600

typedef enum {
  drawSurface,
  drawObstacle,
  drawTravelled,
  drawDimensions,
  drawWaypoint,
  drawUltraSonic,
  drawLaser,
  drawIR,
  drawPosition,

  drawSize

} DrawOrder_t;

Tab_Console::Tab_Console(MasterProxy *proxy, QWidget * parent):
  QGraphicsView(parent),
  MasterProxy_(proxy)
{
  setAlignment((Qt::AlignLeft | Qt::AlignTop));
  Scene_ = new QGraphicsScene();
  setScene(Scene_);
  setSceneRect(QRectF(0, 0, RENDER_AREA_SIZE, RENDER_AREA_SIZE));

  SubModules_t sm = MasterProxy_->getSubModules();

  SceneDimensions_ = new Scene_Dimensions(drawDimensions, RBase_);
  SceneIR_         = new Scene_IR        (drawIR,         RBase_);
  SceneObstacles_  = new Scene_Obstacles (drawObstacle,   RBase_);
  ScenePosition_   = new Scene_Position  (drawPosition,   RBase_);
  SceneSurface_    = new Scene_Surface   (drawSurface,    RBase_);
  SceneTravelled_  = new Scene_Travelled (drawTravelled,  RBase_);
  SceneUltrasonic_ = new Scene_Ultrasonic(drawUltraSonic, RBase_);
  SceneLaser_      = new Scene_Laser     (drawLaser,      RBase_);
  SceneWaypoint_   = new Scene_Waypoint  (drawWaypoint,   RENDER_AREA_SIZE, RBase_);

  Scene_->addItem(ScenePosition_->getItem());
  Scene_->addItem(SceneWaypoint_->getItem());
  Scene_->addItem(SceneDimensions_->getItem());
  Scene_->addItem(SceneLaser_->getItem());

  connect(SceneIR_,         SIGNAL(newItem(QGraphicsItem*)),
          this,             SLOT  (newItem(QGraphicsItem*)));
  connect(SceneObstacles_,  SIGNAL(newItem(QGraphicsItem*)),
          this,             SLOT  (newItem(QGraphicsItem*)));
  connect(SceneTravelled_,  SIGNAL(newItem(QGraphicsItem*)),
          this,             SLOT  (newItem(QGraphicsItem*)));
  connect(SceneUltrasonic_, SIGNAL(newItem(QGraphicsItem*)),
          this,             SLOT  (newItem(QGraphicsItem*)));
  connect(SceneSurface_,    SIGNAL(newItem(QGraphicsItem*)),
          this,             SLOT  (newItem(QGraphicsItem*)));

  connect((QObject *)&sm.map,   SIGNAL(resolutionChanged(int)), &RBase_, SLOT(resolutionChanged(int)));
  connect((QObject *)&sm.map,   SIGNAL(mapDimensionsChanged(float,float,float,float)), &RBase_, SLOT(mapDimensionsChanged(float,float,float,float)));
  connect((QObject *)&sm.map,   SIGNAL(mapDimensionsChanged(float,float,float,float)),  this,   SLOT(mapDimensionsChanged(float,float,float,float)));

  connect((QObject *)&sm.map,   SIGNAL(updateTravelPlot(int16_t,int16_t,const std::vector<uint8_t>&)),
          SceneTravelled_,      SLOT(updatePlot(int16_t,int16_t,const std::vector<uint8_t>&)));
  connect((QObject *)&sm.map,   SIGNAL(updateTravelPlot(int16_t,int16_t,const std::vector<uint8_t>&)),
          SceneSurface_,        SLOT(updatePlot(int16_t,int16_t,const std::vector<uint8_t>&)));
  connect((QObject *)&sm.map,   SIGNAL(updateMapPlot(int16_t,int16_t,const std::vector<uint32_t>&)),
          SceneSurface_,        SLOT(updateMapPlot(int16_t,int16_t,const std::vector<uint32_t>&)));
  connect((QObject *)&sm.map,   SIGNAL(updateMapPlot(int16_t,int16_t,const std::vector<uint32_t>&)),
          SceneObstacles_,      SLOT(updateMapPlot(int16_t,int16_t,const std::vector<uint32_t>&)));
  connect((QObject *)&sm.map,   SIGNAL(newWaypointList(WayPointList_t)),
          SceneWaypoint_,       SLOT(newWaypointList(WayPointList_t)));

  connect((QObject *)&sm.range, SIGNAL(newRange_IR(sensor_msgs::Range, geometry_msgs::Twist)),
           SceneIR_,            SLOT  (newRange_IR(sensor_msgs::Range, geometry_msgs::Twist)));
  connect((QObject *)&sm.range, SIGNAL(newRange_US(sensor_msgs::Range, geometry_msgs::Twist)),
           SceneUltrasonic_,    SLOT  (newRange_US(sensor_msgs::Range, geometry_msgs::Twist)));

//  void newReading(geometry_msgs::Twist pos,  sensor_msgs::LaserScan msg);
  connect((QObject *)&sm.laser, SIGNAL(newReading(geometry_msgs::Twist, sensor_msgs::LaserScan)),
           SceneLaser_,         SLOT  (newReading(geometry_msgs::Twist, sensor_msgs::LaserScan)));

  connect((QObject *)&sm.state, SIGNAL(newRealPosition(geometry_msgs::Twist)),
          ScenePosition_,       SLOT(newRealPosition(geometry_msgs::Twist)));

  connect((QObject *)&sm.state, SIGNAL(newRealPosition(geometry_msgs::Twist)),
          SceneWaypoint_,       SLOT(newRealPosition(geometry_msgs::Twist)));
  connect((QObject *)&sm.state, SIGNAL(newRoboMowerState(int)),
          SceneWaypoint_,       SLOT(newRoboMowerState(int)));

  connect(this,                 SIGNAL(reposition()),
          SceneTravelled_,      SLOT  (reposition()));
  connect(this,                 SIGNAL(reposition()),
          SceneObstacles_,      SLOT  (reposition()));
  connect(this,                 SIGNAL(reposition()),
          SceneSurface_,        SLOT  (reposition()));
  connect(this,                 SIGNAL(reposition()),
          ScenePosition_,       SLOT  (reposition()));
}

Tab_Console::~Tab_Console()
{
}

void Tab_Console::mapDimensionsChanged(float minWidth, float maxWidth, float minHeight, float maxHeight)
{
  emit reposition();
  SceneDimensions_->mapDimensionsChanged(minWidth, maxWidth, minHeight, maxHeight);
  for ( int16_t w = minWidth; w < maxWidth; w ++ ) {
    for ( int16_t h = minHeight; h < maxHeight; h ++ ) {
      SceneSurface_->touch(w, h);
    }
  }
}

void Tab_Console::newItem(QGraphicsItem* item)
{
  Scene_->addItem(item);
}

void Tab_Console::toggle_IR()
{
  SceneIR_->toggleView();
}

void Tab_Console::toggle_US()
{
  SceneUltrasonic_->toggleView();
}

void Tab_Console::toggle_LS()
{
  SceneLaser_->toggleView();
}
