#ifndef TAB_CONSOLE_H
#define TAB_CONSOLE_H

#include <QGraphicsView>

#include "RenderingBase.h"
#include "MasterProxy.h"


class QLabel;
class QCheckBox;
class QGraphicsScene;
class QGraphicsItem;

class Scene_IR;
class Scene_Obstacles;
class Scene_Position;
class Scene_Surface;
class Scene_Travelled;
class Scene_Ultrasonic;
class Scene_Waypoint;
class Scene_Dimensions;
class Scene_Laser;

class Tab_Console: public QGraphicsView
{
  Q_OBJECT

public:
  Tab_Console(MasterProxy *proxy, QWidget * parent = 0);
  virtual ~Tab_Console();

signals:
  void reposition();

public slots:
  void toggle_IR();
  void toggle_US();
  void toggle_LS();

private slots:
  void newItem(QGraphicsItem* item);
  void mapDimensionsChanged(float minWidth, float maxWidth, float minHeight, float maxHeight);

private:
  QGraphicsScene   * Scene_;

  MasterProxy      * MasterProxy_;
  RenderingBase      RBase_;
  Scene_Dimensions * SceneDimensions_;
  Scene_IR         * SceneIR_;
  Scene_Obstacles  * SceneObstacles_;
  Scene_Position   * ScenePosition_;
  Scene_Surface    * SceneSurface_;
  Scene_Travelled  * SceneTravelled_;
  Scene_Ultrasonic * SceneUltrasonic_;
  Scene_Waypoint   * SceneWaypoint_;
  Scene_Laser      * SceneLaser_;

};


#endif
