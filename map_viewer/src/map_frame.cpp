
#include <QtWidgets>

#include "map_frame.h"
#include "Tab_System.h"
#include "Tab_PosVel.h"
#include "Tab_Rover.h"
#include "Tab_PosControl.h"
#include "Tab_Console.h"

#include "MasterProxy.h"
#include <qt_proxy/StateProxy.h>
#include <qt_proxy/OdometryProxy.h>
#include <qt_proxy/PosConProxy.h>

MapFrame::MapFrame()
{
  setWindowTitle("RoboMower");

  QWidget *widget = new QWidget;
  setCentralWidget(widget);

  MasterProxy_ = new MasterProxy(this);

  SubModules_t submodules = MasterProxy_->getSubModules();

  Tab_System     *tabSystem     = new Tab_System(submodules, this);
  Tab_PosVel     *tabPosVel     = new Tab_PosVel(submodules, this);
  Tab_PosControl *tabPosControl = new Tab_PosControl(submodules, this);
  Tab_Console    *tabConsole    = new Tab_Console(MasterProxy_, this);

  TabWidget.addTab(tabSystem,     tr("System"));
  TabWidget.addTab(tabPosVel,     tr("Position"));
  TabWidget.addTab(tabPosControl, tr("Pos Control"));
  TabWidget.addTab(tabConsole,    tr("Console"));


  MainLayout_.addWidget(&TabWidget);
  widget->setLayout(&MainLayout_);
  // --------------------------------------------------
  ROS_INFO("Starting proxy systems");

  connect(&submodules.odom,   SIGNAL(newDistance(float , float )),
           tabSystem,         SLOT(newDistance(float , float )));
  connect(&submodules.posCon, SIGNAL(newAngularPID(float,float,float)),
           tabPosControl,     SLOT(newAngularPID(float,float,float)));
  connect( tabPosControl,     SIGNAL(updateParamater(int, float)),
          &submodules.posCon, SLOT(updateParamater(int, float)));

  MasterProxy_->connect();

  createActions();
  createMenus();

  connect(ViewIrAct_, &QAction::triggered, tabConsole, &Tab_Console::toggle_IR);
  connect(ViewUsAct_, &QAction::triggered, tabConsole, &Tab_Console::toggle_US);
  connect(ViewLsAct_, &QAction::triggered, tabConsole, &Tab_Console::toggle_LS);
}

MapFrame::~MapFrame()
{
}

void MapFrame::createActions()
{
  ExitAct_ = new QAction(tr("E&xit"), this);
  ExitAct_->setShortcuts(QKeySequence::Quit);
  ExitAct_->setStatusTip(tr("Exit the application"));
  connect(ExitAct_, &QAction::triggered, this, &QWidget::close);

  ViewIrAct_ = new QAction(tr("&IR"), this);
  ViewIrAct_->setStatusTip(tr("Enable/Disable Inrared Sensors view"));
  ViewIrAct_->setCheckable(true);
  ViewIrAct_->setChecked(false);

  ViewUsAct_ = new QAction(tr("&US"), this);
  ViewUsAct_->setStatusTip(tr("Enable/Disable Ultrasonic Sensors view"));
  ViewUsAct_->setCheckable(true);
  ViewUsAct_->setChecked(false);

  ViewLsAct_ = new QAction(tr("&LS"), this);
  ViewLsAct_->setStatusTip(tr("Enable/Disable Laser Scanner view"));
  ViewLsAct_->setCheckable(true);
  ViewLsAct_->setChecked(false);

}

void MapFrame::createMenus()
{
  FileMenu_ = menuBar()->addMenu(tr("&File"));
  FileMenu_->addSeparator();
  FileMenu_->addAction(ExitAct_);

  ViewMenu_ = menuBar()->addMenu(tr("&View"));
  ViewMenu_->addAction(ViewIrAct_);
  ViewMenu_->addAction(ViewUsAct_);
  ViewMenu_->addAction(ViewLsAct_);
}
