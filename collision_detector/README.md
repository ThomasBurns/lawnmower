

Collision Detector
---
This is a program that analyses the data from all range sensors and identifies if the robot is at risk of colliding with an object or not.
Since we can have more than one collision alarm there is one instance of this program per sensor. A Sonar sensor has a large wavefront at long range. Not all of this front may lie in the robots path, therefore we have two levels of alerts.

MSG_WARNING:   means the robot *might* collide with an object in front of it, but we cannot be sure.
MSG_ALARM:     means that we are certain we will collide if we stay on this current path.
MSG_ALL_CLEAR: is sent after a WARNING or ALARM when the object is no longer in out path at all.


Parameters:
---
/CollisionDetector/RangeDetectionFactor
Float, between 0.0 & 1.0.
When a sensor cannot see anything it will return MAX_VALUE. Sometimes it may briefly produce something else. In addition long range Sonar sensors have a huge wave front at long distances. This system determines at what distance we start to declare that there may infact be an object in our path.

/CollisionDetector/ExclusionRange
Float, greater than 0.0.
This defines the greatest width of the robot. it is used to determine the exclusion field. This value should be no smaller than the width of the robot but for better avoidance it could be larger.

/CollisionDetector/InputTopic
String.
This is the name of the topic this instance of the program should listen too. Each instance should listen to a different sensor.

/CollisionDetector/OutputTopic
String
This is the name of the topic the program will publish to. This allows each instance of the program to potentially publish to a different publisher.
