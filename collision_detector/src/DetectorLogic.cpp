#include <cmath>
#include <mowerlib/Trigonometry.h>

#include "DetectorLogic.h"


DetectorLogic::DetectorLogic():
  ExclusionRadius_(0.1)
{
}

void DetectorLogic::setExclusionRadius(float exclusionRadius)
{
  ExclusionRadius_ = exclusionRadius;
}


void DetectorLogic::setPosition(float x, float y, float theta)
{
  SensorPos_.linear.x = x;
  SensorPos_.linear.y = x;
  SensorPos_.angular.z = theta;
}

bool DetectorLogic::checkObject(float x, float y, float range)
{
  geometry_msgs::Point target;
  target.x = x;
  target.y = y;
  target.z = 0.0;

  float angle = Trig_CalcAngularError(SensorPos_, target);
  float r = fabs(sin(angle)) * range;
  return ( r < ExclusionRadius_ );
}

