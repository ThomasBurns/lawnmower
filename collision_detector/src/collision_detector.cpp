
#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <sensor_msgs/Range.h>
#include <cmath>
#include <vector>
#include <mowerlib/Trigonometry.h>
#include "collision_detector/Collision.h"

#include "DetectorLogic.h"


// ROS Data
static ros::Publisher  WarningPub_;
static ros::Subscriber SensorSub_;

static DetectorLogic Detector_;
static collision_detector::Collision CollisionMsg_;
static float ScaleFactor_;

//====================================
typedef struct {
  float x, y;
  float theta;
} Pos_2D_t;

typedef std::vector<Pos_2D_t> ObjectArray;

static ObjectArray ObjectParser(Pos_2D_t position, sensor_msgs::Range reading, float steps)
{
  Pos_2D_t objectPos;
  ROS_ASSERT(steps >= 0.0);
  ObjectArray objectArray;

  objectPos.x = reading.range * std::cos(position.theta) + position.x;
  objectPos.y = reading.range * std::sin(position.theta) + position.y;
  objectArray.push_back(objectPos);

  for ( float angle = steps; angle <= (reading.field_of_view/2.0); angle += steps ) {

    float positive_angle = Trig_LimitRadians(position.theta + Trig_DegreesToRadians(angle));
    objectPos.x = reading.range * std::cos(positive_angle) + position.x;
    objectPos.y = reading.range * std::sin(positive_angle) + position.y;
    objectArray.push_back(objectPos);

    float negative_angle = Trig_LimitRadians(position.theta - Trig_DegreesToRadians(angle));
    objectPos.x = reading.range * std::cos(negative_angle) + position.x;
    objectPos.y = reading.range * std::sin(negative_angle) + position.y;
    objectArray.push_back(objectPos);
  }
  return objectArray;
}

//====================================
static void publishMsg(int state)
{
  CollisionMsg_.MsgType = state;
  WarningPub_.publish(CollisionMsg_);
}

//====================================
void newRangeReading(const sensor_msgs::Range & msg)
{
  static float oldRange;
  static tf::TransformListener Listener_;

  float rangeDifference = fabs(msg.range - oldRange);
  oldRange = msg.range;
  if ( rangeDifference >= 0.02 ) {
    return;
  }

  CollisionMsg_.Stamp = ros::Time::now();
  CollisionMsg_.Range = msg.range;
  CollisionMsg_.SensorType = msg.radiation_type;

  float scaledRange = ScaleFactor_ * msg.max_range;
  if ( msg.range >= scaledRange ) {
    if ( CollisionMsg_.MsgType != CollisionMsg_.MSG_ALL_CLEAR ) {
      publishMsg(CollisionMsg_.MSG_ALL_CLEAR);
    }
    // No there is nothing else to do, and nothing to do with this msg type.
    // So might as well return and go back to sleep.
    return;
  }

  try {
    tf::StampedTransform location;
    Listener_.lookupTransform("/world", CollisionMsg_.SensorName, ros::Time(0), location);

    Pos_2D_t sensorPosition;
    sensorPosition.x = location.getOrigin().getX();
    sensorPosition.y = location.getOrigin().getY();
    sensorPosition.theta = tf::getYaw(location.getRotation());

    // Now we take the scan and build an array of objects
    ObjectArray oA = ObjectParser(sensorPosition, msg, 1.0);

    Listener_.lookupTransform("/world", "/base_link", ros::Time(0), location);
    float x = location.getOrigin().getX();
    float y = location.getOrigin().getY();
    float theta = tf::getYaw(location.getRotation());
    Detector_.setPosition(x, y, theta);

    unsigned int count = 0;
    for ( unsigned int i = 0; i < oA.size(); i ++ ) {
      bool rv = Detector_.checkObject(oA[i].x, oA[i].y, msg.range);

      if ( rv == true ) {
        if ( count == 0 ) {
          CollisionMsg_.StartAngle = oA[i].theta;
          CollisionMsg_.StopAngle  = oA[i].theta;
        }
        count++;
        if ( oA[i].theta < CollisionMsg_.StartAngle) {
          CollisionMsg_.StartAngle = oA[i].theta;
        }
        if ( oA[i].theta < CollisionMsg_.StopAngle) {
          CollisionMsg_.StopAngle = oA[i].theta;
        }
      }
    }
    if ( count == 0 ) {
      publishMsg(CollisionMsg_.MSG_ALL_CLEAR);
    } else if ( count != oA.size() ) {
      publishMsg(CollisionMsg_.MSG_WARNING);
    } else {
      publishMsg(CollisionMsg_.MSG_ALARM);
    }

  } catch (tf::TransformException ex) {
    ROS_ERROR("Collision Pos Transform: %s",ex.what());
  }
}

//====================================
int main (int argc, char **argv)
{
  ros::init(argc, argv, "collision_detector", ros::init_options::AnonymousName);
  ros::NodeHandle node;
  std::string outputTopicName;

  ROS_INFO("collision_detector online");

  CollisionMsg_.MsgType = CollisionMsg_.MSG_ALL_CLEAR;

  ros::param::param<float>("/CollisionDetector/RangeDetectionFactor", ScaleFactor_, 0.75);
  ROS_ASSERT( ScaleFactor_ >  0.0);
  ROS_ASSERT( ScaleFactor_ <= 1.0);

  float exclusion;
  ros::param::param<float>("/CollisionDetector/ExclusionRange", exclusion, 0.1);
  ROS_ASSERT( exclusion >  0.0);
  Detector_.setExclusionRadius(exclusion);
  ros::param::param<std::string>("/CollisionDetector/InputTopic", CollisionMsg_.SensorName, "/CollisionInput");
  ros::param::param<std::string>("/CollisionDetector/OutputTopic", outputTopicName, "/CollisionOutput");
  ROS_INFO("Input Topic: %s", CollisionMsg_.SensorName.c_str());
  ROS_INFO("Output Topic: %s", outputTopicName.c_str());


  WarningPub_ = node.advertise<collision_detector::Collision>(outputTopicName,2);
  SensorSub_  = node.subscribe(CollisionMsg_.SensorName, 2, newRangeReading);

  ros::spin();
  return 0;
}

