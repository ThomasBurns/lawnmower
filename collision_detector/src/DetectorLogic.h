#ifndef DETECTOR_LOGIC_H
#define DETECTOR_LOGIC_H

#include <geometry_msgs/Point.h>

class DetectorLogic
{
public:

  DetectorLogic();
  void setExclusionRadius(float exclusionRadius);
  void setPosition(float x, float y, float theta);

  bool checkObject(float x, float y, float range);

private:
  geometry_msgs::Twist SensorPos_;
  float ExclusionRadius_;
};

#endif
