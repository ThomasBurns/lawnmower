# %Tag(FULLTEXT)%
cmake_minimum_required(VERSION 2.8.3)
project(collision_detector)

find_package(catkin REQUIRED COMPONENTS
  roscpp
  rospy
  std_msgs
  genmsg
  tf
  mowerlib
)

add_message_files(FILES
  Collision.msg
)

#add_service_files(FILES)
generate_messages(DEPENDENCIES std_msgs)

catkin_package(
  CATKIN_DEPENDS message_runtime
)
include_directories(
  src/
  ${catkin_INCLUDE_DIRS}
  ${mowerlib_INCLUDE_DIRS}
  ${roscpp_INCLUDE_DIRS}
)
set(CMAKE_CXX_FLAGS "-Wall")


add_executable(collision_detector
  src/collision_detector.cpp
  src/DetectorLogic.cpp
)

target_link_libraries(collision_detector ${catkin_LIBRARIES} ${mowerlib_LIBRARIES})
add_dependencies(collision_detector collision_detector_generate_messages)

# %EndTag(FULLTEXT)%
