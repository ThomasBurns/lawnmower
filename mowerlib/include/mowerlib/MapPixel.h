#ifndef MAP_PIXEL_H
#define MAP_PIXEL_H

/* These structs are used to define the world around the robot
 * as well as its history, i.e. where it has been, etc.
 *
 * To save space these fields can be compressed.
 */

#include <stdint.h>

typedef uint8_t   TravelDataSize_t;
typedef uint32_t  MapDataSize_t;


class TravelData_t
{
public:

  TravelData_t();
  TravelData_t(TravelDataSize_t data);

  TravelDataSize_t compress() const;

  bool Travelled_; // Have we visitied this location.
  bool Mown_;      // Have we cut the grass here.
  // Under normal operation there will be more mown grids than Travelled.

};


class MapData_t
{
public:

  MapData_t();
  MapData_t(MapDataSize_t data);

  MapDataSize_t compress() const;

  // Occupancy Grid Settings.
  uint8_t Occupied_;
  uint8_t Seen_;
  bool  Unreachable_;

  // Surface Type Data
  // TBA

};

typedef struct {
  uint16_t Unknown;
  uint16_t Obstacles;
  uint16_t Unreachable;

} MapDataStats_t;

#endif
