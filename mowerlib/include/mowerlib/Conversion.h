#ifndef CONVERSION_H
#define CONVERSION_H

#include <geometry_msgs/Point.h>
#include <geometry_msgs/Twist.h>
#include <tf/transform_listener.h>


//-----------------------------------------------------------
geometry_msgs::Point ConvertTo_Point(const geometry_msgs::Twist & twist);

geometry_msgs::Twist ConvertTo_Twist(const tf::StampedTransform & location);

//-----------------------------------------------------------
float DifferenceBetween(const geometry_msgs::Twist & a, const geometry_msgs::Twist & b);

bool Equal(const geometry_msgs::Twist & a, const geometry_msgs::Twist & b);
bool Equal(const geometry_msgs::Point & a, const geometry_msgs::Point & b);

#endif
