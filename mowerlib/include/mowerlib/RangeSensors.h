#ifndef RANGE_SENSORS_H
#define RANGE_SENSORS_H

#include <vector>
#include <string>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Range.h>
#include <tf/transform_listener.h>

namespace ros {
  class Subscriber;
};

/* --------------------------------------------------------------------- */
class RangeSensorCallback
{
public:
  virtual void newScan(std::string name, const sensor_msgs::Range & msg, const geometry_msgs::Twist & position) = 0;

};

/* --------------------------------------------------------------------- */
class RangeSensor
{
public:
  RangeSensor(std::string name, tf::TransformListener * listener);
  ~RangeSensor();

  void setCallback(RangeSensorCallback *callback);

  const geometry_msgs::Twist & getPosition() const;
  const sensor_msgs::Range   & getRange() const;
private:
  void newRange(const sensor_msgs::Range & msg);

  const std::string Name_;
  tf::TransformListener *Listener_;
  ros::Subscriber *Subscriber_;

  geometry_msgs::Twist Position_;
  sensor_msgs::Range   Range_;

  RangeSensorCallback *Callback_;
};

/* --------------------------------------------------------------------- */
/* --------------------------------------------------------------------- */

#endif
