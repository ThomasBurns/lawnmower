#ifndef WAY_POINT_H
#define WAY_POINT_H

#include <stdint.h>
#include <list>

class WayPoint {

public:
  WayPoint();
  WayPoint(float x, float y);

  float difference(const WayPoint & wp);

  float X;
  float Y;
};

typedef std::list<WayPoint> WayPointList_t;

float WayPoint_Sum(const WayPointList_t & list);
float WayPoint_Sum(WayPoint start, const WayPointList_t & list);

#endif

