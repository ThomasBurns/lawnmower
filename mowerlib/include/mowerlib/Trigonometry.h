#ifndef TRIGONOMETRY_H
#define TRIGONOMETRY_H

#include <geometry_msgs/Point.h>
#include <geometry_msgs/Twist.h>


// Simple conversion functions.
float Trig_DegreesToRadians(float degrees);
float Trig_RadiansToDegrees(float radians);


// These functions limit the mgnitude of an angle.
// The resultant value will be between -PI & +PI
float Trig_LimitRadians(float angle);
// The resultant value will be between -180 & +180
float Trig_LimitDegrees(float angle);

float Trig_CalcTargetAngle(geometry_msgs::Twist pos, geometry_msgs::Point target);

float Trig_CalcLinearError(geometry_msgs::Twist pos, geometry_msgs::Point target);
float Trig_CalcAngularError(geometry_msgs::Twist pos, geometry_msgs::Point target);
  
  

#endif
