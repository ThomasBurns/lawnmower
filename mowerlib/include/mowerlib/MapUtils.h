#ifndef MAPUTILS_H
#define MAPUTILS_H

#include <stdint.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Twist.h>

//-----------------------------------------------------------
class GridRef_t;
class SubGrid_t;
class Point2D_t;
class GridPosition_t;


//-----------------------------------------------------------
class GridRef_t
{
public:
  int16_t width;
  int16_t height;

  GridRef_t ();
  GridRef_t (int w, int h);
  GridRef_t (double w, double h);
  GridRef_t (const geometry_msgs::Point & pt);
  GridRef_t (const geometry_msgs::Twist & pt);

  bool operator == (const GridRef_t & other) const;
  bool operator != (const GridRef_t & other) const;
  bool operator <  (const GridRef_t & other) const;
  GridRef_t operator +  (const GridRef_t & other) const;
};

typedef std::list<GridRef_t> GridRef_List;
typedef std::vector<GridRef_t> GridRef_Vector;

//-----------------------------------------------------------
class Point2D_t
{
public:
  float X;
  float Y;

  Point2D_t ();
  Point2D_t (double x, double y);
  Point2D_t (const geometry_msgs::Point & pt);
  Point2D_t (const geometry_msgs::Twist & pt);

  bool operator == (const Point2D_t & other) const;
  bool operator != (const Point2D_t & other) const;
  bool operator <  (const Point2D_t & other) const;

  geometry_msgs::Point convert() const;
};

typedef std::list<Point2D_t> Point2D_List;
typedef std::vector<Point2D_t> Point2D_Vector;

//-----------------------------------------------------------
class SubGrid_t
{
public:
  uint8_t width;
  uint8_t height;

  SubGrid_t();
  SubGrid_t(double x, double y);
  SubGrid_t(const Point2D_t & pt);
  SubGrid_t(const geometry_msgs::Point & pt);
  SubGrid_t(const geometry_msgs::Twist & pt);

  bool operator == (const SubGrid_t & other) const;
  bool operator != (const SubGrid_t & other) const;
  bool operator <  (const SubGrid_t & other) const;

  uint16_t calcOffset() const;

  static const uint8_t MAX_RESOLUTION;
  static const uint8_t MIN_RESOLUTION;
  static void setResolution(uint8_t res);
  static uint8_t getResolution();

private:
  static uint8_t Resolution_;
} ;

//-----------------------------------------------------------
class GridPosition_t
{
public:
  GridPosition_t();
  GridPosition_t(const GridRef_t & grid, const SubGrid_t & sub);
  GridPosition_t(const geometry_msgs::Twist & pt);

  void setGrid(const GridRef_t & g);
  void setSub (const SubGrid_t & s);

  void adjustSub(int16_t width, int16_t height);


  Point2D_t convert() const;
  const GridRef_t & getGrid() const;
  const SubGrid_t & getSub() const;

  void operator = (const GridPosition_t & other);
  GridPosition_t operator + (const GridRef_t & other) const;

private:
  GridRef_t Grid_;
  SubGrid_t Sub_;
};

#endif
