
#include "mowerlib/MapUtils.h"

#include <ros/ros.h>

/* ======================================================== */
/* We do this to avoid subtile rounding errors that might
 * otherwise occcur. A value of 1000 means we are good down
 * to mm resolution.
 */
static int convert(float v)
{
  int a = v * 1000.0;
  return floor(a / 1000.0);
}

/* ======================================================== */
GridRef_t::GridRef_t():
  width(0), height(0)
{
}

GridRef_t::GridRef_t(int w, int h):
  width(w), height(h)
{
}

GridRef_t::GridRef_t (double w, double h)
{
  ROS_ASSERT(isnan(w) == false);
  ROS_ASSERT(isnan(h) == false);
  width = convert(w);
  height = convert(h);
}

GridRef_t::GridRef_t (const geometry_msgs::Point & pt)
{
  ROS_ASSERT(isnan(pt.x) == false);
  ROS_ASSERT(isnan(pt.y) == false);
  width = convert(pt.x);
  height = convert(pt.y);
}

GridRef_t::GridRef_t (const geometry_msgs::Twist & pt)
{
  ROS_ASSERT(isnan(pt.linear.x) == false);
  ROS_ASSERT(isnan(pt.linear.y) == false);
  width = convert(pt.linear.x);
  height = convert(pt.linear.y);
}

/* ======================================================== */
bool GridRef_t::operator==(const GridRef_t & other) const
{
  return (other.width == this->width) && ( other.height == this->height);
}

bool GridRef_t::operator!=(const GridRef_t & other) const
{
  return !(*this == other);
}

bool GridRef_t::operator <(const GridRef_t & other) const
{
  if ( this->width == other.width ) {
    return ( this->height < other.height );
  } else {
    return ( this->width < other.width );
  }
}

GridRef_t GridRef_t::operator +  (const GridRef_t & other) const
{
  GridRef_t g = *this;
  g.width += other.width;
  g.height += other.height;
  return g;
}


/* ======================================================== */
/* ======================================================== */
Point2D_t::Point2D_t():
  X(0.0), Y(0.0)
{
}

Point2D_t::Point2D_t(double x, double y):
  X(x), Y(y)
{
}

Point2D_t::Point2D_t(const geometry_msgs::Point & pt):
  X(pt.x), Y(pt.y)
{
}

Point2D_t::Point2D_t(const geometry_msgs::Twist & pt):
  X(pt.linear.x), Y(pt.linear.y)
{
}

/* ======================================================== */
bool Point2D_t::operator == (const Point2D_t & other) const
{
  return (other.Y == this->Y) && ( other.X == this->X);
}

bool Point2D_t::operator != (const Point2D_t & other) const
{
  return !(*this == other);
}

bool Point2D_t::operator <  (const Point2D_t & other) const
{
  if ( this->X == other.X ) {
    return ( this->Y < other.Y );
  } else {
    return ( this->X < other.X );
  }
}

/* ======================================================== */
geometry_msgs::Point Point2D_t::convert() const
{
  geometry_msgs::Point point;

  point.x = X;
  point.y = Y;
  point.z = 0.0;

  return point;
}

/* ======================================================== */
/* ======================================================== */
SubGrid_t::SubGrid_t():
  width(0), height(0)
{

}

SubGrid_t::SubGrid_t(double x, double y)
{
  ROS_ASSERT(isnan(x) == false);
  ROS_ASSERT(isnan(y) == false);

  GridRef_t g(x,y);

  double fractionX = (x * Resolution_) - (g.width * Resolution_);
  width = fractionX;

  double fractionY = (y * Resolution_) - (g.height * Resolution_);
  height = fractionY;

  ROS_ASSERT(width  <  Resolution_ );
  ROS_ASSERT(height <  Resolution_ );
}

SubGrid_t::SubGrid_t(const Point2D_t & pt)
{
  ROS_ASSERT(isnan(pt.X) == false);
  ROS_ASSERT(isnan(pt.Y) == false);

  GridRef_t g(pt.X, pt.Y);

  width  = (pt.X * Resolution_) - (g.width * Resolution_);
  height = (pt.Y * Resolution_) - (g.height * Resolution_);

  ROS_ASSERT(width  <  Resolution_ );
  ROS_ASSERT(height <  Resolution_ );
}

SubGrid_t::SubGrid_t(const geometry_msgs::Point & pt)
{
  ROS_ASSERT(isnan(pt.x) == false);
  ROS_ASSERT(isnan(pt.y) == false);

  GridRef_t g(pt.x,pt.y);

  width  = (pt.x * Resolution_) - (g.width * Resolution_);
  height = (pt.y * Resolution_) - (g.height * Resolution_);

  ROS_ASSERT(width < Resolution_ );
  ROS_ASSERT(height < Resolution_ );
}

SubGrid_t::SubGrid_t(const geometry_msgs::Twist & pt)
{
  ROS_ASSERT(isnan(pt.linear.x) == false);
  ROS_ASSERT(isnan(pt.linear.y) == false);

  GridRef_t g(pt.linear.x,pt.linear.y);

  width  = (pt.linear.x * Resolution_) - (g.width * Resolution_);
  height = (pt.linear.y * Resolution_) - (g.height * Resolution_);

  ROS_ASSERT(width  <  Resolution_ );
  ROS_ASSERT(height <  Resolution_ );
}

/* ======================================================== */
bool SubGrid_t::operator == (const SubGrid_t & other) const
{
  return ((this->width == other.width) && (this->height == other.height));
}

bool SubGrid_t::operator != (const SubGrid_t & other) const
{
  return !(*this == other);
}

bool SubGrid_t::operator <  (const SubGrid_t & other) const
{
  if ( this->width == other.width ) {
    return ( this->height < other.height );
  } else {
    return ( this->width < other.width );
  }
}

uint16_t SubGrid_t::calcOffset() const
{
  ROS_ASSERT(this->width  < Resolution_ );
  ROS_ASSERT(this->height < Resolution_ );
  uint16_t offset = (this->height * Resolution_) + this->width;
  ROS_ASSERT(offset <  (Resolution_*Resolution_));
  return offset;
}

/* ======================================================== */
const uint8_t SubGrid_t::MAX_RESOLUTION = 100;
const uint8_t SubGrid_t::MIN_RESOLUTION = 1;
uint8_t SubGrid_t::Resolution_ = 50;

void SubGrid_t::setResolution(uint8_t res)
{
  ROS_ASSERT(res <= MAX_RESOLUTION);
  ROS_ASSERT(res >= MIN_RESOLUTION);
  Resolution_ = res;
}

uint8_t SubGrid_t::getResolution()
{
  return Resolution_;
}

/* ======================================================== */
/* ======================================================== */
GridPosition_t::GridPosition_t():
  Grid_(0.0, 0.0), Sub_(0,0)
{
}

GridPosition_t::GridPosition_t(const GridRef_t & grid, const SubGrid_t & sub):
  Grid_(grid), Sub_(sub)
{
}

GridPosition_t::GridPosition_t(const geometry_msgs::Twist & pt):
  Grid_(pt), Sub_(pt)
{
}


void GridPosition_t::setGrid(const GridRef_t & g)
{
  Grid_ = g;
}

void GridPosition_t::setSub (const SubGrid_t & s)
{
  Sub_ = s;
}

void GridPosition_t::adjustSub(int16_t width, int16_t height)
{
  int16_t new_width  = (int16_t )this->Sub_.width  + width;
  int16_t new_height = (int16_t )this->Sub_.height + height;

  int8_t res = SubGrid_t::getResolution();
  while ( new_width < 0 ) {
    new_width += res;
    this->Grid_.width --;
  }
  while ( new_width >= res ) {
    new_width -= res;
    this->Grid_.width ++;
  }
  while ( new_height < 0 ) {
    new_height += res;
    this->Grid_.height --;
  }
  while ( new_height >= res ) {
    new_height -= res;
    this->Grid_.height ++;
  }
  ROS_ASSERT(new_width  >= 0);
  ROS_ASSERT(new_height >= 0);
  ROS_ASSERT(new_width  < res);
  ROS_ASSERT(new_height < res);

  this->Sub_.width = new_width;
  this->Sub_.height = new_height;
}

Point2D_t GridPosition_t::convert() const
{
  float pixel_size = 1.0 / SubGrid_t::getResolution();

  Point2D_t point(Grid_.width, Grid_.height);

  point.X += (Sub_.width  * pixel_size) + (pixel_size * 0.5);
  point.Y += (Sub_.height * pixel_size) + (pixel_size * 0.5);

  return point;
}

const GridRef_t & GridPosition_t::getGrid() const
{
  return Grid_;
}

const SubGrid_t & GridPosition_t::getSub() const
{
  return Sub_;
}

void GridPosition_t::operator = (const GridPosition_t & other)
{
  this->Grid_ = other.Grid_;
  this->Sub_  = other.Sub_;
}

GridPosition_t GridPosition_t::operator + (const GridRef_t & other) const
{
  GridPosition_t gp = *this;
  gp.Grid_.width  += other.width;
  gp.Grid_.height += other.height;

  return gp;
}

/* ======================================================== */
/* ======================================================== */
