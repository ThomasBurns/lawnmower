
#include "mowerlib/Conversion.h"
#include <cmath>

geometry_msgs::Point ConvertTo_Point(const geometry_msgs::Twist & twist)
{
  geometry_msgs::Point pt;

  pt.x = twist.linear.x;
  pt.y = twist.linear.y;
  pt.z = twist.linear.z;

  return pt;
}

geometry_msgs::Twist ConvertTo_Twist(const tf::StampedTransform & location)
{
  geometry_msgs::Twist t;

  t.linear.x = location.getOrigin().getX();
  t.linear.y = location.getOrigin().getY();
  t.linear.z = location.getOrigin().getZ();

  t.angular.x = 0.0;
  t.angular.y = 0.0;
  t.angular.z = tf::getYaw(location.getRotation());

  return t;
}



float DifferenceBetween(const geometry_msgs::Twist & a, const geometry_msgs::Twist & b)
{
  float dx = a.linear.x - b.linear.x;
  float dy = a.linear.y - b.linear.y;
  float dz = a.linear.z - b.linear.z;

  return sqrt((dx*dx)+(dy*dy)+(dz*dz));
}

bool Equal(const geometry_msgs::Twist & a, const geometry_msgs::Twist & b)
{
  return (( a.linear.x == b.linear.x) &&
          ( a.linear.y == b.linear.y) &&
          ( a.linear.z == b.linear.z) &&
          ( a.angular.x == b.angular.x) &&
          ( a.angular.y == b.angular.y) &&
          ( a.angular.z == b.angular.z));
}

bool Equal(const geometry_msgs::Point & a, const geometry_msgs::Point & b)
{
  return (( a.x == b.x) &&
          ( a.y == b.y) &&
          ( a.z == b.z));

}

