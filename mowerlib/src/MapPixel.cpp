
#include <mowerlib/MapPixel.h>

/* ======================================================== */
/*
 * Travel Data bit values.
 * 0: Travelled
 * 1: Mown
 * 2:
 * 3:
 * 4:
 * 5:
 * 6:
 * 7:
 */
const TravelDataSize_t BitFlag_Travel = 0x01;
const TravelDataSize_t BitFlag_Mown   = 0x02;

TravelData_t::TravelData_t():
  Travelled_(false), Mown_(false)
{
}

TravelData_t::TravelData_t(TravelDataSize_t data)
{
  Travelled_ = ( data & BitFlag_Travel );
  Mown_      = ( data & BitFlag_Mown   );
}

TravelDataSize_t TravelData_t::compress() const
{
  TravelDataSize_t data = 0;

  if ( Travelled_ )
    data |= BitFlag_Travel;
  if ( Mown_ )
    data |= BitFlag_Mown;

  return data;
}

/* ======================================================== */

MapData_t::MapData_t():
  Occupied_(0), Seen_(0),
  Unreachable_(false)
{
}

MapData_t::MapData_t(MapDataSize_t data)
{
  Seen_        =  data & 0xFF;
  Occupied_    = (data >> 8) & 0xFF;
  Unreachable_ = (data & 0x010000);
}

MapDataSize_t MapData_t::compress() const
{
  MapDataSize_t data;

  data  =  Seen_     & 0xFF;
  data |= (Occupied_ & 0xFF ) << 8;
  data |= (Unreachable_ == true )? 0x010000: 0x0000;

  return data;
}

/* ======================================================== */
/* ======================================================== */
