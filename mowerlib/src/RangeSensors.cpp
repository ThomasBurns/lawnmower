
#include "mowerlib/Conversion.h"
#include "mowerlib/RangeSensors.h"

#include <ros/ros.h>

/* ------------------------------------------------------------------------------- */
RangeSensor::RangeSensor(std::string name, tf::TransformListener * listener):
  Name_(name), Listener_(listener), Callback_(NULL)
{
  ROS_ASSERT(Listener_ != NULL);
  ROS_DEBUG("RangeSensor(%s): created", Name_.c_str());
  ros::NodeHandle node;

  Subscriber_ = new ros::Subscriber();
  *Subscriber_ = node.subscribe(Name_, 5, &RangeSensor::newRange, this);
}

RangeSensor::~RangeSensor()
{
  ROS_DEBUG("RangeSensor(%s): destroyed", Name_.c_str());
  delete Subscriber_;
}

void RangeSensor::setCallback(RangeSensorCallback *callback)
{
  Callback_ = callback;
}

void RangeSensor::newRange(const sensor_msgs::Range & msg)
{
  try {
    tf::StampedTransform location;

    Range_ = msg;
    Listener_->lookupTransform("/world", msg.header.frame_id, ros::Time(0), location);
    Position_ = ConvertTo_Twist(location);
  }
  catch (tf::ConnectivityException ex) {
    ROS_ERROR("ML,tf::ConnectivityException => %s",ex.what());
    return;
  }
  catch (tf::ExtrapolationException ex) {
//    ROS_ERROR("tf::ExtrapolationException => %s",ex.what());
    return;
  }
  catch (tf::InvalidArgument ex) {
    ROS_ERROR("ML,tf::InvalidArgument => %s",ex.what());
    return;
  }
  catch (tf::LookupException ex) {
    ROS_ERROR("ML,tf::LookupException (%s) => %s", msg.header.frame_id.c_str() ,ex.what());
    return;
  }


  if ( Callback_ != NULL ) {
    Callback_->newScan(Name_, msg, Position_);
  }
}

/* ------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------- */
