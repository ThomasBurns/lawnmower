
#include "mowerlib/Trigonometry.h"
#include <cmath>

float Trig_DegreesToRadians(float degrees)
{
 return ( degrees * M_PI ) / 180.0;
}

float Trig_RadiansToDegrees(float radians)
{
  return ( radians * 180.0 ) / M_PI ;
}

float Trig_LimitRadians(float angle)
{
  while ( angle < -M_PI ) {
    angle += 2*M_PI;
  }
  while ( angle > M_PI ) {
    angle -= 2*M_PI;
  }
  return angle;
}

float Trig_LimitDegrees(float angle)
{
  while ( angle < -180 ) {
    angle += 360;
  }
  while ( angle > 180 ) {
    angle -= 360;
  }
  return angle;
}

float Trig_CalcTargetAngle(geometry_msgs::Twist pos, geometry_msgs::Point target)
{
  float d_x = target.x - pos.linear.x;
  float d_y = target.y - pos.linear.y;
  float angle;

  if (( d_x == 0.0 ) && ( d_y == 0.0 )) {
    angle = pos.angular.z;

  } else if ( d_x == 0.0 ) {
      angle = ( d_y > 0 )? M_PI_2: -M_PI_2;

  } else {

    if ( d_x <= 0.0 && d_y >= 0.0 ) {
      angle = atan(d_y / d_x) - M_PI;

    } else if ( d_x <= 0.0 && d_y <= 0.0 ) {
      angle = -M_PI + atan(d_y / d_x);

    } else {
      angle = atan(d_y / d_x);
    }
  }
  return Trig_LimitRadians(angle);  
}


float Trig_CalcLinearError(geometry_msgs::Twist pos, geometry_msgs::Point target)
{
  float d_x = target.x - pos.linear.x;
  float d_y = target.y - pos.linear.y;
  float d_z = target.z - pos.linear.z;

  return sqrt((d_x*d_x) + (d_y*d_y) + (d_z*d_z));;
}

float Trig_CalcAngularError(geometry_msgs::Twist pos, geometry_msgs::Point target)
{
  return Trig_LimitRadians(Trig_CalcTargetAngle(pos, target) - pos.angular.z);  
}
