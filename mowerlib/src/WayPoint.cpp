
#include "mowerlib/WayPoint.h"

#include <cmath>

WayPoint::WayPoint():
  X(0.0), Y(0.0)
{
}

WayPoint::WayPoint(float x, float y):
  X(x), Y(y)
{
}

float WayPoint::difference(const WayPoint & wp)
{
  float dx = this->X - wp.X;
  float dy = this->Y - wp.Y;
  return sqrt((dx*dx)+(dy*dy));
}

float WayPoint_Sum(const WayPointList_t & list)
{
  float sum = 0.0;

  WayPoint a = *list.begin();
  for (WayPointList_t::const_iterator iterator = list.begin(), end = list.end(); iterator != end; ++iterator) {
    WayPoint b = *iterator;
    sum += a.difference(b);
    a = b;
  }
  return sum;
}

float WayPoint_Sum(WayPoint start, const WayPointList_t & list)
{
  float sum = 0.0;

  if ( list.size() == 0 ) {
    return 0.0;
  }

  WayPoint a = start;
  for (WayPointList_t::const_iterator iterator = list.begin(), end = list.end(); iterator != end; ++iterator) {
    WayPoint b = *iterator;
    sum += a.difference(b);
    a = b;
  }
  return sum;
}
