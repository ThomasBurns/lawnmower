
#include <gtest/gtest.h>
#include "mowerlib/MapUtils.h"

namespace {
  class t_GridPos : public ::testing::Test {

  protected:

    t_GridPos() {
    }

    virtual ~t_GridPos() {
    }

    virtual void SetUp() {
      SubGrid_t::setResolution(50);
    }

    virtual void TearDown() {
    }
  };


  /* =================================================== */
  /* =================================================== */
  TEST_F(t_GridPos, GridPos_constructor_blank)
  {
    GridPosition_t gp;

    ASSERT_EQ( 0, gp.getGrid().width );
    ASSERT_EQ( 0, gp.getGrid().height );
    ASSERT_EQ( 0, gp.getSub().width );
    ASSERT_EQ( 0, gp.getSub().height );
  }


  /* =================================================== */
  /* =================================================== */
  TEST_F(t_GridPos, GridPos_adjust_sub)
  {
    GridPosition_t gp;

    gp.adjustSub(1,10);
    ASSERT_EQ( 0, gp.getGrid().width );
    ASSERT_EQ( 0, gp.getGrid().height );
    ASSERT_EQ( 1, gp.getSub().width );
    ASSERT_EQ( 10, gp.getSub().height );
  }

  TEST_F(t_GridPos, GridPos_adjust_sub_next_grid)
  {
    GridPosition_t gp;

    gp.adjustSub(75,-55);
    ASSERT_EQ( 1, gp.getGrid().width );
    ASSERT_EQ( -2, gp.getGrid().height );
    ASSERT_EQ( 25, gp.getSub().width );
    ASSERT_EQ( 45, gp.getSub().height );
  }

  /* =================================================== */
  /* =================================================== */
}
