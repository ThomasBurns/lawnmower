
#include <gtest/gtest.h>
#include "mowerlib/MapUtils.h"

namespace {
  class t_SubGrid : public ::testing::Test {

  protected:

    t_SubGrid() {
    }

    virtual ~t_SubGrid() {
    }

    virtual void SetUp() {
      SubGrid_t::setResolution(100);
    }

    virtual void TearDown() {
    }
  };

  /* =================================================== */
  /* =================================================== */
  TEST_F(t_SubGrid, MapGrid_subgrid_extremes)
  {
    SubGrid_t s(-1.0,5.99);

    ASSERT_EQ( 0, s.width );
    ASSERT_EQ(99, s.height );
  }

  TEST_F(t_SubGrid, MapGrid_subgrid_mid)
  {
    SubGrid_t s(-1.25,5.75);

    ASSERT_EQ( 75, s.width );
    ASSERT_EQ( 75, s.height );
  }

  TEST_F(t_SubGrid, MapGrid_subgrid_negative)
  {
    SubGrid_t s(-1.25,-5.75);

    ASSERT_EQ( 75, s.width );
    ASSERT_EQ( 25, s.height );
  }

  TEST_F(t_SubGrid, MapGrid_subgrid_0_0)
  {
    SubGrid_t s(1.0,5.0);

    ASSERT_EQ( 0, s.width );
    ASSERT_EQ( 0, s.height );
  }

  TEST_F(t_SubGrid, MapGrid_subgrid_neg_0_0)
  {
    SubGrid_t s(-1.0,-5.0);

    ASSERT_EQ( 0, s.width );
    ASSERT_EQ( 0, s.height );
  }

  TEST_F(t_SubGrid, MapGrid_subgrid_point)
  {
    geometry_msgs::Point pt;
    pt.x =  -1.002;
    pt.y =   5.99;
    pt.z = 100.0;

    SubGrid_t s(pt);

    ASSERT_EQ(99, s.width );
    ASSERT_EQ(99, s.height );
  }

  TEST_F(t_SubGrid, MapGrid_subgrid_null_pos)
  {
    geometry_msgs::Point pt;
    pt.x = 0.0;
    pt.y = 0.0;
    pt.z = 0.0;

    SubGrid_t s(pt);

    ASSERT_EQ(0, s.width );
    ASSERT_EQ(0, s.height );
  }

  TEST_F(t_SubGrid, MapGrid_subgrid_null_pos_neg)
  {
    geometry_msgs::Point pt;
    pt.x =  -0.0;
    pt.y =  -0.0;
    pt.z =  -0.0;

    SubGrid_t s(pt);

    ASSERT_EQ(0, s.width );
    ASSERT_EQ(0, s.height );
  }

  TEST_F(t_SubGrid, MapGrid_subgrid_half_res)
  {
    SubGrid_t::setResolution(50);
    SubGrid_t s(-1.5,5.74);

    ASSERT_EQ( 25, s.width );
    ASSERT_EQ( 37, s.height );
  }

  /* =================================================== */
  /* =================================================== */
}
