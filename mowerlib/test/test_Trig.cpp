#include <gtest/gtest.h>
#include "mowerlib/Trigonometry.h"

#include <cmath>

namespace {

  class t_Trig : public ::testing::Test {

  protected:

    t_Trig() {
    }

    virtual ~t_Trig() {
    }

    virtual void SetUp() {
    }

    virtual void TearDown() {
    }
  };

  /* ======================================= */
  TEST_F(t_Trig, Trig_LimitRadians_Normal)
  {
    ASSERT_FLOAT_EQ(0.0, Trig_LimitRadians(0.0));
  }

  TEST_F(t_Trig, Trig_LimitRadians_90)
  {
    ASSERT_FLOAT_EQ(1.57, Trig_LimitRadians(1.57));
  }

  TEST_F(t_Trig, Trig_LimitRadians_minus_90)
  {
    ASSERT_FLOAT_EQ(-1.57, Trig_LimitRadians(-1.57));
  }
  
  TEST_F(t_Trig, Trig_LimitRadians_540degrees)
  {
    ASSERT_FLOAT_EQ(-M_PI/2.0, Trig_LimitRadians(M_PI*1.5));
  }
  
  TEST_F(t_Trig, Trig_LimitRadians_minus_540degrees)
  {
    ASSERT_FLOAT_EQ(M_PI/2.0, Trig_LimitRadians(-M_PI*1.5));
  }
  
  /* ======================================= */
  TEST_F(t_Trig, Trig_LimitDegrees)
  {
    ASSERT_FLOAT_EQ(0.0, Trig_LimitDegrees(0.0));
  }

  TEST_F(t_Trig, Trig_LimitDegrees_plus_90)
  {
    ASSERT_FLOAT_EQ(90.0, Trig_LimitDegrees(90.0));
  }

  TEST_F(t_Trig, Trig_LimitDegrees_minus_90)
  {
    ASSERT_FLOAT_EQ(-90.0, Trig_LimitDegrees(-90.0));
  }

  TEST_F(t_Trig, Trig_LimitDegrees_plus_450)
  {
    ASSERT_FLOAT_EQ(90.0, Trig_LimitDegrees(450.0));
  }

  TEST_F(t_Trig, Trig_LimitDegrees_minus_450)
  {
    ASSERT_FLOAT_EQ(-90.0, Trig_LimitDegrees(-450.0));
    ASSERT_FLOAT_EQ(-90.0, Trig_LimitDegrees(180.0+90.0));
  }
  
/*  
// These functions limit the mgnitude of an angle.
// The resultant value will be between -2xPI & +2xPI
float Trig_LimitRadians(float angle);
// The resultant value will be between -180 & +180
float Trig_LimitDegrees(float angle);
*/

} // Namespace