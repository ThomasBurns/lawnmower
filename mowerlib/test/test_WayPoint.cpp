#include <gtest/gtest.h>
#include "mowerlib/WayPoint.h"

namespace {

  class t_WayPoint : public ::testing::Test {

  protected:

    t_WayPoint() {
    }

    virtual ~t_WayPoint() {
    }

    virtual void SetUp() {
    }

    virtual void TearDown() {
    }
  };

  /* ======================================= */
  TEST_F(t_WayPoint, difference_X)
  {
    WayPoint p1(0.0,0.0);
    WayPoint p2(1.0,0.0);

    ASSERT_EQ(1.0, p1.difference(p2));
  }

  TEST_F(t_WayPoint, difference_Y)
  {
    WayPoint p1(0.0,0.0);
    WayPoint p2(0.0,2.0);

    ASSERT_EQ(2.0, p1.difference(p2));
  }

  /* ======================================= */
  TEST_F(t_WayPoint, list_sum_no_points)
  {
    WayPointList_t list;

    ASSERT_EQ(0.0, WayPoint_Sum( list));
  }


  TEST_F(t_WayPoint, list_sum_1_X_points)
  {
    WayPointList_t list;

    list.push_back(WayPoint(1.0,0.0));
    list.push_back(WayPoint(2.0,0.0));

    ASSERT_EQ(1.0, WayPoint_Sum( list));
  }

  TEST_F(t_WayPoint, list_sum_1_Y_points)
  {
    WayPointList_t list;

    list.push_back(WayPoint(0.0,1.0));
    list.push_back(WayPoint(0.0,2.0));
    ASSERT_EQ(1.0, WayPoint_Sum( list));
  }

  TEST_F(t_WayPoint, list_sum_1_X_1_Y_points)
  {
    WayPointList_t list;

    list.push_back(WayPoint(0.0,0.0));
    list.push_back(WayPoint(1.0,0.0));
    list.push_back(WayPoint(1.0,1.0));
    ASSERT_EQ(2.0, WayPoint_Sum( list));
  }

  /* ======================================= */
  TEST_F(t_WayPoint, start_list_sum_no_points)
  {
    WayPointList_t list;
    WayPoint start(0.0,0.0);

    ASSERT_EQ(0.0, WayPoint_Sum(start, list));
  }


  TEST_F(t_WayPoint, start_list_sum_1_X_points)
  {
    WayPointList_t list;
    WayPoint start(0.0,0.0);

    list.push_back(WayPoint(1.0,0.0));
    ASSERT_EQ(1.0, WayPoint_Sum(start, list));
  }

  TEST_F(t_WayPoint, start_list_sum_1_Y_points)
  {
    WayPointList_t list;
    WayPoint start(0.0,0.0);

    list.push_back(WayPoint(0.0,1.0));
    ASSERT_EQ(1.0, WayPoint_Sum(start, list));
  }

  TEST_F(t_WayPoint, start_list_sum_1_X_1_Y_points)
  {
    WayPointList_t list;
    WayPoint start(0.0,0.0);

    list.push_back(WayPoint(1.0,0.0));
    list.push_back(WayPoint(1.0,1.0));
    ASSERT_EQ(2.0, WayPoint_Sum(start, list));
  }

} // Namespace