
#include <gtest/gtest.h>
#include "mowerlib/MapUtils.h"

#include <cmath>

namespace {

  class t_GridRef : public ::testing::Test {

  protected:

    t_GridRef() {
    }

    virtual ~t_GridRef() {
    }

    virtual void SetUp() {
    }

    virtual void TearDown() {
    }
  };

  /* ======================================= */
  /* =================================================== */
  /* =================================================== */
  TEST_F(t_GridRef, GridRef_t_int)
  {
    GridRef_t g(-1, 5);
    ASSERT_EQ(-1, g.width );
    ASSERT_EQ( 5, g.height );
  }

  TEST_F(t_GridRef, GridRef_t_float_pos)
  {
    GridRef_t g(1.9, 5.9);
    ASSERT_EQ( 1, g.width );
    ASSERT_EQ( 5, g.height );
  }

  TEST_F(t_GridRef, GridRef_t_float_neg)
  {
    GridRef_t g(-1.1, -5.9);
    ASSERT_EQ(-2, g.width );
    ASSERT_EQ(-6, g.height );
  }

  TEST_F(t_GridRef, GridRef_t_none)
  {
    GridRef_t g;
    ASSERT_EQ(0, g.width );
    ASSERT_EQ(0, g.height );
  }

  TEST_F(t_GridRef, GridRef_t_Point)
  {
    geometry_msgs::Point pt;
    pt.x = 10.0;
    pt.y = 50.0;
    pt.z = -1.0;

    GridRef_t g(pt);
    ASSERT_EQ(10, g.width );
    ASSERT_EQ(50, g.height );
  }

  TEST_F(t_GridRef, GridRef_t_small_floats)
  {
    GridRef_t g(0.0000000000000000000000000000002260804775049489118326111, -0.0000000000000000000000000000002260804775049489118326111);
    ASSERT_EQ( 0, g.width );
    ASSERT_EQ( 0, g.height );
  }

} // Namespace
