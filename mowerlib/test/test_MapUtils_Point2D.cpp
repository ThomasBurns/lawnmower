
#include <gtest/gtest.h>
#include "mowerlib/MapUtils.h"

namespace {
  class t_Point2D : public ::testing::Test {

  protected:

    t_Point2D() {
    }

    virtual ~t_Point2D() {
    }

    virtual void SetUp() {
    }

    virtual void TearDown() {
    }
  };

  /* =================================================== */
  /* =================================================== */
  TEST_F(t_Point2D, Point2D_constructor_blank)
  {
    Point2D_t p;

    ASSERT_EQ( 0.0, p.X );
    ASSERT_EQ( 0.0, p.Y );
  }

  TEST_F(t_Point2D, Point2D_constructor_double)
  {
    Point2D_t p(1.0, -11.0);

    ASSERT_EQ(   1.0, p.X );
    ASSERT_EQ( -11.0, p.Y );
  }

  TEST_F(t_Point2D, Point2D_constructor_point)
  {
    geometry_msgs::Point point;
    point.x = 17.5;
    point.y = 57.5;

    Point2D_t p(point);

    ASSERT_EQ( 17.5, p.X );
    ASSERT_EQ( 57.5, p.Y );
  }

  TEST_F(t_Point2D, Point2D_constructor_twist)
  {
    geometry_msgs::Twist twist;
    twist.linear.x = 17.5;
    twist.linear.y = 57.5;
    Point2D_t p(twist);

    ASSERT_EQ( 17.5, p.X );
    ASSERT_EQ( 57.5, p.Y );
  }

  /* =================================================== */
  TEST_F(t_Point2D, Point2D_equal_x)
  {
    Point2D_t p1(1.0, 10.0);
    Point2D_t p2(1.0, 12.0);

    ASSERT_NE( p1, p2 );
  }

  TEST_F(t_Point2D, Point2D_equal_y)
  {
    Point2D_t p1(81.0, 10.0);
    Point2D_t p2(14.0, 10.0);

    ASSERT_NE( p1, p2 );
  }

  TEST_F(t_Point2D, Point2D_equal_xy)
  {
    Point2D_t p1(81.0, 10.0);
    Point2D_t p2(81.0, 10.0);

    ASSERT_EQ( p1, p2 );
  }

  /* =================================================== */
  TEST_F(t_Point2D, Point2D_less_x)
  {
    Point2D_t p1(1.0, 10.0);
    Point2D_t p2(2.0, 10.0);

    ASSERT_EQ( true, (p1 < p2));
  }

  TEST_F(t_Point2D, Point2D_less_y_equal_x)
  {
    Point2D_t p1(1.0, 10.0);
    Point2D_t p2(1.0, 11.0);

    ASSERT_EQ( true, (p1 < p2));
  }

  TEST_F(t_Point2D, Point2D_greater_x)
  {
    Point2D_t p1(2.0, 10.0);
    Point2D_t p2(1.0, 11.0);

    ASSERT_EQ( false, (p1 < p2));
  }

  /* =================================================== */
  TEST_F(t_Point2D, Point2D_convert)
  {
    Point2D_t p(20.0, 10.0);
    geometry_msgs::Point point = p.convert();

    ASSERT_EQ( 20.0, point.x);
    ASSERT_EQ( 10.0, point.y);
  }

  /* =================================================== */
  /* =================================================== */
}
