
---
# Topics
const char *TOPIC_SONIC_RANGE_SENSOR = "/robomower/sensor/US";
"/robomower/sensor/US"
---
# Services

---
# Settings


Ultrasonic Sensors:

US_Sensors: [ "Front", "Left", "Right", "FrontLeft", "FrontRight" ]

Front: {
  MaxRange: 4.0,   # meters
  MinRange: 0.2,   # meters
  BeamAngle: 15.0, # degrees

  Pos:   [0.0, 0.0, 0.0], # meters from center: X; Y; Z.
  Angle: [0.0, 0.0, 0.0]  # degrees: roll; pitch; yaw.
}

