
---
# Topics
## "/robomower/map/MapUnit"
Type: mowerlib::MapUnit
Data about a specific map grid.

## "/robomower/map/Waypoints"
Type: mowerlib::WayPointsMsg
List of the systems current waypoints.

---
# Services
## "/robomower/srv/GetPar"
Type: mowerlib::Map_GetParamater
Used to query the robomower for its map parameters.

## "/robomower/srv/GetUnit"
Type: mowerlib::Map_GetUnit
Used to request the robomower for the data about a specific map grid.
Answer delivered using the /robomower/map/MapUnit topic.

---
# Settings

## "/robomower/map/Width"
## "/robomower/map/Height"
Type: int
Maximum height and width of the map in meters.

## "/robomower/map/Resolution"
Type: int
Divisions per square meter.


