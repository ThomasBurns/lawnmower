# Position and Velocity related information.

---
# Topics
## "/robomower/nav/Position"
Type: geometry_msgs::Twist
Contains the current position of the robot.


## "/robomower/nav/TargetVelocity"
Type: geometry_msgs::Twist
Contains target linear & angular velocities.

## "/robomower/nav/RealVelocity"
Type: geometry_msgs::Twist
Real velocity as measured by the hardware.
*Not yet implemented*


**Manual Commands**
## "/joy"
Type: sensor_msgs::Joy
Used to input the commands when operating in manual mode.

---
# Services:

---
# Settings:

## "/robosetting/velocity/MaxLinear"
Type: float
Maximum linear velocity, meters/second.

## "/robosetting/velocity/MaxAngular"
Type: float
Maximum angular velocity, degrees/second.

