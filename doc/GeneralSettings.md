# System State related information

---
# Topics

## "/robomower/system/State"
Type: mowerlib::MowerState
Prints semi-periodically. Contains system state information.

---
# Services
## "/robomower/system/SetState"
Used to change the mowers current state.

---
# Settings

## "/robomower/system/BladeRadius", float
The radius of the cutting blade in meters.



