# Inertial Navigation related information.

---
# Topics
## "/robomower/nav/Imu"
Type: mowerlib::INS_Data
Contains the current position of the robot.


## "/robomower/nav/Mag"
Type: sensor_msgs::MagneticField
Contains target linear & angular velocities.

---
# Services:

---
# Settings:
