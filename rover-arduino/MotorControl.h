#ifndef MOTORCONTROL_H
#define MOTORCONTROL_H

#include <stdint.h>

class MotorDriver
{
public:
  MotorDriver(uint8_t enPin, uint8_t dirPin, int anPin);

  void runEncoder();
  bool isEncoderPulse() const;
  
  void stop();
  void setSpeed(bool forward, uint8_t speed);

private:
  uint8_t EnPin_;
  uint8_t DirPin_;
  uint8_t AnPin_;
  uint8_t State_;
  bool    EncPulse_;
};

class MotorControl
{
public:
  MotorControl(MotorDriver * motor, const char *name);

  void setSpeed(bool forward, uint16_t dutyTime);
  void run(void);

private:
  MotorDriver * Motor_;
  const char  * Name_;
  uint8_t       DutyPower_;
  bool          FirstPulse_;
  bool          Forward_;
  uint16_t      DutyTime_;
  uint16_t      DutyCounter_;
  
};

class MotorWatchdog
{
public:
  MotorWatchdog(int timeout);

  void reset();
  void run();
  bool isExpired() const;

private:
  uint8_t Timer_;
  uint8_t Timeout_;

};

#endif
