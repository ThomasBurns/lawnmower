
#include "Arduino.h"

#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>

/**************************************************************************/
/**************************************************************************/
static Adafruit_BNO055 BNO_Sensor = Adafruit_BNO055(55);

/**************************************************************************/
void INS_Init(void)
{
  /* Initialise the sensor */
  if(!BNO_Sensor.begin()) {
    while(1);
  }
  delay(1000);
  BNO_Sensor.setExtCrystalUse(true);    
}


void INS_PrintTemp(void)
{
  Serial.print("[Temp=");
  Serial.print(BNO_Sensor.getTemp());
  Serial.println("]");
}

void INS_PrintSensor(void)
{
  /* Display the floating point data */
  imu::Vector<3> euler_ = BNO_Sensor.getVector(Adafruit_BNO055::VECTOR_EULER);
  imu::Vector<3> linAccel_ = BNO_Sensor.getVector(Adafruit_BNO055::VECTOR_LINEARACCEL);
  
  if ( linAccel_[0] == 0 && linAccel_[1] == 0 && linAccel_[2] == 0 ) {
    return;
  }
  if ( euler_[0] == 0 && euler_[1] == 0 && euler_[2] == 0 ) {
    return;
  }

  Serial.print("[INS=");
  Serial.print(linAccel_[0], 4);
  Serial.print(",");
  Serial.print(linAccel_[1], 4);
  Serial.print(",");
  Serial.print(linAccel_[2], 4);

  Serial.print(",");
  Serial.print(euler_[0], 4);
  Serial.print(",");
  Serial.print(euler_[1], 4);
  Serial.print(",");
  Serial.print(euler_[2], 4);

  Serial.println("]");    
}

void INS_PrintMag(void)
{
  /* Display the floating point data */
  imu::Vector<3> mag_ = BNO_Sensor.getVector(Adafruit_BNO055::VECTOR_MAGNETOMETER);
  
  if ( mag_[0] == 0 && mag_[1] == 0 && mag_[2] == 0 ) {
    return;
  }

  Serial.print("[MAG=");
  Serial.print(mag_[0], 4);
  Serial.print(",");
  Serial.print(mag_[1], 4);
  Serial.print(",");
  Serial.print(mag_[2], 4);
  Serial.println("]");      
}

/**************************************************************************/
/**************************************************************************/
#if 0
static void displaySensorDetails(void)
{
  sensor_t sensor;
  BNO_Sensor.getSensor(&sensor);
  Serial.println("------------------------------------");
  Serial.print  ("Sensor:       "); Serial.println(sensor.name);
  Serial.print  ("Driver Ver:   "); Serial.println(sensor.version);
  Serial.print  ("Unique ID:    "); Serial.println(sensor.sensor_id);
  Serial.print  ("Max Value:    "); Serial.print(sensor.max_value); Serial.println(" xxx");
  Serial.print  ("Min Value:    "); Serial.print(sensor.min_value); Serial.println(" xxx");
  Serial.print  ("Resolution:   "); Serial.print(sensor.resolution); Serial.println(" xxx");
  Serial.println("------------------------------------");
  Serial.println("");
}

/**************************************************************************/
static void displaySensorStatus(void)
{
  /* Get the system status values (mostly for debugging purposes) */
  uint8_t system_status, self_test_results, system_error;
  system_status = self_test_results = system_error = 0;
  BNO_Sensor.getSystemStatus(&system_status, &self_test_results, &system_error);

  /* Display the results in the Serial Monitor */
  Serial.println("");
  Serial.print("System Status: 0x");
  Serial.println(system_status, HEX);
  Serial.print("Self Test:     0x");
  Serial.println(self_test_results, HEX);
  Serial.print("System Error:  0x");
  Serial.println(system_error, HEX);
  Serial.println("");
}

void displayCalStatus(void)
{
  /* Get the four calibration values (0..3) */
  /* Any sensor data reporting 0 should be ignored, */
  /* 3 means 'fully calibrated" */
  uint8_t system, gyro, accel, mag;
  system = gyro = accel = mag = 0;
  BNO_Sensor.getCalibration(&system, &gyro, &accel, &mag);

  /* The data should be ignored until the system calibration is > 0 */
  Serial.print("\t");
  if ( !system ) {
    Serial.print("! ");
  }

  /* Display the individual values */
  Serial.print("Sys:");
  Serial.print(system, DEC);
  Serial.print(" G:");
  Serial.print(gyro, DEC);
  Serial.print(" A:");
  Serial.print(accel, DEC);
  Serial.print(" M:");
  Serial.print(mag, DEC);
  Serial.println("");
}
#endif

