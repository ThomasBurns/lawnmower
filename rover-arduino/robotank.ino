
#include "Arduino.h"
#include "CmdProcessor.h"

#include "Distance.h"
#include "INS.h"

#include "MotorControl.h"

#define __INS_EN  1
/**************************************************************************/
// Pin Defines
const int MOTOR_ENABLE_CH_1 = 6; //M1 Speed Control
const int MOTOR_ENABLE_CH_2 = 5; //M2 Speed Control
const int MOTOR_DIRECTION_CH_1 = 8; //M1 Direction Control
const int MOTOR_DIRECTION_CH_2 = 7; //M2 Direction Control


/**************************************************************************/
// Module Declerations.
static MotorWatchdog MWatchdog(5);

static MotorDriver LeftMotor(MOTOR_ENABLE_CH_1, MOTOR_DIRECTION_CH_1, 0);
static MotorDriver RightMotor(MOTOR_ENABLE_CH_2, MOTOR_DIRECTION_CH_2, 1);

static MotorControl LeftControl (&LeftMotor,  "Left");
static MotorControl RightControl(&RightMotor, "Right");

static DistanceSensor Distance(2,3);

/**************************************************************************/
static volatile uint8_t timer_int;
uint8_t timer_1ms;
static uint8_t timer_10ms;
static uint8_t timer_50ms;
static uint16_t timer_1s;

SIGNAL(TIMER0_COMPA_vect) 
{
  timer_1ms++;
}

static void setupTimer(void)
{
  OCR0A = 0xAF;
  TIMSK0 |= _BV(OCIE0A);
}

/**************************************************************************/
static void Cmd_SetSpeed(const char *line)
{
/*
  while ( !isspace(*line) ) {
    line++;
  }
  line++;
  int16_t fwdSpeed = atoi(line);
  
  while ( !isspace(*line) ) {
    line++;
  }
  int16_t turnSpeed = atoi(line);
  
  int16_t leftSpeed  = fwdSpeed + turnSpeed;
  int16_t rightSpeed = fwdSpeed - turnSpeed;

  MWatchdog.reset();
//  LeftMotor.setSpeed(leftSpeed);
//  RightMotor.setSpeed(rightSpeed);
*/
}

static void Cmd_SetDuty(const char *line)
{
  bool LeftFwd = true;
  bool RightFwd = true;

  while ( !isspace(*line) ) {
    line++;
  }
  while ( isspace(*line) ) {
    line++;
  }
  int16_t leftDuty = atoi(line);
  if ( leftDuty < 0 ) {
    LeftFwd = false;    
    leftDuty = -leftDuty;
  }
  
  while ( !isspace(*line) ) {
    line++;
  }
  while ( isspace(*line) ) {
    line++;
  }
  int16_t rightDuty = atoi(line);
  if ( rightDuty < 0 ) {
    RightFwd = false;
    rightDuty = -rightDuty;
  }
  MWatchdog.reset();
  LeftControl.setSpeed (LeftFwd,  leftDuty);
  RightControl.setSpeed(RightFwd, rightDuty);
}

/**************************************************************************/
const struct CmdTable Commands[] = {
  { "motor", Cmd_SetSpeed },
  { "duty",  Cmd_SetDuty },
  { NULL,    NULL }
};

void RunSerialTerminal(void)
{
  while ( Serial.available() != 0 ) {
    char val = Serial.read();
    CmdProc_RecordLine(val, Commands);
  }
}

/**************************************************************************/
/**************************************************************************/
void setup(void)
{
  Serial.begin(115200);
  Serial.println("Robomower Online");
  Serial.println("Firmware: V0.01");
  INS_Init();
  setupTimer();
}

void loop(void)
{
  noInterrupts();
  if ( timer_int ) {
    timer_1ms ++;
  }
  interrupts();

  RunSerialTerminal();

  if ( timer_1ms ) {
    timer_1ms--;

    LeftMotor.runEncoder();
    RightMotor.runEncoder();
    LeftControl.run();
    RightControl.run();

    if ( ++timer_10ms >= 10 ) {
      timer_10ms = 0;
#if __INS_EN      
      INS_PrintSensor();
#endif      
 
      if ( MWatchdog.isExpired() ) {
#if __INS_EN      
        Serial.println("Watchdog Expired");
#endif      
        LeftMotor.stop();
        RightMotor.stop();
        LeftControl.setSpeed(true, 0);
        RightControl.setSpeed(true, 0);
        MWatchdog.reset();
      }
    } 
    if ( ++timer_50ms >= 50 ) {
      timer_50ms = 0;
#if __INS_EN      
      INS_PrintMag();  
#endif      
    } 
    if ( ++timer_1s >= 1000 ) {
      timer_1s = 0;
#if __INS_EN      
      INS_PrintTemp();
#endif      
      MWatchdog.run();
    }
  }
}

