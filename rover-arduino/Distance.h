#ifndef DISTANCE_H
#define DISTANCE_H

class DistanceSensor
{
public:
  DistanceSensor(int trigger, int receive);

  void run();
  bool isRunning() const;
  uint16_t getReading() const;

  void trigger();

private:
  uint8_t TriggerPin_;
  uint8_t ReceivePin_;
  uint16_t Reading_;
  
  
};

#endif

