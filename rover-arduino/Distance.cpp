
#include "Arduino.h"
#include "Distance.h"


/**************************************************************************/
DistanceSensor::DistanceSensor(int trigger, int receive):
  TriggerPin_(trigger), ReceivePin_(receive), Reading_(0)
{
}

void DistanceSensor::run()
{
  
}

bool DistanceSensor::isRunning() const
{
  return true;
}
uint16_t DistanceSensor::getReading() const
{
  return Reading_;
}

void DistanceSensor::trigger()
{
  
}

/*
const int TRIGGER_PIN = 2;
const int ECHO_PIN = 3;

static void measureDistance(void)
{
  digitalWrite(TRIGGER_PIN, LOW);
  delayMicroseconds(2);
  digitalWrite(TRIGGER_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIGGER_PIN, LOW);
  
  long time = pulseIn(ECHO_PIN, HIGH);
  long dist = (time/2) / 29.1;
  
  Serial.print("US=");
  Serial.print(dist);
  Serial.println("");
}
*/
