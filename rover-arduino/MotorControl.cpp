#include "MotorControl.h"
#include "Arduino.h"

//#define DEBUG  1

/**************************************************************************/
MotorDriver::MotorDriver(uint8_t enPin, uint8_t dirPin, int anPin):
  EnPin_(enPin), DirPin_(dirPin), AnPin_(anPin), 
  State_(0), EncPulse_(false)
{
  pinMode(EnPin_, OUTPUT);
  pinMode(DirPin_, OUTPUT);
  stop();
}

void MotorDriver::stop()
{
  analogWrite (EnPin_, 0);
}

void MotorDriver::runEncoder()
{
  EncPulse_ = false;
  int16_t rawValue = analogRead(AnPin_);
  int16_t newState = (rawValue < 600);  //Min value is 400 and max value is 800, so state chance can be done at 600.
  
  if (newState != State_){
    State_ = newState;
    EncPulse_ = true;
  }
}

bool MotorDriver::isEncoderPulse() const
{
  return EncPulse_;
}

void MotorDriver::setSpeed(bool forward, uint8_t speed)
{
  analogWrite (EnPin_, speed);
  digitalWrite(DirPin_,(forward == true)? LOW: HIGH);
}

/**************************************************************************/
/**************************************************************************/
const uint16_t DutyTimeLookup[24] = 
{
  323, // 140
  238, // 144
  227, // 150
  192, // 155
  172, // 160
  161, // 165
  149, // 170
  141, // 175
  133, // 180
  125, // 185
  120, // 190
  115, // 195
  110, // 200
  106, // 205
  103, // 210
   98, // 215
   96, // 220
   96, // 225
   90, // 230
   87, // 235
   85, // 240
   83, // 245
   78, // 250
   75  // 255  
};

const uint8_t DutyCycleLookup[24] = 
{
  140, 144, 150, 155,
  160, 165, 170, 175,
  180, 185, 190, 195,
  200, 205, 210, 215,
  220, 225, 230, 235,
  240, 245, 250, 255  
};


uint8_t LookupDuty(uint16_t dutyTime)
{
  uint8_t i;
  if ( dutyTime > DutyTimeLookup[0] ) {
    // too slow for the system
    return 0;
  }
  if ( dutyTime < DutyTimeLookup[23] ) {
    return 255;
  }

  for ( i = 0; i < 24; i++ ) {
    if ( dutyTime < DutyTimeLookup[i] ) {
      return DutyCycleLookup[i];
    }
  }
  // If we get here we have been given a speed 
  // faster than we can handle. The only thing 
  // we can do is move at full speed.
  return 255;  
}

  MotorDriver * Motor_;
  const char  * Name_;
  uint8_t       DutyPower_;
  bool          FirstPulse_;
  bool          Forward_;
  uint16_t      DutyTime_;
  uint16_t      DutyCounter_;

/**************************************************************************/
MotorControl::MotorControl(MotorDriver * motor, const char * name):
  Motor_(motor), Name_(name), DutyPower_(0), 
  FirstPulse_(false), Forward_(true), 
  DutyTime_(0), DutyCounter_(0)
{
}

void MotorControl::setSpeed(bool forward, uint16_t dutyTime)
{
  if (( Forward_ != forward ) ||
      ( DutyTime_ != dutyTime )) {
    
    Forward_     = forward;
    DutyTime_    = dutyTime;
    DutyPower_   = LookupDuty(dutyTime);  
    DutyCounter_ = 0;
    FirstPulse_  = false;
    Motor_->setSpeed(Forward_, 255);
  }
}

const int8_t Hysteresis = 10;

void MotorControl::run(void)
{
  if ( DutyTime_ == 0 ) {
    Motor_->stop();
    return;
  }
  DutyCounter_++;
  if ( Motor_->isEncoderPulse() ) {
    
    if ( FirstPulse_ == false ) {
      FirstPulse_ = true;  
      Motor_->setSpeed(Forward_, 255);
      
    } else if ( DutyCounter_ < (DutyTime_-Hysteresis) ) {
      DutyPower_ = (DutyPower_-5) & 0xFF;
      Motor_->setSpeed(Forward_, DutyPower_);
      
    } else if ( DutyCounter_ > (DutyTime_+Hysteresis) ) {
      if ( DutyPower_ <= 250 ) {
        DutyPower_ += 5;
      }
      Motor_->setSpeed(Forward_, DutyPower_);
    }
    DutyCounter_ = 0;
  }
}

/**************************************************************************/
/**************************************************************************/
MotorWatchdog::MotorWatchdog(int timeout):
  Timer_(0), Timeout_(timeout)
{
}

void MotorWatchdog::reset()
{
  Timer_ = Timeout_;
}

void MotorWatchdog::run()
{
  if ( Timer_ ) {
    Timer_ --;
  }
}

bool MotorWatchdog::isExpired() const
{
  return Timer_ == 0;
}


