
#include "Arduino.h"
#include "CmdProcessor.h"

/**************************************************************************/
static void LookupCommand(const struct CmdTable *cPtr);


/**************************************************************************/
#define BUFFER_SIZE 64
static char CmdBuffer[BUFFER_SIZE];
static uint8_t CmdBufferLength;

void CmdProc_RecordLine(char c, const struct CmdTable *cPtr)
{
  if (( c == '\n' ) || ( c == '\r')) {
    if ( CmdBufferLength != 0 ) {
      LookupCommand(cPtr);
      CmdProc_ClearBuffer();
      return;
    }
    
  } else if ( CmdBufferLength < (BUFFER_SIZE-1)) {
    CmdBuffer[CmdBufferLength] = c;
    CmdBufferLength++;
    CmdBuffer[CmdBufferLength] = 0;
  }
}


void CmdProc_ClearBuffer(void)
{
  CmdBufferLength = 0;
  CmdBuffer[0] = 0;
}

static void LookupCommand(const struct CmdTable *cPtr)
{
  while ( cPtr->cmd != NULL ) {
    if ( strncmp(cPtr->cmd, CmdBuffer, strlen(cPtr->cmd)) == 0 ) {
      (cPtr->fn)(CmdBuffer);
      return;
    }
    cPtr++;
  }
  Serial.print("Parser Error: ");
  Serial.println(CmdBuffer);
}

