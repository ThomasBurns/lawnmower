#ifndef CMD_PROC_H
#define CMD_PROC_H

struct CmdTable {
  const char * cmd;
  void (*fn)(const char *);
};

void CmdProc_RecordLine(char c, const struct CmdTable *cPtr);
void CmdProc_ClearBuffer(void);

#endif

