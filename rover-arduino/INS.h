#ifndef INS_H
#define INS_H

void INS_Init(void);

void INS_Run(void);
void INS_PrintTemp(void);
void INS_PrintSensor(void);
void INS_PrintMag(void);


#endif

