# %Tag(FULLTEXT)%
cmake_minimum_required(VERSION 2.8.3)
project(position_control)

find_package(catkin REQUIRED COMPONENTS
  actionlib_msgs
  roscpp
  rospy
  std_msgs
  genmsg
  mowerlib
)

#add_message_files(FILES INS_Data.msg)
#generate_messages(DEPENDENCIES std_msgs)

add_action_files(
  DIRECTORY action
  FILES ControlVector.action
)

add_service_files(FILES
  Limits.srv
  PID.srv
  setPID.srv
)
generate_messages(DEPENDENCIES actionlib_msgs std_msgs)


catkin_package(
  CATKIN_DEPENDS message_runtime
)
include_directories(
  src/
  ${catkin_INCLUDE_DIRS}
  ${mowerlib_INCLUDE_DIRS}
  ${roscpp_INCLUDE_DIRS}
)
set(CMAKE_CXX_FLAGS "-Wall")


#catkin_add_gtest(${PROJECT_NAME}_test
#  test/utest.cpp
#  src/ControllerCommon.cpp
#  test/test_ControllerCommon.cpp
#  src/AngularController.cpp
#  test/test_AngularController.cpp
#  src/LinearController.cpp
#)
#target_link_libraries(${PROJECT_NAME}_test ${catkin_LIBRARIES})
#target_link_libraries(${PROJECT_NAME}_test ${catkin_LIBRARIES} ${mowerlib_LIBRARIES})

add_executable(position_control
  src/ControllerCommon.cpp
  src/AngularController.cpp
  src/LinearController.cpp
  src/position_control.cpp )
target_link_libraries(position_control ${catkin_LIBRARIES})
target_link_libraries(position_control ${catkin_LIBRARIES} ${mowerlib_LIBRARIES})
add_dependencies(position_control position_control_generate_messages)

# %EndTag(FULLTEXT)%
