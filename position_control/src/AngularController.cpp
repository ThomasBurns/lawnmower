

#include "AngularController.h"
#include "ControllerCommon.h"

#include <ros/ros.h>
#include <cmath>
#include <mowerlib/Trigonometry.h>


AngularController::AngularController(const ControllerCommon & con):
  Control_(con), Error_(0.0), PrevError_(0.0), Velocity_(0.0), MaxVelocity_(0.0)
{
}

void AngularController::setMaxVelocity(float velocity)
{
  MaxVelocity_ = velocity;
}

float AngularController::calcError(float target)
{
  ROS_ASSERT(isnan(target) == false );
  
  PrevError_ = Error_;
  Error_ = Trig_LimitRadians(target - Control_.Position_.angular.z);
  
  ROS_ASSERT(isnan(Error_) == false );
  return getError();
}

float AngularController::calcError(void)
{
  PrevError_ = Error_;
  Error_ = Trig_CalcAngularError(Control_.Position_, Control_.Target_);
  ROS_ASSERT(isnan(Error_) == false );
  return getError();
}

float AngularController::getError(void) const
{
  ROS_ASSERT(isnan(Error_) == false );
  return Error_;
}

float AngularController::calcVelocity(void)
{
  ROS_ASSERT(Pid_Angle[0] >= 0.0);
  ROS_ASSERT(isnan(Pid_Angle[0]) == false);
  ROS_ASSERT(Pid_Angle[1] >= 0.0);
  ROS_ASSERT(isnan(Pid_Angle[1]) == false);
  ROS_ASSERT(Pid_Angle[2] >= 0.0);
  ROS_ASSERT(isnan(Pid_Angle[2]) == false);

  if ( Error_ == 0.0) {
    Velocity_ = 0.0;
  } else {
    Velocity_ = Error_ * -Pid_Angle[0];
    Velocity_ += ((Error_ - PrevError_ ) / 0.02) * -Pid_Angle[2];
  }
  
  if ( Velocity_ > MaxVelocity_ ) {
    Velocity_ = MaxVelocity_;
  }
  if ( Velocity_ < -MaxVelocity_ ) {
    Velocity_ = -MaxVelocity_;
  }
  return getVelocity();
}

float AngularController::getVelocity(void) const
{
  return Velocity_;
}

void AngularController::setAngular_P(float p)
{
  ROS_ASSERT(p >= 0.0);
  ROS_ASSERT(isnan(p) == false);
  Pid_Angle[0] = p;
}

void AngularController::setAngular_I(float i)
{
  ROS_ASSERT(i >= 0.0);
  ROS_ASSERT(isnan(i) == false);
  Pid_Angle[1] = i;
}

void AngularController::setAngular_D(float d)
{
  ROS_ASSERT(d >= 0.0);
  ROS_ASSERT(isnan(d) == false);
  Pid_Angle[2] = d;
}

float AngularController::getAngular_P() const
{
  return Pid_Angle[0];
}

float AngularController::getAngular_I() const
{
  return Pid_Angle[1];
}

float AngularController::getAngular_D() const
{
  return Pid_Angle[2];
}

