
#include "LinearController.h"
#include "ControllerCommon.h"

#include <mowerlib/Trigonometry.h>

#include <ros/ros.h>
#include <cmath>

LinearController::LinearController(const ControllerCommon & con):
  Control_(con), Error_(0.0),  Velocity_(0.0), MaxVelocity_(0.0)
{
}

void LinearController::setMaxVelocity(float velocity)
{
  MaxVelocity_ = velocity;
}

float LinearController::calcError(void)
{
  Error_ = Trig_CalcLinearError(Control_.Position_, Control_.Target_);
  return getError();
}

float LinearController::getError(void) const
{
  ROS_ASSERT(isnan(Error_) == false );
  return Error_;
}

float LinearController::calcVelocity(void)
{
  Velocity_ = ( Error_ != 0.0 ) ? log(Error_+1.1): 0.0;
//  Velocity_ = ( Error_ != 0.0 ) ? log(Error_+2.0): 0.0;
  
  if ( Velocity_ > MaxVelocity_ ) {
    Velocity_ = MaxVelocity_;
  }
  if ( Velocity_ < -MaxVelocity_ ) {
    Velocity_ = -MaxVelocity_;
  }

  return getVelocity();
}

float LinearController::getVelocity(void) const
{
  ROS_ASSERT(isnan(Velocity_) == false );
  return Velocity_;
}



