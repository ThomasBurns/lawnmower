#ifndef CONTROLLER_COMMON_H
#define CONTROLLER_COMMON_H

#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Twist.h>

class ControllerCommon
{
public:
  ControllerCommon();
  
  void odomCallback(const nav_msgs::Odometry & odom);
  void targetCallback(const geometry_msgs::Point & msg);
  
  geometry_msgs::Twist Velocity_;
  geometry_msgs::Twist Position_;
  geometry_msgs::Point Target_;
  
  bool TargetReady_;
  bool PositionReady_;
};

#endif

