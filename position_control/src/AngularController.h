#ifndef ANGULAR_CONTROL_H
#define ANGULAR_CONTROL_H

class ControllerCommon;

class AngularController
{
public:
  AngularController(const ControllerCommon & con);

  void setMaxVelocity(float velocity);
  float calcError(float target);
  float calcError(void);
  float getError(void) const;

  float calcVelocity(void);
  float getVelocity(void) const;


  void setAngular_P(float p);
  void setAngular_I(float i);
  void setAngular_D(float d);

  float getAngular_P() const;
  float getAngular_I() const;
  float getAngular_D() const;


private:
  const ControllerCommon & Control_;
  float Error_;
  float PrevError_;
  float Velocity_;
  float Pid_Angle[3];
  float MaxVelocity_;
};


#endif
