#ifndef LINEAR_CONTROLLER_H
#define LINEAR_CONTROLLER_H

class ControllerCommon;

class LinearController
{
public:
  LinearController(const ControllerCommon & con);

  void setMaxVelocity(float velocity);
  float calcError(void);
  float getError(void) const;
  
  float calcVelocity(void);
  float getVelocity(void) const;

  
private:
  const  ControllerCommon  & Control_;
  float Error_;
  float Velocity_;
  float MaxVelocity_;

};


#endif

