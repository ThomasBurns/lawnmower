
#include <ros/ros.h>
#include "ControllerCommon.h"
#include <tf/transform_listener.h>

ControllerCommon::ControllerCommon():
  TargetReady_(false), PositionReady_(false)
{
}

void ControllerCommon::odomCallback(const nav_msgs::Odometry & odom)
{
  Velocity_ = odom.twist.twist;

  Position_.linear.x = odom.pose.pose.position.x;
  Position_.linear.y = odom.pose.pose.position.y;
  Position_.linear.z = odom.pose.pose.position.z;
  
  tf::Quaternion q;
  tf::quaternionMsgToTF(odom.pose.pose.orientation, q);
  tf::Matrix3x3(q).getRPY(Position_.angular.x, 
                          Position_.angular.y, 
                          Position_.angular.z);  
  
  if (( PositionReady_ == false ) &&
      ( TargetReady_ == false )) {
    Target_.x = Position_.linear.x;
    Target_.y = Position_.linear.y;
    Target_.z = Position_.linear.z;
  }
  PositionReady_ = true;
}

void ControllerCommon::targetCallback(const geometry_msgs::Point & msg)
{
  ROS_ASSERT(isnan(msg.x) == false );
  ROS_ASSERT(isnan(msg.y) == false );
  ROS_ASSERT(isnan(msg.z) == false );
  
  if ( PositionReady_ == true ) {
    Target_ = msg;
    TargetReady_ = true;
  }
}


