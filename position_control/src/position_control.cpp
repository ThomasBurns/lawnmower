
#include <ros/ros.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Twist.h>
#include <position_control/Limits.h>

#include <actionlib/server/simple_action_server.h>


#include <position_control/ControlVectorAction.h>
#include <mowerlib/Trigonometry.h>

#include "position_control/PID.h"
#include "position_control/setPID.h"

#include <cmath>

#include "AngularController.h"
#include "ControllerCommon.h"
#include "LinearController.h"

//====================================
class PositionController
{
protected:
  /************************************
   * ROS Variables
   */

  ros::NodeHandle Node_;
  actionlib::SimpleActionServer<position_control::ControlVectorAction> ActServer_; // NodeHandle instance must be created before this line. Otherwise strange error occurs.

  ros::Subscriber OdomSub_;
  ros::Publisher  TargetVelocityPub_;

  ros::ServiceServer GetPIDService_ ;
  ros::ServiceServer SetPIDService_ ;

  ros::Timer Heartbeat_;

  ControllerCommon CommonControl_;
  AngularController AngleControl_;
  LinearController LinearControl_;

  // create messages that are used to published feedback/result
  position_control::ControlVectorResult Result_;

  /************************************
   * Other Variables
   */
  bool  SystemActive_;
  float LastAngle_;
  float LinearSpeed_;
  float AngularSpeed_;
  unsigned int PacketTime_;

  bool  MovePosition_;
  bool  MoveAngle_;
  float TargetAngle_;

  float TotalRmsError_NumSamples_;
  float TotalRmsError_Linear_;
  float TotalRmsError_Angular_;


public:

  PositionController(std::string name) :
    ActServer_(Node_, name, false),
    AngleControl_(CommonControl_),
    LinearControl_(CommonControl_),
    SystemActive_(false), LastAngle_(1e9),
    LinearSpeed_(0.0), AngularSpeed_(0.0),
    PacketTime_(0),
    MovePosition_(false), MoveAngle_(false),
    TargetAngle_(0.0)
  {
    //register the goal and feeback callbacks
    ActServer_.registerGoalCallback(boost::bind(&PositionController::goalCB, this));
    ActServer_.registerPreemptCallback(boost::bind(&PositionController::preemptCB, this));
    ActServer_.start();

    Heartbeat_ = Node_.createTimer(ros::Duration(1.0/50.0), &PositionController::timerCallback, this);

    OdomSub_ = Node_.subscribe("odom", 20, &ControllerCommon::odomCallback, &CommonControl_);
    TargetVelocityPub_ = Node_.advertise<geometry_msgs::Twist>("/cmd_vel",50, true);

    setupServices(&Node_);
    publishVelocity(0.0, 0.0);
  }

  ~PositionController(void)
  {
  }

  void goalCB()
  {
    position_control::ControlVectorGoalConstPtr goal = ActServer_.acceptNewGoal();

    ROS_DEBUG("Position Controller: New goal(%f:%f)", goal->X, goal->Y);
    ROS_DEBUG("Speed      %f:%f", goal->AngularVelocity, goal->LinearVelocity);
    ROS_DEBUG("Angle      %d:%f", goal->MoveToAngle, goal->TargetAngle);

    AngleControl_.setMaxVelocity(goal->AngularVelocity);
    LinearControl_.setMaxVelocity(goal->LinearVelocity);

    geometry_msgs::Point pt;
    pt.x = goal->X;
    pt.y = goal->Y;
    pt.z = 0.0;
    CommonControl_.targetCallback(pt);
    TargetAngle_ = goal->TargetAngle;
//    TargetAngle_ = Trig_LimitRadians(goal->TargetAngle);

    MovePosition_ = goal->MoveToPosition;
    MoveAngle_ = goal->MoveToAngle;

    SystemActive_ = true;

    TotalRmsError_NumSamples_ = 0.0;
    TotalRmsError_Linear_  = 0.0;
    TotalRmsError_Angular_ = 0.0;
  }

  void preemptCB()
  {
    ROS_DEBUG("Position Controller: Preempted");
    // set the action state to preempted
    ActServer_.setPreempted();
    HaltSystem();
  }

  void timerCallback(const ros::TimerEvent & e)
  {
    if ( ActServer_.isActive() == false ) {
      if ( SystemActive_ == true ) {
        HaltSystem();
      }
      return;
    }

    if ( MovePosition_ == true ) {
      if ( moveToPosition() == true ) {
        MovePosition_ = false;
        publishVelocity(0.0, 0.0);
        if ( MoveAngle_ == false ) {
          publishResults();
        }
      }
      return;
    }
    if ( MoveAngle_ == true ) {

      if ( moveToAngle() == true ) {
        MoveAngle_ = false;
        publishResults();
      }
      return;
    }
    // if we get here then we have not been instructed
    // to move to a position, or to a target angle.
    publishResults();
  }

  bool moveToPosition()
  {
    AngleControl_.calcError();
    LinearControl_.calcError();

    if ( reachedTargetPosition() == true ) {
      return true;
    }

    float vz = AngleControl_.calcVelocity();
    float vx = LinearControl_.calcVelocity();

    float errDegrees = fabs(( AngleControl_.getError() * 180.0 ) / M_PI);
    vx *= exp(errDegrees/-2.0);
    publishVelocity(vx, vz);
    publishFeedback();

    return false;
  }

  bool moveToAngle()
  {
    AngleControl_.calcError(TargetAngle_);

    float vz = AngleControl_.calcVelocity();
    publishVelocity(0.0, vz);
    publishFeedback();

    return fabs(AngleControl_.getError()) < Trig_DegreesToRadians(1);
  }

  bool reachedTargetPosition()
  {
    float distance = LinearControl_.getError();
    float angle    = fabs(AngleControl_.getError());

    if ( distance < 0.01 ) {
      return true;
    }
    if ( distance < 0.01 ) {
      if (( angle > M_PI_2 ) && ( LastAngle_ < Trig_DegreesToRadians(3) )) {
        return true;
      }
    }
    LastAngle_ = angle;
    return false;
  }

  void publishFeedback()
  {
    float lin_err = LinearControl_.getError();
    float ang_err = AngleControl_.getError();
    position_control::ControlVectorFeedback feedback;

    feedback.Error_Linear  = lin_err;
    feedback.Error_Angular = ang_err;

    TotalRmsError_Linear_  += (lin_err * lin_err);
    TotalRmsError_Angular_ += (ang_err * ang_err);
    TotalRmsError_NumSamples_ += 1.0;

    ActServer_.publishFeedback(feedback);
  }

  void publishResults()
  {
    Result_.TotalRmsError_Linear  = sqrt(TotalRmsError_Linear_ / TotalRmsError_NumSamples_);
    Result_.TotalRmsError_Angular = sqrt(TotalRmsError_Angular_ / TotalRmsError_NumSamples_);
    ActServer_.setSucceeded(Result_);
    HaltSystem();
  }


  void publishVelocity(float fwd, float turn)
  {
    unsigned int now = ros::WallTime::now().sec;

    if (( LinearSpeed_  != fwd ) ||
        ( AngularSpeed_ != turn ) ||
        ( PacketTime_ != now  )) {

      LinearSpeed_  = fwd;
      AngularSpeed_ = turn;
      PacketTime_ = now;

      geometry_msgs::Twist targetVelocity;
      targetVelocity.linear.x  = LinearSpeed_;
      targetVelocity.angular.z = AngularSpeed_;
      TargetVelocityPub_.publish(targetVelocity);
    }
  }

  void HaltSystem()
  {
    publishVelocity(0.0, 0.0);
    LastAngle_ = 1e9;
    SystemActive_ = false;
  }

  /************************************************
   * Services
   */
  void setupServices(ros::NodeHandle * node)
  {
    GetPIDService_ = node->advertiseService("/robomower/poscon/GetPID", &PositionController::GetPIDCallback_, this);
    SetPIDService_ = node->advertiseService("/robomower/poscon/SetPID", &PositionController::SetPIDCallback_, this);

    float value;
    ros::param::param<float>("/robomower/poscon/PID_A_P", value, 1.60);
    AngleControl_.setAngular_P(value);
    ros::param::param<float>("/robomower/poscon/PID_A_I", value, 0.00);
    AngleControl_.setAngular_I(value);
    ros::param::param<float>("/robomower/poscon/PID_A_D", value, 0.10);
    AngleControl_.setAngular_D(value);

  }

  bool SetPIDCallback_(position_control::setPID::Request  &req,
                       position_control::setPID::Response &res )
  {
    if ( req.Parameter == req.PAR_ANGLE_P ) {
      AngleControl_.setAngular_P(req.Value);

    } else if ( req.Parameter == req.PAR_ANGLE_I ) {
      AngleControl_.setAngular_I(req.Value);

    } else if ( req.Parameter == req.PAR_ANGLE_D ) {
      AngleControl_.setAngular_D(req.Value);

    } else {
      return false;
    }
    return true;
  }

  bool GetPIDCallback_(position_control::PID::Request  &req,
                       position_control::PID::Response &res )
  {
    res.Angle_P = AngleControl_.getAngular_P();
    res.Angle_I = AngleControl_.getAngular_I();
    res.Angle_D = AngleControl_.getAngular_D();
    return true;
  }
};


//====================================
int main (int argc, char **argv)
{
  ros::init(argc, argv, "position_control");
  ROS_DEBUG("Positioning Controller online");

  PositionController posControl("/pos_con_act");
  ros::spin();
  return 0;
}
