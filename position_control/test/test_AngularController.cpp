#include <gtest/gtest.h>
#include "../src/AngularController.h"
#include "../src/ControllerCommon.h"

namespace {

  class t_AngularControl : public ::testing::Test {

  protected:
    
    ControllerCommon CCom_;
    AngularController ACon_;

    t_AngularControl():
      ACon_(CCom_)
    {
    }

    virtual ~t_AngularControl() {
    }

    virtual void SetUp() {
      
      geometry_msgs::Twist pos;
      pos.linear.x = 0.0;
      pos.linear.y = 0.0;
      pos.linear.z = 0.0;
      pos.angular.x = 0.0;
      pos.angular.y = 0.0;
      pos.angular.z = 0.0;
      CCom_.positionCallback(pos);
    }

    virtual void TearDown() {
    }
  };  
  
  TEST_F(t_AngularControl, target_quad_top_right)
  {
    geometry_msgs::Point p;
    p.x = 1;
    p.y = 1;
    CCom_.targetCallback(p);
    float err = ACon_.calcError();
    ASSERT_LT(0.78, err);
    ASSERT_GT(0.79, err);
  }

  TEST_F(t_AngularControl, target_quad_top_left)
  {
    geometry_msgs::Point p;
    p.x = -1;
    p.y = 1;
    CCom_.targetCallback(p);
    float err = ACon_.calcError();
    ASSERT_LT(2.35, err);
    ASSERT_GT(2.36, err);
  }

  TEST_F(t_AngularControl, target_quad_bottom_right)
  {
    geometry_msgs::Point p;
    p.x = 1;
    p.y = -1;
    CCom_.targetCallback(p);
    float err = ACon_.calcError();
    ASSERT_GT(-0.78, err);
    ASSERT_LT(-0.79, err);
  }

  TEST_F(t_AngularControl, target_quad_bottom_left)
  {
    geometry_msgs::Point p;
    p.x = -1;
    p.y = -1;
    CCom_.targetCallback(p);
    float err = ACon_.calcError();
    ASSERT_GT(-2.35, err);
    ASSERT_LT(-2.36, err);
  }

  TEST_F(t_AngularControl, contructor_default_error)
  {
    ASSERT_EQ(0.0, ACon_.calcError());
  }

} // Namespace