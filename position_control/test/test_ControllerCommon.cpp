#include <gtest/gtest.h>

#include "../src/ControllerCommon.h"

namespace {

  class t_ControlCommon : public ::testing::Test {

  protected:
    
    ControllerCommon Control_;

    t_ControlCommon() {
    }

    virtual ~t_ControlCommon() {
    }

    virtual void SetUp() {
    }

    virtual void TearDown() {
    }
  };  
  
  TEST_F(t_ControlCommon, no_target_target_mirrors_position)
  {
    geometry_msgs::Twist pos;
    pos.linear.x = 1.0;
    pos.linear.y = -40.0;
    pos.linear.z = 5.0;
    Control_.positionCallback(pos);
    
    ASSERT_EQ(pos.linear.x, Control_.Target_.x);
    ASSERT_EQ(pos.linear.y, Control_.Target_.y);
    ASSERT_EQ(pos.linear.z, Control_.Target_.z);
  }
  
  TEST_F(t_ControlCommon, target_set_so_target_no_longer_mirrors_position)
  {
    geometry_msgs::Twist pos;
    pos.linear.x = 1.0;
    pos.linear.y = -40.0;
    pos.linear.z = 5.0;
    Control_.positionCallback(pos);
    geometry_msgs::Point p;
    p.x = 30.0;
    p.y = 50.0;
    p.z = -110.0;
    Control_.targetCallback( p);
    
    ASSERT_NE(pos.linear.x, Control_.Target_.x);
    ASSERT_NE(pos.linear.y, Control_.Target_.y);
    ASSERT_NE(pos.linear.z, Control_.Target_.z);
  }
  
  /* =========================================================== */
  TEST_F(t_ControlCommon, contructor_default_target)
  {
    ASSERT_EQ(0.0, Control_.Target_.x);
    ASSERT_EQ(0.0, Control_.Target_.y);
    ASSERT_EQ(0.0, Control_.Target_.z);
  }
  /*
  TEST_F(t_ControlCommon, setTarget_AssertDeath_X)
  {
    ControllerCommon con;
    geometry_msgs::Point p;
    p.x = NAN;
    p.y = 0.0;
    p.z = 0.0;
    ASSERT_DEATH (con.targetCallback( p);, ".*ASSERTION FAILED.*");
  }
  
  TEST_F(t_ControlCommon, setTarget_AssertDeath_Y)
  {
    ControllerCommon con;
    geometry_msgs::Point p;
    p.x = 0.0;
    p.y = NAN;
    p.z = 0.0;
    ASSERT_DEATH (con.targetCallback( p);, ".*ASSERTION FAILED.*");
  }
  TEST_F(t_ControlCommon, setTarget_AssertDeath_Z)
  {
    ControllerCommon con;
    geometry_msgs::Point p;
    p.x = 0.0;
    p.y = 0.0;
    p.z = NAN;
    ASSERT_DEATH (con.targetCallback( p);, ".*ASSERTION FAILED.*");
  }
*/
} // Namespace